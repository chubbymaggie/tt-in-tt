{-# OPTIONS --no-eta #-}

open import Cats
open import NBE.TM

module NBE.LogPred.Ty
  (U̅ : FamPSh TMU)
  (E̅l : FamPSh (TMU ,P TMEl ,P U̅ [ wkn ]F))
  where

open import lib
open import JM.JM
open import TT.Syntax renaming (_$_ to _$$_)
open import TT.Elim
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.LogPred.Motives
open import NBE.LogPred.Cxt
open import NBE.LogPred.Ty.Subst
open import NBE.LogPred.Ty.U U̅
open import NBE.LogPred.Ty.El U̅ E̅l
open import NBE.LogPred.Ty.Pi

mTy : MethodsTy M mCon
mTy = record
  { _[_]Tᴹ = m[]Tᴹ._[_]Tᴹ
  ; Uᴹ     = mUᴹ.Uᴹ  
  ; Elᴹ    = mElᴹ.Elᴹ
  ; Πᴹ     = mΠᴹ.Πᴹ
  }
