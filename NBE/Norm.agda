module NBE.Norm where

open import lib
open import JM.JM
open import Cats
open import TT.Syntax
open import TT.Laws
open import NBE.Renamings
open import NBE.TM
open import NBE.Nf
open import NBE.Nfs
open import NBE.Quote.Motives
open import NBE.Quote.Quote
open import NBE.LogPred.Motives using (Tbase=)
open import NBE.LogPred.LogPred U̅ E̅l

ID : (Γ : Con) → ⟦ Γ ⟧C $F id
ID Γ = coe ($F= ⟦ Γ ⟧C (⌜Vne⌝ ⁻¹ ◾ ⌜idV⌝)) (uC (QC Γ) $S ⌜ idV ⌝Vne)


NORM : ∀{Γ A}(t : Tm Γ A) → ≡NFT A $F (id , coe (TmΓ= ([id]T ⁻¹)) t)
NORM {Γ}{A} t = qT (QT A) $S ( id
                             , coe (TmΓ= ([id]T ⁻¹)) t
                             , ID Γ
                             , coe ($F= ⟦ A ⟧T pcoe) (⟦ t ⟧t $S (id , ID Γ)))
  where
    
    abstract
      pcoe : _≡_ {A = (TMC Γ ,P TMT A ,P ⟦ Γ ⟧C [ wkn ]F) $P Γ}
                 ((TMt t ^S ⟦ Γ ⟧C) $n (id , ID Γ))
                 (id , coe (TmΓ= ([id]T ⁻¹)) t , ID Γ)
      pcoe = Tbase= ⟦ Γ ⟧C refl ([id]t' ◾̃ uncoe (TmΓ= ([id]T ⁻¹))) r̃
    
norm : ∀{Γ A} → Tm Γ A → Nf Γ A
norm t = coe (NfΓ= [id]T) (proj₁ (NORM t))

compl : ∀{Γ A}(t : Tm Γ A) → t ≡ ⌜ norm t ⌝nf
compl t = from≃ (uncoe (TmΓ= ([id]T ⁻¹)) ◾̃ to≃ (proj₂ (NORM t)) ◾̃ ⌜coe⌝nf [id]T ⁻¹̃)
