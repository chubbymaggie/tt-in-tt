module NBE.Stability where

open import lib
open import JM.JM
open import Cats
open import TT.Syntax
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.TM
open import NBE.Nf
open import NBE.Nfs
open import NBE.Norm
open import NBE.Quote.Motives
open import NBE.Quote.Quote
open import NBE.LogPred.Motives using (Tbase=)
open import NBE.LogPred.LogPred U̅ E̅l

-- we first need to add computational content to NBE.Quote.Tm,
-- NBE.Quote.Tms

stabNf : ∀{Γ A}(n : Nf Γ A)
       → norm ⌜ n ⌝nf ≡ n

stabNe : ∀{Γ A}(n : Ne Γ A)
       → ⟦ ⌜ n ⌝ne ⟧t $S (id , (ID Γ))
       ≃ uT (QT A) $S (id , coe (NeΓ= ([id]T ⁻¹)) n , ID Γ)

stabNf (neuU n) = from≃ {!!}

stabNf (neuEl n) = {!!}

stabNf (lamNf n) = {!!}

stabNe (var vze) = {!!}

stabNe (var (vsu x)) = {!!}

stabNe (appNe n v) = {!!}
