{-# OPTIONS --no-eta #-}

module NBE.Nfs where

open import lib
open import JM.JM
open import Cats
open import TT.Syntax hiding (_$_)
open import TT.Congr
open import TT.Elim
open import NBE.Renamings
open import NBE.Nf
open import NBE.TM

------------------------------------------------------------------------------
-- lists of neutral terms and normal forms together with embeddings
-- into the syntax
------------------------------------------------------------------------------

data Nes : Con → Con → Set
data Nfs : Con → Con → Set

⌜_⌝nes : ∀{Γ Δ} → Nes Γ Δ → Tms Γ Δ
⌜_⌝nfs : ∀{Γ Δ} → Nfs Γ Δ → Tms Γ Δ

data Nes where
  ε      : ∀{Γ} → Nes Γ •
  _,nes_ : ∀{Γ Δ}(ρ : Nes Γ Δ){A : Ty Δ} → Ne Γ (A [ ⌜ ρ ⌝nes ]T) → Nes Γ (Δ , A)

data Nfs where
  ε      : ∀{Γ} → Nfs Γ •
  _,nfs_ : ∀{Γ Δ}(ρ : Nfs Γ Δ){A : Ty Δ} → Nf Γ (A [ ⌜ ρ ⌝nfs ]T) → Nfs Γ (Δ , A)

infixl 5 _,nfs_ _,nes_

⌜ ε ⌝nes = ε
⌜ ρ ,nes n ⌝nes = ⌜ ρ ⌝nes , ⌜ n ⌝ne

⌜ ε ⌝nfs = ε
⌜ ρ ,nfs v ⌝nfs = ⌜ ρ ⌝nfs , ⌜ v ⌝nf

------------------------------------------------------------------------------
-- renaming lists of neutral terms, normal forms and their relation to
-- substitutions
------------------------------------------------------------------------------

_[_]nes : ∀{Γ Δ Θ} → Nes Δ Γ → Vars Θ Δ → Nes Θ Γ
⌜[]nes⌝ : ∀{Γ Δ Θ}{ρ : Nes Δ Γ}{β : Vars Θ Δ} → ⌜ ρ ⌝nes ∘ ⌜ β ⌝V ≡ ⌜ ρ [ β ]nes ⌝nes

infixl 8 _[_]nes

ε [ β ]nes = ε
(ρ ,nes n) [ β ]nes = ρ [ β ]nes ,nes coe (NeΓ= ([][]T ◾ ap (_[_]T _) ⌜[]nes⌝)) (n [ β ]ne)

abstract
  ⌜[]nes⌝ {ρ = ε} = εη
  ⌜[]nes⌝ {ρ = ρ ,nes n}{β} = ,∘
                            ◾ ,s≃' (⌜[]nes⌝ {ρ = ρ})
                                   ( uncoe (TmΓ= [][]T) ⁻¹̃
                                   ◾̃ to≃ (⌜[]ne⌝ {n = n})
                                   ◾̃ ⌜coe⌝ne ([][]T ◾ ap (_[_]T _) ⌜[]nes⌝) ⁻¹̃)

_[_]nfs : ∀{Γ Δ Θ} → Nfs Δ Γ → Vars Θ Δ → Nfs Θ Γ
⌜[]nfs⌝ : ∀{Γ Δ Θ}{ρ : Nfs Δ Γ}{β : Vars Θ Δ} → ⌜ ρ ⌝nfs ∘ ⌜ β ⌝V ≡ ⌜ ρ [ β ]nfs ⌝nfs

infixl 8 _[_]nfs

ε [ β ]nfs = ε
(ρ ,nfs n) [ β ]nfs = ρ [ β ]nfs ,nfs coe (NfΓ= ([][]T ◾ ap (_[_]T _) ⌜[]nfs⌝)) (n [ β ]nf)

abstract
  ⌜[]nfs⌝ {ρ = ε} = εη
  ⌜[]nfs⌝ {ρ = ρ ,nfs v}{β} = ,∘
                            ◾ ,s≃' (⌜[]nfs⌝ {ρ = ρ})
                                   ( uncoe (TmΓ= [][]T) ⁻¹̃
                                   ◾̃ to≃ (⌜[]nf⌝ {v = v})
                                   ◾̃ ⌜coe⌝nf ([][]T ◾ ap (_[_]T _) ⌜[]nfs⌝) ⁻¹̃)

------------------------------------------------------------------------------
-- congruence rules
------------------------------------------------------------------------------

,nes= : ∀{Γ Δ}{ρ₀ ρ₁ : Nes Γ Δ}(ρ₂ : ρ₀ ≡ ρ₁){A : Ty Δ}
        {n₀ : Ne Γ (A [ ⌜ ρ₀ ⌝nes ]T)}{n₁ : Ne Γ (A [ ⌜ ρ₁ ⌝nes ]T)}
        (n₂ : n₀ ≃ n₁)
      → ρ₀ ,nes n₀ ≡ ρ₁ ,nes n₁
,nes= refl (refl , refl) = refl

,nfs= : ∀{Γ Δ}{ρ₀ ρ₁ : Nfs Γ Δ}(ρ₂ : ρ₀ ≡ ρ₁){A : Ty Δ}
        {v₀ : Nf Γ (A [ ⌜ ρ₀ ⌝nfs ]T)}{v₁ : Nf Γ (A [ ⌜ ρ₁ ⌝nfs ]T)}
        (v₂ : v₀ ≃ v₁)
      → ρ₀ ,nfs v₀ ≡ ρ₁ ,nfs v₁
,nfs= refl (refl , refl) = refl

------------------------------------------------------------------------------
-- categorical laws for renaming
------------------------------------------------------------------------------

abstract
  [id]nes : ∀{Γ Δ}{ρ : Nes Γ Δ} → ρ [ idV ]nes ≡ ρ
  [id]nes {ρ = ε} = refl
  [id]nes {ρ = ρ ,nes n} = ,nes= ([id]nes {ρ = ρ})
                                 ( uncoe (NeΓ= ([][]T ◾ ap (_[_]T _) ⌜[]nes⌝)) ⁻¹̃
                                 ◾̃ [id]ne)

abstract
  [][]nes : ∀{Γ Δ Θ Σ}{ρ : Nes Θ Σ}{β : Vars Δ Θ}{γ : Vars Γ Δ} → ρ [ β ∘V γ ]nes ≡ ρ [ β ]nes [ γ ]nes
  [][]nes {ρ = ε} = refl
  [][]nes {ρ = ρ ,nes n} = ,nes= ([][]nes {ρ = ρ})
                                 ( uncoe (ap (Ne _) ([][]T ◾ ap (_[_]T _) ⌜[]nes⌝)) ⁻¹̃
                                 ◾̃ [][]ne {n = n} ⁻¹̃
                                 ◾̃ coe[]ne' ([][]T ◾ ap (_[_]T _) ⌜[]nes⌝) ⁻¹̃
                                 ◾̃ uncoe (NeΓ= ([][]T ◾ ap (_[_]T _) ⌜[]nes⌝)))

abstract
  [id]nfs : ∀{Γ Δ}{ρ : Nfs Γ Δ} → ρ [ idV ]nfs ≡ ρ
  [id]nfs {ρ = ε} = refl
  [id]nfs {ρ = ρ ,nfs v} = ,nfs= ([id]nfs {ρ = ρ})
                                 ( uncoe (NfΓ= ([][]T ◾ ap (_[_]T _) ⌜[]nfs⌝)) ⁻¹̃
                                 ◾̃ [id]nf)

abstract
  [][]nfs : ∀{Γ Δ Θ Σ}{ρ : Nfs Θ Σ}{β : Vars Δ Θ}{γ : Vars Γ Δ} → ρ [ β ∘V γ ]nfs ≡ ρ [ β ]nfs [ γ ]nfs
  [][]nfs {ρ = ε} = refl
  [][]nfs {ρ = ρ ,nfs v} = ,nfs= ([][]nfs {ρ = ρ})
                                 ( uncoe (ap (Nf _) ([][]T ◾ ap (_[_]T _) ⌜[]nfs⌝)) ⁻¹̃
                                 ◾̃ [][]nf {v = v} ⁻¹̃
                                 ◾̃ coe[]nf' ([][]T ◾ ap (_[_]T _) ⌜[]nfs⌝) ⁻¹̃
                                 ◾̃ uncoe (NfΓ= ([][]T ◾ ap (_[_]T _) ⌜[]nfs⌝)))

------------------------------------------------------------------------------
-- categorical definitions of NE, NF
------------------------------------------------------------------------------

open Cat (REN ᵒᵖ)

NEC : (Γ : Con) → PSh (REN ᵒᵖ)
NEC Γ = record
  { _$P_   = λ Δ → Nes Δ Γ
  ; _$P_$_ = λ β ρ → ρ [ β ]nes
  ; idP    = [id]nes
  ; compP  = [][]nes }

NFC : (Γ : Con) → PSh (REN ᵒᵖ)
NFC Γ = record
  { _$P_   = λ Δ → Nfs Δ Γ
  ; _$P_$_ = λ β ρ → ρ [ β ]nfs
  ; idP    = [id]nfs
  ; compP  = [][]nfs }

abstract
  NETidF : ∀{Γ}{A : Ty Γ}{I : Obj}{α : TMC Γ $P I}
           {n : Ne I (A [ α ]T)}
         → coe (NeΓ= [][]T) (n [ idV ]ne)
         ≡[ ap (λ ρ → Ne I (A [ ρ ]T)) (idP (TMC Γ)) ]≡
           n
  NETidF {Γ}{A}
     = from≃ ( uncoe (ap (λ ρ → Ne _ (A [ ρ ]T)) (idP (TMC Γ))) ⁻¹̃
             ◾̃ uncoe (NeΓ= [][]T) ⁻¹̃
             ◾̃ [id]ne)
abstract
  NETcompF : ∀{Γ}{A : Ty Γ}{I J K : Obj}{f : I ⇒ J}{g : J ⇒ K}
             {α : TMC Γ $P I}{n : Ne I (A [ α ]T)}
           → coe (NeΓ= [][]T) (n [ g ∘c f ]ne)
           ≡[ ap (λ ρ → Ne K (A [ ρ ]T)) (compP (TMC Γ)) ]≡
             coe (NeΓ= [][]T) (coe (NeΓ= [][]T) (n [ f ]ne) [ g ]ne)
  NETcompF {Γ}{A}{n = n}
    = from≃ ( uncoe (ap (λ ρ → Ne _ (A [ ρ ]T)) (compP (TMC Γ))) ⁻¹̃
            ◾̃ uncoe (NeΓ= [][]T) ⁻¹̃
            ◾̃ [][]ne {n = n} ⁻¹̃ 
            ◾̃ coe[]ne' [][]T ⁻¹̃
            ◾̃ uncoe (NeΓ= [][]T))

NET : ∀{Γ}(A : Ty Γ) → FamPSh (TMC Γ)
NET {Γ} A = record

  { _$F_   = λ {Δ} ρ → Ne Δ (A [ ρ ]T)
  
  ; _$F_$_ = λ β n → coe (NeΓ= [][]T) (n [ β ]ne)
                         
  ; idF    = NETidF

  ; compF  = λ {_}{_}{_}{_}{_}{_}{n} →  NETcompF {n = n}

  }

abstract
  NFTidF : ∀{Γ}{A : Ty Γ}{I : Obj}{α : TMC Γ $P I}
           {v : Nf I (A [ α ]T)}
         → coe (NfΓ= [][]T) (v [ idV ]nf)
         ≡[ ap (λ ρ → Nf I (A [ ρ ]T)) (idP (TMC Γ)) ]≡
           v
  NFTidF {Γ}{A}
     = from≃ ( uncoe (ap (λ ρ → Nf _ (A [ ρ ]T)) (idP (TMC Γ))) ⁻¹̃
             ◾̃ uncoe (NfΓ= [][]T) ⁻¹̃
             ◾̃ [id]nf)

abstract
  NFTcompF : ∀{Γ}{A : Ty Γ}{I J K : Obj}{f : I ⇒ J}{g : J ⇒ K}
             {α : TMC Γ $P I}{v : Nf I (A [ α ]T)}
           → coe (NfΓ= [][]T) (v [ g ∘c f ]nf)
           ≡[ ap (λ ρ → Nf K (A [ ρ ]T)) (compP (TMC Γ)) ]≡
             coe (NfΓ= [][]T) (coe (NfΓ= [][]T) (v [ f ]nf) [ g ]nf)
  NFTcompF {Γ}{A}{v = v}
    = from≃ ( uncoe (ap (λ ρ → Nf _ (A [ ρ ]T)) (compP (TMC Γ))) ⁻¹̃
            ◾̃ uncoe (NfΓ= [][]T) ⁻¹̃
            ◾̃ [][]nf {v = v} ⁻¹̃ 
            ◾̃ coe[]nf' [][]T ⁻¹̃
            ◾̃ uncoe (NfΓ= [][]T))



NFT : ∀{Γ}(A : Ty Γ) → FamPSh (TMC Γ)
NFT {Γ} A = record

  { _$F_   = λ {Δ} ρ → Nf Δ (A [ ρ ]T)
  
  ; _$F_$_ = λ β v → coe (NfΓ= [][]T) (v [ β ]nf)
                         
  ; idF    = NFTidF

  ; compF  = λ {_}{_}{_}{_}{_}{_}{v} →  NFTcompF {v = v}
  
  }

------------------------------------------------------------------------------
-- embeddings of NE, NF into TM
------------------------------------------------------------------------------

⌜NE⌝C : ∀{Γ} → NEC Γ →n TMC Γ
⌜NE⌝C {Γ} = record { _$n_ = ⌜_⌝nes ; natn = ⌜[]nes⌝ }

⌜NE⌝T : ∀{Γ}{A : Ty Γ} → NET A →N TMT A
⌜NE⌝T {Γ}{A} = record
  { _$N_ = ⌜_⌝ne
  ; natN = λ {_}{_}{_}{_}{n}
           → from≃ (uncoe (TmΓ= [][]T) ⁻¹̃
                   ◾̃ to≃ (⌜[]ne⌝ {n = n})
                   ◾̃ ⌜coe⌝ne [][]T ⁻¹̃)
  }

⌜NF⌝C : ∀{Γ} → NFC Γ →n TMC Γ
⌜NF⌝C {Γ} = record { _$n_ = ⌜_⌝nfs ; natn = ⌜[]nfs⌝ }

⌜NF⌝T : ∀{Γ}{A : Ty Γ} → NFT A →N TMT A
⌜NF⌝T {Γ}{A} = record
  { _$N_ = ⌜_⌝nf
  ; natN = λ {_}{_}{_}{_}{v}
           → from≃ (uncoe (TmΓ= [][]T) ⁻¹̃
                   ◾̃ to≃ (⌜[]nf⌝ {v = v})
                   ◾̃ ⌜coe⌝nf [][]T ⁻¹̃)
  }

------------------------------------------------------------------------------
-- projections for Nes
------------------------------------------------------------------------------

π₁nes : ∀{Γ Δ A} → Nes Δ (Γ , A) → Nes Δ Γ
π₁nes (ρ ,nes n) = ρ

π₂nes : ∀{Γ Δ A}(ρ : Nes Δ (Γ , A)) → Ne Δ (A [ ⌜ π₁nes ρ ⌝nes ]T)
π₂nes (ρ ,nes n) = n

abstract
  ⌜π₁nes⌝ : ∀{Γ Δ A}{ρ : Nes Δ (Γ , A)} → ⌜ π₁nes ρ ⌝nes ≡ π₁ ⌜ ρ ⌝nes
  ⌜π₁nes⌝ {ρ = ρ ,nes n} = π₁β ⁻¹

abstract
  ⌜π₂nes⌝ : ∀{Γ Δ A}{ρ : Nes Δ (Γ , A)} → ⌜ π₂nes ρ ⌝ne ≃ π₂ ⌜ ρ ⌝nes
  ⌜π₂nes⌝ {ρ = ρ ,nes n} = from≡ (TmΓ= (ap (_[_]T _) π₁β)) π₂β ⁻¹̃

abstract
  π₁nes[] : ∀{Γ Δ A}{ρ : Nes Δ (Γ , A)}{Θ : Con}{β : Vars Θ Δ}
          → π₁nes ρ [ β ]nes ≡ π₁nes (ρ [ β ]nes)
  π₁nes[] {ρ = ρ ,nes n} = refl

abstract
  π₂nes[] : ∀{Γ Δ A}{ρ : Nes Δ (Γ , A)}{Θ : Con}{β : Vars Θ Δ}
          → π₂nes ρ [ β ]ne ≃ π₂nes (ρ [ β ]nes)
  π₂nes[] {ρ = ρ ,nes n} = NeΓ= ([][]T ◾ ap (_[_]T _) ⌜[]nes⌝)
                         , refl

------------------------------------------------------------------------------
-- embedding variables into neutral terms, neutral terms into normal forms
------------------------------------------------------------------------------

⌜_⌝Vne : ∀{Ψ Ω} → Vars Ω Ψ → Nes Ω Ψ
⌜Vne⌝ : ∀{Ψ Ω}{β : Vars Ω Ψ} → ⌜ β ⌝V ≡ ⌜ ⌜ β ⌝Vne ⌝nes

⌜ ε ⌝Vne = ε
⌜ β , x ⌝Vne = ⌜ β ⌝Vne ,nes var (coe (VarΓ= (ap (_[_]T _) ⌜Vne⌝)) x)

abstract
  ⌜Vne⌝ {β = ε} = refl
  ⌜Vne⌝ {β = β , x} = ,s≃' (⌜Vne⌝ {β = β}) (⌜coe⌝v (ap (_[_]T _) ⌜Vne⌝) ⁻¹̃)
