{-# OPTIONS --no-eta #-}

module NBE.Renamings where

-- the category of renamings

open import lib
open import JM.JM
open import TT.Syntax
open import TT.Congr
open import TT.Laws
open import Cats

------------------------------------------------------------------------------
-- renamings and variables together with embeddings into the syntax
------------------------------------------------------------------------------

data Vars : (Γ : Con)(Δ : Con)  → Set
data Var  : (Γ : Con)(A : Ty Γ) → Set

⌜_⌝v : ∀{Γ A} → Var Γ A → Tm Γ A
⌜_⌝V : ∀{Γ Δ} → Vars Γ Δ → Tms Γ Δ

data Vars where
  ε     : ∀{Γ} → Vars Γ •
  _,_   : ∀{Γ Δ}(β : Vars Γ Δ){A : Ty Δ} → Var Γ (A [ ⌜ β ⌝V ]T) → Vars Γ (Δ , A)

infixl 5 _,_

data Var where
  vze : ∀ {Γ}{A : Ty Γ} → Var (Γ , A) (A [ wk ]T)
  vsu : ∀ {Γ}{A B : Ty Γ} → Var Γ A → Var (Γ , B) (A [ wk ]T)

⌜ ε ⌝V = ε
⌜ β , x ⌝V = ⌜ β ⌝V , ⌜ x ⌝v

⌜ vze ⌝v = vz
⌜ vsu v ⌝v = vs ⌜ v ⌝v

-- congruence rules

VarΓ= : {Γ : Con}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁) → Var Γ A₀ ≡ Var Γ A₁
VarΓ= {Γ} = ap (Var Γ)

⌜coe⌝v : ∀{Γ A₀ A₁}(A₂ : A₀ ≡ A₁){x : Var Γ A₀}
       → ⌜ coe (VarΓ= A₂) x ⌝v ≃ ⌜ x ⌝v
⌜coe⌝v refl = refl , refl

,V≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
      {Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
      {β₀ : Vars Γ₀ Δ₀}{β₁ : Vars Γ₁ Δ₁}(β₂ : β₀ ≃ β₁)
      {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≃ A₁)
      {x₀ : Var Γ₀ (A₀ [ ⌜ β₀ ⌝V ]T)}{x₁ : Var Γ₁ (A₁ [ ⌜ β₁ ⌝V ]T)}(x₂ : x₀ ≃ x₁)
    → _≃_ {A = Vars Γ₀ (Δ₀ , A₀)}{Vars Γ₁ (Δ₁ , A₁)} (β₀ , x₀) (β₁ , x₁)
,V≃ refl refl (refl , refl) (refl , refl) (refl , refl) = refl , refl

,V≃' : ∀{Γ Δ}{β₀ β₁ : Vars Γ Δ}(β₂ : β₀ ≡ β₁)
       {A : Ty Δ}
       {x₀ : Var Γ (A [ ⌜ β₀ ⌝V ]T)}
       {x₁ : Var Γ (A [ ⌜ β₁ ⌝V ]T)}
       (x₂ : x₀ ≃ x₁)
     → _≡_ {A = Vars Γ (Δ , A)} (β₀ , x₀) (β₁ , x₁)
,V≃' refl (refl , refl) = refl

vsu≃ : ∀{Γ A₀ A₁ B}(A₂ : A₀ ≡ A₁)
       {x₀ : Var Γ A₀}{x₁ : Var Γ A₁}
     → x₀ ≃ x₁ → vsu {B = B} x₀ ≃ vsu {B = B} x₁
vsu≃ refl (refl , refl) = refl , refl

vze≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
     → vze {Γ₀}{A₀} ≃ vze {Γ₁}{A₁}
vze≃ refl (refl , refl) = refl , refl

⌜⌝V≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
       {β₀ : Vars Γ₀ Δ₀}{β₁ : Vars Γ₁ Δ₁}(β₂ : β₀ ≃ β₁)
     → ⌜ β₀ ⌝V ≃ ⌜ β₁ ⌝V
⌜⌝V≃ refl refl (refl , refl) = refl , refl

------------------------------------------------------------------------------
-- renaming a variable
------------------------------------------------------------------------------

_[_]v : ∀{Γ Δ A}(x : Var Δ A)(β : Vars Γ Δ) → Var Γ (A [ ⌜ β ⌝V ]T)
vze   [ β , x ]v = coe (VarΓ= (ap (_[_]T _) π₁idβ ⁻¹ ◾ [][]T ⁻¹)) x
vsu x [ β , y ]v = coe (VarΓ= (ap (_[_]T _) π₁idβ ⁻¹ ◾ [][]T ⁻¹)) (x [ β ]v)
infixl 8 _[_]v

abstract
  ⌜[]v⌝ : ∀{Γ Δ A}{x : Var Δ A}{β : Vars Γ Δ} → ⌜ x ⌝v [ ⌜ β ⌝V ]t ≡ ⌜ x [ β ]v ⌝v
  ⌜[]v⌝ {x = vze}  {β , x} = from≃ ( π₂idβ
                                   ◾̃ ⌜coe⌝v (ap (_[_]T _) π₁idβ ⁻¹ ◾ [][]T ⁻¹) ⁻¹̃)
  ⌜[]v⌝ {x = vsu x}{β , y} = from≃ ( [][]t'
                                   ◾̃ []t≃'' π₁idβ
                                   ◾̃ to≃ ⌜[]v⌝
                                   ◾̃ ⌜coe⌝v (ap (_[_]T _) π₁idβ ⁻¹ ◾ [][]T ⁻¹) ⁻¹̃)

-- congruence

coe[]v' : ∀{Γ Δ}{β : Vars Γ Δ}{A₀ A₁ : Ty Δ}(A₂ : A₀ ≡ A₁){x : Var Δ A₀}
        → coe (VarΓ= A₂) x [ β ]v ≃ x [ β ]v
coe[]v' refl = refl , refl

------------------------------------------------------------------------------
-- composition
------------------------------------------------------------------------------

_∘V_ : ∀{Γ Δ Θ} → Vars Γ Δ → Vars Θ Γ → Vars Θ Δ
infix  6 _∘V_

⌜∘V⌝  : ∀{Γ Δ Θ}{β : Vars Γ Δ}{γ : Vars Θ Γ} → ⌜ β ⌝V ∘ ⌜ γ ⌝V ≡ ⌜ β ∘V γ ⌝V

ε ∘V γ = ε
(β , x) ∘V γ = β ∘V γ , coe (VarΓ= ([][]T ◾ ap (_[_]T _) ⌜∘V⌝)) (x [ γ ]v)

abstract
  ⌜∘V⌝ {β = ε} = εη
  ⌜∘V⌝ {β = β , x} = ,∘
                   ◾ ,s≃' ⌜∘V⌝
                          ( uncoe (TmΓ= [][]T) ⁻¹̃
                          ◾̃ to≃ ⌜[]v⌝
                          ◾̃ ⌜coe⌝v ([][]T ◾ ap (_[_]T _) ⌜∘V⌝) ⁻¹̃)

-- congruence

∘V≃ : ∀{Γ Δ}{β : Vars Γ Δ}
      {Θ₀ Θ₁ : Con}(Θ₂ : Θ₀ ≡ Θ₁)
      {γ₀ : Vars Θ₀ Γ}{γ₁ : Vars Θ₁ Γ}(γ₂ : γ₀ ≃ γ₁)
    → (β ∘V γ₀) ≃ (β ∘V γ₁)
∘V≃ refl (refl , refl) = refl , refl

------------------------------------------------------------------------------
-- weakening
------------------------------------------------------------------------------

wkV   : ∀{Γ Δ A} → Vars Γ Δ → Vars (Γ , A) Δ
⌜wkV⌝ : ∀{Γ Δ A}{β : Vars Γ Δ} → ⌜ β ⌝V ∘ wk {A = A} ≡ ⌜ wkV β ⌝V

wkV ε = ε
wkV (β , x) = wkV β , coe (VarΓ= ([][]T ◾ ap (_[_]T _) ⌜wkV⌝)) (vsu x)

abstract
  ⌜wkV⌝ {β = ε} = εη
  ⌜wkV⌝ {β = β , x} = ,∘
                    ◾ ,s≃' (⌜wkV⌝ {β = β})
                           ( uncoe (TmΓ= [][]T) ⁻¹̃
                           ◾̃ ⌜coe⌝v ([][]T ◾ ap (_[_]T _) (⌜wkV⌝ {β = β})) ⁻¹̃)

-- congruence rule

wkV≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
       {β₀ : Vars Γ₀ Δ₀}{β₁ : Vars Γ₁ Δ₁}(β₂ : β₀ ≃ β₁)
       {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
     → wkV {A = A₀} β₀ ≃ wkV {A = A₁} β₁
wkV≃ refl refl (refl , refl) (refl , refl) = refl , refl

------------------------------------------------------------------------------
-- identity
------------------------------------------------------------------------------

idV   : ∀{Γ} → Vars Γ Γ
⌜idV⌝ : ∀{Γ} → ⌜ idV {Γ} ⌝V ≡ id

abstract
  ⌜wkid⌝ : ∀{Γ A} → wk {Γ}{A} ≡ ⌜ wkV idV ⌝V
  ⌜wkid⌝ = idl ⁻¹ ◾ ap (λ z → z ∘ wk) (⌜idV⌝ ⁻¹) ◾ ⌜wkV⌝

idV {•} = ε
idV {Γ , A} = wkV idV , coe (VarΓ= (ap (_[_]T A) ⌜wkid⌝)) vze

abstract
  ⌜idV⌝ {•} = εη ⁻¹
  ⌜idV⌝ {Γ , A} = ,s≃' (⌜wkid⌝ ⁻¹) (⌜coe⌝v (ap (_[_]T A) ⌜wkid⌝)) ◾ πη

------------------------------------------------------------------------------
-- categorical laws
------------------------------------------------------------------------------

abstract
  wkVβ : ∀{Θ Γ Δ A}{β : Vars Γ Δ}{γ : Vars Θ Γ}{x : Var Θ (A [ ⌜ γ ⌝V ]T)}
       → wkV β ∘V (γ , x) ≡ β ∘V γ
  wkVβ {β = ε} = refl
  wkVβ {β = β , x}{γ}{y}
  
    = ,V≃' wkVβ
           ( uncoe (VarΓ= ([][]T ◾ ap (_[_]T _) ⌜∘V⌝)) ⁻¹̃
           ◾̃ coe[]v' ([][]T ◾ ap (_[_]T _) ⌜wkV⌝)
           ◾̃ uncoe (VarΓ= (ap (_[_]T (_ [ ⌜ β ⌝V ]T)) π₁idβ ⁻¹ ◾ [][]T ⁻¹)) ⁻¹̃
           ◾̃ uncoe (VarΓ= ([][]T ◾ ap (_[_]T _) ⌜∘V⌝)))

abstract
  idlV : ∀{Γ Δ}{β : Vars Γ Δ} → idV ∘V β ≡ β
  idlV {β = ε} = refl
  idlV {β = β , x} = ,V≃' (wkVβ ◾ idlV {β = β})
                          ( uncoe (VarΓ= ([][]T ◾ ap (_[_]T _) ⌜∘V⌝)) ⁻¹̃
                          ◾̃ coe[]v' (ap (_[_]T _) ⌜wkid⌝)
                          ◾̃ uncoe (VarΓ= (ap (_[_]T _) π₁idβ ⁻¹ ◾ [][]T ⁻¹)) ⁻¹̃)

abstract
  vsu[] : ∀{Γ Δ A B}{x : Var Δ A}{β : Vars Γ Δ}
        → vsu {B = B} (x [ β ]v) ≃ x [ wkV {A = B} β ]v
  vsu[] {x = vze}   {β , x} = vsu≃ ([][]T ◾ ap (_[_]T _) π₁idβ)
                                   (uncoe (VarΓ= (ap (_[_]T _) π₁idβ ⁻¹ ◾ [][]T ⁻¹)) ⁻¹̃)
                            ◾̃ uncoe (VarΓ= ([][]T ◾ ap (_[_]T _) ⌜wkV⌝))
                            ◾̃ uncoe (VarΓ= (ap (_[_]T _) π₁idβ ⁻¹ ◾ [][]T ⁻¹))
  vsu[] {x = vsu x} {β , y} = vsu≃ ([][]T ◾ ap (_[_]T _) π₁idβ)
                                   (uncoe (VarΓ= (ap (_[_]T _) π₁idβ ⁻¹ ◾ [][]T ⁻¹)) ⁻¹̃)
                            ◾̃ vsu[] {x = x}{β}
                            ◾̃ uncoe (VarΓ= (ap (_[_]T _) π₁idβ ⁻¹ ◾ [][]T ⁻¹))

abstract
  [id]v : ∀{Γ A}{x : Var Γ A} → x ≃ x [ idV ]v
  [id]v {x = vze} = uncoe (VarΓ= (ap (_[_]T _) ⌜wkid⌝))
                  ◾̃ uncoe (VarΓ= (ap (_[_]T _) π₁idβ ⁻¹ ◾ [][]T ⁻¹))
  [id]v {x = vsu x} = vsu≃ ([id]T ⁻¹ ◾ ap (_[_]T _) (⌜idV⌝ ⁻¹)) ([id]v {x = x})
                    ◾̃ vsu[] {x = x}{β = idV}
                    ◾̃ uncoe (VarΓ= (ap (_[_]T _) π₁idβ ⁻¹ ◾ [][]T ⁻¹))

abstract
  idrV : ∀{Γ Δ}{β : Vars Γ Δ} →  β ∘V idV ≡ β
  idrV {β = ε} = refl
  idrV {β = β , x} = ,V≃' idrV
                          ( uncoe (VarΓ= ([][]T ◾ ap (_[_]T _) ⌜∘V⌝)) ⁻¹̃
                          ◾̃ [id]v ⁻¹̃)

abstract
  [][]v : ∀{Γ Δ Θ A}{x : Var Θ A}{β : Vars Γ Δ}{γ : Vars Δ Θ}
        → x [ γ ]v [ β ]v ≃ x [ γ ∘V β ]v
  [][]v {x = vze}  {γ = γ , x} = coe[]v' (ap (_[_]T _) π₁idβ ⁻¹ ◾ [][]T ⁻¹)
                               ◾̃ uncoe (VarΓ= ([][]T ◾ ap (_[_]T _) ⌜∘V⌝))
                               ◾̃ uncoe (VarΓ= (ap (_[_]T _) π₁idβ ⁻¹ ◾ [][]T ⁻¹))
  [][]v {x = vsu x}{γ = γ , y} = coe[]v' (ap (_[_]T _) π₁idβ ⁻¹ ◾ [][]T ⁻¹)
                               ◾̃ [][]v {x = x}
                               ◾̃ uncoe (VarΓ= (ap (_[_]T _) π₁idβ ⁻¹ ◾ [][]T ⁻¹))

abstract
  assV : ∀{Γ Δ Θ Σ}{β : Vars Γ Δ}{γ : Vars Δ Θ}{ι : Vars Θ Σ}
       → (ι ∘V γ) ∘V β ≡ ι ∘V (γ ∘V β)
  assV {ι = ε} = refl
  assV {ι = ι , x} = ,V≃' assV
                          ( uncoe (VarΓ= ([][]T ◾ ap (_[_]T _) ⌜∘V⌝)) ⁻¹̃
                          ◾̃ coe[]v' ([][]T ◾ ap (_[_]T _) ⌜∘V⌝)
                          ◾̃ [][]v {x = x}
                          ◾̃ uncoe (VarΓ= ([][]T ◾ ap (_[_]T _) ⌜∘V⌝)))

------------------------------------------------------------------------------
-- the category of renamings
------------------------------------------------------------------------------

REN : Cat
REN = record 
  { Obj  = Con
  ; _⇒_  = Vars
  ; idc  = idV
  ; _∘c_ = _∘V_
  ; idl  = idlV
  ; idr  = idrV
  ; ass  = assV }

------------------------------------------------------------------------------
-- functor laws for renaming types and terms
------------------------------------------------------------------------------

abstract
  [⌜idV⌝]T : ∀{Γ}{A : Ty Γ} → A [ ⌜ idV ⌝V ]T ≡ A
  [⌜idV⌝]T = ap (_[_]T _) ⌜idV⌝ ◾ [id]T

abstract
  [⌜∘V⌝]T : ∀{Γ Δ Θ A}{β : Vars Δ Θ}{γ : Vars Γ Δ} → A [ ⌜ β ⌝V ]T [ ⌜ γ ⌝V ]T ≡ A [ ⌜ β ∘V γ ⌝V ]T
  [⌜∘V⌝]T = [][]T ◾ ap (_[_]T _) ⌜∘V⌝

abstract
  [⌜idV⌝]t : ∀{Γ}{A : Ty Γ}{t : Tm Γ A} → t [ ⌜ idV ⌝V ]t ≃ t
  [⌜idV⌝]t = []t≃ refl (to≃ ⌜idV⌝) ◾̃ [id]t'

abstract
  [⌜∘V⌝]t : ∀{Γ Δ Θ A}{β : Vars Δ Θ}{γ : Vars Γ Δ}{t : Tm Θ A} → t [ ⌜ β ⌝V ]t [ ⌜ γ ⌝V ]t ≃ t [ ⌜ β ∘V γ ⌝V ]t
  [⌜∘V⌝]t = [][]t' ◾̃ []t≃ refl (to≃ ⌜∘V⌝)

------------------------------------------------------------------------------
-- lifting a renaming
------------------------------------------------------------------------------

_^V_ : ∀{Γ Δ}(β : Vars Γ Δ)(A : Ty Δ) → Vars (Γ , A [ ⌜ β ⌝V ]T) (Δ , A)
β ^V A = (β ∘V wkV idV)
       , coe (VarΓ= ( [][]T
                    ◾ ap (_[_]T A)
                         ( ⌜wkV⌝
                         ◾ ap (⌜_⌝V)
                              ( idrV ⁻¹
                              ◾ wkVβ))))
             vze

infixl 5 _^V_

abstract
  ⌜^⌝ : ∀{Γ Δ A}{β : Vars Γ Δ} → ⌜ β ^V A ⌝V ≡ ⌜ β ⌝V ^ A
  ⌜^⌝ {Γ}{Δ}{A}{β}

    = ,s≃' (⌜∘V⌝ ⁻¹ ◾ ap (_∘_ ⌜ β ⌝V) ⌜wkid⌝ ⁻¹)
           ( ⌜coe⌝v ([][]T ◾ ap (_[_]T A) (⌜wkV⌝ ◾ ap ⌜_⌝V (idrV ⁻¹ ◾ wkVβ)))
           ◾̃ uncoe (TmΓ= [][]T))

abstract
  ⌜idV^⌝ : ∀{Γ A} → ⌜ idV {Γ} ^V A ⌝V ≃ ⌜ idV {Γ , A} ⌝V
  ⌜idV^⌝ {Γ}{A} = ,s≃ (,C≃ refl (to≃ (ap (_[_]T _) ⌜idV⌝ ◾ [id]T)))
                     refl
                     (⌜⌝V≃ (,C≃ refl (to≃ (ap (_[_]T _) ⌜idV⌝ ◾ [id]T)))
                           refl
                           ( to≃ idlV
                           ◾̃ wkV≃ refl
                                  refl
                                  r̃
                                  (to≃ (ap (_[_]T _) ⌜idV⌝ ◾ [id]T))))
                     r̃
                     ( ⌜coe⌝v ([][]T ◾ ap (_[_]T A) (⌜wkV⌝ ◾ ap ⌜_⌝V (idrV ⁻¹ ◾ wkVβ)))
                     ◾̃ vz≃ refl (to≃ (ap (_[_]T _) ⌜idV⌝ ◾ [id]T))
                     ◾̃ ⌜coe⌝v (ap (_[_]T A) ⌜wkid⌝) ⁻¹̃)

abstract
  ∘V^ : ∀{Γ Δ Θ A}{β : Vars Δ Θ}{γ : Vars Γ Δ}
       → (β ^V A) ∘V (γ ^V A [ ⌜ β ⌝V ]T)
       ≃ (β ∘V γ) ^V A
  ∘V^ {Γ}{Δ}{Θ}{A}{β}{γ}

    = ,V≃ (,C≃ refl (to≃ [⌜∘V⌝]T))
          refl
          ( to≃ assV
          ◾̃ ∘V≃ (,C≃ refl (to≃ [⌜∘V⌝]T))
                ( to≃ (wkVβ ◾ idlV)
                ◾̃ ∘V≃ (,C≃ refl (to≃ [⌜∘V⌝]T))
                      (wkV≃ refl refl r̃ (to≃ [⌜∘V⌝]T)))
          ◾̃ to≃ (assV ⁻¹))
          r̃
          ( uncoe (VarΓ= ([][]T ◾ ap (_[_]T A) ⌜∘V⌝)) ⁻¹̃
          ◾̃ coe[]v' ([][]T ◾ ap (_[_]T A) (⌜wkV⌝ ◾ ap ⌜_⌝V (idrV ⁻¹ ◾ wkVβ)))
          ◾̃ uncoe (VarΓ= (ap (_[_]T (A [ ⌜ β ⌝V ]T)) π₁idβ ⁻¹ ◾ [][]T ⁻¹)) ⁻¹̃
          ◾̃ uncoe (VarΓ= ([][]T ◾ ap (_[_]T (A [ ⌜ β ⌝V ]T)) (⌜wkV⌝ ◾ ap ⌜_⌝V (idrV ⁻¹ ◾ wkVβ)))) ⁻¹̃
          ◾̃ vze≃ refl (to≃ [⌜∘V⌝]T)
          ◾̃ uncoe (VarΓ= ([][]T ◾ ap (_[_]T A) (⌜wkV⌝ ◾ ap ⌜_⌝V (idrV ⁻¹ ◾ wkVβ)))))

