module NBE.Cheat where

postulate
  cheat : ∀{i}{A : Set i} → A
