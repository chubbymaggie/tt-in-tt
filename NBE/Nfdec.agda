module NBE.Nfdec where

open import lib
open import JM.JM
open import TT.Syntax
open import NBE.Renamings
open import NBE.Nf
open import NBE.NTy

-- definition of decidable types

data isDec {i}(A : Set i) : Set i where
  yes : A → isDec A
  no  : (A → ⊥) → isDec A

-- variables are decidable

injvsu : ∀{Γ A B}{x₀ x₁ : Var Γ A} → vsu {B = B} x₀ ≡ vsu x₁ → x₀ ≡ x₁
injvsu refl = refl

decVar : ∀{Γ A}(x₀ x₁ : Var Γ A) → isDec (x₀ ≡ x₁)
decVar vze vze = yes refl
decVar vze (vsu x₁) = no λ { () }
decVar (vsu x₀) vze = no λ { () }
decVar (vsu x₀) (vsu x₁) with decVar x₀ x₁
decVar (vsu x₀) (vsu .x₀) | yes refl = yes refl
decVar (vsu x₀) (vsu x₁) | no p = no λ q → p (injvsu q)

-- total space of a term

record Tot (T : (Γ : Con) → Ty Γ → Set) : Set₁ where
  constructor tot
  field
    {con} : Con
    {ty}  : Ty con
    tm    : T con ty

-- constructors are injective

injtotNe : ∀{Γ A}{n₀ n₁ : Ne Γ A} → _≡_ {A = Tot Ne} (tot n₀) (tot n₁)
         → n₀ ≡ n₁
injtotNe refl = refl

injvar : ∀{Γ A}{x₀ x₁ : Var Γ A} → _≡_ {A = Ne Γ A} (var x₀) (var x₁) → x₀ ≡ x₁
injvar refl = refl

injtotNf : ∀{Γ A}{v₀ v₁ : Nf Γ A} → _≡_ {A = Tot Nf} (tot v₀) (tot v₁) → v₀ ≡ v₁
injtotNf refl = refl

injneuU : ∀{Γ}{n₀ n₁ : Ne Γ U} → neuU n₀ ≡ neuU n₁ → n₀ ≡ n₁
injneuU refl = refl

injneuEl : ∀{Γ Â}{n₀ n₁ : Ne Γ (El Â)} → neuEl n₀ ≡ neuEl n₁ → n₀ ≡ n₁
injneuEl refl = refl

injlam : ∀{Γ A B}{v₀ v₁ : Nf (Γ , A) B} → _≡_ {A = Nf Γ (Π A B)} (lamNf v₀) (lamNf v₁) → v₀ ≡ v₁
injlam refl = refl

injElN : ∀{Γ}{Â₀ Â₁ : Nf Γ U} → ElN Â₀ ≡ ElN Â₁ → Â₀ ≡ Â₁
injElN refl = refl

injΠN : ∀{Γ}{A₀ A₁ : NT Γ}{B₀ : NT (Γ , ⌜ A₀ ⌝nt)}{B₁ : NT (Γ , ⌜ A₁ ⌝nt)}
      → ΠN A₀ B₀ ≡ ΠN A₁ B₁ → A₀ ≡ A₁
injΠN refl = refl

injΠN' : ∀{Γ}{A : NT Γ}{B₀ B₁ : NT (Γ , ⌜ A ⌝nt)}
       → ΠN A B₀ ≡ ΠN A B₁ → B₀ ≡ B₁
injΠN' refl = refl

-- decidability of normal types, neutral terms and normal forms
-- (indexed by normal types) defined mutually

decNT : ∀{Γ}(A₀ A₁ : NT Γ)                   → isDec (A₀ ≡ A₁)
decNe : ∀{Γ}(A : NT Γ)(n₀ n₁ : Ne Γ ⌜ A ⌝nt) → isDec (_≡_ {A = Tot Ne} (tot n₀) (tot n₁))
decNf : ∀{Γ}(A : NT Γ)(n₀ n₁ : Nf Γ ⌜ A ⌝nt) → isDec (_≡_ {A = Tot Nf} (tot n₀) (tot n₁))

decNT (ΠN A₀ B₀) (ΠN A₁  B₁)  with decNT A₀ A₁
decNT (ΠN A₀ B₀) (ΠN .A₀ B₁)  | yes refl with decNT B₀ B₁
decNT (ΠN A₀ B₀) (ΠN .A₀ .B₀) | yes refl | yes refl = yes refl
decNT (ΠN A₀ B₀) (ΠN .A₀ B₁)  | yes refl | no p = no (λ q → p (injΠN' q))
decNT (ΠN A₀ B₀) (ΠN A₁  B₁)  | no p = no (λ q → p (injΠN q))
decNT (ΠN _  _)  UN           = no (λ { () })
decNT (ΠN _  _)  (ElN Â)      = no (λ { () })
decNT UN         (ΠN _ _)     = no (λ { () })
decNT UN         UN           = yes refl
decNT UN         (ElN Â)      = no (λ { () })
decNT (ElN Â)    (ΠN _ _)     = no (λ { () })
decNT (ElN Â)    UN           = no (λ { () })
decNT (ElN Â₀)   (ElN Â₁)     with decNf _ Â₀ Â₁
decNT (ElN Â₀)   (ElN .Â₀)    | yes refl = yes refl
decNT (ElN Â₀)   (ElN Â₁)     | no p = no (λ q → p (ap tot (injElN q)))

decNe (ΠN A₀ A₁) (var x₀) (var x₁) with decVar x₀ x₁
decNe (ΠN A₀ A₁) (var x₀) (var .x₀) | yes refl = yes refl
decNe (ΠN A₀ A₁) (var x₀) (var x₁) | no p = no λ q → p (injvar (injtotNe q))
decNe (UN) (var x₀) (var x₁) with decVar x₀ x₁
decNe UN (var x₀) (var .x₀) | yes refl = yes refl
decNe UN (var x₀) (var x₁) | no p = no λ q → p (injvar (injtotNe q))
decNe (ElN Â) (var x₀) (var x₁) with decVar x₀ x₁
decNe (ElN Â) (var x₀) (var .x₀) | yes refl = yes refl
decNe (ElN Â) (var x₀) (var x₁) | no p = no λ q → p (injvar (injtotNe q))

decNf (ΠN A B) (lamNf n₀) (lamNf n₁) with decNf _ n₀ n₁
decNf (ΠN A B) (lamNf n₀) (lamNf .n₀) | yes refl = yes refl
decNf (ΠN A B) (lamNf n₀) (lamNf n₁) | no p = no λ q → p (ap tot (injlam (injtotNf q)))
decNf (UN) (neuU n₀) (neuU n₁) with decNe _ n₀ n₁
decNf (UN) (neuU n₀) (neuU .n₀) | yes refl = yes refl
decNf (UN) (neuU n₀) (neuU n₁) | no p = no λ q → p (ap tot (injneuU (injtotNf q)))
decNf (ElN Â) (neuEl n₀) (neuEl n₁) with decNe (ElN Â) n₀ n₁
decNf (ElN Â) (neuEl n₀) (neuEl .n₀) | yes refl = yes refl
decNf (ElN Â) (neuEl n₀) (neuEl n₁) | no p = no λ q → p (ap tot (injneuEl (injtotNf q)))

-- decidability of normal forms for general types

decNf' : ∀{Γ}(A : Ty Γ)(n n' : Nf Γ A) → isDec (n ≡ n')
decNf' {Γ} A n n' with decNf (proj₁ (normT A)) (coe (NfΓ= (proj₂ (normT A))) n) (coe (NfΓ= (proj₂ (normT A))) n')
decNf' A n n' | yes p = yes (from≃ (uncoe (NfΓ= (proj₂ (normT A))) ◾̃ to≃ (injtotNf p) ◾̃ uncoe (NfΓ= (proj₂ (normT A))) ⁻¹̃))
decNf' A n n' | no p = no λ q → p (ap (λ z → tot (coe (NfΓ= (proj₂ (normT A))) z)) q)
