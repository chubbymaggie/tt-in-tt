module NBE.NTy where

open import lib
open import JM.JM
open import TT.Syntax
open import TT.Congr
open import TT.Laws
open import NBE.Nf
open import NBE.Norm

-- normal types together with their embeddings

data NT : Con → Set
⌜_⌝nt : ∀{Γ} → NT Γ → Ty Γ

data NT where
  ΠN  : {Γ : Con}(A : NT Γ) → NT (Γ , ⌜ A ⌝nt) → NT Γ
  UN  : {Γ : Con} → NT Γ
  ElN : {Γ : Con}(Â : Nf Γ U) → NT Γ

⌜ ΠN A B ⌝nt = Π ⌜ A ⌝nt ⌜ B ⌝nt
⌜ UN ⌝nt = U
⌜ ElN Â ⌝nt = El ⌜ Â ⌝nf

-- congruence rules

NT= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁) → NT Γ₀ ≡ NT Γ₁
NT= = ap NT

⌜coe⌝nt : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){N : NT Γ₀}
        → ⌜ coe (NT= Γ₂) N ⌝nt ≃ ⌜ N ⌝nt
⌜coe⌝nt refl = refl , refl

-- substitution of normal types

[]NT : ∀{Γ Δ}(N : NT Δ)(σ : Tms Γ Δ) → Σ (NT Γ) λ N' → ⌜ N ⌝nt [ σ ]T ≡ ⌜ N' ⌝nt

[]NT (ΠN AN BN) σ

  = let
      (AN' , pA) = []NT AN σ
      (BN' , pB) = []NT BN (σ ^ ⌜ AN ⌝nt)
    in
        ΠN AN' (coe (NT= (,C≃ refl (to≃ pA))) BN')
      , (Π[] ◾ Π≃' pA
                   (to≃ pB ◾̃ ⌜coe⌝nt (,C≃ refl (to≃ (proj₂ ([]NT AN σ)))) ⁻¹̃))

[]NT UN σ = UN , U[]

[]NT (ElN Â) σ

  = ElN (coe (NfΓ= U[]) (norm (⌜ Â ⌝nf [ σ ]t)))
  , (El[] ◾ ap El (from≃ (uncoe (TmΓ= U[]) ⁻¹̃ ◾̃ to≃ (compl (⌜ Â ⌝nf [ σ ]t)) ◾̃ ⌜coe⌝nf U[] ⁻¹̃)))

-- calculating normal types from types together with completeness

normT : ∀{Γ}(A : Ty Γ) → Σ (NT Γ) λ N → A ≡ ⌜ N ⌝nt

normT (A [ σ ]T)

  = let
      (N , p) = normT A
      (N' , p') = []NT N σ
    in
      N' , (ap (λ z → z [ σ ]T) p ◾ p')

normT U = UN , refl

normT (El Â) = ElN (norm Â) , ap El (compl Â) 

normT (Π A B)

  = let
      (AN , pA) = normT A
      (BN , pB) = normT B
    in   (ΠN AN (coe (NT= (,C≃ refl (to≃ pA))) BN))
       , Π≃' pA (to≃ pB ◾̃ ⌜coe⌝nt (,C≃ refl (to≃ pA)) ⁻¹̃)
