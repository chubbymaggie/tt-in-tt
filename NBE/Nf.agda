{-# OPTIONS --no-eta #-}

module NBE.Nf where

open import lib
open import JM.JM
open import TT.Syntax
open import TT.Congr
open import TT.Laws
open import NBE.Renamings

------------------------------------------------------------------------------
-- neutral terms and normal forms together with embeddings into the syntax
------------------------------------------------------------------------------

data Ne : ∀ Γ → Ty Γ → Set
data Nf  : ∀ Γ → Ty Γ → Set

⌜_⌝ne : ∀{Γ A} → Ne Γ A → Tm Γ A
⌜_⌝nf : ∀{Γ A} → Nf Γ A → Tm Γ A

data Ne where
  var : ∀{Γ A}(x : Var Γ A) → Ne Γ A
  appNe : ∀{Γ A B}(f : Ne Γ (Π A B))(v : Nf Γ A) → Ne Γ (B [ < ⌜ v ⌝nf > ]T)

data Nf where
  neuU  : ∀{Γ}(n : Ne Γ U) → Nf Γ U
  neuEl : ∀{Γ Â}(n : Ne Γ (El Â)) → Nf Γ (El Â)
  lamNf : ∀{Γ A B}(v : Nf (Γ , A) B) → Nf Γ (Π A B)

⌜ var x ⌝ne = ⌜ x ⌝v
⌜ appNe n u ⌝ne = ⌜ n ⌝ne $ ⌜ u ⌝nf

⌜ neuU n ⌝nf = ⌜ n ⌝ne
⌜ neuEl n ⌝nf = ⌜ n ⌝ne
⌜ lamNf t ⌝nf = lam ⌜ t ⌝nf

------------------------------------------------------------------------------
-- renaming neutral terms, normal forms and their relation to terms
-- (specification)
------------------------------------------------------------------------------

_[_]ne : ∀{Γ Δ A}(n : Ne Δ A)(β : Vars Γ Δ) → Ne Γ (A [ ⌜ β ⌝V ]T)
_[_]nf : ∀{Γ Δ A}(v : Nf Δ A)(β : Vars Γ Δ) → Nf Γ (A [ ⌜ β ⌝V ]T)

infixl 8 _[_]ne _[_]nf

⌜[]ne⌝ : ∀{Γ Δ A}{n : Ne Δ A}{β : Vars Γ Δ}
       → ⌜ n ⌝ne [ ⌜ β ⌝V ]t ≡ ⌜ n [ β ]ne ⌝ne

⌜[]nf⌝ : ∀{Γ Δ A}{v : Nf Δ A}{β : Vars Γ Δ}
       → ⌜ v ⌝nf [ ⌜ β ⌝V ]t ≡ ⌜ v [ β ]nf ⌝nf

------------------------------------------------------------------------------
-- congruence and conversion rules
------------------------------------------------------------------------------

NeΓ= : {Γ : Con}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁) → Ne Γ A₀ ≡ Ne Γ A₁
NeΓ= {Γ} = ap (Ne Γ)

⌜coe⌝ne : ∀{Γ A₀ A₁}(A₂ : A₀ ≡ A₁){n : Ne Γ A₀}
        → ⌜ coe (NeΓ= A₂) n ⌝ne ≃ ⌜ n ⌝ne
⌜coe⌝ne refl = refl , refl

coe[]ne' : ∀{Γ Δ}{β : Vars Γ Δ}{A₀ A₁ : Ty Δ}(A₂ : A₀ ≡ A₁){n : Ne Δ A₀}
        → coe (NeΓ= A₂) n [ β ]ne ≃ n [ β ]ne
coe[]ne' refl = refl , refl

NfΓ= : {Γ : Con}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁) → Nf Γ A₀ ≡ Nf Γ A₁
NfΓ= {Γ} = ap (Nf Γ)

[]nf≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ A}{v : Nf Δ A}
        {β₀ : Vars Γ₀ Δ}{β₁ : Vars Γ₁ Δ}(β₂ : β₀ ≃ β₁)
      → v [ β₀ ]nf ≃ v [ β₁ ]nf
[]nf≃ refl (refl , refl) = refl , refl

⌜coe⌝nf : ∀{Γ A₀ A₁}(A₂ : A₀ ≡ A₁){v : Nf Γ A₀}
        → ⌜ coe (NfΓ= A₂) v ⌝nf ≃ ⌜ v ⌝nf
⌜coe⌝nf refl = refl , refl

coe[]nf' : ∀{Γ Δ}{β : Vars Γ Δ}{A₀ A₁ : Ty Δ}(A₂ : A₀ ≡ A₁){v : Nf Δ A₀}
        → coe (NfΓ= A₂) v [ β ]nf ≃ v [ β ]nf
coe[]nf' refl = refl , refl

neuU= : ∀{Γ}{n₀ n₁ : Ne Γ U}(n₂ : n₀ ≡ n₁) → neuU n₀ ≡ neuU n₁
neuU= refl = refl

neuEl≃ : ∀{Γ Â₀ Â₁}(Â₂ : Â₀ ≡ Â₁){n₀ : Ne Γ (El Â₀)}{n₁ : Ne Γ (El Â₁)}(n₂ : n₀ ≃ n₁)
      → neuEl n₀ ≃ neuEl n₁
neuEl≃ refl (refl , refl) = refl , refl

var≃ : ∀{Γ A₀ A₁}(A₂ : A₀ ≡ A₁){x₀ : Var Γ A₀}{x₁ : Var Γ A₁}(x₂ : x₀ ≃ x₁)
     → var x₀ ≃ var x₁
var≃ refl (refl , refl) = refl , refl

lamNf≃ : ∀{Γ A₀ A₁}(A₂ : A₀ ≡ A₁)
         {B₀ : Ty (Γ , A₀)}{B₁ : Ty (Γ , A₁)}(B₂ : B₀ ≃ B₁)
         {v₀ : Nf (Γ , A₀) B₀}{v₁ : Nf (Γ , A₁) B₁}(v₂ : v₀ ≃ v₁)
       → _≃_ {A = Nf Γ (Π A₀ B₀)}{B = Nf Γ (Π A₁ B₁)} (lamNf v₀) (lamNf v₁)
lamNf≃ refl (refl , refl) (refl , refl) = refl , refl

appNe≃ : ∀{Γ A₀ A₁}(A₂ : A₀ ≡ A₁)
         {B₀ : Ty (Γ , A₀)}{B₁ : Ty (Γ , A₁)}(B₂ : B₀ ≃ B₁)
         {n₀ : Ne Γ (Π A₀ B₀)}{n₁ : Ne Γ (Π A₁ B₁)}(n₂ : n₀ ≃ n₁)
         {v₀ : Nf Γ A₀}{v₁ : Nf Γ A₁}(v₂ : v₀ ≃ v₁)
       → _≃_ {A = Ne Γ (B₀ [ < ⌜ v₀ ⌝nf > ]T)}
             {B = Ne Γ (B₁ [ < ⌜ v₁ ⌝nf > ]T)}
             (appNe n₀ v₀)
             (appNe n₁ v₁)
appNe≃ refl (refl , refl) (refl , refl) (refl , refl) = refl , refl

------------------------------------------------------------------------------
-- renaming neutral terms, normal forms and their relation to terms
-- (implementation)
------------------------------------------------------------------------------

var x [ β ]ne = var (x [ β ]v)
appNe {A = A} n v [ β ]ne = coe (NeΓ= ([][]T ◾ ap (_[_]T _) ^∘<⌜⌝> ◾ [][]T ⁻¹))
                                (appNe (coe (NeΓ= Π[]) (n [ β ]ne))
                                     (v [ β ]nf))
  where
    abstract
      ^∘<⌜⌝> : (⌜ β ⌝V ^ A) ∘ < ⌜ v [ β ]nf ⌝nf > ≡ < ⌜ v ⌝nf > ∘ ⌜ β ⌝V
      ^∘<⌜⌝> = ,∘
             ◾ ,s≃' (ass ◾ ap (_∘_ ⌜ β ⌝V) π₁idβ ◾ idr ◾ idl ⁻¹)
                    ( uncoe (TmΓ= [][]T) ⁻¹̃
                    ◾̃ coe[]t' [][]T
                    ◾̃ π₂idβ
                    ◾̃ uncoe (TmΓ= ([id]T ⁻¹)) ⁻¹̃
                    ◾̃ to≃ (⌜[]nf⌝ {v = v}{β = β} ⁻¹)
                    ◾̃ coe[]t' ([id]T ⁻¹) ⁻¹̃
                    ◾̃ uncoe (TmΓ= [][]T))
             ◾ ,∘ ⁻¹
    
neuU n [ β ]nf = coe (NfΓ= (U[] ⁻¹)) (neuU (coe (NeΓ= U[]) (n [ β ]ne)))
neuEl n [ β ]nf = coe (NfΓ= (El[] ⁻¹)) (neuEl (coe (NeΓ= El[]) (n [ β ]ne))) 
lamNf v [ β ]nf = coe (NfΓ= (Π≃' refl (to≃ (ap (_[_]T _) ⌜^⌝)) ◾ Π[] ⁻¹))
                      (lamNf (v [ β ^V _ ]nf))


abstract
  ⌜[]ne⌝ {n = var x} = ⌜[]v⌝ {x = x}
  ⌜[]ne⌝ {n = appNe n v} = from≃ ( $[]
                               ◾̃ $≃ (from≃ ( uncoe (TmΓ= Π[]) ⁻¹̃
                                           ◾̃ to≃ (⌜[]ne⌝ {n = n})
                                           ◾̃ ⌜coe⌝ne Π[] ⁻¹̃))
                                    (⌜[]nf⌝ {v = v})
                               ◾̃ ⌜coe⌝ne ([][]T ◾ ap (_[_]T _) _ ◾ [][]T ⁻¹) ⁻¹̃)
abstract
  ⌜[]nf⌝ {v = neuU n}  = ⌜[]ne⌝ {n = n}
                       ◾ from≃ (⌜coe⌝ne U[] ⁻¹̃ ◾̃ ⌜coe⌝nf (U[] ⁻¹) ⁻¹̃)
  ⌜[]nf⌝ {v = neuEl n} = ⌜[]ne⌝ {n = n}
                       ◾ from≃ (⌜coe⌝ne El[] ⁻¹̃ ◾̃ ⌜coe⌝nf (El[] ⁻¹) ⁻¹̃)
  ⌜[]nf⌝ {v = lamNf v}

    = from≃ ( from≡ (TmΓ= Π[]) lam[]
            ◾̃ lam≃ refl
                   r̃
                   (to≃ (ap (_[_]T _) (⌜^⌝ ⁻¹)))
                   ( []t≃ refl (to≃ (⌜^⌝ ⁻¹))
                   ◾̃ to≃ (⌜[]nf⌝ {v = v}))
            ◾̃ ⌜coe⌝nf (Π≃' refl (to≃ (ap (_[_]T _) ⌜^⌝)) ◾ Π[] ⁻¹) ⁻¹̃)

------------------------------------------------------------------------------
-- categorical laws for renaming
------------------------------------------------------------------------------

[id]ne : ∀{Γ A}{n : Ne Γ A} → n [ idV ]ne ≃ n
[id]nf : ∀{Γ A}{v : Nf Γ A} → v [ idV ]nf ≃ v

abstract
  [id]ne {n = var x} = var≃ [⌜idV⌝]T ([id]v ⁻¹̃)
  [id]ne {n = appNe n v} = uncoe (NeΓ= ([][]T ◾ ap (_[_]T _) _ ◾ [][]T ⁻¹)) ⁻¹̃
                         ◾̃ appNe≃ (ap (_[_]T _) ⌜idV⌝ ◾ [id]T)
                                  ( []T≃ (,C≃ refl (to≃ [⌜idV⌝]T))
                                        ( to≃ (⌜^⌝ ⁻¹)
                                        ◾̃ ⌜idV^⌝
                                        ◾̃ to≃ ⌜idV⌝)
                                  ◾̃ to≃ [id]T)
                                  (uncoe (NeΓ= Π[]) ⁻¹̃ ◾̃ [id]ne)
                                  [id]nf

abstract
  [id]nf {v = neuU n} = uncoe (NfΓ= (U[] ⁻¹)) ⁻¹̃
                      ◾̃ to≃ (neuU= (from≃ (uncoe (NeΓ= U[]) ⁻¹̃ ◾̃ [id]ne)))
  [id]nf {v = neuEl n} = uncoe (NfΓ= (El[] ⁻¹)) ⁻¹̃
                       ◾̃ neuEl≃ (from≃ ( uncoe (TmΓ= U[]) ⁻¹̃
                                       ◾̃ []t≃ refl (to≃ ⌜idV⌝)
                                       ◾̃ from≡ (TmΓ= [id]T) [id]t))
                                ( uncoe (NeΓ= El[]) ⁻¹̃
                                ◾̃ [id]ne)
  [id]nf {v = lamNf {Γ}{A} v} = uncoe (NfΓ= (Π≃' refl (to≃ (ap (_[_]T _) ⌜^⌝)) ◾ Π[] ⁻¹)) ⁻¹̃
                              ◾̃ lamNf≃ [⌜idV⌝]T
                                       ( []T≃ (,C≃ refl (to≃ [⌜idV⌝]T))
                                              ( ⌜⌝V≃ (,C≃ refl (to≃ [⌜idV⌝]T)) refl p
                                              ◾̃ to≃ ⌜idV⌝)
                                       ◾̃ to≃ [id]T)
                                       ( []nf≃ (,C≃ refl (to≃ [⌜idV⌝]T)) {v = v} p
                                       ◾̃ [id]nf)
    where
      p : _≃_ {A = Vars (Γ , A [ ⌜ idV ⌝V ]T) (Γ , A)}
              {B = Vars (Γ , A) (Γ , A)}
              (idV ∘V wkV idV , coe (VarΓ= ([][]T ◾ ap (_[_]T A) (⌜wkV⌝ ◾ ap ⌜_⌝V (idrV ⁻¹ ◾ wkVβ)))) vze)
              (wkV idV , coe (VarΓ= (ap (_[_]T A) ⌜wkid⌝)) vze)
      p
      
        = ,V≃ (,C≃ refl (to≃ [⌜idV⌝]T))
              refl
              ( to≃ idlV
              ◾̃ wkV≃ refl refl r̃ (to≃ [⌜idV⌝]T))
              r̃
              ( uncoe (VarΓ=  ([][]T ◾ ap (_[_]T A) (⌜wkV⌝ ◾ ap ⌜_⌝V (idrV ⁻¹ ◾ wkVβ)))) ⁻¹̃
              ◾̃ vze≃ refl (to≃ [⌜idV⌝]T)
              ◾̃ uncoe (VarΓ= (ap (_[_]T A) ⌜wkid⌝)))

[][]ne : ∀{Γ Δ Θ A}{n : Ne Θ A}{β : Vars Δ Θ}{γ : Vars Γ Δ} → n [ β ]ne [ γ ]ne ≃ n [ β ∘V γ ]ne
[][]nf : ∀{Γ Δ Θ A}{v : Nf Θ A}{β : Vars Δ Θ}{γ : Vars Γ Δ} → v [ β ]nf [ γ ]nf ≃ v [ β ∘V γ ]nf

abstract
  [][]ne {n = var x} = var≃ [⌜∘V⌝]T ([][]v {x = x})
  [][]ne {n = appNe n v}{β}{γ}

    = coe[]ne' ([][]T ◾ ap (_[_]T _) _ ◾ [][]T ⁻¹)
    ◾̃ uncoe (NeΓ= ([][]T ◾ ap (_[_]T _) _ ◾ [][]T ⁻¹)) ⁻¹̃
    ◾̃ appNe≃ [⌜∘V⌝]T
             ( to≃ [][]T
             ◾̃ []T≃ (,C≃ refl (to≃ [⌜∘V⌝]T))
                    ( to≃ (ap (_∘_ (⌜ β ⌝V ^ _)) ⌜^⌝ ⁻¹)
                    ◾̃ to≃ (ap (λ z → z ∘ ⌜ γ ^V (_ [ ⌜ β ⌝V ]T) ⌝V) ⌜^⌝ ⁻¹)
                    ◾̃ to≃ ⌜∘V⌝
                    ◾̃ ⌜⌝V≃ (,C≃ refl (to≃ [⌜∘V⌝]T)) refl ∘V^
                    ◾̃ to≃ ⌜^⌝))
             ( uncoe (NeΓ= Π[]) ⁻¹̃
             ◾̃ coe[]ne' Π[]
             ◾̃ [][]ne {n = n}
             ◾̃ uncoe (NeΓ= Π[]))
             ([][]nf {v = v})
    ◾̃ uncoe (NeΓ= ([][]T ◾ ap (_[_]T _) _ ◾ [][]T ⁻¹))

abstract
  [][]nf {v = neuU n}  = coe[]nf' (U[] ⁻¹)
                       ◾̃ uncoe (NfΓ= (U[] ⁻¹)) ⁻¹̃
                       ◾̃ to≃ (neuU= (from≃ ( uncoe (NeΓ= U[]) ⁻¹̃
                                           ◾̃ coe[]ne' U[]
                                           ◾̃ [][]ne {n = n}
                                           ◾̃ uncoe (NeΓ= U[]))))
                       ◾̃ uncoe (NfΓ= (U[] ⁻¹))
  [][]nf {v = neuEl n} = coe[]nf' (El[] ⁻¹)
                       ◾̃ uncoe (NfΓ= (El[] ⁻¹)) ⁻¹̃
                       ◾̃ neuEl≃ (from≃ ( uncoe (TmΓ= U[]) ⁻¹̃
                                       ◾̃ coe[]t' U[]
                                       ◾̃ [][]t'
                                       ◾̃ []t≃'' ⌜∘V⌝
                                       ◾̃ uncoe (TmΓ= U[])))
                                (uncoe (NeΓ= El[]) ⁻¹̃
                                ◾̃ coe[]ne' El[]
                                ◾̃ [][]ne {n = n}
                                ◾̃ uncoe (NeΓ= El[]))
                       ◾̃ uncoe (NfΓ= (El[] ⁻¹))
  [][]nf {v = lamNf v}{β}{γ}

    = coe[]nf' (Π≃' refl (to≃ (ap (_[_]T _) ⌜^⌝)) ◾ Π[] ⁻¹)
    ◾̃ uncoe (NfΓ= (Π≃' refl (to≃ (ap (_[_]T _) ⌜^⌝)) ◾ Π[] ⁻¹)) ⁻¹̃
    ◾̃ lamNf≃ [⌜∘V⌝]T
             ( to≃ [][]T
             ◾̃ []T≃ (,C≃ refl (to≃ [⌜∘V⌝]T))
                    ( to≃ ⌜∘V⌝
                    ◾̃ ⌜⌝V≃ (,C≃ refl (to≃ [⌜∘V⌝]T))
                           refl
                           (∘V^ {β = β}{γ = γ})))
             ( [][]nf {v = v}
             ◾̃ []nf≃ (,C≃ refl (to≃ [⌜∘V⌝]T)) {v = v} (∘V^ {β = β}{γ = γ}))
    ◾̃ uncoe (NfΓ= (Π≃' refl (to≃ (ap (_[_]T _) ⌜^⌝)) ◾ Π[] ⁻¹))
