module NBE.Quote.Ty where

open import lib
open import JM.JM
open import Cats
open import TT.Syntax renaming (_$_ to _$$_)
open import TT.Elim
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.TM
open import NBE.Nf
open import NBE.Nfs
open import NBE.Quote.Motives
open import NBE.Quote.Cxt
open import NBE.LogPred.Motives using (Tbase=)
open import NBE.LogPred.LogPred U̅ E̅l
import NBE.LogPred.Ty.Pi

open Motives M
open MethodsCon mCon

open import NBE.Cheat

module m[]Tᴹ
  {Γ : Con}{Γᴹ : Conᴹ Γ}
  {Δ : Con}{Δᴹ : Conᴹ Δ}
  {A : Ty Δ}(Aᴹ : Tyᴹ Δᴹ A)
  {σ : Tms Γ Δ}(σᴹ : Tmsᴹ Γᴹ Δᴹ σ)
  where

    module qT$S
      {Ψ : Con}
      (ρ : TMC Γ $P Ψ)
      (s : TMT (A [ σ ]T) $F ρ)
      (α : ⟦ Γ ⟧C $F ρ)
      (a : ⟦ A [ σ ]T ⟧T $F (ρ , s , α))
      where

        abstract
          pcoe : Σ (NFT A $F (TMs σ $n ρ)) (λ n → coe (TmΓ= [][]T) s ≡ ⌜ n ⌝nf)
               ≡ Σ (NFT (A [ σ ]T) $F ρ)   (λ n → s ≡ ⌜ n ⌝nf)
          pcoe = Σ≃ (NfΓ= ([][]T ⁻¹))
                    (funext≃ (NfΓ= ([][]T ⁻¹))
                             (λ x₂ → ≡≃ (TmΓ= ([][]T ⁻¹))
                                        (uncoe (TmΓ= [][]T) ⁻¹̃)
                                        (ap≃ {B = Nf _} ⌜_⌝nf ([][]T ⁻¹) x₂)))

        ret : ≡NFT (A [ σ ]T) $F (ρ , s)
        ret = coe pcoe (qT Aᴹ $S (TMs σ $n ρ , coe (TmΓ= [][]T) s , ⟦ σ ⟧s $S (ρ , α) , a))

    module uT$S
      {Ψ : Con}
      (ρ : TMC Γ $P Ψ)
      (n : NET (A [ σ ]T) $F ρ)
      (α : ⟦ Γ ⟧C $F ρ)
      where

        abstract
          pcoe : ⟦ A ⟧T $F (TMs σ $n ρ , ⌜ coe (NeΓ= [][]T) n ⌝ne , ⟦ σ ⟧s $S (ρ , α))
               ≡ ⟦ A ⟧T $F (TMs σ $n ρ , coe (TmΓ= [][]T) ⌜ n ⌝ne , ⟦ σ ⟧s $S (ρ , α))
          pcoe = $F= ⟦ A ⟧T (Tbase= ⟦ Δ ⟧C refl (⌜coe⌝ne [][]T ◾̃ uncoe (TmΓ= [][]T)) r̃)

        ret : ⟦ A [ σ ]T ⟧T $F (ρ , ⌜ n ⌝ne , α)
        ret = coe pcoe (uT Aᴹ $S (TMs σ $n ρ , coe (NeΓ= [][]T) n , ⟦ σ ⟧s $S (ρ , α)))

    _[_]Tᴹ : Tyᴹ Γᴹ (A [ σ ]T)
    _[_]Tᴹ = record
      { qT = record
          { _$S_ = λ { (ρ , s , α , a) → qT$S.ret ρ s α a }
          ; natS = cheat
          }
      ; uT =  record
          { _$S_ = λ { (ρ , n , α) → uT$S.ret ρ n α }
          ; natS = cheat
          }
      }

module mUᴹ
  {Γ : Con}{Γᴹ : Conᴹ Γ}
  where

    module qT$S
      {Ψ : Con}
      (ρ : TMC Γ $P Ψ)
      (s : TMT U $F ρ)
      (α : ⟦ Γ ⟧C $F ρ)
      (a : ⟦ U ⟧T $F (ρ , s , α))
      where

        abstract
          pcoe : Σ (NFT U $F id) (λ n → coe (TmΓ= ([id]T ⁻¹)) (coe (TmΓ= U[]) s) ≡ ⌜ n ⌝nf)
               ≡ Σ (NFT U $F ρ)  (λ n → s ≡ ⌜ n ⌝nf)
          pcoe = Σ≃ (NfΓ= ([id]T ◾ U[] ⁻¹))
                    (funext≃ (NfΓ= ([id]T ◾ U[] ⁻¹))
                             (λ x₂ → ≡≃ (TmΓ= ([id]T ◾ U[] ⁻¹))
                                        (uncoe (TmΓ= ([id]T ⁻¹)) ⁻¹̃ ◾̃ uncoe (TmΓ= U[]) ⁻¹̃)
                                        (ap≃ {B = Nf _} ⌜_⌝nf ([id]T ◾ U[] ⁻¹) x₂)))

    module uT$S
      {Ψ : Con}
      (ρ : TMC Γ $P Ψ)
      (n : NET U $F ρ)
      (α : ⟦ Γ ⟧C $F ρ)
      where

        abstract
          eq : coe (TmΓ= ([id]T ⁻¹)) (coe (TmΓ= U[]) ⌜ n ⌝ne)
             ≡  ⌜ coe (NfΓ= ([id]T ⁻¹)) (neuU (coe (NeΓ= U[]) n)) ⌝nf
          eq = from≃ ( uncoe (TmΓ= ([id]T ⁻¹)) ⁻¹̃
                     ◾̃ uncoe (TmΓ= U[]) ⁻¹̃
                     ◾̃ ⌜coe⌝ne U[] ⁻¹̃
                     ◾̃ ⌜coe⌝nf ([id]T ⁻¹) ⁻¹̃)

    Uᴹ : Tyᴹ Γᴹ U
    Uᴹ = record
      { qT = record
          { _$S_ = λ { (ρ , s , α , a) → coe (qT$S.pcoe ρ s α a) a }
          ; natS = cheat
          }
      ; uT = record
          { _$S_ = λ { (ρ , n , α) → coe (NfΓ= ([id]T ⁻¹)) (neuU (coe (NeΓ= U[]) n)) , uT$S.eq ρ n α }
          ; natS = cheat
          }
      }

module mElᴹ
  {Γ : Con}{Γᴹ : Conᴹ Γ}
  {Â : Tm Γ U}(Âᴹ : Tmᴹ Γᴹ mUᴹ.Uᴹ Â)
  where

    module qT$S
      {Ψ : Con}
      (ρ : TMC Γ $P Ψ)
      (s : TMT (El Â) $F ρ)
      (α : ⟦ Γ ⟧C $F ρ)
      (a : ⟦ El Â ⟧T $F (ρ , s , α))
      where

        abstract
          pcoe : Σ (NFT (El (coe (TmΓ= U[]) (Â [ ρ ]t))) $F id) (λ n → coe (TmΓ= ([id]T ⁻¹)) (coe (TmΓ= El[]) s) ≡ ⌜ n ⌝nf)
               ≡ Σ (NFT (El Â) $F ρ) (λ n → s ≡ ⌜ n ⌝nf)
          pcoe = Σ≃ (NfΓ= ([id]T ◾ El[] ⁻¹))
                    (funext≃ (NfΓ= ([id]T ◾ El[] ⁻¹))
                             (λ x₂ → ≡≃ (TmΓ= ([id]T ◾ El[] ⁻¹))
                                        (uncoe (TmΓ= ([id]T ⁻¹)) ⁻¹̃ ◾̃ uncoe (TmΓ= El[]) ⁻¹̃)
                                        (ap≃ {B = Nf _} ⌜_⌝nf ([id]T ◾ El[] ⁻¹) x₂)))
                    
        ret : ≡NFT (El Â) $F (ρ , s)
        ret = coe pcoe a
      
    module uT$S
      {Ψ : Con}
      (ρ : TMC Γ $P Ψ)
      (n : NET (El Â) $F ρ)
      (α : ⟦ Γ ⟧C $F ρ)
      where

        abstract
          eq : coe (TmΓ= ([id]T ⁻¹)) (coe (TmΓ= El[]) ⌜ n ⌝ne)
             ≡ ⌜ coe (NfΓ= ([id]T ⁻¹)) (neuEl (coe (NeΓ= El[]) n)) ⌝nf
          eq = from≃ ( uncoe (TmΓ= ([id]T ⁻¹)) ⁻¹̃
                     ◾̃ uncoe (TmΓ= El[]) ⁻¹̃
                     ◾̃ ⌜coe⌝ne El[] ⁻¹̃
                     ◾̃ ⌜coe⌝nf ([id]T ⁻¹) ⁻¹̃)

    Elᴹ : Tyᴹ Γᴹ (El Â)
    Elᴹ = record
      { qT = record
          { _$S_ = λ { (ρ , s , α , a) → qT$S.ret ρ s α a }
          ; natS = cheat
          }
      ; uT = record
          { _$S_ = λ { (ρ , n , α) → (coe (NfΓ= ([id]T ⁻¹)) (neuEl (coe (NeΓ= El[]) n))) , uT$S.eq ρ n α }
          ; natS = cheat
          }
      }

module mΠᴹ
  {Γ : Con}{Γᴹ : Conᴹ Γ}
  {A : Ty Γ}(Aᴹ : Tyᴹ Γᴹ A)
  {B : Ty (Γ , A)}(Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B)
  where

    module qT$S
      {Ψ : Con}
      (ρ : TMC Γ $P Ψ)
      (s : TMT (Π A B) $F ρ)
      (α : ⟦ Γ ⟧C $F ρ)
      (f : ⟦ Π A B ⟧T $F (ρ , s , α))
      where

        open NBE.LogPred.Ty.Pi.mΠᴹ ⟦ A ⟧T ⟦ B ⟧T
        open Exp f

        abstract
          lcoe1 : TMC Γ $P wkV idV $ ρ ≡ π₁ (ρ ^ A)
          lcoe1 = ap (_∘_ ρ) (⌜wkid⌝ ⁻¹) ◾ π₁β ⁻¹

        α' : ⟦ Γ ⟧C $F π₁ (ρ ^ A)
        α' = coe ($F= ⟦ Γ ⟧C lcoe1) (⟦ Γ ⟧C $F wkV idV $ α)

        abstract
          lcoe2 : A [ ρ ]T [ wk ]T ≡ A [ π₁ (ρ ^ A) ]T
          lcoe2 = [][]T ◾ ap (_[_]T A) (π₁β ⁻¹)

        a' : ⟦ A ⟧T $F (π₁ (ρ ^ A) , ⌜ coe (NeΓ= lcoe2) (var vze) ⌝ne , α')
        a' = uT Aᴹ $S (π₁ (ρ ^ A) , coe (NeΓ= lcoe2) (var vze) , α')

        abstract
          lcoe3 : _≡_ {A = (TMC Γ ,P TMT A ,P ⟦ Γ ⟧C [ wkn ]F) $P (Ψ , A [ ρ ]T)}
                      (π₁ (ρ ^ A) , ⌜ coe (NeΓ= lcoe2) (var vze) ⌝ne , α')
                      (π₁ (ρ ^ A) , π₂ (ρ ^ A) , α')
          lcoe3 = Tbase= ⟦ Γ ⟧C refl (⌜coe⌝ne lcoe2 ◾̃ uncoe (TmΓ= [][]T) ◾̃ π₂β' ⁻¹̃) r̃

        a : ⟦ A ⟧T $F (π₁ (ρ ^ A) , π₂ (ρ ^ A) , α')
        a = coe ($F= ⟦ A ⟧T lcoe3) a'

        α',a : ⟦ Γ , A ⟧C $F (ρ ^ A)
        α',a = α' , a

        abstract
          lcoe4 : A [ ρ ]T [ π₁ {A = A [ ρ ]T} id ]T ≡ A [ ρ ∘ ⌜ wkV idV ⌝V ]T
          lcoe4 = [][]T ◾ ap (_[_]T A) (ap (_∘_ ρ) ⌜wkid⌝)
        

        zero : Tm (Ψ , A [ ρ ]T) (A [ ρ ∘ ⌜ wkV idV ⌝V ]T)
        zero = coe (TmΓ= lcoe4) (π₂ id)

        abstract
          lcoe5 : _≡_ {A = (TMC Γ ,P TMT A ,P ⟦ Γ ⟧C [ wkn ]F) $P (Ψ , A [ ρ ]T)}
                      (π₁ (ρ ^ A) , π₂ (ρ ^ A) , α')(
                      ρ ∘ ⌜ wkV idV ⌝V , zero , ⟦ Γ ⟧C $F wkV idV $ α)
          lcoe5 = Tbase= ⟦ Γ ⟧C (π₁β ◾ ap (_∘_ ρ) ⌜wkid⌝)
                                (π₂β' ◾̃ uncoe (TmΓ= [][]T) ⁻¹̃ ◾̃ uncoe (TmΓ= lcoe4))
                                (uncoe ($F= ⟦ Γ ⟧C lcoe1) ⁻¹̃)

        a''' : ⟦ A ⟧T $F (ρ ∘ ⌜ wkV idV ⌝V , zero , ⟦ Γ ⟧C $F wkV idV $ α)
        a''' = coe ($F= ⟦ A ⟧T lcoe5) a

        b : ⟦ B ⟧T $F ( (ρ ∘ ⌜ wkV idV ⌝V , zero)
                      , coe (TmΓ= pcoe1) (coe (TmΓ= pcoe2) (s [ ⌜ wkV idV ⌝V ]t) $$ zero)
                      , ( coe ($F= ⟦ Γ ⟧C (π₁β ⁻¹)) (⟦ Γ ⟧C $F wkV idV $ α)
                        , coe ($F= ⟦ A ⟧T (NBE.LogPred.Ty.Pi.projTbase ⟦ Γ ⟧C)) a'''))
        b = map {Ψ , A [ ρ ]T}(wkV idV) a'''

        abstract
          lcoe6a : (coe (TmΓ= pcoe2) (s [ ⌜ wkV {A = A [ ρ ]T} idV ⌝V ]t)) $$ zero
                 ≃ (coe (TmΓ= Π[]) (coe (TmΓ= Π[]) s [ ⌜ wkV {A = A [ ρ ]T} idV ⌝V ]t)) $$ coe (TmΓ= ([][]T ⁻¹)) zero
          lcoe6a = []t≃' refl
                         (,C≃ refl (to≃ ([][]T ⁻¹)))
                         ([]T≃ (,C≃ refl (to≃ ([][]T ⁻¹))) ∘^ ◾̃ to≃ ([][]T ⁻¹))
                         (app≃ refl
                               (to≃ ([][]T ⁻¹))
                               ([]T≃ (,C≃ refl (to≃ ([][]T ⁻¹))) ∘^ ◾̃ to≃ ([][]T ⁻¹))
                               (uncoe (TmΓ= pcoe2) ⁻¹̃ ◾̃ coe[]t' Π[] ⁻¹̃ ◾̃ uncoe (TmΓ= Π[])))
                         (,s≃ refl refl r̃ (to≃ ([][]T ⁻¹)) (uncoe (TmΓ= ([id]T ⁻¹)) ⁻¹̃ ◾̃ uncoe (TmΓ= ([][]T ⁻¹)) ◾̃ uncoe (TmΓ= ([id]T ⁻¹))))

        abstract
          lcoe6b : (coe (TmΓ= Π[]) (coe (TmΓ= Π[]) s [ ⌜ wkV {A = A [ ρ ]T} idV ⌝V ]t)) $$ coe (TmΓ= ([][]T ⁻¹)) zero
                 ≃ app (coe (TmΓ= Π[]) s) [ ⌜ wkV {A = A [ ρ ]T} idV ⌝V ^ A [ ρ ]T ]t [ < coe (TmΓ= ([][]T ⁻¹)) zero > ]t
          lcoe6b = []t≃' refl
                         refl
                         r̃
                         (to≃ app[])
                         r̃

        abstract
          lcoe6c : (⌜ wkV idV ⌝V ^ A [ ρ ]T) ∘ < coe (TmΓ= ([][]T ⁻¹)) zero > ≃ id {Ψ , A [ ρ ]T}
          lcoe6c = to≃ (^∘<> refl) ◾̃ ,s≃ refl refl (to≃ (⌜wkid⌝ ⁻¹)) r̃ (uncoe (TmΓ= ([][]T ⁻¹)) ⁻¹̃ ◾̃ uncoe (TmΓ= lcoe4) ⁻¹̃) ◾̃ to≃ πη

        abstract
          lcoe6 : _≡_ {A = (TMC (Γ , A) ,P TMT B ,P ⟦ Γ , A ⟧C [ wkn ]F) $P (Ψ , A [ ρ ]T)}
                      ( (ρ ∘ ⌜ wkV idV ⌝V , zero)
                      , coe (TmΓ= pcoe1) (coe (TmΓ= pcoe2) (s [ ⌜ wkV idV ⌝V ]t) $$ zero)
                      , ( coe ($F= ⟦ Γ ⟧C (π₁β ⁻¹)) (⟦ Γ ⟧C $F wkV idV $ α)
                        , coe ($F= ⟦ A ⟧T (NBE.LogPred.Ty.Pi.projTbase ⟦ Γ ⟧C)) a'''))
                      (ρ ^ A , app (coe (TmΓ= Π[]) s) , α',a)
          lcoe6 = Tbase= ⟦ Γ , A ⟧C (,s≃' (ap (_∘_ ρ) (⌜wkid⌝ ⁻¹)) (uncoe (TmΓ= lcoe4) ⁻¹̃ ◾̃ uncoe (TmΓ= [][]T)))
                                    (uncoe (TmΓ= pcoe1) ⁻¹̃ ◾̃ lcoe6a ◾̃ lcoe6b ◾̃ [][]t' ◾̃ []t≃ refl lcoe6c ◾̃ [id]t')
                                    (,≃ (funext≃ ($F= ⟦ Γ ⟧C (π₁β ◾ ap (_∘_ ρ) (⌜wkid⌝ ⁻¹) ◾ π₁β ⁻¹))
                                                 (λ x₂ → to≃ ($F= ⟦ A ⟧T (Tbase= ⟦ Γ ⟧C (π₁β ◾ ap (_∘_ ρ) (⌜wkid⌝ ⁻¹) ◾ π₁β ⁻¹)
                                                                                        (π₂β' ◾̃ uncoe (TmΓ= lcoe4) ⁻¹̃ ◾̃ uncoe (TmΓ= [][]T) ◾̃ π₂β' ⁻¹̃)
                                                                                        x₂))))
                                        (uncoe ($F= ⟦ Γ ⟧C (π₁β ⁻¹)) ⁻¹̃ ◾̃ uncoe ($F= ⟦ Γ ⟧C lcoe1))
                                        ( uncoe ($F= ⟦ A ⟧T (NBE.LogPred.Ty.Pi.projTbase ⟦ Γ ⟧C)) ⁻¹̃
                                        ◾̃ uncoe ($F= ⟦ A ⟧T lcoe5) ⁻¹̃))

        b' : ⟦ B ⟧T $F (ρ ^ A , app (coe (TmΓ= Π[]) s) , α',a)
        b' = coe ($F= ⟦ B ⟧T lcoe6) b

        p : ≡NFT B $F (ρ ^ A , app (coe (TmΓ= Π[]) s))
        p = qT Bᴹ $S (ρ ^ A , app (coe (TmΓ= Π[]) s) , α',a , b')

        abstract
          eq : s ≡ ⌜ coe (NfΓ= (Π[] ⁻¹)) (lamNf (proj₁ p)) ⌝nf
          eq = from≃ ( uncoe (TmΓ= Π[])
                     ◾̃ to≃ (Πη ⁻¹)
                     ◾̃ to≃ (ap lam (proj₂ p))
                     ◾̃ ⌜coe⌝nf (Π[] ⁻¹) ⁻¹̃)

        ret : ≡NFT (Π A B) $F (ρ , s)
        ret = coe (NfΓ= (Π[] ⁻¹)) (lamNf (proj₁ p)) , eq

    module uT$S
      {Ψ : Con}
      (ρ : TMC Γ $P Ψ)
      (n : NET (Π A B) $F ρ)
      (α : ⟦ Γ ⟧C $F ρ)
      where

        open NBE.LogPred.Ty.Pi.mΠᴹ ⟦ A ⟧T ⟦ B ⟧T

        module map
          {Ω : Con}
          (β : Vars Ω Ψ)
          {u : TMT A $F (ρ ∘ ⌜ β ⌝V)}
          (v : ⟦ A ⟧T $F (ρ ∘ ⌜ β ⌝V , u , ⟦ Γ ⟧C $F β $ α))
          where

            p : ≡NFT A $F (TMC Γ $P β $ ρ , u)
            p = qT Aᴹ $S (TMC Γ $P β $ ρ , u , ⟦ Γ ⟧C $F β $ α , v)

            abstract
              lcoe1 : B [ ρ ∘ ⌜ β ⌝V ^ A ]T [ < ⌜ proj₁ p ⌝nf > ]T
                    ≡ B [ TMC Γ $P β $ ρ , u ]T
              lcoe1 = [][]T ◾ ap (_[_]T B) (^∘<> refl ◾ ,s≃' refl (to≃ (proj₂ p ⁻¹)))

            abstract
              lcoe2 : _≡_ {A = (TMC Γ ,P TMT A ,P ⟦ Γ ⟧C [ wkn ]F) $P Ω}
                          (ρ ∘ ⌜ β ⌝V , u , ⟦ Γ ⟧C $F β $ α)
                          ( π₁ (TMC Γ $P β $ ρ , u)
                          , π₂ (TMC Γ $P β $ ρ , u)
                          , coe ($F= ⟦ Γ ⟧C (π₁β ⁻¹)) (⟦ Γ ⟧C $F β $ α))
              lcoe2 = Tbase= ⟦ Γ ⟧C (π₁β ⁻¹) (π₂β' ⁻¹̃) (uncoe ($F= ⟦ Γ ⟧C (π₁β ⁻¹)))

            b : ⟦ B ⟧T $F ( (TMC Γ $P β $ ρ , u)
                          , ⌜ coe (NeΓ= lcoe1) (appNe (coe (NeΓ= ([][]T ◾ Π[])) (n [ β ]ne)) (proj₁ p)) ⌝ne
                          , ((coe ($F= ⟦ Γ ⟧C (π₁β ⁻¹)) (⟦ Γ ⟧C $F β $ α)) , coe ($F= ⟦ A ⟧T lcoe2) v))
            b = uT Bᴹ $S ( (TMC Γ $P β $ ρ , u)
                         , coe (NeΓ= lcoe1)
                               (appNe (coe (NeΓ= ([][]T ◾ Π[])) (n [ β ]ne)) (proj₁ p))
                         , ((coe ($F= ⟦ Γ ⟧C (π₁β ⁻¹)) (⟦ Γ ⟧C $F β $ α)) , coe ($F= ⟦ A ⟧T lcoe2) v))

            abstract
              lcoe3 : _≡_ {A = (TMC (Γ , A) ,P TMT B ,P ⟦ Γ , A ⟧C [ wkn ]F) $P Ω}
                          ( (TMC Γ $P β $ ρ , u)
                          , ⌜ coe (NeΓ= lcoe1) (appNe (coe (NeΓ= ([][]T ◾ Π[])) (n [ β ]ne)) (proj₁ p)) ⌝ne
                          , (coe ($F= ⟦ Γ ⟧C (π₁β ⁻¹)) (⟦ Γ ⟧C $F β $ α) , coe ($F= ⟦ A ⟧T lcoe2) v))
                          ( (ρ ∘ ⌜ β ⌝V , u)
                          , coe (TmΓ= pcoe1) (coe (TmΓ= pcoe2) (⌜ n ⌝ne [ ⌜ β ⌝V ]t) $$ u)
                          , (coe ($F= ⟦ Γ ⟧C (π₁β ⁻¹)) (⟦ Γ ⟧C $F β $ α) , coe ($F= ⟦ A ⟧T (NBE.LogPred.Ty.Pi.projTbase ⟦ Γ ⟧C)) v))
              lcoe3 = Tbase= ⟦ Γ , A ⟧C
                             refl
                             ( ⌜coe⌝ne lcoe1
                             ◾̃ $≃ (from≃ (⌜coe⌝ne ([][]T ◾ Π[]) ◾̃ to≃ (⌜[]ne⌝ {n = n} ⁻¹) ◾̃ uncoe (TmΓ= pcoe2))) (proj₂ p ⁻¹)
                             ◾̃ uncoe (TmΓ= pcoe1))
                             (,≃ r̃
                                 r̃
                                 (uncoe ($F= ⟦ A ⟧T lcoe2) ⁻¹̃ ◾̃ uncoe ($F= ⟦ A ⟧T (NBE.LogPred.Ty.Pi.projTbase ⟦ Γ ⟧C))))

            ret : ⟦ B ⟧T $F ( (ρ ∘ ⌜ β ⌝V , u)
                            , coe (TmΓ= pcoe1)
                                  (coe (TmΓ= pcoe2)
                                       (⌜ n ⌝ne [ ⌜ β ⌝V ]t) $$ u)
                            , ( coe ($F= ⟦ Γ ⟧C (π₁β ⁻¹))
                                    (⟦ Γ ⟧C $F β $ α)
                              , coe ($F= ⟦ A ⟧T (NBE.LogPred.Ty.Pi.projTbase ⟦ Γ ⟧C)) v))
            ret = coe ($F= ⟦ B ⟧T lcoe3) b

        ret : ⟦ Π A B ⟧T $F (ρ , ⌜ n ⌝ne , α)
        ret = record
          { map = map.ret
          ; nat = cheat
          }

    Πᴹ : Tyᴹ Γᴹ (Π A B)
    Πᴹ = record
      { qT = record
          { _$S_ = λ { (ρ , s , α , a) → qT$S.ret ρ s α a }
          ; natS = cheat
          }
      ; uT = record
          { _$S_ = λ { (ρ , n , α) → uT$S.ret ρ n α }
          ; natS = cheat
          }
      }

mTy : MethodsTy M mCon
mTy = record
  { _[_]Tᴹ = m[]Tᴹ._[_]Tᴹ
  ; Uᴹ     = mUᴹ.Uᴹ
  ; Elᴹ    = mElᴹ.Elᴹ
  ; Πᴹ     = mΠᴹ.Πᴹ }
