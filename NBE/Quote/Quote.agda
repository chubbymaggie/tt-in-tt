module NBE.Quote.Quote where

open import lib
open import TT.Elim

open import NBE.Quote.Motives
open import NBE.Quote.Cxt
open import NBE.Quote.Ty
open import NBE.Quote.HTy

mHTms : MethodsHTms M mCon mTy mTms mTm mHTy
mHTms = record { idlᴹ = refl ; idrᴹ = refl ; assᴹ = refl ; π₁βᴹ = refl ; πηᴹ = refl ; εηᴹ = refl ; ,∘ᴹ = refl }

mHTm : MethodsHTm M mCon mTy mTms mTm mHTy mHTms
mHTm = record { π₂βᴹ = refl ; lam[]ᴹ = refl ; Πβᴹ = refl ; Πηᴹ = refl }

open elim M mCon mTy mTms mTm mHTy mHTms mHTm

QC = Con-elim
QT = Ty-elim
