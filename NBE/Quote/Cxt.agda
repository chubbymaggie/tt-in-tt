module NBE.Quote.Cxt where

open import lib
open import JM.JM
open import Cats
open import TT.Syntax
open import TT.Elim
open import TT.Congr
open import TT.Laws
open import NBE.TM
open import NBE.Nf
open import NBE.Nfs
open import NBE.LogPred.Motives using (Tbase=)
open import NBE.Quote.Motives
open import NBE.LogPred.LogPred U̅ E̅l

open Motives M

open import NBE.Cheat

•ᴹ : Conᴹ •
•ᴹ = record
  { qC = record { _$S_ = λ _ → ε , εη ; natS = cheat }
  ; uC = record { _$S_ = λ _ → tt ; natS = refl }
  }

module m,Cᴹ
  {Δ : Con}(Δᴹ : Conᴹ Δ)
  {A : Ty Δ}(Aᴹ : Tyᴹ Δᴹ A)
  where

    _,Cᴹ_ : Conᴹ (Δ , A)
    _,Cᴹ_ = record
    
      { qC = record
      
          { _$S_ = λ { (ρ , α) → let
                                   (τ , p)  = qC Δᴹ $S (π₁ ρ , proj₁ α)
                                   (n , p') = qT Aᴹ $S (π₁ ρ , π₂ ρ , proj₁ α , proj₂ α)
                                 in   (τ ,nfs coe (NfΓ= (ap (_[_]T A) p)) n)
                                    , (πη ⁻¹ ◾ ,s≃' p (to≃ p' ◾̃ ⌜coe⌝nf (ap (_[_]T A) p) ⁻¹̃))
                   }
          
          ; natS = cheat
          
          }
      
      ; uC = record
      
          { _$S_ = λ { (τ ,nes n) → let
                                      α = uC Δᴹ $S τ
                                    in
                                        coe ($F= ⟦ Δ ⟧C (π₁β ⁻¹)) α
                                      , coe ($F= ⟦ A ⟧T (Tbase= ⟦ Δ ⟧C (π₁β ⁻¹) (π₂β' ⁻¹̃) (uncoe ($F= ⟦ Δ ⟧C (π₁β ⁻¹)))))
                                            (uT Aᴹ $S (⌜ τ ⌝nes , n , α)) }
          
          ; natS = cheat
          
          }
      }

mCon : MethodsCon M
mCon = record { •ᴹ = •ᴹ ; _,Cᴹ_ = m,Cᴹ._,Cᴹ_ }
