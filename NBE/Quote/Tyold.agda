module NBE.Quote.Ty where

open import lib
open import JM.JM
open import Cats
open import TT.Syntax renaming (_$_ to _$$_)
open import TT.Elim
open import TT.Congr
open import TT.Laws
open import NBE.TM
open import NBE.Nf
open import NBE.NF
open import NBE.Renamings
open import NBE.LogPred.Motives using (T$F=)
open import NBE.LogPred.Cxt using (cheat)
open import NBE.LogPred.LogPred
open import NBE.Quote.Motives
open import NBE.Quote.Cxt
import NBE.LogPred.Ty

proj₁coe⟦⟧ : ∀{Γ}{A : Ty Γ}{Δ : Con}{ρ₀ ρ₁ : TMC (Γ , A) $P Δ}(ρ₂ : ρ₀ ≡ ρ₁)
             {α : ⟦ Γ , A ⟧C $F ρ₀}
           → proj₁ (coe (ap (_$F_ ⟦ Γ , A ⟧C) ρ₂) α) ≃ proj₁ α
proj₁coe⟦⟧ refl = refl , refl

proj₂coe⟦⟧ : ∀{Γ}{A : Ty Γ}{Δ : Con}{ρ₀ ρ₁ : TMC (Γ , A) $P Δ}(ρ₂ : ρ₀ ≡ ρ₁)
             {α : ⟦ Γ , A ⟧C $F ρ₀}
           → proj₂ (coe (ap (_$F_ ⟦ Γ , A ⟧C) ρ₂) α) ≃ proj₂ α
proj₂coe⟦⟧ refl = refl , refl

Πᴹ : {Γ : Con}{Γᴹ : Motives.Conᴹ M Γ}
     {A : Ty Γ}(Aᴹ : Motives.Tyᴹ M Γᴹ A)
     {B : Ty (Γ , A)}(Bᴹ : Motives.Tyᴹ M ((mCon MethodsCon.,Cᴹ Γᴹ) Aᴹ) B)
   → Motives.Tyᴹ M Γᴹ (Π A B)
Πᴹ {Γ}{QΓ , UΓ}{A}(QA , UA){B}(QB , UB) = QΠAB , UΠAB

  where

    QΠAB : {Δ : Con} {ρ : TMC Γ $P Δ} (α : ⟦ Γ ⟧C $F ρ)
           (t : TMT (Π A B) $F ρ)
         → ⟦ Π A B ⟧T $F (ρ , t , α)
         → Σ (NFT (Π A B) $F ρ) (λ t' → t ≡ ⌜ t' ⌝nf)
    QΠAB {Δ}{ρ} α t f

      = coe (NfΓ= (Π[] ⁻¹)) (lam (proj₁ q)) , pq
    
      where

        abstract
          l1 : A [ ρ ]T [ π₁ {Δ , A [ ρ ]T}{Δ}{A [ ρ ]T} id ]T
              ≡ A [ TMC Γ $P wkV idV $ ρ ]T
          l1 = [][]T ◾ ap (λ z → A [ ρ ∘ z ]T) ⌜wkid⌝
        
        a : ⟦ A ⟧T $F ( TMC Γ $P wkV idV $ ρ
                      , ⌜ coe (NeΓ= l1) (var vze) ⌝ne
                      , ⟦ Γ ⟧C $F wkV idV $ α)
        a = UA (⟦ Γ ⟧C $F wkV idV $ α) (coe (NeΓ= l1) (var vze))

        abstract
          l2 : ⟦ Γ ⟧C $F (TMC Γ $P wkV {Δ}{Δ}{A [ ρ ]T} idV $ ρ)
             ≡ ⟦ Γ ⟧C $F π₁ (TMC Γ $P wkV idV $ ρ , ⌜ coe (NeΓ= l1) (var vze) ⌝ne)
          l2 = ap (_$F_ ⟦ Γ ⟧C) π₁β ⁻¹

        abstract
          l3 : ⟦ A ⟧T $F ( TMC Γ $P wkV idV $ ρ
                         , ⌜ coe (NeΓ= l1) (var vze) ⌝ne
                         , ⟦ Γ ⟧C $F wkV idV $ α)
             ≡ ⟦ A ⟧T $F ( π₁ (TMC Γ $P wkV idV $ ρ , ⌜ coe (NeΓ= l1) (var vze) ⌝ne)
                         , π₂ (TMC Γ $P wkV idV $ ρ , ⌜ coe (NeΓ= l1) (var vze) ⌝ne)
                         , coe l2 (⟦ Γ ⟧C $F wkV idV $ α))
          l3 = T$F= ⟦ A ⟧T (π₁β ⁻¹) (π₂β' ⁻¹̃) (uncoe (ap (_$F_ ⟦ Γ ⟧C) π₁β ⁻¹))

        α' : ⟦ Γ , A ⟧C $F (TMC Γ $P wkV idV $ ρ , ⌜ coe (NeΓ= l1) (var vze) ⌝ne)
        α' = coe l2 (⟦ Γ ⟧C $F wkV idV $ α) , coe l3 a

        abstract
          l4 : TMC Γ $P wkV idV $ ρ , ⌜ coe (NeΓ= l1) (var vze) ⌝ne
             ≡ ρ ^ A
          l4 = ,s≃' (ap (_∘_ ρ) (⌜wkid⌝ ⁻¹))
                    (⌜coe⌝ne l1 ◾̃ uncoe (TmΓ= [][]T))

        α'' : ⟦ Γ , A ⟧C $F (ρ ^ A)
        α'' = coe (ap (_$F_ ⟦ Γ , A ⟧C) l4) α'

--  b' : ⟦ B ⟧T $F (ρ ^ A , app (coe (TmΓ= Π[]) t) , α'')

        b' : ⟦ B ⟧T $F ( TMC Γ $P wkV idV $ ρ , ⌜ coe (NeΓ= l1) (var vze) ⌝ne
                       , coe (TmΓ= (NBE.LogPred.Ty.Πᴹ.m$F.l1 ⟦ A ⟧T ⟦ B ⟧T (ρ , t , α)))
                              (coe (TmΓ= Π[])
                                   (TMT (Π A B) $F wkV idV $ t)
                               $$ ⌜ coe (NeΓ= l1) (var vze) ⌝ne)
                       , (( coe (ap (_$F_ ⟦ Γ ⟧C) (π₁β ⁻¹)) (⟦ Γ ⟧C $F wkV idV $ α)
                          , coe (NBE.LogPred.Ty.Πᴹ.m$F.l2 ⟦ A ⟧T ⟦ B ⟧T (ρ , t , α)) a)))
        b' = f {Δ , A [ ρ ]T}(wkV idV) a

        abstract
          l5 : _≃_ {A = Tm (Δ , A [ ρ ]T) (B [ TMC Γ $P wkV idV $ ρ ^ A ]T [ < ⌜ coe (NeΓ= l1) (var vze) ⌝ne > ]T)}
                   {B = Tm (Δ , A [ ρ ]T) (B [ ρ ^ A ]T)}
                   (coe (TmΓ= Π[]) (TMT (Π A B) $F wkV idV $ t) $$ ⌜ coe (NeΓ= l1) (var vze) ⌝ne)
                   (app {Δ}{A [ ρ ]T}{B [ ρ ^ A ]T} (coe (TmΓ= Π[]) t))
          l5 = {!!}

        abstract
          l6 : ⟦ B ⟧T $F ( TMC Γ $P wkV idV $ ρ , ⌜ coe (NeΓ= l1) (var vze) ⌝ne
                       , coe (TmΓ= (NBE.LogPred.Ty.Πᴹ.m$F.l1 ⟦ A ⟧T ⟦ B ⟧T (ρ , t , α)))
                              (coe (TmΓ= Π[])
                                   (TMT (Π A B) $F wkV idV $ t)
                               $$ ⌜ coe (NeΓ= l1) (var vze) ⌝ne)
                       , (( coe (ap (_$F_ ⟦ Γ ⟧C) (π₁β ⁻¹)) (⟦ Γ ⟧C $F wkV idV $ α)
                          , coe (NBE.LogPred.Ty.Πᴹ.m$F.l2 ⟦ A ⟧T ⟦ B ⟧T (ρ , t , α)) a)))
             ≡ ⟦ B ⟧T $F (ρ ^ A , app (coe (TmΓ= Π[]) t) , α'')
          l6 = T$F= ⟦ B ⟧T
                    (,s≃' (ap (_∘_ ρ) (⌜wkid⌝ ⁻¹))
                          (⌜coe⌝ne l1 ◾̃ uncoe (TmΓ= [][]T)))
                    
                    ( uncoe (TmΓ= (NBE.LogPred.Ty.Πᴹ.m$F.l1 ⟦ A ⟧T ⟦ B ⟧T (ρ , t , α))) ⁻¹̃
                    ◾̃ l5)
                    (,≃ (funext≃ (ap (_$F_ ⟦ Γ ⟧C)
                                     (π₁β ◾ ap (_∘_ ρ) (⌜wkid⌝ ⁻¹) ◾ π₁β ⁻¹))
                                 (λ x₂ → to≃ (T$F= ⟦ A ⟧T ((π₁β ◾ ap (_∘_ ρ) (⌜wkid⌝ ⁻¹) ◾ π₁β ⁻¹))
                                                          (π₂β' ◾̃ ⌜coe⌝ne l1 ◾̃ uncoe (TmΓ= [][]T) ◾̃ π₂β' ⁻¹̃)
                                                          x₂)))
                        ( uncoe (ap (_$F_ ⟦ Γ ⟧C) (π₁β ⁻¹)) ⁻¹̃
                        ◾̃ uncoe l2
                        ◾̃ proj₁coe⟦⟧ l4 ⁻¹̃)
                        ( uncoe (NBE.LogPred.Ty.Πᴹ.m$F.l2 ⟦ A ⟧T ⟦ B ⟧T (ρ , t , α)) ⁻¹̃
                        ◾̃ uncoe l3
                        ◾̃ proj₂coe⟦⟧ l4 ⁻¹̃))
{-
app (coe (TmΓ= Π[]) (coe (TmΓ= [][]T) (t [ ⌜ wkV idV ⌝V ]t)))
[ id , coe (TmΓ= ([id]T ⁻¹)) ⌜ coe (NeΓ= ([][]T ◾ ap (λ z → A [ ρ ∘ z ]T) ⌜wkid⌝)) (var vze) ⌝ne
]t

app (coe (TmΓ= Π[]) t)

        → app (coe (TmΓ= Π[]) (t [ δ ]t)) ≡ (app t) [ δ ^ A ]t
-}


        q : Σ (Nf (Δ , A [ ρ ]T) (B [ ρ ^ A ]T)) λ t'
          → app (coe (TmΓ= Π[]) t) ≡ ⌜ t' ⌝nf
        q = QB {Δ , A [ ρ ]T}
               {ρ ^ A}
               {!!}
               (app (coe (TmΓ= Π[]) t))
               {!!}

        abstract
          pq : t ≡ ⌜ coe (NfΓ= (Π[] ⁻¹)) (lam (proj₁ q)) ⌝nf
          pq = from≃ (uncoe (TmΓ= Π[])
                ◾̃ to≃ (Πη ⁻¹)
                ◾̃ to≃ (ap lam (proj₂ q))
                ◾̃ ⌜coe⌝nf (Π[] ⁻¹) ⁻¹̃)

{-

λ { {Γ}{QΓ , UΓ}{A}(QA , UA){B}(QB , UB)
  
         → (λ {Δ}{ρ} α t f

            → let
{-
                a : ⟦ A ⟧T $F ( ρ ∘ ⌜ wkV idV ⌝V
                              , ⌜ coe (NeΓ= ([][]T ◾ ap (λ z → A [ ρ ∘ z ]T) ⌜wkid⌝)) (var vze) ⌝ne
                              , ⟦ Γ ⟧C $F wkV idV $ α)
                a = UA (⟦ Γ ⟧C $F wkV idV $ α)
                       (coe (NeΓ= ([][]T ◾ ap (λ z → A [ ρ ∘ z ]T) ⌜wkid⌝ ))
                            (var vze))

                α' : ⟦ Γ , A ⟧C $F (ρ ^ A)
                α' = coe (ap (_$F_ ⟦ Γ ⟧C) (ap (_∘_ ρ) (⌜wkid⌝ ⁻¹) ◾ π₁β ⁻¹)) (⟦ Γ ⟧C $F wkV idV $ α)
                   , coe (T$F= ⟦ A ⟧T
                               (ap (_∘_ ρ) (⌜wkid⌝ ⁻¹) ◾ π₁β ⁻¹)
                               ( ⌜coe⌝ne ([][]T ◾ ap (λ z → A [ ρ ∘ z ]T) ⌜wkid⌝)
                               ◾̃ uncoe (TmΓ= [][]T)
                               ◾̃ π₂β' ⁻¹̃)
                               (uncoe (ap (_$F_ ⟦ Γ ⟧C) (ap (_∘_ ρ) (⌜wkid⌝ ⁻¹) ◾ π₁β ⁻¹))))
                         a
-}

                a : ⟦ A ⟧T $F ( ρ ∘ wk
                              , coe (TmΓ= [][]T)
                                    (π₂ id)
                              , coe (ap (_$F_ ⟦ Γ ⟧C) (ap (_∘_ ρ) (⌜wkid⌝ ⁻¹)))
                                    (⟦ Γ ⟧C $F wkV idV $ α))
                a = coe (T$F= ⟦ A ⟧T
                              (ap (_∘_ ρ) (⌜wkid⌝ ⁻¹))
                              ( ⌜coe⌝ne ([][]T ◾ ap (λ z → A [ ρ ∘ z ]T) ⌜wkid⌝)
                              ◾̃ uncoe (TmΓ= [][]T))
                              (uncoe (ap (_$F_ ⟦ Γ ⟧C) (ap (_∘_ ρ) (⌜wkid⌝ ⁻¹)))))
                        (UA (⟦ Γ ⟧C $F wkV idV $ α)
                            (coe (NeΓ= ([][]T ◾ ap (λ z → A [ ρ ∘ z ]T) ⌜wkid⌝ ))
                            (var vze)))

                α' : ⟦ Γ , A ⟧C $F (ρ ^ A)
                α' = coe (ap (_$F_ ⟦ Γ ⟧C) {!!})
                         (⟦ Γ ⟧C $F wkV idV $ α)
                   , {!!}

                b : Σ (Nf (Δ , A [ ρ ]T) (B [ ρ ^ A ]T)) λ t' → app (coe (TmΓ= Π[]) t) ≡ ⌜ t' ⌝nf
                b = QB {Δ , A [ ρ ]T}
                       {ρ ^ A}
                       α'
                       (app (coe (TmΓ= Π[]) t))
                       {!!}
{-
                    QB {Δ , A [ ρ ]T}
                       {ρ ^ A}
                       α'
                       (app (coe (TmΓ= Π[]) t))
                       (coe ? (f {Δ , A [ ρ ]T} (wkV idV) a))
-}
              in

                  coe (NfΓ= (Π[] ⁻¹)) (lam (proj₁ b))
                , from≃ (uncoe (TmΓ= Π[])
                        ◾̃ to≃ (Πη ⁻¹)
                        ◾̃ to≃ (ap lam (proj₂ b))
                        ◾̃ ⌜coe⌝nf (Π[] ⁻¹) ⁻¹̃)
           )
-}

    UΠAB : {Δ : Con} {ρ : TMC Γ $P Δ} (α : ⟦ Γ ⟧C $F ρ)
           (n : NET (Π A B) $F ρ)
         → ⟦ Π A B ⟧T $F (ρ , ⌜ n ⌝ne , α)
    UΠAB {Δ}{ρ} α n {Θ} β {u} v = {!!}
{-
      = coe (T$F= ⟦ B ⟧T
                  refl
                  ( ⌜coe⌝ne ([][]T ◾ ap (_[_]T B) (^∘<> refl ◾ ap (_,s_ (ρ ∘ ⌜ β ⌝V)) (proj₂ (QA (⟦ Γ ⟧C $F β $ α) u v) ⁻¹)))
                  ◾̃ $≃ (from≃ ( ⌜coe⌝ne Π[]
                              ◾̃ ⌜coe⌝ne ( [][]T)
                              ◾̃ to≃ (⌜[]ne⌝ {n = n} ⁻¹)
                              ◾̃ uncoe (ap (Tm Θ) [][]T)
                              ◾̃ uncoe (TmΓ= Π[])))
                       (proj₂ a ⁻¹)
                  ◾̃ uncoe (TmΓ= ([][]T ◾ ap (_[_]T B) (^∘<> refl))))
                  r̃)
            (UB {Θ}{ρ ∘ ⌜ β ⌝V , u} α' b)
            
      where
      
        a : Σ (NFT A $F (ρ ∘ ⌜ β ⌝V)) λ u' → u ≡ ⌜ u' ⌝nf
        a = QA (⟦ Γ ⟧C $F β $ α) u v

        α' : ⟦ Γ , A ⟧C $F (ρ ∘ ⌜ β ⌝V , u)
        α' = (coe (ap (_$F_ ⟦ Γ ⟧C) (π₁β ⁻¹)) (⟦ Γ ⟧C $F β $ α))

           , coe (T$F= ⟦ A ⟧T (π₁β ⁻¹)
                              (π₂β' ⁻¹̃)
                              (uncoe (ap (_$F_ ⟦ Γ ⟧C) (π₁β ⁻¹)))) v

        b : Ne Θ (B [ ρ ∘ ⌜ β ⌝V , u ]T)
        b = coe (NeΓ= ( [][]T ◾ ap (_[_]T B) ( ^∘<> refl ◾ ap (_,s_ (ρ ∘ ⌜ β ⌝V)) (proj₂ a ⁻¹))))
                (app (coe (NeΓ= Π[]) (NET (Π A B) $F β $ n)) (proj₁ a))

-}

{-
mTy : MethodsTy M mCon
mTy = record

  { _[_]Tᴹ = λ { {Γ}{QΓ , UΓ}{Θ}{QΘ , UΘ}{A}(QA , UA){σ} _

             → (λ {Δ}{ρ} α t a → coe (Σ≃ (NfΓ= ([][]T ⁻¹))
                                         (funext≃ (NfΓ= ([][]T ⁻¹))
                                                  (λ x₂ → ≡≃ (TmΓ= ([][]T ⁻¹))
                                                             (uncoe (TmΓ= [][]T) ⁻¹̃)
                                                             (ap≃ {B = Nf Δ} ⌜_⌝nf ([][]T ⁻¹) x₂))))
                                     (QA (⟦ σ ⟧s $S (ρ , α)) (coe (TmΓ= [][]T) t) a))

             , (λ {Δ}{ρ} α n → coe (T$F= ⟦ A ⟧T refl (⌜coe⌝ne [][]T ◾̃ uncoe (TmΓ= [][]T)) r̃)
                                   (UA (⟦ σ ⟧s $S (ρ , α)) (coe (NeΓ= [][]T) n)))

             }

  ; Uᴹ = λ { {Γ}{QΓ , UΓ}
  
         → (λ {Δ}{ρ} α Â p

            → (coe (NfΓ= (U[] ⁻¹)) (neuU (coe (NeΓ= U[]) (proj₁ p))))

            , (proj₂ p ◾ from≃ (⌜coe⌝ne U[] ⁻¹̃ ◾̃ ⌜coe⌝nf (U[] ⁻¹) ⁻¹̃)))

         , (λ {Δ}{ρ} α n → n , refl) }
  
  ; Elᴹ = λ { {Γ}{QΓ , UΓ}{Â} _

          → (λ {Δ}{ρ} α t p

             → coe (NfΓ= ( ap El (from≃ ( uncoe (TmΓ= U[]) ⁻¹̃
                                        ◾̃ to≃ (proj₂ (⟦ Â ⟧t $S (ρ , α)) ⁻¹)
                                        ◾̃ uncoe (TmΓ= U[])))
                         ◾ El[] ⁻¹))
                   (neuEl (proj₁ p))

             , from≃ (uncoe (TmΓ= (El[] ◾ ap (λ z → El (coe (TmΓ= U[]) z)) (proj₂ (⟦ Â ⟧t $S (ρ , α)))))
                     ◾̃ to≃ (proj₂ p)
                     ◾̃ ⌜coe⌝nf (ap El (from≃ (uncoe (TmΓ= U[]) ⁻¹̃ ◾̃ to≃ (proj₂ (⟦ Â ⟧t $S (ρ , α)) ⁻¹) ◾̃ uncoe (TmΓ= U[]))) ◾ El[] ⁻¹) ⁻¹̃))

          , (λ {Δ}{ρ} α n

            → coe (NeΓ= (El[] ◾ ap El (from≃ ( uncoe (TmΓ= U[]) ⁻¹̃
                                             ◾̃ to≃ (proj₂ (⟦ Â ⟧t $S (ρ , α)) ⁻¹)
                                             ◾̃ uncoe (TmΓ= U[])) ⁻¹)))
                  n

            , from≃ ( uncoe (TmΓ= (El[] ◾ ap (λ z → El (coe (TmΓ= U[]) z)) (proj₂ (⟦ Â ⟧t $S (ρ , α))))) ⁻¹̃
                    ◾̃ ⌜coe⌝ne (El[] ◾ ap El (from≃ (uncoe (TmΓ= U[]) ⁻¹̃ ◾̃ to≃ (proj₂ (⟦ Â ⟧t $S (ρ , α)) ⁻¹) ◾̃ uncoe (TmΓ= U[])) ⁻¹)) ⁻¹̃))

          }
  
  ; Πᴹ = λ {_}{Γᴹ} → Πᴹ {_}{Γᴹ}

{-

λ { {Γ}{QΓ , UΓ}{A}(QA , UA){B}(QB , UB)
  
         → (λ {Δ}{ρ} α t f

            → let
{-
                a : ⟦ A ⟧T $F ( ρ ∘ ⌜ wkV idV ⌝V
                              , ⌜ coe (NeΓ= ([][]T ◾ ap (λ z → A [ ρ ∘ z ]T) ⌜wkid⌝)) (var vze) ⌝ne
                              , ⟦ Γ ⟧C $F wkV idV $ α)
                a = UA (⟦ Γ ⟧C $F wkV idV $ α)
                       (coe (NeΓ= ([][]T ◾ ap (λ z → A [ ρ ∘ z ]T) ⌜wkid⌝ ))
                            (var vze))

                α' : ⟦ Γ , A ⟧C $F (ρ ^ A)
                α' = coe (ap (_$F_ ⟦ Γ ⟧C) (ap (_∘_ ρ) (⌜wkid⌝ ⁻¹) ◾ π₁β ⁻¹)) (⟦ Γ ⟧C $F wkV idV $ α)
                   , coe (T$F= ⟦ A ⟧T
                               (ap (_∘_ ρ) (⌜wkid⌝ ⁻¹) ◾ π₁β ⁻¹)
                               ( ⌜coe⌝ne ([][]T ◾ ap (λ z → A [ ρ ∘ z ]T) ⌜wkid⌝)
                               ◾̃ uncoe (TmΓ= [][]T)
                               ◾̃ π₂β' ⁻¹̃)
                               (uncoe (ap (_$F_ ⟦ Γ ⟧C) (ap (_∘_ ρ) (⌜wkid⌝ ⁻¹) ◾ π₁β ⁻¹))))
                         a
-}

                a : ⟦ A ⟧T $F ( ρ ∘ wk
                              , coe (TmΓ= [][]T)
                                    (π₂ id)
                              , coe (ap (_$F_ ⟦ Γ ⟧C) (ap (_∘_ ρ) (⌜wkid⌝ ⁻¹)))
                                    (⟦ Γ ⟧C $F wkV idV $ α))
                a = coe (T$F= ⟦ A ⟧T
                              (ap (_∘_ ρ) (⌜wkid⌝ ⁻¹))
                              ( ⌜coe⌝ne ([][]T ◾ ap (λ z → A [ ρ ∘ z ]T) ⌜wkid⌝)
                              ◾̃ uncoe (TmΓ= [][]T))
                              (uncoe (ap (_$F_ ⟦ Γ ⟧C) (ap (_∘_ ρ) (⌜wkid⌝ ⁻¹)))))
                        (UA (⟦ Γ ⟧C $F wkV idV $ α)
                            (coe (NeΓ= ([][]T ◾ ap (λ z → A [ ρ ∘ z ]T) ⌜wkid⌝ ))
                            (var vze)))

                α' : ⟦ Γ , A ⟧C $F (ρ ^ A)
                α' = coe (ap (_$F_ ⟦ Γ ⟧C) {!!})
                         (⟦ Γ ⟧C $F wkV idV $ α)
                   , {!!}

                b : Σ (Nf (Δ , A [ ρ ]T) (B [ ρ ^ A ]T)) λ t' → app (coe (TmΓ= Π[]) t) ≡ ⌜ t' ⌝nf
                b = QB {Δ , A [ ρ ]T}
                       {ρ ^ A}
                       α'
                       (app (coe (TmΓ= Π[]) t))
                       {!!}
{-
                    QB {Δ , A [ ρ ]T}
                       {ρ ^ A}
                       α'
                       (app (coe (TmΓ= Π[]) t))
                       (coe ? (f {Δ , A [ ρ ]T} (wkV idV) a))
-}
              in

                  coe (NfΓ= (Π[] ⁻¹)) (lam (proj₁ b))
                , from≃ (uncoe (TmΓ= Π[])
                        ◾̃ to≃ (Πη ⁻¹)
                        ◾̃ to≃ (ap lam (proj₂ b))
                        ◾̃ ⌜coe⌝nf (Π[] ⁻¹) ⁻¹̃)
           )

         , (λ {Δ}{ρ} α n {Θ} β {u} v
         
           → let
           
                a : Σ (NFT A $F (ρ ∘ ⌜ β ⌝V)) λ u' → u ≡ ⌜ u' ⌝nf
                a = QA (⟦ Γ ⟧C $F β $ α) u v

                α' : ⟦ Γ , A ⟧C $F (ρ ∘ ⌜ β ⌝V , u)
                α' = (coe (ap (_$F_ ⟦ Γ ⟧C) (π₁β ⁻¹)) (⟦ Γ ⟧C $F β $ α))

                   , coe (T$F= ⟦ A ⟧T (π₁β ⁻¹)
                                      (π₂β' ⁻¹̃)
                                      (uncoe (ap (_$F_ ⟦ Γ ⟧C) (π₁β ⁻¹)))) v

                b : Ne Θ (B [ ρ ∘ ⌜ β ⌝V , u ]T)
                b = coe (NeΓ= ( [][]T ◾ ap (_[_]T B) ( ^∘<> refl ◾ ap (_,s_ (ρ ∘ ⌜ β ⌝V)) (proj₂ a ⁻¹))))
                        (app (coe (NeΓ= Π[]) (NET (Π A B) $F β $ n)) (proj₁ a))
              in

                coe (T$F= ⟦ B ⟧T
                          refl
                          ( ⌜coe⌝ne ([][]T ◾ ap (_[_]T B) (^∘<> refl ◾ ap (_,s_ (ρ ∘ ⌜ β ⌝V)) (proj₂ (QA (⟦ Γ ⟧C $F β $ α) u v) ⁻¹)))
                          ◾̃ $≃ (from≃ ( ⌜coe⌝ne Π[]
                                      ◾̃ ⌜coe⌝ne ( [][]T)
                                      ◾̃ to≃ (⌜[]ne⌝ {n = n} ⁻¹)
                                      ◾̃ uncoe (ap (Tm Θ) [][]T)
                                      ◾̃ uncoe (TmΓ= Π[])))
                               (proj₂ a ⁻¹)
                          ◾̃ uncoe (TmΓ= ([][]T ◾ ap (_[_]T B) (^∘<> refl))))
                          r̃)
                    (UB {Θ}{ρ ∘ ⌜ β ⌝V , u} α' b))

         }
-}           
  }
-}
