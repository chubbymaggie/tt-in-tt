module NBE.TM where

open import lib
open import JM.JM
open import TT.Syntax
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import Cats

TMC : Con → PSh (REN ᵒᵖ)
TMC Γ = record
  { _$P_   = λ Δ → Tms Δ Γ
  ; _$P_$_ = λ β ρ → ρ ∘ ⌜ β ⌝V
  ; idP    = ap (_∘_ _) ⌜idV⌝ ◾ idr
  ; compP  = ap (_∘_ _) ⌜∘V⌝ ⁻¹ ◾ ass ⁻¹
  }

abstract
  TMTidF : ∀{Γ Δ}{A : Ty Γ}{α : TMC Γ $P Δ}{t : Tm Δ (A [ α ]T)}
         → coe (TmΓ= [][]T) (t [ ⌜ idV ⌝V ]t)
         ≡[ ap (λ z → Tm Δ (A [ z ]T)) (idP (TMC Γ)) ]≡
           t
  TMTidF {A = A} = from≃ ( uncoe (ap (λ z → Tm _ (A [ z ]T)) (idP (TMC _))) ⁻¹̃
                         ◾̃ uncoe (TmΓ= [][]T) ⁻¹̃
                         ◾̃ []t≃ refl (to≃ ⌜idV⌝)
                         ◾̃ from≡ (TmΓ= [id]T) [id]t)

abstract
  TMTcompF : ∀{Γ}{A : Ty Γ}{Δ Θ Ω : Con}{β : Vars Θ Δ}{γ : Vars Ω Θ}
             {α : TMC Γ $P Δ} {t : Tm Δ (A [ α ]T)}
           → coe (TmΓ= [][]T) (t [ ⌜ β ∘V γ ⌝V ]t)
           ≡[ ap (λ z → Tm Ω (A [ z ]T)) (compP (TMC Γ)) ]≡
             coe (TmΓ= [][]T) (coe (TmΓ= [][]T) (t [ ⌜ β ⌝V ]t) [ ⌜ γ ⌝V ]t)
  TMTcompF {A = A} = from≃ ( uncoe (ap (λ z → Tm _ (A [ z ]T)) (compP (TMC _))) ⁻¹̃
                           ◾̃ uncoe (TmΓ= [][]T) ⁻¹̃
                           ◾̃ []t≃ refl (to≃ (⌜∘V⌝ ⁻¹))
                           ◾̃ [][]t' ⁻¹̃
                           ◾̃ coe[]t' [][]T ⁻¹̃
                           ◾̃ uncoe (TmΓ= [][]T))

TMT : ∀{Γ} → Ty Γ → FamPSh (TMC Γ)
TMT A = record
  { _$F_   = λ {Δ} ρ → Tm Δ (A [ ρ ]T)
  ; _$F_$_ = λ {Δ}{Θ} β {ρ} t → coe (TmΓ= [][]T) (t [ ⌜ β ⌝V ]t)
  ; idF    = TMTidF
  ; compF  = TMTcompF
  }

TMs : ∀{Γ Δ} → Tms Δ Γ → TMC Δ →n TMC Γ
TMs ρ = record
  { _$n_ = _∘_ ρ
  ; natn = ass
  }

TMt : ∀{Γ A} → Tm Γ A → TMC Γ →S TMT A
TMt t = record
  { _$S_ = _[_]t t
  ; natS = [][]t
  }

abstract
  TMUidP : {I : Con} {α : Tm I U} → coe (TmΓ= U[]) (α [ ⌜ idV ⌝V ]t) ≡ α
  TMUidP = from≃ (uncoe (TmΓ= U[]) ⁻¹̃ ◾̃ [⌜idV⌝]t)

abstract
  TMUcompP : {I : Con} {J : Con} {K : Con} {f : Vars J I}
      {g : Vars K J} {α : Tm I U} →
      coe (TmΓ= U[]) (α [ ⌜ f ∘V g ⌝V ]t) ≡
      coe (TmΓ= U[]) (coe (TmΓ= U[]) (α [ ⌜ f ⌝V ]t) [ ⌜ g ⌝V ]t)
  TMUcompP = from≃ ( uncoe (TmΓ= U[]) ⁻¹̃
                   ◾̃ [⌜∘V⌝]t ⁻¹̃
                   ◾̃ coe[]t' U[] ⁻¹̃
                   ◾̃ uncoe (TmΓ= U[]))

TMU : PSh (REN ᵒᵖ)
TMU =  record
  { _$P_   = λ Γ → Tm Γ U
  ; _$P_$_ = λ β t → coe (TmΓ= U[]) (t [ ⌜ β ⌝V ]t)
  ; idP    = TMUidP
  ; compP  = TMUcompP
  }

abstract
  TMElidF : {I : Con} {α : TMU $P I} {x : Tm I (El α)} →
      coe (TmΓ= El[]) (x [ ⌜ idV ⌝V ]t) ≡[
      ap (λ Â → Tm I (El Â)) (idP TMU) ]≡ x
  TMElidF = from≃ ( uncoe (ap (λ Â → Tm _ (El Â)) (idP TMU)) ⁻¹̃
                  ◾̃ uncoe (TmΓ= El[]) ⁻¹̃
                  ◾̃ [⌜idV⌝]t)

abstract
  TMElcompF : {I : Con} {J : Con} {K : Con} {f : Vars J I}
      {g : Vars K J} {α : TMU $P I} {x : Tm I (El α)} →
      coe (TmΓ= El[]) (x [ ⌜ f ∘V g ⌝V ]t) ≡[
      ap (λ Â → Tm K (El Â)) (compP TMU) ]≡
      coe (TmΓ= El[]) (coe (TmΓ= El[]) (x [ ⌜ f ⌝V ]t) [ ⌜ g ⌝V ]t)
  TMElcompF = from≃ ( uncoe (ap (λ Â → Tm _ (El Â)) (compP TMU)) ⁻¹̃
                    ◾̃ uncoe (TmΓ= El[]) ⁻¹̃
                    ◾̃ [⌜∘V⌝]t ⁻¹̃
                    ◾̃ coe[]t' El[] ⁻¹̃
                    ◾̃ uncoe (TmΓ= El[]))

TMEl : FamPSh TMU
TMEl = record
  { _$F_   = λ {Γ} Â → Tm Γ (El Â)
  ; _$F_$_ = λ β t → coe (TmΓ= El[]) (t [ ⌜ β ⌝V ]t)
  ; idF    = TMElidF
  ; compF  = TMElcompF
  }
