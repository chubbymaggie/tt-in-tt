module Readme where

-- This repository contains formalisations accompanying the following
-- papers:
-- 
--  * Type Theory in Type Theory using Quotient Inductive Types, POPL
--    2016
--
--  * Normalisation by Evaluation for Dependent Types
--
-- by Thorsten Altenkirch and Ambrus Kaposi.

------------------------------------------------------------------------------
-- Usage
------------------------------------------------------------------------------

-- We used the development version of Agda (2.4.3) to typecheck the
-- files, this is how you can install it:
-- 
-- $ cabal update
-- $ git clone https://github.com/agda/agda.git
-- $ cd agda
-- $ git checkout 0188c95
-- $ make
-- $ agda-mode setup
--
-- If you encounter any error, please consult
-- https://github.com/agda/agda/blob/master/README.md
--
-- To start browsing the code for these formal developments, use the
-- following commands:
-- 
-- $ git clone git@bitbucket.org:akaposi/tt-in-tt.git
-- $ cd tt-in-tt
-- $ emacs Readme.agda
--
-- Typechecking can be started by selecting the option Agda/Load from
-- the menu bar.
-- 
-- Typechecking all the dependencies of this file takes around 7
-- minutes on a computer with an Intel i3 CPU.

------------------------------------------------------------------------------
-- Contact
------------------------------------------------------------------------------

-- If you have any questions or comments, please don't hesitate to
-- contact Ambrus: kaposi.ambrus@gmail.com.

------------------------------------------------------------------------------
-- Local standard library
------------------------------------------------------------------------------

import lib                           -- our standard library
import JM.JM                         -- heterogeneous equality

------------------------------------------------------------------------------
-- Materials for "Type Theory in Type Theory using Quotient Inductive
-- Types"
------------------------------------------------------------------------------

-- Section 1 - Introduction

import Section1-2.STL                -- example of simply typed
                                     -- λ-calculus

-- Section 2 - The Type Theory we live in

import Section1-2.IndIndEx           -- induction-induction example
import Section1-2.QITEx              -- QIT example

-- Section 3 - Representing the syntax of Type Theory

import TT.Syntax                     -- the syntax
import TT.Rec                        -- recursor for the syntax
import TT.Elim                       -- eliminator for the syntax

import TT.Congr                      -- congruence rules
import TT.Laws                       -- derived laws

-- Section 4 - The standard model

import StandardModel.StandardModel   -- the standard model
import StandardModel.StandardModelIR -- the standard model with an
                                     -- inductive-recursive universe

-- Section 5 - The logical predicate interpretation

-- import LogPred.LogPred               -- the logical predicate
                                     -- interpretation (under refactoring)

-- Section 6 - Homotopy Type Theory

import HoTT.Syntax                   -- the 0-truncated syntax of
                                     -- type theory
import HoTT.Rec                      -- recursor for the 0-trucnated
                                     -- syntax
import HoTT.Elim                     -- eliminator for the
                                     -- 0-truncated syntax

import HoTT.UniverseSet              -- an inductive-recursive
                                     -- universe with ⊤,Π,Σ and a
                                     -- proofs that it is a set
import HoTT.StandardModel            -- the standard interpretation
                                     -- using the previously defined
                                     -- universe

-- import HoTT.Nf                       -- experimentation with normal
                                     -- forms

import HoTT.JM                       -- definition of heterogeneous
                                     -- equality for families
import HoTT.Congr                    -- congruence rules for the
                                     -- syntax using JM

------------------------------------------------------------------------------
-- Materials for "Normalisation by Evaluation for Dependent Types"
------------------------------------------------------------------------------

-- The formalisation for this paper is not finished, in some places we
-- used cheating, but all the occurrences of cheat can be filled (has
-- been checked on paper). We use cheat in NBE.LogPred and NBE.Quote.

import NBE.Cheat

-- Section 3 - Object theory

import TT.Syntax                     -- the syntax
import TT.Elim                       -- eliminator for the syntax

-- Section 4 - The category of renamings

import NBE.Renamings

-- Section 5 - The logical predicate interpretation

import Cats                          -- the categorical definitions
import NBE.TM                        -- the presheaf structure of the
                                     -- syntax
import NBE.LogPred.LogPred           -- the logical predicate
                                     -- interpretation

-- Section 6 - Normal forms

import NBE.Nf                        -- neutral terms and normal forms
import NBE.Nfs                       -- lists of neutral terms and
                                     -- normal forms

-- Section 7 - Quote and unquote

import NBE.Quote.Quote               -- definition of quote and
                                     -- unquote

-- Section 8

import NBE.Norm                      -- normalisation and completeness
import NBE.Nfdec                     -- decidability of normal forms

------------------------------------------------------------------------------
-- Other stuff not belonging anywhere
------------------------------------------------------------------------------

import Term.Term                     -- the term model
import PSh.PSh                       -- the presheaf model
