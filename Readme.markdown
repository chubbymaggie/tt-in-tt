This repository contains formalisations accompanying the following papers:

  * [Type Theory in Type Theory using Quotient Inductive Types](http://akaposi.bitbucket.org/tt-in-tt.pdf), POPL 2016

  * [Normalisation by Evaluation for Dependent Types](http://akaposi.bitbucket.org/nbe.pdf)

by [Thorsten Altenkirch](http://cs.nott.ac.uk/~txa) and [Ambrus Kaposi](http://akaposi.bitbucket.org).

See [Readme.agda](Readme.agda) for more information.