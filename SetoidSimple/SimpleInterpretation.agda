{-# OPTIONS --type-in-type #-} 

open import Relation.Binary.PropositionalEquality hiding  ([_])
open import Data.Nat
open import Data.Unit
open import Data.Product
open import SimplyTyped
open import Setoid

open ≡-Reasoning

module SimpleInterpretation where

------ Interpretation in 'Setoid' ------

-- we use K, but we could do it without (ℕ is a set)
ℕₛ : Setoid
ℕₛ = cₛ ℕ _≡_ (anything-is-set ℕ) refl sym trans 
         where
           anything-is-set : (X : Set) → {x₁ x₂ : X} → (p₁ p₂ : x₁ ≡ x₂) → p₁ ≡ p₂
           anything-is-set X refl refl = refl


λtappₛ : {Γ Δ Θ : Setoid}{t : Γ ⇒ₛ Setoid⇒ {Δ}{Θ}} → λtₛ (appₛ t) ≡ t

λ[]ₛ : {Γ Δ A B : Setoid}{t : (Δ ×ₛ A) ⇒ₛ B}{σ : Γ ⇒ₛ Δ} → λtₛ t ∘ₛ σ ≡ λtₛ (t ∘ₛ ((σ ∘ₛ (proj₁ₛ ∘ₛ idₛ)) ,sₛ (proj₂ₛ ∘ₛ idₛ)))
λ[]ₛ {t = t}{σ = σ} = begin
                                     λtₛ t ∘ₛ σ 
                                         ≡⟨ sym λtappₛ ⟩
                                     λtₛ (appₛ (λtₛ t ∘ₛ σ)) 
                                         ≡⟨ refl ⟩
                                     λtₛ ( (appₛ (λtₛ t)) ∘ₛ ( (σ ∘ₛ (proj₁ₛ ∘ₛ idₛ)) ,sₛ (proj₂ₛ ∘ₛ idₛ)) ) --  Uses rule similar to: app ( (λt t) [ σ ] ) ≡ (app λt t) [ σ ∘ π₁ id , π₂ id ]
                                         ≡⟨ refl ⟩
                                     {-λtₛ (t ∘ₛ ( (σ ∘ₛ (proj₁ₛ ∘ₛ idₛ)) ,sₛ (proj₂ₛ ∘ₛ idₛ))) 
                                         ≡⟨ refl ⟩
                                     λtₛ (t ∘ₛ ((σ ∘ₛ proj₁ₛ) ,sₛ proj₂ₛ))
                                         ≡⟨ refl ⟩ -}
                                     λtₛ (t ∘ₛ ((σ ∘ₛ (proj₁ₛ ∘ₛ idₛ)) ,sₛ (proj₂ₛ ∘ₛ idₛ))) 
                                         ∎

motSd : Motives
motSd = record { 
               Tyᴿ = Setoid
             ; Conᴿ = Setoid
             ; Tmᴿ = λ Γᴿ Aᴿ → Γᴿ ⇒ₛ Aᴿ
             ; _⇒ᴿ_ = λ Γᴿ Δᴿ → Γᴿ ⇒ₛ Δᴿ
             }
    

metSd : Methods motSd
metSd = record {
               ℕ₁ᴿ = ℕₛ
             ; _→Fᴿ_ = λ Γᴿ Δᴿ → Setoid⇒ {Γᴿ}{Δᴿ}
             ; ∙ᴿ = ⊤ₛ
             ; _,cᴿ_ = λ Γ A →  Γ ×ₛ A
             ; π₂ᴿ = λ δ → proj₂ₛ ∘ₛ δ
             ; λtᴿ = λ t →  λtₛ t 
             ; appᴿ = λ t →  appₛ t
             ; _[_]ᴿ =  λ t δ →  t ∘ₛ δ
             ; idᴿ = idₛ
             ; εᴿ = cₘ (λ a → tt) (λ x → refl)
             ; _∘ᴿ_ = λ δ σ →  δ ∘ₛ σ
             ; _,sᴿ_ = λ δ t → δ ,sₛ t
             ; π₁ᴿ = λ δ → proj₁ₛ ∘ₛ δ
             ; ηᴿ = λtappₛ
             ; βᴿ = refl
             ; λ[]ᴿ = sym λtappₛ --λ[]ₛ{t = t}    λ[]ₛ not actually used, but shows how this works
             ; idᵣᴿ = refl
             ; idₗᴿ = refl
             ; assocᴿ = refl
             ; π₁βᴿ = refl
             ; π₂βᴿ = refl
             ; π₁ηᴿ = refl
             ; ,∘ᴿ = refl
             }


open _⇒ₛ_ 
open Setoid.Setoid

λtappₛ {Γ}{Δ}{Θ}{t} = ⇒ₛ≡ _ _ (funext _ _ (λ γ →  

  begin

  (f (λtₛ (appₛ t))) γ 

        ≡⟨ refl ⟩

  cₘ (λ a → (f (appₛ t) (γ , a))) 
       (λ {a₁}{a₂} a₁∼a₂ → resp (appₛ t) ( reflₛ Γ , a₁∼a₂))

        ≡⟨ refl ⟩
  
  cₘ (λ a → ( (λ γa →  f (f t (proj₁ γa)) (proj₂ γa)) (γ , a))) 
       (λ {a₁}{a₂} a₁∼a₂ → resp t (reflₛ Γ) (a₁∼a₂))

        ≡⟨ ⇒ₛ≡ _ _ (funext _ _ (λ a → refl)) ⟩

  (f t) γ
        ∎))
