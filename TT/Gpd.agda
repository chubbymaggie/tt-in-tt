module TT.Gpd where

open import lib

-- types of the syntax

data Con : Set
data Ty : Con → Set
data Tms : Con → Con → Set
data Tm : ∀ Γ → Ty Γ → Set
--data IsVar : ∀ {Γ}{A : Ty Γ} → Tm Γ A → Set
data IsContr : Con → Set

-- a congruence rule (corresponds to refl(TmΓ) in the cubical syntax:
-- Γ is in the context, we degenerate it)
TmΓ= : {Γ : Con}{A₀ : Ty Γ}{A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
     → Tm Γ A₀ ≡ Tm Γ A₁
TmΓ= {Γ} refl = refl

-- constructors
-- note: we are using the categorical application rule and π₁, π₂

data Con where
  •     : Con  -- \bub
  _,_   : (Γ : Con) → Ty Γ → Con
  
infixl 5 _,_

data Ty where
  _[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ
  *     : ∀{Γ} → Ty Γ
  Hom   : ∀{Γ}(A : Ty Γ)(a b : Tm Γ A) → Ty Γ

infixl 7 _[_]T

data Tms where
  ε     : ∀{Γ} → Tms Γ •
  _,_   : ∀{Γ Δ}(δ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ δ ]T) → Tms Γ (Δ , A)
  id    : ∀{Γ} → Tms Γ Γ
  _∘_   : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
  π₁    : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ , A) →  Tms Γ Δ

infix 6 _∘_

data Tm where
  _[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (δ : Tms Γ Δ) → Tm Γ (A [ δ ]T) 
  π₂    : ∀{Γ Δ}{A : Ty Δ}(δ : Tms Γ (Δ , A)) → Tm Γ (A [ π₁ δ ]T)
  coh   : ∀{Γ} → IsContr Γ → (A : Ty Γ) → Tm Γ A

infixl 8 _[_]t

wk : ∀{Γ}{A : Ty Γ} → Tms (Γ , A) Γ
wk = π₁ id

vz : ∀ {Γ}{A : Ty Γ} → Tm (Γ , A) (A [ wk ]T)
vz = π₂ id

vs : ∀ {Γ}{A B : Ty Γ} → Tm Γ A → Tm (Γ , B) (A [ wk ]T) 
vs x = x [ wk ]t

{-
data IsVar where
  zero : ∀{Γ}{A : Ty Γ} → IsVar (vz {Γ} {A})
  suc : ∀ {Γ}{A B : Ty Γ}{x : Tm Γ A} → IsVar x → IsVar (vs {B = B} x)
-}

data IsContr where
  * : IsContr (• , *)
  ext : ∀ {Γ} → IsContr Γ → {A : Ty Γ}{x : Tm Γ A} -- → IsVar x 
        → IsContr  (Γ , A , Hom (A [ wk ]T) (x [ wk ]t) vz) 

-- higher constructors are postulated

postulate
   -- higher constructors for Ty
   [id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
   [][]T : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ} → A [ δ ]T [ σ ]T ≡ A [ δ ∘ σ ]T
   *[]   : ∀{Γ Δ}{δ : Tms Γ Δ} → * [ δ ]T ≡ *
   Hom[]  : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ}{a b : Tm Δ A}
            → (Hom A a b)[ δ ]T ≡ Hom (A [ δ ]T) (a [ δ ]t) (b [ δ ]t) 

postulate
   -- higher constructors for Tms
   idl   : ∀{Γ Δ}{δ : Tms Γ Δ} → id ∘ δ ≡ δ 
   idr   : ∀{Γ Δ}{δ : Tms Γ Δ} → δ ∘ id ≡ δ 
   ass   : ∀{Δ Γ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Γ Σ}{ν : Tms Δ Γ}
         → (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
   ,∘    : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ}{a : Tm Γ (A [ δ ]T)}
         → (δ , a) ∘ σ ≡ (δ ∘ σ) , coe (TmΓ= [][]T) (a [ σ ]t)
   π₁β   : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)} → π₁ (δ , a) ≡ δ

   -- higher constructors for Tm
   [id]t : ∀{Γ}{A : Ty Γ}{t : Tm Γ A} → t [ id ]t ≡[ TmΓ= [id]T ]≡ t
   [][]t : ∀{Γ Δ Σ}{A : Ty Σ}{t : Tm Σ A}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
         → (t [ δ ]t) [ σ ]t ≡[ TmΓ= [][]T ]≡  t [ δ ∘ σ ]t
   π₂β   : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
         → π₂ (δ , a) ≡[ TmΓ= (ap (λ z → A [ z ]T) π₁β) ]≡ a
   coh[] : ∀{Γ Δ}{cΓ : IsContr Γ}{cΔ : IsContr Δ}{A : Ty Δ}{δ : Tms Γ Δ}
           → (coh cΔ A) [ δ ]t ≡ coh cΓ (A [ δ ]T)
{-
   -- higher constructors for IsVar
   irrVar :  ∀ {Γ}{A : Ty Γ}{x : Tm Γ A}{p q : IsVar x} → p ≡ q
-}
   -- higher constructors for IsContr
   irrContr : ∀ {Γ}{p q : IsContr Γ} → p ≡ q
   
