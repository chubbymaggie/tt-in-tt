module TT.SyntaxPlus where

open import lib

-- types of the syntax

data Con : Set
data Ty : Con → Set
data Tms : Con → Con → Set
data Tm : ∀ Γ → Ty Γ → Set

-- a congruence rule (corresponds to refl(TmΓ) in the cubical syntax:
-- Γ is in the context, we degenerate it)
TmΓ= : {Γ : Con}{A₀ : Ty Γ}{A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
     → Tm Γ A₀ ≡ Tm Γ A₁
TmΓ= {Γ} p = ap (Tm Γ) p

Tms-Γ= : ∀{Δ Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁) → Tms Γ₀ Δ ≡ Tms Γ₁ Δ
Tms-Γ= {Δ} = ap (λ z → Tms z Δ)

-- constructors
-- note: we are using the categorical application rule and π₁, π₂

data Con where
  •     : Con  -- \bub
  _,_   : (Γ : Con) → Ty Γ → Con

infixl 5 _,_

wk : ∀{Γ}{A : Ty Γ} → Tms (Γ , A) Γ

data Ty where
  _[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ
  U     : ∀{Γ} → Ty Γ
  Π     : ∀{Γ}(A : Ty Γ)(B : Ty (Γ , A)) → Ty Γ
  ΣT    : ∀{Γ}(A : Ty Γ)(B : Ty (Γ , A)) → Ty Γ
  Eq    : ∀{Γ}(A : Ty Γ) → Ty ((Γ , A) , A [ wk ]T) 
  El    : ∀{Γ}(A : Tm Γ U) → Ty Γ

infixl 7 _[_]T

data Tms where
  ε     : ∀{Γ} → Tms Γ •
  _,_   : ∀{Γ Δ}(δ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ δ ]T) → Tms Γ (Δ , A)
  id    : ∀{Γ} → Tms Γ Γ
  _∘_   : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
  π₁    : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ , A) →  Tms Γ Δ
  -- Σ types
  pair  : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)} → Tms ((Γ , A) , B) (Γ , ΣT A B)
  unpair : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)} → Tms (Γ , ΣT A B) ((Γ , A) , B)
  -- Euality types
  rfl    : ∀{Γ Δ}{A : Ty Γ} → Tms (Γ , A) Δ → Tms (((Γ , A) , A [ wk ]T) , Eq A) Δ 
  j      : ∀{Γ Δ}{A : Ty Γ} → Tms (((Γ , A) , A [ wk ]T) , Eq A) Δ → Tms (Γ , A) Δ 

infix 6 _∘_

wk = π₁ id

data Tm where
  _[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (δ : Tms Γ Δ) → Tm Γ (A [ δ ]T) 
  π₂    : ∀{Γ Δ}{A : Ty Δ}(δ : Tms Γ (Δ , A)) → Tm Γ (A [ π₁ δ ]T)
  app   : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)} → Tm Γ (Π A B) → Tm (Γ , A) B
  lam   : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)} → Tm (Γ , A) B → Tm Γ (Π A B)

infixl 8 _[_]t

-- higher constructors are postulated

postulate
   -- higher constructors for Ty
   [id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
   [][]T : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ} → (A [ δ ]T) [ σ ]T ≡ A [ δ ∘ σ ]T
   U[]   : ∀{Γ Δ}{δ : Tms Γ Δ} → U [ δ ]T ≡ U
   El[]  : ∀{Γ Δ}{δ : Tms Γ Δ}{Â : Tm Δ U} → El Â [ δ ]T ≡ El (coe (TmΓ= U[]) (Â [ δ ]t))

_^_ : ∀{Γ Δ}(δ : Tms Γ Δ)(A : Ty Δ) → Tms (Γ , A [ δ ]T) (Δ , A)
δ ^ A = (δ ∘ π₁ id) , coe (TmΓ= [][]T) (π₂ id)

postulate
   Π[]   : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}
         → (Π A B) [ δ ]T ≡ Π (A [ δ ]T) (B [ δ ^ A ]T)
   Σ[]   : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}
         → ΣT (A [ δ ]T) (B [ δ ^ A ]T) ≡ (ΣT A B) [ δ ]T 
   Eq[]  : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ} → (Eq A) [ (δ ^ A) ^ (A [ wk ]T) ]T ≡[ {!!} ]≡ Eq (A [ δ ]T)

   -- higher constructors for Tms
   idl   : ∀{Γ Δ}{δ : Tms Γ Δ} → id ∘ δ ≡ δ 
   idr   : ∀{Γ Δ}{δ : Tms Γ Δ} → δ ∘ id ≡ δ 
   ass   : ∀{Δ Γ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Γ Σ}{ν : Tms Δ Γ}
         → (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
   ,∘    : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ}{a : Tm Γ (A [ δ ]T)}
         → (δ , a) ∘ σ ≡ (δ ∘ σ) , coe (TmΓ= [][]T) (a [ σ ]t)
   π₁β   : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)} → π₁ (δ , a) ≡ δ
   πη    : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ (Δ , A)} → (π₁ δ , π₂ δ) ≡ δ
   εη    : ∀{Γ}{σ : Tms Γ •} → σ ≡ ε
   -- Σ types
   Σβ    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}
         → unpair ∘ pair ≡ id {(Γ , A) , B}
   Ση    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}
         → pair ∘ unpair ≡ id {Γ , (ΣT A B)}
   unpair∘ :  ∀{Γ Δ}{A : Ty Γ}{B : Ty (Γ , A)}{γ : Tms Δ Γ}
         → (((γ ^ A) ^ B) ∘ unpair) ≡[ Tms-Γ= (ap (_,_ Δ) Σ[]) ]≡ (unpair ∘ (γ ^ (ΣT A B)))
   -- Equality types
   Eqβ    :  ∀{Γ Δ}{A : Ty Γ}{δ : Tms (Γ , A) Δ}
          → j (rfl δ) ≡ δ
   -- no η !
   -- need subst-rules for rfl & J

   -- higher constructors for Tm
   [id]t : ∀{Γ}{A : Ty Γ}{t : Tm Γ A} → t [ id ]t ≡[ TmΓ= [id]T ]≡ t
   [][]t : ∀{Γ Δ Σ}{A : Ty Σ}{t : Tm Σ A}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
         → (t [ δ ]t) [ σ ]t ≡[ TmΓ= [][]T ]≡  t [ δ ∘ σ ]t
   π₂β   : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
         → π₂ (δ , a) ≡[ TmΓ= (ap (λ z → A [ z ]T) π₁β) ]≡ a
   lam[] : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}{t : Tm (Δ , A) B}                       
         → (lam t) [ δ ]t ≡[ TmΓ= Π[] ]≡ lam (t [ δ ^ A ]t)
   Πβ    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm (Γ , A) B}
         →  app (lam t) ≡ t
   Πη    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm Γ (Π A B)}
         → lam (app t) ≡ t
