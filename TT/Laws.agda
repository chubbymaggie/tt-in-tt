module TT.Laws where

open import lib
open import JM.JM
open import TT.Syntax
open import TT.Congr

------------------------------------------------------------------------------
-- Derived laws for Con
------------------------------------------------------------------------------

------------------------------------------------------------------------------
-- Derived laws for Ty
------------------------------------------------------------------------------

------------------------------------------------------------------------------
-- Derived laws for Tms
------------------------------------------------------------------------------

abstract
  π₁∘ : ∀{Γ Δ Θ}{A : Ty Δ}{δ : Tms Γ (Δ , A)}{ρ : Tms Θ Γ}
       → π₁ δ ∘ ρ ≡ π₁ (δ ∘ ρ)
  π₁∘ {Γ}{Δ}{Θ}{A}{δ}{ρ}
    = π₁β ⁻¹ ◾ ap π₁ (,∘ ⁻¹) ◾ ap (λ σ → π₁ (σ ∘ ρ)) πη

abstract
  π₁idβ : ∀{Γ Δ}{ρ : Tms Γ Δ}{A : Ty Δ}{t : Tm Γ (A [ ρ ]T)}
        → π₁ id ∘ (ρ , t) ≡ ρ
  π₁idβ = π₁∘ ◾ ap π₁ idl ◾ π₁β

abstract
  ∘π₁id : ∀{Γ Δ}{ρ : Tms Γ Δ}{A : Ty Δ}
        → ρ ∘ π₁ {A = A [ ρ ]T} id ≡ π₁ id ∘ (ρ ^ A)
  ∘π₁id {Γ}{Δ}{ρ}{A} = π₁β ⁻¹ ◾ ap π₁ idl ⁻¹ ◾ π₁∘ ⁻¹

abstract
  id^ : ∀{Γ}{A : Ty Γ} → id ^ A ≃ id {Γ , A}
  id^ {Γ}{A} = ,s≃ (,C≃ refl (to≃ [id]T))
                   refl
                   (to≃ idl ◾̃ π₁id≃ refl (to≃ [id]T))
                   r̃
                   (uncoe (TmΓ= [][]T) ⁻¹̃ ◾̃ π₂id≃ refl (to≃ [id]T))
             ◾̃ to≃ πη

abstract
  π₁id∘coe : ∀{Δ Γ₀ Γ₁}{A : Ty Δ}(Γ₂ : Γ₀ ≡ Γ₁){σ : Tms Γ₀ (Δ , A)}
           → π₁ id ∘ coe (Tms-Γ= Γ₂) σ
           ≡ coe (Tms-Γ= Γ₂) (π₁ σ)
  π₁id∘coe refl = π₁∘ ◾ ap π₁ idl

abstract
  π₁id∘coe' : ∀{Δ Γ₀ Γ₁}{A : Ty Δ}(Γ₂ : Γ₀ ≡ Γ₁){σ : Tms Γ₀ (Δ , A)}
           → π₁ id ∘ coe (Tms-Γ= Γ₂) σ
           ≃ π₁ σ
  π₁id∘coe' refl = to≃ (π₁∘ ◾ ap π₁ idl)

abstract
  π₁id∘coe⁻¹ : ∀{Δ Γ}{A₀ A₁ : Ty Δ}(A₂ : A₀ ≡ A₁){σ : Tms Γ (Δ , A₁)}
           → π₁ id ∘ coe (TmsΓ-= (,C≃ refl (to≃ A₂) ⁻¹)) σ
           ≃ π₁ σ
  π₁id∘coe⁻¹ refl = to≃ (π₁∘ ◾ ap π₁ idl)

------------------------------------------------------------------------------
-- Derived laws for Tm
------------------------------------------------------------------------------

abstract
  π₂β' : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
       → π₂ (δ , a) ≃ a
  π₂β' {A = A} = from≡ (TmΓ= (ap (_[_]T A) π₁β)) π₂β

-- other naturality for comprehension

abstract
  π₂[]' : ∀{Γ Δ Θ}{A : Ty Δ}{δ : Tms Γ (Δ , A)}{ρ : Tms Θ Γ}
        → π₂ δ [ ρ ]t ≃ π₂ (δ ∘ ρ)
  π₂[]' = uncoe (TmΓ= [][]T)
        ◾̃ π₂β' ⁻¹̃
        ◾̃ π₂≃ (,∘ ⁻¹)
        ◾̃ π₂≃ (ap (λ z → z ∘ _) πη)

abstract
  π₂idβ : ∀{Γ Δ}{ρ : Tms Γ Δ}{A : Ty Δ}{t : Tm Γ (A [ ρ ]T)}
        → π₂ id [ ρ , t ]t ≃ t
  π₂idβ = π₂[]' ◾̃ π₂≃ idl ◾̃ π₂β'

abstract
  π₂coe : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ}{A : Ty Δ}{ρ : Tms Γ₀ (Δ , A)}
        → π₂ id [ coe (Tms-Γ= Γ₂) ρ ]t
        ≃ π₂ ρ
  π₂coe refl = π₂[]' ◾̃ π₂≃ idl

abstract
  π₂coe⁻¹ : ∀{Γ Δ}{A₀ A₁ : Ty Δ}{ρ : Tms Γ (Δ , A₁)}(A₂ : A₀ ≡ A₁)
        → π₂ id [ coe (TmsΓ-= (,C≃ refl (to≃ A₂) ⁻¹)) ρ ]t
        ≃ π₂ ρ
  π₂coe⁻¹ refl = π₂[]' ◾̃ π₂≃ idl

-- categorical laws for term substitution

abstract
  [id]t : ∀{Γ}{A : Ty Γ}{t : Tm Γ A} → t [ id ]t ≡[ TmΓ= [id]T ]≡ t
  [id]t {Γ}{A}{t}

    = from≃ ( uncoe (TmΓ= [id]T) ⁻¹̃
            ◾̃ from≡ (TmΓ= (ap (_[_]T A) π₁β)) (π₂β {a = t [ id ]t}) ⁻¹̃
            ◾̃ π₂≃ (,s≃' (idl ⁻¹){_}{t [ id ]t}
                        {coe (TmΓ= [][]T)
                             (coe (TmΓ= ([id]T ⁻¹)) t [ id ]t)}
                        (coe[]t' ([id]T ⁻¹) ⁻¹̃ ◾̃ uncoe (TmΓ= [][]T)))
            ◾̃ π₂≃ (,∘ ⁻¹)
            ◾̃ π₂≃ idr
            ◾̃ π₂β'
            ◾̃ uncoe (TmΓ= ([id]T ⁻¹)) ⁻¹̃)

abstract
  [id]t' : ∀{Γ}{A : Ty Γ}{t : Tm Γ A} → t [ id ]t ≃ t
  [id]t' = from≡ (TmΓ= [id]T) [id]t

abstract
  [][]t : ∀{Γ Δ Σ}{A : Ty Σ}{t : Tm Σ A}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
          → (t [ δ ]t) [ σ ]t ≡[ TmΓ= [][]T ]≡  t [ δ ∘ σ ]t
  [][]t {Γ}{Δ}{Σ}{A}{t}{σ}{δ}

    = from≃ ( uncoe (TmΓ= [][]T) ⁻¹̃
            ◾̃ []t≃' refl refl
                    (to≃ (ap (_[_]T A) (idl ⁻¹) ◾ [][]T ⁻¹))
                    (coe[]t' ([id]T ⁻¹) ⁻¹̃) r̃
            ◾̃ coe[]t' [][]T ⁻¹̃
            ◾̃ uncoe (TmΓ= [][]T)
            ◾̃ π₂β' ⁻¹̃
            ◾̃ π₂≃ (,∘ ⁻¹)
            ◾̃ π₂≃ (ap (λ z → z ∘ σ) (,∘ ⁻¹))
            ◾̃ π₂≃ ass
            ◾̃ π₂≃ ,∘
            ◾̃ π₂β' {δ = id ∘ (δ ∘ σ)}
                   {coe (TmΓ= [][]T) (coe (TmΓ= ([id]T ⁻¹)) t [ δ ∘ σ ]t)}
            ◾̃ uncoe (TmΓ= [][]T) ⁻¹̃
            ◾̃ coe[]t' ([id]T ⁻¹))

abstract
  [][]t' : ∀{Γ Δ Σ}{A : Ty Σ}{t : Tm Σ A}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
         → t [ δ ]t [ σ ]t ≃ t [ δ ∘ σ ]t
  [][]t' = from≡ (TmΓ= [][]T) [][]t

-- other naturality for Π

abstract
  app[] : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}{t : Tm Δ (Π A B)}
        → app (coe (TmΓ= Π[]) (t [ δ ]t)) ≡ (app t) [ δ ^ A ]t
  app[] {Γ}{Δ}{δ}{A}{B}{t}

    = ap (λ z → app (coe (TmΓ= Π[]) (z [ δ ]t))) (Πη ⁻¹)
    ◾ app= lam[]
    ◾ Πβ

------------------------------------------------------------------------------
-- Derived laws for Tms and Tm (we need this section because of mutual
-- dependencies)
------------------------------------------------------------------------------

abstract
  ∘^ : ∀{Γ Δ Θ}{δ : Tms Δ Θ}{σ : Tms Γ Δ}{A : Ty Θ}
     → ((δ ∘ σ) ^ A) ≃ ((δ ^ A) ∘ (σ ^ (A [ δ ]T)))
  ∘^ {Γ}{Δ}{Θ}{δ}{σ}{A}

    = ,s≃ p
          refl
          ( to≃ ass
          ◾̃ ∘≃ p
               ( ∘≃ p (π₁id≃ refl q)
               ◾̃ to≃ (π₁β ⁻¹ ◾ ap π₁ (idl ⁻¹) ◾ π₁∘ ⁻¹))
          ◾̃ to≃ (ass ⁻¹))
          r̃
          ( uncoe (TmΓ= [][]T) ⁻¹̃
          ◾̃ π₂id≃ refl q
          ◾̃ uncoe (TmΓ= [][]T)
          ◾̃ π₂idβ ⁻¹̃
          ◾̃ coecoe[]t [][]T [][]T ⁻¹̃)
    ◾̃ to≃ (,∘ ⁻¹)
    where
      q : A [ δ ∘ σ ]T ≃ A [ δ ]T [ σ ]T
      q = to≃ ([][]T ⁻¹)
      p : _≡_ {A = Con} (Γ , A [ δ ∘ σ ]T) (Γ , A [ δ ]T [ σ ]T)
      p = ,C≃ refl q

abstract
  ∘^^ : ∀{Γ Δ Θ}{δ : Tms Δ Θ}{σ : Tms Γ Δ}{A : Ty Θ}{B : Ty (Θ , A)}
     → ((δ ∘ σ) ^ A ^ B) ≃ ((δ ^ A ^ B) ∘ (σ ^ A [ δ ]T ^ B [ δ ^ A ]T))
  ∘^^ = ^≃' (,C≃ refl (to≃ ([][]T ⁻¹))) ∘^ ◾̃ ∘^

abstract
  ^∘<> : ∀{Γ Δ}{ρ : Tms Γ Δ}{A : Ty Δ}{B : Ty Γ}{t : Tm Γ B}(p : A [ ρ ]T ≡ B)
       → coe (Tms-Γ= (,C≃ refl (to≃ p))) (ρ ^ A) ∘ < t > ≡ (ρ , coe (TmΓ= (p ⁻¹)) t)
  ^∘<> {ρ = ρ} refl

    = ,∘
    ◾ ,s≃' ( ass
           ◾ ap (_∘_ ρ)
                (π₁∘ ◾ ap π₁ idl ◾ π₁β)
           ◾ idr)
           ( coecoe[]t [][]T [][]T
           ◾̃ π₂[]'
           ◾̃ π₂≃ idl
           ◾̃ π₂β'
           ◾̃ uncoe (TmΓ= ([id]T ⁻¹)) ⁻¹̃)

abstract
  <>∘ : ∀{Γ Δ A}{u : Tm Δ A}{δ : Tms Γ Δ}
      → < u > ∘ δ ≃ (δ ^ A) ∘ < u [ δ ]t >
  <>∘ {Γ}{Δ}{A}{u}{δ}

    = to≃ ,∘
    ◾̃ ,s≃ refl
          refl
          (to≃ ( idl
               ◾ idr ⁻¹
               ◾ ap (_∘_ δ) (π₁idβ ⁻¹)
               ◾ ass ⁻¹))
          r̃
          ( uncoe (TmΓ= [][]T) ⁻¹̃
          ◾̃ coe[]t' ([id]T ⁻¹)
          ◾̃ uncoe (TmΓ= ([id]T ⁻¹))
          ◾̃ π₂idβ ⁻¹̃
          ◾̃ coe[]t' [][]T ⁻¹̃
          ◾̃ uncoe (TmΓ= [][]T))
    ◾̃ to≃ (,∘ ⁻¹)

abstract
  $[] : ∀{Γ Δ A B}{t : Tm Δ (Π A B)}{u : Tm Δ A}{δ : Tms Γ Δ}
      → (t $ u) [ δ ]t ≃ coe (TmΓ= Π[]) (t [ δ ]t) $ (u [ δ ]t)
  $[] {Γ}{Δ}{A}{B}{t}{u}{δ}

    = [][]t'
    ◾̃ []t≃ refl <>∘
    ◾̃ [][]t' ⁻¹̃
    ◾̃ to≃ (ap (λ z → z [  < u [ δ ]t > ]t) (app[] {δ = δ}{t = t}) ⁻¹)

abstract
  ,→^ : ∀{Γ Δ}{A : Ty Δ}{ρ : Tms Γ Δ}{u : Tm Γ (A [ ρ ]T)}
      → ρ , u ≡ (ρ ^ A) ∘ (id , coe (TmΓ= ([id]T ⁻¹)) u)
  ,→^ = ,s≃' (idr ⁻¹ ◾ ap (_∘_ _) (π₁idβ ⁻¹) ◾ ass ⁻¹)
             ( uncoe (TmΓ= ([id]T ⁻¹))
             ◾̃ π₂β' ⁻¹̃
             ◾̃ π₂≃ idl ⁻¹̃
             ◾̃ π₂[]' ⁻¹̃
             ◾̃ coe[]t' [][]T ⁻¹̃
             ◾̃ uncoe (TmΓ= [][]T))
      ◾ ,∘ ⁻¹

abstract
  app[,] : ∀{Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}{t : Tm Δ (Π A B)}
           {ρ : Tms Γ Δ}{u : Tm Γ (A [ ρ ]T)}
         → app t [ ρ , u ]t ≃ app (coe (TmΓ= Π[]) (t [ ρ ]t)) [ < u > ]t
  app[,] = []t≃'' ,→^
         ◾̃ [][]t' ⁻¹̃
         ◾̃ to≃ ((ap (λ z → z [ < _ > ]t)) (app[] ⁻¹))
