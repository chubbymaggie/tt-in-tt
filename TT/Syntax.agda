{-# OPTIONS --no-eta #-}

module TT.Syntax where

open import lib
open import JM.JM

-- types of the syntax

data Con : Set
data Ty : Con → Set
data Tms : Con → Con → Set
data Tm : ∀ Γ → Ty Γ → Set

-- a congruence rule (corresponds to refl(TmΓ) in the cubical syntax:
-- Γ is in the context, we degenerate it)
TmΓ= : {Γ : Con}{A₀ : Ty Γ}{A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
     → Tm Γ A₀ ≡ Tm Γ A₁
TmΓ= {Γ} = ap (Tm Γ)

-- constructors. note: we are using the categorical version of the
-- application rule and π₁, π₂

data Con where
  •     : Con  -- \bub
  _,_   : (Γ : Con) → Ty Γ → Con
  
infixl 5 _,_

data Ty where
  _[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ
  U     : ∀{Γ} → Ty Γ
  El    : ∀{Γ}(A : Tm Γ U) → Ty Γ
  Π     : ∀{Γ}(A : Ty Γ)(B : Ty (Γ , A)) → Ty Γ

infixl 7 _[_]T

data Tms where
  ε     : ∀{Γ} → Tms Γ •
  _,_   : ∀{Γ Δ}(δ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ δ ]T) → Tms Γ (Δ , A)
  id    : ∀{Γ} → Tms Γ Γ
  _∘_   : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
  π₁    : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ , A) →  Tms Γ Δ

_,s_   : ∀{Γ Δ}(δ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ δ ]T) → Tms Γ (Δ , A)
_,s_ = _,_

infixl 5 _,s_
infix 6 _∘_

data Tm where
  _[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (δ : Tms Γ Δ) → Tm Γ (A [ δ ]T) 
  π₂    : ∀{Γ Δ}{A : Ty Δ}(δ : Tms Γ (Δ , A)) → Tm Γ (A [ π₁ δ ]T)
  app   : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)} → Tm Γ (Π A B) → Tm (Γ , A) B
  lam   : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)} → Tm (Γ , A) B → Tm Γ (Π A B)

infixl 8 _[_]t

-- usual syntax derived: weakening and de Bruijn variables

wk : ∀{Γ}{A : Ty Γ} → Tms (Γ , A) Γ
wk = π₁ id

vz : ∀ {Γ}{A : Ty Γ} → Tm (Γ , A) (A [ wk ]T)
vz = π₂ id

vs : ∀ {Γ}{A B : Ty Γ} → Tm Γ A → Tm (Γ , B) (A [ wk ]T) 
vs x = x [ wk ]t

-- higher constructors are postulated

postulate
   -- higher constructors for Ty
   [id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
   [][]T : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ} → A [ δ ]T [ σ ]T ≡ A [ δ ∘ σ ]T
   U[]   : ∀{Γ Δ}{δ : Tms Γ Δ} → U [ δ ]T ≡ U
   El[]  : ∀{Γ Δ}{δ : Tms Γ Δ}{Â : Tm Δ U} → El Â [ δ ]T ≡ El (coe (TmΓ= U[]) (Â [ δ ]t))

-- lifting a substitution along a type: Tms Γ Δ → Tms Γ.A[δ] Δ.A

_^_ : ∀{Γ Δ}(δ : Tms Γ Δ)(A : Ty Δ) → Tms (Γ , A [ δ ]T) (Δ , A)
δ ^ A = (δ ∘ wk) , coe (TmΓ= [][]T) vz
infixl 5 _^_

postulate
   Π[]   : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}
         → (Π A B) [ δ ]T ≡ Π (A [ δ ]T) (B [ δ ^ A ]T)

   -- higher constructors for Tms
   idl   : ∀{Γ Δ}{δ : Tms Γ Δ} → id ∘ δ ≡ δ 
   idr   : ∀{Γ Δ}{δ : Tms Γ Δ} → δ ∘ id ≡ δ 
   ass   : ∀{Δ Γ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Γ Σ}{ν : Tms Δ Γ}
         → (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
   ,∘    : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ}{a : Tm Γ (A [ δ ]T)}
         → (δ , a) ∘ σ ≡ (δ ∘ σ) , coe (TmΓ= [][]T) (a [ σ ]t)
   π₁β   : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)} → π₁ (δ , a) ≡ δ
   πη    : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ (Δ , A)} → (π₁ δ , π₂ δ) ≡ δ
   εη    : ∀{Γ}{σ : Tms Γ •} → σ ≡ ε

   -- higher constructors for Tm. we don't need [id]t and [][]t as
   -- pointed out by Steven Schäfer
   π₂β   : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
         → π₂ (δ , a) ≡[ TmΓ= (ap (λ z → A [ z ]T) π₁β) ]≡ a
   lam[] : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}{t : Tm (Δ , A) B}                       
         → (lam t) [ δ ]t ≡[ TmΓ= Π[] ]≡ lam (t [ δ ^ A ]t)
   Πβ    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm (Γ , A) B}
         →  app (lam t) ≡ t
   Πη    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm Γ (Π A B)}
         → lam (app t) ≡ t

-- substituting only the last element

<_> : ∀ {Γ}{A : Ty Γ} → Tm Γ A → Tms Γ (Γ , A)
< t > = id , coe (TmΓ= ([id]T ⁻¹)) t

-- usual application derived

_$_ : ∀ {Γ}{A : Ty Γ}{B : Ty (Γ , A)} → (t : Tm Γ (Π A B)) → (u : Tm Γ A) → Tm Γ (B [ < u > ]T)
t $ u = (app t) [ < u > ]t

infixl 5 _$_
