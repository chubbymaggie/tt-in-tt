{-# OPTIONS --no-eta #-}

module TT.TySpine where

open import Agda.Primitive
open import lib
open import JM.JM
open import TT.Syntax
open import TT.Elim
open import TT.Congr
open import TT.Laws

-- a view on types

data Ty' : ∀{Γ} → Ty Γ → Set where
  U'  : ∀{Γ} → Ty' (U {Γ})
  Π'  : ∀{Γ}(A : Ty Γ)(B : Ty (Γ , A)) → Ty' (Π A B)
  El' : ∀{Γ}(Â : Tm Γ U) → Ty' (El Â)

Ty'= : ∀{Γ}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁) → Ty' A₀ ≡ Ty' A₁
Ty'= = ap Ty'

Π'≃ : ∀{Γ}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
      {B₀ : Ty (Γ , A₀)}{B₁ : Ty (Γ , A₁)}(B₂ : B₀ ≃ B₁)
    → Π' A₀ B₀ ≃ Π' A₁ B₁
Π'≃ refl (refl , refl) = refl , refl

El'≃ : ∀{Γ}{Â₀ Â₁ : Tm Γ U}(Â₂ : Â₀ ≡ Â₁) → El' Â₀ ≃ El' Â₁
El'≃ refl = refl , refl

private
  M : Motives
  M = record
    { Conᴹ = λ _ → ⊤
    ; Tyᴹ  = λ {Γ} _ A → Ty' A
    ; Tmsᴹ = λ _ _ _ → ⊤
    ; Tmᴹ  = λ _ _ _ → ⊤
    }

  open Motives M

  mCon : MethodsCon M
  mCon = record { •ᴹ = tt ; _,Cᴹ_ = λ _ _ → tt }

  to' : ∀{Γ Δ}{A : Ty Δ}(ρ : Tms Γ Δ)(A' : Ty' A) → Ty' (A [ ρ ]T)
  to' ρ U'       = coe (Ty'= (U[] ⁻¹))  U'
  to' ρ (Π' A B) = coe (Ty'= (Π[] ⁻¹))  (Π' (A [ ρ ]T) (B [ ρ ^ A ]T))
  to' ρ (El' Â)  = coe (Ty'= (El[] ⁻¹)) (El' (coe (TmΓ= U[]) (Â [ ρ ]t)))

  abstract
    to'id : ∀{Γ}{A : Ty Γ}{A' : Ty' A} → to' id A' ≃ A'
    to'id {A' = U'}     = uncoe (Ty'= (U[] ⁻¹)) ⁻¹̃
    to'id {A' = Π' A B} = uncoe (Ty'= (Π[] ⁻¹)) ⁻¹̃
                        ◾̃ Π'≃ [id]T
                              ( []T≃ (,C≃ refl (to≃ [id]T))
                                     id^
                              ◾̃ to≃ [id]T)
    to'id {A' = El' Â}  = uncoe (Ty'= (El[] ⁻¹)) ⁻¹̃
                        ◾̃ El'≃ (from≃ ( uncoe (TmΓ= U[]) ⁻¹̃
                                      ◾̃ uncoe  (TmΓ= [id]T)
                                      ◾̃ to≃ [id]t))

  abstract
    to'coe : ∀{Γ Δ}{σ : Tms Γ Δ}{A₀ A₁ : Ty Δ}(A₂ : A₀ ≡ A₁){A' : Ty' A₀}
           → to' σ (coe (Ty'= A₂) A') ≃ to' σ A'
    to'coe refl = refl , refl

  abstract
    to'∘ : ∀{Γ Δ Θ}{ρ : Tms Γ Δ}{σ : Tms Θ Γ}{A : Ty Δ}{A' : Ty' A}
         → to' σ (to' ρ A') ≃ to' (ρ ∘ σ) A'
    to'∘ {A' = U'}     = to'coe (U[] ⁻¹)
                       ◾̃ uncoe (Ty'= (U[] ⁻¹)) ⁻¹̃
                       ◾̃ uncoe (Ty'= (U[] ⁻¹))
    to'∘ {A' = Π' A B} = to'coe (Π[] ⁻¹)
                       ◾̃ uncoe (ap Ty' (Π[] ⁻¹)) ⁻¹̃
                       ◾̃ Π'≃ [][]T
                             ( to≃ [][]T
                             ◾̃ []T≃ (,C≃ refl (to≃ [][]T))
                                    (∘^ ⁻¹̃))
                       ◾̃ uncoe (ap Ty' (Π[] ⁻¹))
    to'∘ {A' = El' Â}  = to'coe (El[] ⁻¹)
                       ◾̃ uncoe (ap Ty' (El[] ⁻¹)) ⁻¹̃
                       ◾̃ El'≃ (from≃ ( uncoe (TmΓ= U[]) ⁻¹̃
                                     ◾̃ coe[]t' U[]
                                     ◾̃ [][]t'
                                     ◾̃ uncoe (TmΓ= U[])))
                       ◾̃ uncoe (ap Ty' (El[] ⁻¹))

  mTy : MethodsTy M mCon
  mTy = record
    { _[_]Tᴹ = λ A' {ρ} _ → to' ρ A'
    ; Uᴹ     = λ {Γ}{_} → U' {Γ}
    ; Elᴹ    = λ {_}{_}{Â} _ → El' Â
    ; Πᴹ     = λ {_}{_}{A} _ {B} _ → Π' A B
    }

  mTms : MethodsTms M mCon mTy
  mTms = record { εᴹ = tt ; _,sᴹ_ = λ _ _ → tt ; idᴹ = tt ; _∘ᴹ_ = λ _ _ → tt ; π₁ᴹ = λ _ → tt }

  mTm : MethodsTm M mCon mTy mTms
  mTm = record { _[_]tᴹ = λ _ _ → tt ; π₂ᴹ = λ _ → tt ; appᴹ = λ _ → tt ; lamᴹ = λ _ → tt }

  mHTy : MethodsHTy M mCon mTy mTms mTm
  mHTy = record
    { [id]Tᴹ = from≃ ( uncoe (TyΓᴹ= [id]T) ⁻¹̃
                     ◾̃ to'id)
    ; [][]Tᴹ = λ {_}{_}{_}{_}{_}{_}{_}{A'}
             → from≃ ( uncoe (TyΓᴹ= [][]T) ⁻¹̃
                     ◾̃ to'∘ {A' = A'})
    ; U[]ᴹ   = from≃ ( uncoe (TyΓᴹ= U[]) ⁻¹̃
                     ◾̃ uncoe (Ty'= (U[] ⁻¹)) ⁻¹̃)
    ; El[]ᴹ  = from≃ ( uncoe (TyΓᴹ= El[]) ⁻¹̃
                     ◾̃ uncoe (Ty'= (El[] ⁻¹)) ⁻¹̃)
    ; Π[]ᴹ   = from≃ ( uncoe (TyΓᴹ= Π[]) ⁻¹̃
                     ◾̃ uncoe (Ty'= (Π[] ⁻¹)) ⁻¹̃) }

  mHTms : MethodsHTms M mCon mTy mTms mTm
  mHTms = record { idlᴹ = refl ; idrᴹ = refl ; assᴹ = refl ; π₁βᴹ = refl ; πηᴹ = refl ; εηᴹ = refl }

  mHTm : MethodsHTm M mCon mTy mTms mTm mHTy mHTms
  mHTm = record { π₂βᴹ = refl ; lam[]ᴹ = refl ; Πβᴹ = refl ; Πηᴹ = refl }

-- the view function

toTy' : ∀{Γ}(A : Ty Γ) → Ty' A
toTy' = Ty-elim
  where
    open elim M mCon mTy mTms mTm mHTy mHTms mHTm
