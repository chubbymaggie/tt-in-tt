{-# OPTIONS --no-eta #-}

-- a collection of congruence rules and derived rules using JM, for
-- convenience

module TT.Congr where

open import lib
open import TT.Syntax
open import JM.JM

------------------------------------------------------------------------------
-- Congruence rules for judgement types
------------------------------------------------------------------------------

Tm≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
    → Tm Γ₀ A₀ ≡ Tm Γ₁ A₁
Tm≃ refl (refl , refl) = refl

Tms-Γ= : ∀{Δ Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁) → Tms Γ₀ Δ ≡ Tms Γ₁ Δ
Tms-Γ= {Δ} = ap (λ z → Tms z Δ)

TmsΓ-= : ∀{Δ Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁) → Tms Δ Γ₀ ≡ Tms Δ Γ₁
TmsΓ-= {Δ} = ap (Tms Δ)

------------------------------------------------------------------------------
-- Congruence rules for elements of Con
------------------------------------------------------------------------------

,C≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
      → _≡_ {A = Con} (Γ₀ , A₀) (Γ₁ , A₁)
,C≃ refl (refl , refl) = refl

------------------------------------------------------------------------------
-- Congruence rules for elements of Ty
------------------------------------------------------------------------------

[]T≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ}{A : Ty Δ}
         {ρ₀ : Tms Γ₀ Δ}{ρ₁ : Tms Γ₁ Δ}(ρ₂ : ρ₀ ≃ ρ₁)
       → A [ ρ₀ ]T ≃ A [ ρ₁ ]T
[]T≃ refl (refl , refl) = refl , refl

[]T≃' : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
        {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≃ A₁)
        {ρ₀ : Tms Γ₀ Δ₀}{ρ₁ : Tms Γ₁ Δ₁}(ρ₂ : ρ₀ ≃ ρ₁)
       → A₀ [ ρ₀ ]T ≃ A₁ [ ρ₁ ]T
[]T≃' refl refl (refl , refl) (refl , refl) = refl , refl

U≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
   → U {Γ₀} ≃ U {Γ₁}
U≃ refl = refl , refl

El≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Â₀ : Tm Γ₀ U}{Â₁ : Tm Γ₁ U}(Â₂ : Â₀ ≃ Â₁)
    → El Â₀ ≃ El Â₁
El≃ refl (refl , refl) = refl , refl

Π= : ∀{Γ}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁){B₀ : Ty (Γ , A₀)}{B₁ : Ty (Γ , A₁)}
     (B₂ : B₀ ≡[ ap Ty (ap (_,_ Γ) A₂) ]≡ B₁)
   → Π A₀ B₀ ≡ Π A₁ B₁
Π= refl refl = refl

Π≃ : ∀{Γ₀}{Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
     {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}(B₂ : B₀ ≃ B₁)
   → Π A₀ B₀ ≃ Π A₁ B₁
Π≃ refl (refl , refl) (refl , refl) = refl , refl

Π≃' : ∀{Γ}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
     {B₀ : Ty (Γ , A₀)}{B₁ : Ty (Γ , A₁)}(B₂ : B₀ ≃ B₁)
   → Π A₀ B₀ ≡ Π A₁ B₁
Π≃' refl (refl , refl) = refl

------------------------------------------------------------------------------
-- Congruence rules for elements of Tms
------------------------------------------------------------------------------

,s≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
      {Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
      {ρ₀ : Tms Γ₀ Δ₀}{ρ₁ : Tms Γ₁ Δ₁}(ρ₂ : ρ₀ ≃ ρ₁)
      {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≃ A₁)
      {t₀ : Tm Γ₀ (A₀ [ ρ₀ ]T)}{t₁ : Tm Γ₁ (A₁ [ ρ₁ ]T)}(t₂ : t₀ ≃ t₁)
    → _≃_ {A = Tms Γ₀ (Δ₀ , A₀)}{Tms Γ₁ (Δ₁ , A₁)} (ρ₀ , t₀) (ρ₁ , t₁)
,s≃ refl refl (refl , refl) (refl , refl) (refl , refl) = refl , refl

,s≃' : ∀{Γ}{Δ}
       {ρ₀ ρ₁ : Tms Γ Δ}(ρ₂ : ρ₀ ≡ ρ₁)
       {A : Ty Δ}
       {t₀ : Tm Γ (A [ ρ₀ ]T)}{t₁ : Tm Γ (A [ ρ₁ ]T)}(t₂ : t₀ ≃ t₁)
     → _≡_ {A = Tms Γ (Δ , A)} (ρ₀ , t₀) (ρ₁ , t₁)
,s≃' refl (refl , refl) = refl

,st= : ∀{Γ}{Δ}
       {ρ₀ ρ₁ : Tms Γ Δ}(ρ₂ : ρ₀ ≡ ρ₁)
       {A : Ty Δ}{t : Tm Γ (A [ ρ₀ ]T)}
     → _≡_ {A = Tms Γ (Δ , A)} (ρ₀ , t) (ρ₁ , coe (TmΓ= (ap (_[_]T A) ρ₂)) t)
,st= refl = refl
       

∘≃ : ∀{Γ Δ}{ρ : Tms Γ Δ}
     {Θ₀ Θ₁ : Con}(Θ₂ : Θ₀ ≡ Θ₁)
     {σ₀ : Tms Θ₀ Γ}{σ₁ : Tms Θ₁ Γ}(σ₂ : σ₀ ≃ σ₁)
   → (ρ ∘ σ₀) ≃ (ρ ∘ σ₁)
∘≃ refl (refl , refl) = refl , refl

∘≃' : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
      {Δ₀ Δ₁ : Con}(Δ₂ : Δ₀ ≡ Δ₁)
      {Θ₀ Θ₁ : Con}(Θ₂ : Θ₀ ≡ Θ₁)
      {ρ₀ : Tms Γ₀ Δ₀}{ρ₁ : Tms Γ₁ Δ₁}(ρ₂ : ρ₀ ≃ ρ₁)
      {σ₀ : Tms Θ₀ Γ₀}{σ₁ : Tms Θ₁ Γ₁}(σ₂ : σ₀ ≃ σ₁)
    → (ρ₀ ∘ σ₀) ≃ (ρ₁ ∘ σ₁)
∘≃' refl refl refl (refl , refl) (refl , refl) = refl , refl

coe∘ : ∀{Θ Δ Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){σ : Tms Γ₀ Δ}{δ : Tms Θ Γ₁}
     → coe (Tms-Γ= Γ₂) σ ∘ δ
     ≡ σ ∘ coe (TmsΓ-= (Γ₂ ⁻¹)) δ
coe∘ refl = refl

coe∘'' : ∀{Θ Δ Γ}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁){σ : Tms (Γ , A₀) Δ}{δ : Tms Θ Γ}
      → coe (Tms-Γ= (,C≃ refl (to≃ A₂))) σ ∘ (δ ^ A₁)
      ≃ σ ∘ (δ ^ A₀)
coe∘'' refl = refl , refl

π₁≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
      {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≃ A₁)
      {δ₀ : Tms Γ₀ (Δ₀ , A₀)}{δ₁ : Tms Γ₁ (Δ₁ , A₁)}(δ₂ : δ₀ ≃ δ₁)
    → π₁ δ₀ ≃ π₁ δ₁
π₁≃ refl refl (refl , refl) (refl , refl) = refl , refl

π₁id≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
        {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
      → π₁ {Γ₀ , A₀} id ≃ π₁ {Γ₁ , A₁} id
π₁id≃ refl (refl , refl) = refl , refl

π₁π₁id≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
          {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
          {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}(B₂ : B₀ ≃ B₁)
        → π₁ {(Γ₀ , A₀) , B₀} (π₁ id) ≃ π₁ {(Γ₁ , A₁) , B₁} (π₁ id)
π₁π₁id≃ refl (refl , refl) (refl , refl) = refl , refl

π₁id∘coe''' : ∀{Δ Γ₀ Γ₁}{A : Ty Δ}(Γ₂ : Γ₀ ≡ Γ₁){σ : Tms Γ₀ (Δ , A)}
         → π₁ id ∘ coe (Tms-Γ= Γ₂) σ
         ≃ π₁ id ∘ σ
π₁id∘coe''' refl = r̃

id,t≃ : ∀{Γ}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁){t : Tm Γ (A₀ [ id ]T)}
      → _≃_ {A = Tms Γ (Γ , A₀)}
            {Tms Γ (Γ , A₁)}
            (id , t)
            (id , coe (TmΓ= (ap (λ z → z [ id ]T) A₂)) t)
id,t≃ refl = refl , refl

id,t= : ∀{Γ}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁){t : Tm Γ (A₀ [ id ]T)}
      → _≡_ {A = Tms Γ (Γ , A₁)}
            (coe (TmsΓ-= (,C≃ refl (to≃ A₂))) (id , t))
            (id , coe (TmΓ= (ap (λ z → z [ id ]T) A₂)) t)
id,t= refl = refl

^≃ : ∀{Γ Δ}{σ₀ σ₁ : Tms Γ Δ}(σ₂ : σ₀ ≡ σ₁){A : Ty Δ}
   → (σ₀ ^ A) ≃ (σ₁ ^ A)
^≃ refl = refl , refl

^≃' : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ}
      {σ₀ : Tms Γ₀ Δ}{σ₁ : Tms Γ₁ Δ}(σ₂ : σ₀ ≃ σ₁)
      {A : Ty Δ}
    → (σ₀ ^ A) ≃ (σ₁ ^ A)
^≃' refl (refl , refl) = refl , refl

^≃'' : ∀{Γ Δ}{σ : Tms Γ Δ}{A₀ A₁ : Ty Δ}(A₂ : A₀ ≡ A₁)
     → (σ ^ A₀) ≃ (σ ^ A₁)
^≃'' refl = refl , refl

^≃''' : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
        {Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
        {σ₀ : Tms Γ₀ Δ₀}{σ₁ : Tms Γ₁ Δ₁}(σ₂ : σ₀ ≃ σ₁)
        {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≃ A₁)
      → (σ₀ ^ A₀) ≃ (σ₁ ^ A₁)
^≃''' refl refl (refl , refl) (refl , refl) = refl , refl

ε≃ : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
   → ε {Γ₀} ≃ ε {Γ₁}
ε≃ refl = refl , refl

------------------------------------------------------------------------------
-- Congruence rules for elements of Tm
------------------------------------------------------------------------------

[]t= : ∀{Γ}{Δ}{A : Ty Δ}{t₀ t₁ : Tm Δ A}(t₂ : t₀ ≡ t₁)
        {ρ : Tms Γ Δ}
       → t₀ [ ρ ]t ≡ t₁ [ ρ ]t
[]t= refl = refl

[]t≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ}{A : Ty Δ}{t : Tm Δ A}
         {ρ₀ : Tms Γ₀ Δ}{ρ₁ : Tms Γ₁ Δ}(ρ₂ : ρ₀ ≃ ρ₁)
       → t [ ρ₀ ]t ≃ t [ ρ₁ ]t
[]t≃ refl (refl , refl) = refl , refl

[]t≃' : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
        {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≃ A₁)
        {t₀ : Tm Δ₀ A₀}{t₁ : Tm Δ₁ A₁}(t₂ : t₀ ≃ t₁)
        {ρ₀ : Tms Γ₀ Δ₀}{ρ₁ : Tms Γ₁ Δ₁}(ρ₂ : ρ₀ ≃ ρ₁)
      → t₀ [ ρ₀ ]t ≃ t₁ [ ρ₁ ]t
[]t≃' refl refl (refl , refl) (refl , refl) (refl , refl) = refl , refl

[]t≃'' : ∀{Γ Δ}{A : Ty Δ}{t : Tm Δ A}{ρ₀ ρ₁ : Tms Γ Δ}(ρ₂ : ρ₀ ≡ ρ₁)
       → t [ ρ₀ ]t ≃ t [ ρ₁ ]t
[]t≃'' refl = refl , refl

coe[]t : ∀{Γ Δ}{ρ : Tms Γ Δ}{A₀ A₁ : Ty Δ}(A₂ : A₀ ≡ A₁){u : Tm Δ A₀}
       → coe (TmΓ= A₂) u [ ρ ]t ≡ coe (TmΓ= (ap (λ z → z [ ρ ]T) A₂)) (u [ ρ ]t)
coe[]t refl = refl

coe[]t' : ∀{Γ Δ}{ρ : Tms Γ Δ}{A₀ A₁ : Ty Δ}(A₂ : A₀ ≡ A₁){u : Tm Δ A₀}
        → coe (TmΓ= A₂) u [ ρ ]t ≃ u [ ρ ]t
coe[]t' refl = refl , refl

coecoe[]t : ∀{Γ Δ}{ρ : Tms Γ Δ}{A B : Ty Δ}{u : Tm Δ A}(q : A ≡ B)
            {C : Ty Γ}(r : B [ ρ ]T ≡ C)
          → coe (TmΓ= r) (coe (TmΓ= q) u [ ρ ]t) ≃ u [ ρ ]t
coecoe[]t refl refl = refl , refl

π₂≃ : ∀{Γ Δ}{A : Ty Δ}{ρ₀ ρ₁ : Tms Γ (Δ , A)}(ρ₂ : ρ₀ ≡ ρ₁)
    → π₂ ρ₀ ≃ π₂ ρ₁
π₂≃ refl = refl , refl

π₂≃' : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
       {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≃ A₁)
       {ρ₀ : Tms Γ₀ (Δ₀ , A₀)}{ρ₁ : Tms Γ₁ (Δ₁ , A₁)}(ρ₂ : ρ₀ ≃ ρ₁)
     → π₂ ρ₀ ≃ π₂ ρ₁
π₂≃' refl refl (refl , refl) (refl , refl) = refl , refl

π₂id≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
        {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
       → π₂ {Γ₀ , A₀} id ≃ π₂ {Γ₁ , A₁} id
π₂id≃ refl (refl , refl) = refl , refl

π₂π₁id≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
          {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
          {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}(B₂ : B₀ ≃ B₁)
        → π₂ {(Γ₀ , A₀) , B₀} (π₁ id) ≃ π₂ {(Γ₁ , A₁) , B₁} (π₁ id)
π₂π₁id≃ refl (refl , refl) (refl , refl) = refl , refl

app= : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t₀ t₁ : Tm Γ (Π A B)} → t₀ ≡ t₁ → app t₀ ≡ app t₁
app= refl = refl

app≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
       {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
       {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}(B₂ : B₀ ≃ B₁)
       {t₀ : Tm Γ₀ (Π A₀ B₀)}{t₁ : Tm Γ₁ (Π A₁ B₁)}(t₂ : t₀ ≃ t₁)
     → app t₀ ≃ app t₁
app≃ refl (refl , refl) (refl , refl) (refl , refl) = refl , refl

lam≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
       {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
       {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}(B₂ : B₀ ≃ B₁)
       {t₀ : Tm (Γ₀ , A₀)  B₀}{t₁ : Tm (Γ₁ , A₁) B₁}(t₂ : t₀ ≃ t₁)
     → lam t₀ ≃ lam t₁
lam≃ refl (refl , refl) (refl , refl) (refl , refl) = refl , refl

vz≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
    → vz {Γ₀}{A₀} ≃ vz {Γ₁}{A₁}
vz≃ refl (refl , refl) = refl , refl

$≃ : ∀{Γ A B}{f₀ f₁ : Tm Γ (Π A B)}(f₂ : f₀ ≡ f₁)
     {a₀ a₁ : Tm Γ A}(a₂ : a₀ ≡ a₁)
   → f₀ $ a₀ ≃ f₁ $ a₁
$≃ refl refl = refl , refl

$≃' : ∀{Γ}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
      {B₀ : Ty (Γ , A₀)}{B₁ : Ty (Γ , A₁)}(B₂ : B₀ ≃ B₁)
      {f₀ : Tm Γ (Π A₀ B₀)}{f₁ : Tm Γ (Π A₁ B₁)}(f₂ : f₀ ≃ f₁)
      {a₀ : Tm Γ A₀}{a₁ : Tm Γ A₁}(a₂ : a₀ ≃ a₁)
    → f₀ $ a₀ ≃ f₁ $ a₁
$≃' refl (refl , refl) (refl , refl) (refl , refl) = refl , refl
