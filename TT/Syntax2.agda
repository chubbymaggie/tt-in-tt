{-# OPTIONS --no-eta #-}

module TT.Syntax2 where

open import lib
open import Data.Bool
--open import JM.Congr

-- types of the syntax

data Con : Set
data Ty : Con → Set
data Tms : Con → Con → Set
data Tm : ∀ Γ → Ty Γ → Set

-- a congruence rule (corresponds to refl(TmΓ) in the cubical syntax:
-- Γ is in the context, we degenerate it)
TmΓ= : {Γ : Con}{A₀ : Ty Γ}{A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
     → Tm Γ A₀ ≡ Tm Γ A₁
TmΓ= {Γ} refl = refl

Tms-Γ= : ∀{Δ Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁) → Tms Γ₀ Δ ≡ Tms Γ₁ Δ
Tms-Γ= {Δ} = ap (λ z → Tms z Δ)

-- constructors
-- note: we are using the categorical application rule and π₁, π₂

data Con where
  •     : Con  -- \bub
  _,_   : (Γ : Con) → Ty Γ → Con
  
infixl 5 _,_

data Ty where
  _[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ
  U     : ∀{Γ} → Ty Γ
  Π     : ∀{Γ}(A : Ty Γ)(B : Ty (Γ , A)) → Ty Γ
  El    : ∀{Γ}(A : Tm Γ U) → Ty Γ
  BoolT : ∀{Γ} → Ty Γ

infixl 7 _[_]T

data Tms where
  ε     : ∀{Γ} → Tms Γ •
  _,_   : ∀{Γ Δ}(δ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ δ ]T) → Tms Γ (Δ , A)
  id    : ∀{Γ} → Tms Γ Γ
  _∘_   : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
  π₁    : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ , A) →  Tms Γ Δ
  boolI :  ∀{Γ Δ} → Tms (Γ , BoolT) Δ → Bool → Tms Γ Δ
  boolE :  ∀{Γ Δ} → (Bool → Tms Γ Δ) → Tms (Γ , BoolT) Δ 

infix 6 _∘_

data Tm where
  _[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (δ : Tms Γ Δ) → Tm Γ (A [ δ ]T) 
  π₂    : ∀{Γ Δ}{A : Ty Δ}(δ : Tms Γ (Δ , A)) → Tm Γ (A [ π₁ δ ]T)
  app   : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)} → Tm Γ (Π A B) → Tm (Γ , A) B
  lam   : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)} → Tm (Γ , A) B → Tm Γ (Π A B)

infixl 8 _[_]t

wk : ∀{Γ}{A : Ty Γ} → Tms (Γ , A) Γ
wk = π₁ id

vz : ∀ {Γ}{A : Ty Γ} → Tm (Γ , A) (A [ wk ]T)
vz = π₂ id

vs : ∀ {Γ}{A B : Ty Γ} → Tm Γ A → Tm (Γ , B) (A [ wk ]T) 
vs x = x [ wk ]t

-- higher constructors are postulated

postulate
   -- higher constructors for Ty
   [id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
   [][]T : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ} → A [ δ ]T [ σ ]T ≡ A [ δ ∘ σ ]T
   U[]   : ∀{Γ Δ}{δ : Tms Γ Δ} → U [ δ ]T ≡ U
   El[]  : ∀{Γ Δ}{δ : Tms Γ Δ}{Â : Tm Δ U} → El Â [ δ ]T ≡ El (coe (TmΓ= U[]) (Â [ δ ]t))
   Bool[]   : ∀{Γ Δ}{δ : Tms Γ Δ} → BoolT [ δ ]T ≡ BoolT

_^_ : ∀{Γ Δ}(δ : Tms Γ Δ)(A : Ty Δ) → Tms (Γ , A [ δ ]T) (Δ , A)
δ ^ A = (δ ∘ wk) , coe (TmΓ= [][]T) vz
infixl 5 _^_

postulate
   Π[]   : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}
         → (Π A B) [ δ ]T ≡ Π (A [ δ ]T) (B [ δ ^ A ]T)

   -- higher constructors for Tms
   idl   : ∀{Γ Δ}{δ : Tms Γ Δ} → id ∘ δ ≡ δ 
   idr   : ∀{Γ Δ}{δ : Tms Γ Δ} → δ ∘ id ≡ δ 
   ass   : ∀{Δ Γ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Γ Σ}{ν : Tms Δ Γ}
         → (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
   ,∘    : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ}{a : Tm Γ (A [ δ ]T)}
         → (δ , a) ∘ σ ≡ (δ ∘ σ) , coe (TmΓ= [][]T) (a [ σ ]t)
   π₁β   : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)} → π₁ (δ , a) ≡ δ
   πη    : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ (Δ , A)} → (π₁ δ , π₂ δ) ≡ δ
   εη    : ∀{Γ}{σ : Tms Γ •} → σ ≡ ε

   -- higher constructors for Tm
   [id]t : ∀{Γ}{A : Ty Γ}{t : Tm Γ A} → t [ id ]t ≡[ TmΓ= [id]T ]≡ t
   [][]t : ∀{Γ Δ Σ}{A : Ty Σ}{t : Tm Σ A}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
         → (t [ δ ]t) [ σ ]t ≡[ TmΓ= [][]T ]≡  t [ δ ∘ σ ]t
   π₂β   : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
         → π₂ (δ , a) ≡[ TmΓ= (ap (λ z → A [ z ]T) π₁β) ]≡ a
   lam[] : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}{t : Tm (Δ , A) B}                       
         → (lam t) [ δ ]t ≡[ TmΓ= Π[] ]≡ lam (t [ δ ^ A ]t)
   Πβ    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm (Γ , A) B}
         →  app (lam t) ≡ t
   Πη    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm Γ (Π A B)}
         → lam (app t) ≡ t
   Boolβ : ∀{Γ Δ}{δ : Tms (Γ , BoolT) Δ} 
         → boolE (boolI δ) ≡ δ
   Boolη : ∀{Γ Δ}{δs : Bool → Tms Γ Δ} 
         → boolI (boolE δs) ≡ δs
   boolE∘ : ∀{Γ Δ Θ}{δs : Bool → Tms Γ Δ}{γ : Tms Θ Γ} 
          → (boolE δs) ∘ coe (Tms-Γ= (ap (_,_ Θ) Bool[])) (γ ^ BoolT) ≡ boolE (λ b → (δs b) ∘ γ) 
   boolI∘ : ∀{Γ Δ Θ}{δ : Tms (Γ , BoolT) Δ}{γ : Tms Θ Γ} 
          → (λ b → (boolI δ b) ∘ γ)  ≡ boolI (δ ∘ coe (Tms-Γ= (ap (_,_ Θ) Bool[])) (γ ^ BoolT)) 


app= : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t₀ t₁ : Tm Γ (Π A B)} → t₀ ≡ t₁ → app t₀ ≡ app t₁
app= refl = refl

app[] : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}{t : Tm Δ (Π A B)}
      → app (coe (TmΓ= Π[]) (t [ δ ]t)) ≡ (app t) [ δ ^ A ]t
app[] {Γ}{Δ}{δ}{A}{B}{t}

  = ap (λ z → app (coe (TmΓ= Π[]) (z [ δ ]t))) (Πη ⁻¹)
  ◾ app= lam[]
  ◾ Πβ

π₁∘ : ∀{Γ Δ Θ}{A : Ty Δ}{δ : Tms Γ (Δ , A)}{ρ : Tms Θ Γ}
     → π₁ δ ∘ ρ ≡ π₁ (δ ∘ ρ)
π₁∘ {Γ}{Δ}{Θ}{A}{δ}{ρ}
  = π₁β ⁻¹ ◾ ap π₁ (,∘ ⁻¹) ◾ ap (λ σ → π₁ (σ ∘ ρ)) πη

coe[]t : ∀{Γ Δ}{ρ : Tms Γ Δ}{A₀ A₁ : Ty Δ}(A₂ : A₀ ≡ A₁){u : Tm Δ A₀}
       → coe (TmΓ= A₂) u [ ρ ]t ≡ coe (TmΓ= (ap (λ z → z [ ρ ]T) A₂)) (u [ ρ ]t)
coe[]t refl = refl

Π= : ∀{Γ}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁){B₀ : Ty (Γ , A₀)}{B₁ : Ty (Γ , A₁)}
     (B₂ : B₀ ≡[ ap Ty (ap (_,_ Γ) A₂) ]≡ B₁)
   → Π A₀ B₀ ≡ Π A₁ B₁
Π= refl refl = refl

<_> : ∀ {Γ}{A : Ty Γ} → Tm Γ A → Tms Γ (Γ , A)
< t > = id , coe (TmΓ= ([id]T ⁻¹)) t

_$_ : ∀ {Γ}{A : Ty Γ}{B : Ty (Γ , A)} → (t : Tm Γ (Π A B)) → (u : Tm Γ A) → Tm Γ (B [ < u > ]T)
t $ u = (app t) [ < u > ]t

bb :  ∀ {Γ} → Bool → Tm Γ BoolT
bb b = coe (TmΓ= Bool[]) (π₂ (boolI id b))

belim : ∀ {Γ}{M : Ty (Γ , BoolT)} →  ((b : Bool) → Tm Γ (M [ < bb b > ]T)) → Tm (Γ , BoolT) M
belim {Γ} {M} m = coe (TmΓ= (ap (_[_]T M) p ◾ [id]T)) (π₂ (boolE {Γ} {(Γ , BoolT) , M}  (λ b → < bb b > , m b )))  
                  where p : π₁ (boolE (λ b → < bb b > , m b)) ≡ id
                        p = π₁ (boolE (λ b → < bb b > , m b)) 
                            ≡⟨ {!!} ⟩
                            boolE (λ b → π₁ (< bb b > , m b)) 
                            ≡⟨ {!!} ⟩ 
                            id ∎
--                             boolE (λ b → π₁ (< bb b > , m b)) 
