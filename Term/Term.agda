module Term.Term where

-- the term model

open import lib
open import TT.Rec
open import TT.Syntax

M : Motives
M = record
  { Conᴹ = Con
  ; Tyᴹ  = Ty
  ; Tmsᴹ = Tms
  ; Tmᴹ  = Tm }

m : Methods M
m = record
  { •ᴹ = •
  ; _,Cᴹ_ = _,_
  ; _[_]Tᴹ = _[_]T
  ; Uᴹ = U
  ; Πᴹ = Π
  ; Elᴹ = El
  ; εᴹ = ε
  ; _,sᴹ_ = _,_
  ; idᴹ = id
  ; _∘ᴹ_ = _∘_
  ; π₁ᴹ = π₁
  ; _[_]tᴹ = _[_]t
  ; π₂ᴹ = π₂
  ; appᴹ = app
  ; lamᴹ = lam
  ; [id]Tᴹ = [id]T
  ; [][]Tᴹ = [][]T
  ; U[]ᴹ = U[]
  ; El[]ᴹ = El[]
  ; Π[]ᴹ = Π[]
  ; idlᴹ = idl
  ; idrᴹ = idr
  ; assᴹ = ass
  ; ,∘ᴹ = ,∘
  ; π₁βᴹ = π₁β
  ; πηᴹ = πη
  ; εηᴹ = εη
  ; π₂βᴹ = π₂β
  ; lam[]ᴹ = lam[]
  ; Πβᴹ = Πβ
  ; Πηᴹ = Πη
  }
