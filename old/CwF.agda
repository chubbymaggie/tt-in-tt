{-# OPTIONS --type-in-type --without-K #-}

module CwF where

open import stdlib

-- the category of families:
-- objects:
Fam : Set
Fam = Σ Set λ A → (A → Set)
-- morphisms:
Hom_Fam : Fam → Fam → Set
Hom_Fam (A , F) (A' , F') = Σ (A → A') λ f → ((a : A) → F a → F' (f a))
-- identity morphisms:
id_Fam : {x : Fam} → Hom_Fam x x
id_Fam = id , (λ _ → id)
-- composition:
_⊚_ : {x y z : Fam} → Hom_Fam y z → Hom_Fam x y → Hom_Fam x z
(f , f') ⊚ (g , g') = f ∘ g , (λ a → f' (g a) ∘ g' a)
infix 9 _⊚_


-- a category with families:
record CwF : Set where
  field
    -- there is a category of contexts
    Con   : Set
    _⇨_   : Con → Con → Set -- \r7
    one   : {Γ : Con} → Γ ⇨ Γ
    _◦_   : {Δ Γ Θ : Con} → Δ ⇨ Γ → Θ ⇨ Δ → Θ ⇨ Γ -- \bu2
    lid   : {Δ Γ : Con} {σ : Δ ⇨ Γ} → one ◦ σ ≡ σ
    rid   : {Δ Γ : Con} {σ : Δ ⇨ Γ} → σ ◦ one ≡ σ
    assoc : {Δ Γ Θ Ω : Con} (σ : Θ ⇨ Ω) (δ : Γ ⇨ Θ) (ν : Δ ⇨ Γ) → (σ ◦ δ) ◦ ν ≡ σ ◦ (δ ◦ ν)
    -- Con has a terminal object
    ○     : Con -- \ci2

    -- we have a functor from Con to Fam
    -- object part:
    F     : Con → Fam
    -- morphism part (contravariant):
    F' : {Γ Δ : Con} → Γ ⇨ Δ → Hom_Fam (F Δ) (F Γ)
    -- functor laws for F':
    id-cong : {Γ : Con} → F' {Γ} {Γ} one ≡ id_Fam
    ◦-cong  : {Γ Δ Θ : Con} (σ : Δ ⇨ Θ) (δ : Γ ⇨ Δ) → F' (σ ◦ δ) ≡ _⊚_ {F Θ} {F Δ} {F Γ} (F' δ) (F' σ)

  -- synonyms for the object part of F:
  Ty : Con → Set
  Ty Γ = proj₁ (F Γ)
  Tm : (Γ : Con) → Ty Γ → Set
  Tm Γ A = proj₂ (F Γ) A 

  field    
    -- comprehension:
    _∙_   : (Γ : Con) (A : Ty Γ) → Con
    p     : {Γ : Con} {A : Ty Γ} → Γ ∙ A ⇨ Γ
    q     : {Γ : Con} {A : Ty Γ} → Tm (Γ ∙ A) (proj₁ (F' p) A)
    ⟨_,_⟩ : {Δ Γ : Con} {A : Ty Γ} (σ : Δ ⇨ Γ) (u : Tm Δ (proj₁ (F' σ) A)) → Δ ⇨ Γ ∙ A
    proj1 : {Δ Γ : Con} {A : Ty Γ} (σ : Δ ⇨ Γ) (u : Tm Δ (proj₁ (F' σ) A)) → p ◦ ⟨ σ , u ⟩ ≡ σ
    proj2 : {Δ Γ : Con} {A : Ty Γ} {σ : Δ ⇨ Γ} {u : Tm Δ (proj₁ (F' σ) A)}
          → transport (Tm Δ)
                      (ap (λ f → f A) ((ap proj₁ (◦-cong p ⟨ σ , u ⟩)) ⁻¹ ◾ ap (proj₁ ∘ F') (proj1 σ u)))
                      (proj₂ (F' ⟨ σ , u ⟩) _ q)
            ≡ u
    uniq  : {Δ Γ : Con} {A : Ty Γ} (σ : Δ ⇨ Γ) (u : Tm Δ (proj₁ (F' σ) A))
            (ν : Δ ⇨ Γ ∙ A)
            (proj1 : p ◦ ν ≡ σ)
            (proj2 : transport (Tm Δ)
                               (ap (λ f → f A) ((ap proj₁ (◦-cong p ν)) ⁻¹ ◾ ap (proj₁ ∘ F') proj1))
                               (proj₂ (F' ν) _ q)
                     ≡ u)
          → ν ≡ ⟨ σ , u ⟩

  infix 9 _◦_
  infix 1 _⇨_

  -- synonyms for the morphism part of F (F'):
  _[_]ty : {Γ Δ : Con} → Ty Δ → (Γ ⇨ Δ) → Ty Γ
  A [ σ ]ty = proj₁ (F' σ) A
  _[_]tm : {Γ Δ : Con} {A : Ty Δ} → Tm Δ A → (σ : Γ ⇨ Δ) → Tm Γ (A [ σ ]ty)
  t [ σ ]tm = proj₂ (F' σ) _ t

  -- abbreviations for the functor laws for F':
  id-ty : {Γ : Con} {A : Ty Γ} → A [ one ]ty ≡ A
  id-ty {A = A} = ap ((λ f → f A) ∘ proj₁) id-cong
  id-tm : {Γ : Con} {A : Ty Γ} {u : Tm Γ A} → transport (Tm Γ) id-ty (u [ one ]tm) ≡ u
  id-tm {u = u} = {! (apd proj₂ id-cong)!}
  -- ...
