module STT_normalforms where

-- simple type theory with only normal forms

data Ty : Set where
  o : Ty
  _⇒_ : Ty → Ty → Ty

data Con : Set where
  ∅ : Con
  _,_ : Con → Ty → Con

data Var : Con → Ty → Set where
  zero : {Γ : Con}{A : Ty} → Var (Γ , A) A
  suc  : {Γ : Con}{A B : Ty} → Var Γ A → Var (Γ , B) A

data Tm : Con → Ty → Set

data Neu : Con → Ty → Set where
  var : {Γ : Con}{A : Ty} → Var Γ A → Neu Γ A
  app : {Γ : Con}{A B : Ty} → Neu Γ (A ⇒ B) → Tm Γ A → Neu Γ B

data Tm where
  neu : {Γ : Con}{A : Ty} → Neu Γ A → Tm Γ A
  lam : {Γ : Con}{A B : Ty} → Tm (Γ , A) B → Tm Γ (A ⇒ B)

q : {Γ : Con}{A : Ty} → Tm (Γ , A) A
q = neu (var zero)

data Tms : Con → Con → Set where
  ε : {Γ : Con} → Tms Γ ∅
  _,_ : {Γ Δ : Con}{A : Ty} → Tms Γ Δ → Tm Γ A → Tms Γ (Δ , A)

p : {Γ : Con}{A : Ty} → Tms (Γ , A) Γ
p = emb p'

  where

    data Vars : Con → Con → Set where
      ε : {Γ : Con} → Vars Γ ∅
      _,_ : {Γ Δ : Con}{A : Ty} → Vars Γ Δ → Var Γ A → Vars Γ (Δ , A)

    +1 : {Γ Δ : Con}{A : Ty} → Vars Γ Δ → Vars (Γ , A) Δ
    +1 ε = ε
    +1 (xs , x) = +1 xs , suc x

    p' : {Γ : Con}{A : Ty} → Vars (Γ , A) Γ
    p' {∅} = ε
    p' {Γ , A} = +1 (p' {Γ}) , suc zero

    emb : {Γ Δ : Con} → Vars Γ Δ → Tms Γ Δ
    emb ε = ε
    emb (vs , x) = emb vs , neu (var x)

_!!_ : {Γ Δ : Con}{A : Ty} → Tms Δ Γ → Var Γ A → Tm Δ A
ε !! ()
(σ , t) !! zero = t
(σ , t) !! suc x = σ !! x

id : {Γ : Con} → Tms Γ Γ
_∘_ : {Γ Δ Θ : Con} → Tms Δ Θ → Tms Γ Δ → Tms Γ Θ

id {∅} = ε
id {∅ , A} = ε , neu (var zero)
id {(Γ , A) , B} = (id {Γ , A} ∘ p) , neu (var zero)

_[_] : {Γ Δ : Con}{A : Ty} → Tm Γ A → Tms Δ Γ → Tm Δ A

ε ∘ σ = ε
(ρ , t) ∘ σ = ((ρ ∘ σ) , t [ σ ])

App : {Γ : Con}{A B : Ty} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B

neu (var x) [ σ ] = σ !! x
neu (app f u) [ σ ] = App (neu f [ σ ]) (u [ σ ])
lam t [ σ ] = lam (t [ (σ ∘ p) , q ])

App (neu x) u = neu (app x u)
App (lam t) u = t [ id , u ]
