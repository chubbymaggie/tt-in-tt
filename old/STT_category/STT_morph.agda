
{-# OPTIONS --without-K #-}

module STT_morph where

open import stdlib

open import STT_model

-- we don't need to add congruence rules for lid, rid etc. because everything is a set
-- the fields of this record have F as a prefix
record STTmorph (m : STT) (n : STT) : Set₁ where
  open STT m
  open STT n renaming ( Con to Con'
                      ; Conset to Conset'
                      ; _⇨_ to _⇨'_
                      ; ⇨set to ⇨set'
                      ; one to one'
                      ; _◦_ to _◦'_
                      ; lid to lid'
                      ; rid to rid'
                      ; ◦-ass to ◦-ass'
                      ; ◇ to ◇'
                      ; ! to !'
                      ; !uniq to !uniq'
                      ; Ty to Ty'
                      ; Tyset to Tyset'
                      ; Tm to Tm'
                      ; Tmset to Tmset'
                      ; _[_] to _[_]'
                      ; []id to []id'
                      ; []cong to []cong'
                      ; _∙_ to _∙'_
                      ; ⟨_⟩ to ⟨_⟩'
                      ; pq to pq'
                      ; ∙β to ∙β'
                      ; ∙η to ∙η'
                      ; ∙ξ to ∙ξ'
                      ; _⇒_ to _⇒'_
                      ; lam to lam'
                      ; app to app'
                      ; ⇒β to ⇒β'
                      ; ⇒η to ⇒η'
                      ; ⇒ξ to ⇒ξ'
                      )
  field
    -----------------------------------------------------------------------
    -- mapping between categories of contexts
    -----------------------------------------------------------------------
    -- a functor from Con to Con' preserving terminal objects
    FCon   : Con → Con'
    F⇨     : {Δ Γ : Con} → Δ ⇨ Γ → FCon Δ ⇨' FCon Γ
    F⇨id   : {Γ : Con} → F⇨ (one {Γ}) ≡ one' {FCon Γ}
    F⇨cong : {Δ Γ Θ : Con} (σ : Δ ⇨ Γ) (δ : Γ ⇨ Θ) → F⇨ (δ ◦ σ) ≡ F⇨ δ ◦' F⇨ σ
    FCon◇  : FCon ◇ ≡ ◇'

    -----------------------------------------------------------------------
    -- mapping between sets of types
    -----------------------------------------------------------------------
    -- a mapping between types
    FTy    : Ty → Ty'
    
    -----------------------------------------------------------------------
    -- mapping between Tm_A : Con^op → Set functors
    -----------------------------------------------------------------------
    -- a natural transformation from Tm_A to (Tm'_{F A} ∘ FCon)
    -- 
    --  δ : Δ ⇨ Γ
    --                       FTm_Γ
    --            Tm_A Γ -------------> Tm'_{F A} (F Γ)
    --              |                         |
    --              |                         |
    -- Tm_A δ = _[δ]|                         |(Tm'_{F A}∘FCon) δ = _[F⇨ δ]'
    --              |                         |
    --              ∨        FTm_Δ            ∨
    --            Tm_A Δ -------------> Tm'_{F A} (F Δ)
    -- 
    FTm    : {A : Ty} {Γ : Con} → Tm A Γ → Tm' (FTy A) (FCon Γ)
    FTmnat : {A : Ty} {Γ Δ : Con} (t : Tm A Γ) (δ : Δ ⇨ Γ)
             → FTm (t [ δ ]) ≡ (FTm t) [ F⇨ δ ]'

    -----------------------------------------------------------------------
    -- comprehension
    -----------------------------------------------------------------------
    -- _∙_ are compatible in the following sense:
    -- 
    --                    _∙_
    --         Con × Ty ------> Con
    --             |             |
    --  FCon ×′ FTy|             |FCon
    --             |             |
    --             ∨      _∙'_   ∨
    --        Con' × Ty' -----> Con'
    -- 
    -- Con and Ty are STT → Cat functors, Con M is the
    -- category of contexts in STT-model M, Ty M is the
    -- discrete category of types in STT-model M.
    -- _∙_ is a natural transformation from Con × Ty to Con,
    -- so if M and N are STT-models, f : M → N, this commutes:
    -- 
    --                         _∙M_
    --           Con M × Ty M ------> Con M
    --               |                  |
    --  Con f ×′ Ty f|                  |Con f
    --               |                  |
    --               ∨         _∙N_     ∨
    --          Con N × Ty N -------> Con N
    -- 
    -- and F preserves ⟨_⟩ (and also pq, see below)
    -- 
    --                      F⇨ ×′ FTm
    --      Δ ⇨ Γ × Tm_A Δ ------------> F Δ ⇨' F Γ × Tm'_{F A} (F Δ)
    --            ∧ |                               ∧ |
    --            | |                               | |
    --          pq|≃|⟨_⟩                         pq'|≃|⟨_⟩'
    --            | |                               | |
    --            | ∨           F⇨                  | ∨
    --         Δ ⇨ Γ ∙ A -------------------> F Δ ⇨' F Γ ∙' F A
    --                                               \________/
    --                                               = F (Γ ∙ A)
    FCon∙ : (Γ : Con) (A : Ty) → FCon (Γ ∙ A) ≡ FCon Γ ∙' FTy A
    F⇨⟨⟩  : {Δ Γ : Con} {A : Ty} (σ,u : Δ ⇨ Γ × Tm A Δ)
            → transport (λ z → FCon Δ ⇨' z) (FCon∙ Γ A)
              (F⇨ ⟨ σ,u ⟩) ≡ ⟨ (F⇨ ×′ FTm) σ,u ⟩'

    -- the relationship between F⇨, FTm and substitution (∙ξ)
    -- can be proven by F⇨cong, F⇨id and FTmnat, i.e. for
    -- 
    --   (ν, δ) : (Θ, Γ) ⇨ (Δ, Ω)
    --
    -- the following diagram commutes:
    --
    --                           F⇨ ×′ FTm
    --            Δ ⇨ Γ × Tm A Δ ----------> F Δ ⇨' F Γ × Tm'_{F A} (F Δ)
    --                  |                               |
    --       δ◦_◦ν×′_[ν]|                               |Fδ ◦' _ ◦' Fν ×′ _[Fν]
    --                  |                               |
    --                  ∨         F⇨ ×′ FTm             ∨              
    --            Θ ⇨ Ω × Tm A Θ ----------> F Θ ⇨' F Ω × Tm'_{F A} (F Θ)
    -- 
    -- because
    --   if σ : Δ ⇨ Γ,  F⇨ (δ◦σ◦ν) = F⇨ δ ◦ F⇨ σ ◦ F⇨ ν  by the functor law F⇨cong
    --   if t : Tm A Δ,  FTm_Θ t[ν] = FTm_Δ t [F ν]  by the naturality FTmnat
    -- 
    -- also, for the same ν and δ, the following diagram commutes:
    -- 
    --                      F⇨
    --         Δ ⇨ Γ ∙ A  ------> F Δ ⇨' F Γ ∙' F A
    --             |                    |
    --  ⟨δ◦p,q⟩◦_◦ν|                    |⟨Fδ ◦' p',q'⟩' ◦' _ ◦' Fν
    --             |                    |
    --             ∨       F⇨           ∨
    --         Θ ⇨ Ω ∙ A ------> F Θ ⇨' F Ω ∙' F A
    -- 
    -- because
    --   if σ : Δ ⇨ Γ ∙ A, F (⟨δ◦p,q⟩◦σ◦ν)
    --                                                = F⇨cong
    --                     F⟨δ◦p,q⟩ ◦' Fσ ◦' Fν
    --                                                = F⇨⟨⟩
    --                     ⟨Fδ ◦' Fp, Fq⟩ ◦' Fσ ◦' Fν                                 F⇨pq           F⇨id
    --                                                = ap proj₁|proj₂ to   F (pq one)  =  pq' (F one)  =  pq' one'
    --                     ⟨Fδ ◦' p',q'⟩' ◦' Fσ ◦' Fν
    
    -----------------------------------------------------------------------
    -- function space
    -----------------------------------------------------------------------
    -- the functors Fty and _⇒_ are compatible in the following sense:
    -- 
    --                     _⇒_
    --            Ty × Ty -----> Ty
    --               |            |
    --       FTy×′FTy|            |FTy
    --               |            |
    --               ∨     _⇒'_   ∨
    --           Ty' × Ty' ----> Ty'
    -- 
    -- this is a naturality condition where ⇒ is a natural transformation, i.e.
    -- for each STT-model M, _⇒M_ : Ty_M × Ty_M to Ty_M where Ty : STT → STT functor
    -- so given STT-models M, N and an STT morphism f : M → N, the following commutes:
    -- 
    -- f : M → N
    --                         ⇒M
    --            Ty_M × Ty_M -----> Ty_M
    --                 |               |
    --     Ty f ×′ Ty f|               |Ty f
    --                 |               |
    --                 ∨       ⇒N      ∨
    --            Ty_N × Ty_N -----> Ty_N
    -- 
    -- and FCon, F⇨ preserves ⟨_⟩ (and also pq, see below)
    -- 
    --                      F⇨ ×′ FTm
    --      Δ ⇨ Γ × Tm_A Δ ------------> F Δ ⇨' F Γ × Tm'_{F A} (F Δ)   -- TODO: this looks like naturality
    --            ∧ |                               ∧ |
    --            | |                               | |
    --          pq|≃|⟨_⟩                         pq'|≃|⟨_⟩'
    --            | |                               | |
    --            | ∨           F⇨                  | ∨
    --         Δ ⇨ Γ ∙ A -------------------> F Δ ⇨' F Γ ∙' F A
    --                                               \________/
    --                                               = F (Γ ∙ A)
    FTy⇒  : (A B : Ty) → FTy (A ⇒ B) ≡ FTy A ⇒' FTy B
    -- ...

  -- F preserves unicity of terminal objects
  F⇨! : (Γ : Con) → transport (λ z → FCon Γ ⇨' z) FCon◇ (F⇨ (! Γ)) ≡ !' (FCon Γ)
  F⇨! Γ = !uniq' _

  -- and FCon, F⇨ preserves pq
  F⇨pq : {Δ Γ : Con} {A : Ty} (δ : Δ ⇨ Γ ∙ A) → (F⇨ ×′ FTm) (pq δ) ≡ pq' (transport (λ z → FCon Δ ⇨' z) (FCon∙ Γ A) (F⇨ δ))
  F⇨pq {Δ} {Γ} {A} δ =
    begin
                                                       (F⇨ ×′ FTm) (pq δ)
    ≡⟨ ∙β' _ ⁻¹ ⟩
                                                       pq' ⟨ (F⇨ ×′ FTm) (pq δ) ⟩'
    ≡⟨ ap pq' $ F⇨⟨⟩ (pq δ) ⁻¹ ⟩
                                                       pq' (transport (λ z → FCon Δ ⇨' z) (FCon∙ Γ A) (F⇨ ⟨ pq δ ⟩))
    ≡⟨ ap (λ z → pq' $ transport (λ z → FCon Δ ⇨' z) (FCon∙ Γ A) $ F⇨ z) $ ∙η δ ⟩
                                                       pq' (transport (λ z → FCon Δ ⇨' z) (FCon∙ Γ A) (F⇨ δ))
    ∎
