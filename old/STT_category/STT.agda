
{-# OPTIONS --without-K #-}

module STT where

open import stdlib
open import STT_model
open import STT_synt

term-model : STT
term-model = record { Con    = Con
                    ; Conset = Conset
                    ; _⇨_    = _⇨_
                    ; ⇨set   = ⇨set
                    ; one    = one
                    ; _◦_    = _◦_
                    ; lid    = lid
                    ; rid    = rid
                    ; ◦-ass  = ◦-ass
                    ; ◇      = ◇
                    ; !      = !
                    ; !uniq  = !uniq
                    ; Ty     = Ty
                    ; Tyset  = Tyset
                    ; Tm     = Tm
                    ; Tmset  = Tmset
                    ; _[_]   = _[_]
                    ; []id   = []id
                    ; []cong = []cong
                    ; _∙_    = _∙_
                    ; p      = p
                    ; q      = q
                    ; ⟨_⟩    = ⟨_⟩
                    ; pqβ    = pqβ
                    ; pqη    = pqη
                    ; _⇒_    = _⇒_
                    ; lam    = lam
                    ; app    = app
                    ; ⇒β     = ⇒β
                    ; ⇒η     = ⇒η
                    ; lamnat = lamnat
                    }
