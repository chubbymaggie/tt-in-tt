
-- using Paolo's library

{-# OPTIONS --without-K --universe-polymorphism #-}

module STT_Paolo_library where

open import equality
open ≡-Reasoning
open import level
open import function
open import function.isomorphism
open import category.category
open import category.category.opposite
open import category.functor
open import category.graph
open import category.graph.core
open import category.instances.set
open import category.terminal
open import sum

transport = subst
ap        = cong
ap₂       = cong₂

------------------------------
-- model of STT
------------------------------

record STT' : Set₄ where
  field
    Con    : Category lzero lzero
  open as-category Con
  field
    ◇      : obj Con                             -- \di2
    isterm : terminal Con ◇
    Ty     : Set
    Tm     : Ty → Functor (op Con) (set lzero)
    _∙_    : (Γ : obj Con) (A : Ty) → obj Con    -- \.
    ∙-comp : (Δ Γ : obj Con) (A : Ty) → hom Δ (Γ ∙ A) ≅ hom Δ Γ × proj₁ (apply (Tm A) Δ)
    _⇒_    : Ty → Ty → Ty
    ⇒-fun  : (Γ : obj Con) (A B : Ty) → proj₁ (apply (Tm B) (Γ ∙ A)) ≅ proj₁ (apply (Tm (A ⇒ B)) Γ)

  infix 9 _∙_
  infix 1 _⇒_
