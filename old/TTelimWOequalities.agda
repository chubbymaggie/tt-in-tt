{-# OPTIONS --without-K --type-in-type #-}

-- syntax of type theory defined with an eliminator without equalities

module TTelimWOequalities where

open import Relation.Binary.PropositionalEquality
open import Data.Unit
open import Data.Product

data Con : Set
data Ty : Con → Set
data Tms : Con → Con → Set
data Tm : ∀ Γ → Ty Γ → Set

data Con where
  • : Con  -- \bub
  _,_ : (Γ : Con) → Ty Γ → Con

data Ty where
  _[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ
  U     : ∀{Γ} → Ty Γ
  Π     : ∀{Γ}(A : Ty Γ)(B : Ty (Γ , A)) → Ty Γ

data Tms where
  ε   : ∀{Γ} → Tms Γ • 
  _,_ : ∀{Γ Δ}(δ : Tms Γ Δ) → {A : Ty Δ} → Tm Γ (A [ δ ]T) → Tms Γ (Δ , A)
  id  : ∀{Γ} → Tms Γ Γ
  _∘_ : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
  π₁  : ∀{Γ}{A : Ty Γ} → Tms (Γ , A) Γ

data Tm where
  _[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (δ : Tms Γ Δ) → Tm Γ (A [ δ ]T) 
  π₂    : ∀{Γ}{A : Ty Γ} → Tm (Γ , A) (A [ π₁ ]T)
  app   : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)} → Tm Γ (Π A B) → Tm (Γ , A) B
  lam   : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)} → Tm (Γ , A) B → Tm Γ (Π A B)

module ConTyTmsTm-elim

  (P : Con → Set)
  (Q : ∀{Γ} → P Γ → Ty Γ → Set)
  (R : ∀{Γ Δ} → P Γ → P Δ → Tms Γ Δ → Set)
  (S : ∀{Γ}(p : P Γ) → {A : Ty Γ} → Q p A → Tm Γ A → Set)

  (m•   : P •)
  (m,C  : ∀{Γ}(p : P Γ){A : Ty Γ} → Q p A → P (Γ , A))

  (m[]T : ∀{Γ}(pΓ : P Γ){Δ}(pΔ : P Δ){A : Ty Δ} → Q pΔ A
        → {δ : Tms Γ Δ}(r : R pΓ pΔ δ) → Q pΓ (A [ δ ]T))
  (mU   : ∀{Γ}(p : P Γ) → Q p U)
  (mΠ   : ∀{Γ}(p : P Γ){A : Ty Γ}(qA : Q p A){B : Ty (Γ , A)}
          (qB : Q (m,C p qA) B) → Q p (Π A B))

  (mε   : ∀{Γ}(p : P Γ) → R p m• ε)
  (m,s  : ∀{Γ}(pΓ : P Γ){Δ}(pΔ : P Δ){δ : Tms Γ Δ}(r : R pΓ pΔ δ)
          {A : Ty Δ}(q : Q pΔ A){t : Tm Γ (A [ δ ]T)}
        → S pΓ (m[]T pΓ pΔ q r) t → R pΓ (m,C pΔ q) (δ , t))
  (mid  : ∀{Γ}(p : P Γ) → R p p id)
  (m∘   : ∀{Γ}(pΓ : P Γ){Δ}(pΔ : P Δ){Σ}(pΣ : P Σ){σ : Tms Δ Σ} → R pΔ pΣ σ
        → {δ : Tms Γ Δ} → R pΓ pΔ δ → R pΓ pΣ (σ ∘ δ))
  (mπ₁  : ∀{Γ}(p : P Γ){A : Ty Γ}(q : Q p A) → R (m,C p q) p π₁)

  (m[]t : ∀{Γ}(pΓ : P Γ){Δ}(pΔ : P Δ){A : Ty Δ}(q : Q pΔ A){t : Tm Δ A} → S pΔ q t
        → {δ : Tms Γ Δ}(r : R pΓ pΔ δ) → S pΓ (m[]T pΓ pΔ q r) (t [ δ ]t))
  (mπ₂  : ∀{Γ}(p : P Γ){A : Ty Γ}(q : Q p A) → S (m,C p q) (m[]T (m,C p q) p q (mπ₁ p q)) π₂)
  (mapp : ∀{Γ}(p : P Γ){A : Ty Γ}(qA : Q p A){B : Ty (Γ , A)}(qB : Q (m,C p qA) B)
          {t : Tm Γ (Π A B)} → S p (mΠ p qA qB) t → S (m,C p qA) qB (app t))
  (mlam : ∀{Γ}(p : P Γ){A : Ty Γ}(qA : Q p A){B : Ty (Γ , A)}(qB : Q (m,C p qA) B)
          {t : Tm (Γ , A) B} → S (m,C p qA) qB t → S p (mΠ p qA qB) (lam t))
  
  where

    Con-elim : ∀ Γ → P Γ
    Ty-elim  : ∀{Γ}(A : Ty Γ) → Q (Con-elim Γ) A
    Tms-elim : ∀{Γ Δ}(δ : Tms Γ Δ) → R (Con-elim Γ) (Con-elim Δ) δ
    Tm-elim  : ∀{Γ}{A : Ty Γ}(t : Tm Γ A) → S (Con-elim Γ) (Ty-elim A) t

    Con-elim •       = m•
    Con-elim (Γ , A) = m,C (Con-elim Γ) (Ty-elim A)
    
    Ty-elim {Γ} (A [ δ ]T) = m[]T (Con-elim Γ) (Con-elim _) (Ty-elim A) (Tms-elim δ)
    Ty-elim {Γ} U          = mU   (Con-elim Γ)
    Ty-elim {Γ} (Π A B)    = mΠ   (Con-elim Γ) (Ty-elim  A) (Ty-elim B)
    
    Tms-elim ε       = mε  (Con-elim _)
    Tms-elim (δ , t) = m,s (Con-elim _) (Con-elim _) (Tms-elim δ) (Ty-elim  _) (Tm-elim t)
    Tms-elim id      = mid (Con-elim _)
    Tms-elim (δ ∘ σ) = m∘  (Con-elim _) (Con-elim _) (Con-elim _) (Tms-elim δ) (Tms-elim σ)
    Tms-elim π₁      = mπ₁ (Con-elim _) (Ty-elim _)

    Tm-elim (t [ δ ]t) = m[]t (Con-elim _) (Con-elim _) (Ty-elim _) (Tm-elim t) (Tms-elim δ)
    Tm-elim π₂         = mπ₂  (Con-elim _) (Ty-elim  _)
    Tm-elim (app t)    = mapp (Con-elim _) (Ty-elim  _) (Ty-elim _) (Tm-elim t)
    Tm-elim (lam t)    = mlam (Con-elim _) (Ty-elim  _) (Ty-elim _) (Tm-elim t)

module use-ConTyTmsTm-elim where
  open ConTyTmsTm-elim

    (λ Γ         → Set)
    (λ ⟦Γ⟧ A     → ⟦Γ⟧ → Set)
    (λ ⟦Γ⟧ ⟦Δ⟧ δ → ⟦Γ⟧ → ⟦Δ⟧)
    (λ ⟦Γ⟧ ⟦A⟧ t → (γ : ⟦Γ⟧) → ⟦A⟧ γ)
    
    ⊤
    (λ ⟦Γ⟧ ⟦A⟧ → Σ ⟦Γ⟧ ⟦A⟧)
    
    (λ ⟦Γ⟧ ⟦Δ⟧ ⟦A⟧ ⟦δ⟧ γ → ⟦A⟧ (⟦δ⟧ γ))
    (λ ⟦Γ⟧             γ → ⊤)
    (λ ⟦Γ⟧ ⟦A⟧ ⟦B⟧     γ → (x : ⟦A⟧ γ) → ⟦B⟧ (γ , x))

    (λ ⟦Γ⟧                 γ      → tt)
    (λ ⟦Γ⟧ ⟦Δ⟧ ⟦δ⟧ ⟦A⟧ ⟦t⟧ γ      → ⟦δ⟧ γ , ⟦t⟧ γ)
    (λ ⟦Γ⟧                 γ      → γ)
    (λ ⟦Γ⟧ ⟦Δ⟧ ⟦Σ⟧ ⟦σ⟧ ⟦δ⟧ γ      → ⟦σ⟧ (⟦δ⟧ γ))
    (λ { ⟦Γ⟧ ⟦A⟧          (γ , _) → γ })

    (λ ⟦Γ⟧ ⟦Δ⟧ ⟦A⟧ ⟦t⟧ ⟦δ⟧ γ       → ⟦t⟧ (⟦δ⟧ γ))
    (λ { ⟦Γ⟧ ⟦A⟧           (_ , a) → a })
    (λ { ⟦Γ⟧ ⟦A⟧ ⟦B⟧ ⟦t⟧   (γ , a) → ⟦t⟧ γ a })
    (λ ⟦Γ⟧ ⟦A⟧ ⟦B⟧ ⟦t⟧      γ   a  → ⟦t⟧ (γ , a))

  ⟦_⟧C : Con → Set
  ⟦_⟧T : ∀{Γ} → Ty Γ → ⟦ Γ ⟧C → Set
  ⟦_⟧s : ∀{Γ Δ} → Tms Γ Δ → ⟦ Γ ⟧C → ⟦ Δ ⟧C
  ⟦_⟧t : ∀{Γ}{A : Ty Γ} → (t : Tm Γ A) → (γ : ⟦ Γ ⟧C) → ⟦ A ⟧T γ

  ⟦_⟧C = Con-elim
  ⟦_⟧T = Ty-elim
  ⟦_⟧s = Tms-elim
  ⟦_⟧t = Tm-elim

  -- some checks
  ⟦[id]T⟧ : ∀{Γ}{A : Ty Γ} → ⟦ A [ id ]T ⟧T ≡ ⟦ A ⟧T
  ⟦[id]T⟧ = refl
  ⟦[][]T⟧ : ∀{Γ Δ Θ}{A : Ty Θ}{σ : Tms Γ Δ}{δ : Tms Δ Θ} → ⟦ (A [ δ ]T) [ σ ]T ⟧T ≡ ⟦ A [ δ ∘ σ ]T ⟧T
  ⟦[][]T⟧ = refl
  ⟦U[]⟧   : ∀{Γ Δ}{δ : Tms Γ Δ} → ⟦ U [ δ ]T ⟧T ≡ ⟦ U {Γ} ⟧T
  ⟦U[]⟧   = refl
  ⟦Π[]⟧  : ∀ {Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)} → 
           (λ γ → (x : ⟦ A ⟧T (⟦ δ ⟧s γ)) → ⟦ B ⟧T (⟦ δ ⟧s γ , x)) ≡
           (λ γ →
             (x : ⟦ A ⟧T (⟦ δ ⟧s γ)) →
             ⟦ B ⟧T
             (⟦ δ ⟧s γ ,  subst (λ X → (γ : ⟦ Γ , (A [ δ ]T) ⟧C) → X γ) (⟦[][]T⟧ {A = A}{σ = π₁ {Γ} {A [ δ ]T}}{δ = δ}) ⟦ π₂ {Γ = Γ}{A = A [ δ ]T} ⟧t (γ , x)))
  ⟦Π[]⟧ = refl
  ⟦idl⟧ : ∀{Γ Δ}{δ : Tms Γ Δ} → ⟦ id ∘ δ ⟧s ≡ ⟦ δ ⟧s
  ⟦idl⟧ = refl 
  ⟦idr⟧ : ∀{Γ Δ}{δ : Tms Γ Δ} → ⟦ δ ∘ id ⟧s ≡ ⟦ δ ⟧s 
  ⟦idr⟧ = refl
  ⟦ass⟧ : ∀{Δ Γ Θ Ω}{σ : Tms Θ Ω}{δ : Tms Γ Θ}{ν : Tms Δ Γ} → ⟦ (σ ∘ δ) ∘ ν ⟧s ≡ ⟦ σ ∘ (δ ∘ ν) ⟧s
  ⟦ass⟧ = refl
  ⟦π₁β⟧ : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)} → ⟦ π₁ ∘ (δ , a) ⟧s ≡ ⟦ δ ⟧s
  ⟦π₁β⟧ = refl
  ⟦πid⟧ : ∀{Γ}{A : Ty Γ} → ⟦ π₁ {Γ} {A} , π₂ ⟧s ≡ ⟦ id {Γ , A} ⟧s
  ⟦πid⟧ = refl
  ⟦εη⟧  : ∀{Γ}{σ : Tms Γ •} → ⟦ σ ⟧s ≡ ⟦ ε {Γ} ⟧s
  ⟦εη⟧  = refl
