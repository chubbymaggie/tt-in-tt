{-# OPTIONS --type-in-type #-} 

module Setoid.Interpretation where

open import lib hiding (coe)
open import TT.Syntax
open import TT.Rec
open import Setoid.Setoid

-- this REALLY needs to be in the library!!
transport : {A : Set}{a₁ a₂ : A} → (P : A → Set) → (p : a₁ ≡ a₂) → (P a₁) → P a₂
transport P refl x = x


M : Motives
M = record { Conᴹ = Setoid ; Tyᴹ = FamSetoid ; Tmsᴹ = _⇒ₛ_ ; Tmᴹ = SectSetoid }
            
open Setoid
open FamSetoid
open SectSetoid
open _⇒ₛ_

{-
symlemma4 : {Γ : Setoid}{A : FamSetoid Γ}{γ₁ γ₂ : ∣ Γ ∣}{a₁ : ∣ F₀ A γ₁ ∣}{a₂ : ∣ F₀ A γ₂ ∣}{p₁ : Γ ∶ γ₁ ∼ γ₂}{p₂ : F₀ A γ₂ ∶ f (F₁ A p₁) a₁ ∼ a₂}
                       → (F₁ A (symₛ Γ p₁)) ∘ₛ (F₁ A p₁) ≡ idₛ
symlemma4 {Γ}{A}{γ₁}{γ₂}{a₁}{a₂}{p₁}{p₂} = F-hom A p₁ (symₛ Γ p₁) ◾ ap (F₁ A) (ab∘ba {Γ}{γ₁}{γ₂}p₁ (symₛ Γ p₁)) ◾ F-id A

symlemma3 : {Γ : Setoid}{A : FamSetoid Γ}{γ₁ γ₂ : ∣ Γ ∣}{a₁ : ∣ F₀ A γ₁ ∣}{a₂ : ∣ F₀ A γ₂ ∣}{p₁ : Γ ∶ γ₁ ∼ γ₂}{p₂ : F₀ A γ₂ ∶ f (F₁ A p₁) a₁ ∼ a₂}
                       → f (F₁ A (symₛ Γ p₁) ∘ₛ F₁ A p₁) a₁ ≡ f (idₛ {F₀ A γ₁}) a₁
symlemma3 {Γ}{A}{γ₁}{γ₂}{a₁}{a₂}{p₁}{p₂} = f≡₁ (symlemma4 {Γ}{A}{γ₁}{γ₂}{a₁}{a₂}{p₁}{p₂})

symlemma2 : {Γ : Setoid}{A : FamSetoid Γ}{γ₁ γ₂ : ∣ Γ ∣}{a₁ : ∣ F₀ A γ₁ ∣}{a₂ : ∣ F₀ A γ₂ ∣}{p₁ : Γ ∶ γ₁ ∼ γ₂}{p₂ : F₀ A γ₂ ∶ f (F₁ A p₁) a₁ ∼ a₂}
                       → F₀ A γ₁ ∶ 
                          f (F₁ A (symₛ Γ p₁)) (f (F₁ A p₁) a₁) 
                          ∼
                          f (F₁ A (symₛ Γ p₁)) a₂
symlemma2 {Γ}{A}{γ₁}{γ₂}{a₁}{a₂}{p₁}{p₂} = resp (F₁ A (symₛ Γ p₁)) p₂

symlemma1 :  {Γ : Setoid}{A : FamSetoid Γ}{γ₁ γ₂ : ∣ Γ ∣}{a₁ : ∣ F₀ A γ₁ ∣}{a₂ : ∣ F₀ A γ₂ ∣}{p₁ : Γ ∶ γ₁ ∼ γ₂}{p₂ : F₀ A γ₂ ∶ f (F₁ A p₁) a₁ ∼ a₂}
                       → F₀ A γ₁ ∶ a₁ ∼ f (F₁ A (symₛ Γ p₁)) a₂
symlemma1 {Γ}{A}{γ₁}{γ₂}{a₁}{a₂}{p₁}{p₂} = coe (∶∼≡₁ {F₀ A γ₁}{f (F₁ A (symₛ Γ p₁)) (f (F₁ A p₁) a₁)}{a₁}{ f (F₁ A (symₛ Γ p₁)) a₂}
                                                                                                   (symlemma3 {Γ}{A}{γ₁}{γ₂}{a₁}{a₂}{p₁}{p₂})) --was previously symlemma2
                                                                                        (symlemma2 {Γ}{A}{γ₁}{γ₂}{a₁}{a₂}{p₁}{p₂})


translemma3 : {Γ : Setoid}{A : FamSetoid Γ}{γ₁ γ₂ γ₃ : ∣ Γ ∣}
                       {a₁ : ∣ F₀ A γ₁ ∣}{a₂ : ∣ F₀ A γ₂ ∣}{a₃ : ∣ F₀ A γ₃ ∣}
                       {p₁ : Γ ∶ γ₁ ∼ γ₂}{p₂ : F₀ A γ₂ ∶ f (F₁ A p₁) a₁ ∼ a₂}
                       {q₁ : Γ ∶ γ₂ ∼ γ₃}{q₂ : F₀ A γ₃ ∶ f (F₁ A q₁) a₂ ∼ a₃}
                       → F₀ A γ₃ ∶ (f (F₁ A q₁)) (f (F₁ A p₁) a₁) ∼ a₃
translemma3 {Γ}{A}{γ₁}{γ₂}{γ₃}{a₁}{a₂}{a₃}{p₁}{p₂}{q₁}{q₂} = transₛ (F₀ A γ₃) (resp (F₁ A q₁) p₂) q₂


translemma2 : {Γ : Setoid}{A : FamSetoid Γ}{γ₁ γ₂ γ₃ : ∣ Γ ∣}
                       {a₁ : ∣ F₀ A γ₁ ∣}{a₂ : ∣ F₀ A γ₂ ∣}{a₃ : ∣ F₀ A γ₃ ∣}
                       {p₁ : Γ ∶ γ₁ ∼ γ₂}{p₂ : F₀ A γ₂ ∶ f (F₁ A p₁) a₁ ∼ a₂}
                       {q₁ : Γ ∶ γ₂ ∼ γ₃}{q₂ : F₀ A γ₃ ∶ f (F₁ A q₁) a₂ ∼ a₃}
                       → f (F₁ A q₁ ∘ₛ F₁ A p₁) a₁ ≡ f (F₁ A (transₛ Γ p₁ q₁)) a₁
translemma2 {Γ}{A}{γ₁}{γ₂}{γ₃}{a₁}{a₂}{a₃}{p₁}{p₂}{q₁}{q₂} = f≡₁ (F-hom A p₁ q₁)


translemma1 : {Γ : Setoid}{A : FamSetoid Γ}{γ₁ γ₂ γ₃ : ∣ Γ ∣}
                       {a₁ : ∣ F₀ A γ₁ ∣}{a₂ : ∣ F₀ A γ₂ ∣}{a₃ : ∣ F₀ A γ₃ ∣}
                       {p₁ : Γ ∶ γ₁ ∼ γ₂}{p₂ : F₀ A γ₂ ∶ f (F₁ A p₁) a₁ ∼ a₂}
                       {q₁ : Γ ∶ γ₂ ∼ γ₃}{q₂ : F₀ A γ₃ ∶ f (F₁ A q₁) a₂ ∼ a₃}
                       → F₀ A γ₃ ∶ f (F₁ A (transₛ Γ p₁ q₁)) a₁ ∼ a₃
translemma1 {Γ}{A}{γ₁}{γ₂}{γ₃}{a₁}{a₂}{a₃}{p₁}{p₂}{q₁}{q₂} = coe ( ∶∼≡₁ {F₀ A γ₃}{ (f (F₁ A q₁)) (f (F₁ A p₁) a₁)}{f (F₁ A (transₛ Γ p₁ q₁)) a₁}{a₃}
                                                                                                                                  (f≡₁ (F-hom A p₁ q₁)) ) --(translemma2 {Γ}{A}{γ₁}{γ₂}{γ₃}{a₁}{a₂}{a₃}{p₁}{p₂}{q₁}{q₂}))
                                                                                                                      (transₛ (F₀ A γ₃) (resp (F₁ A q₁) p₂) q₂) --(translemma3 {Γ}{A}{γ₁}{γ₂}{γ₃}{a₁}{a₂}{a₃}{p₁}{p₂}{q₁}{q₂})



_,c_ : (Γ : Setoid) → (A : FamSetoid Γ) → Setoid
Γ ,c A = cₛ (Σ ∣ Γ ∣ (λ γ → ∣ F₀ A γ ∣))
                  (λ {(γ₁ , a₁) (γ₂ , a₂) → Σ (Γ ∶ γ₁ ∼ γ₂) (λ p → (F₀ A γ₂) ∶ f (F₁ A p) a₁ ∼ a₂)})
                  (λ { {(γ₁ , a₁)} {(γ₂ , a₂)} (p₁ , p₂) (q₁ , q₂) →  Σ= ((Γ ∼prp) p₁ q₁) 
                                                                                               (((F₀ A γ₂) ∼prp) _ q₂) })
                  (λ { {γ , a} → reflₛ Γ , coe (∶∼≡₁ {F₀ A γ} (f≡₁ {δ = idₛ}{σ = F₁ A (reflₛ Γ)} (F-id A ⁻¹))) (reflₛ (F₀ A γ) {a}) })
                  (λ { {γ₁ , a₁}{γ₂ , a₂} (p₁ , p₂) → symₛ Γ p₁ , symₛ (F₀ A γ₁) (symlemma1 {Γ}{A}{γ₁}{γ₂}{a₁}{a₂}{p₁}{p₂})})
                  (λ { {γ₁ , a₁}{γ₂ , a₂}{γ₃ , a₃} (p₁ , p₂) (q₁ , q₂) → transₛ Γ p₁ q₁ , translemma1 {Γ}{A}{γ₁}{γ₂}{γ₃}{a₁}{a₂}{a₃}{p₁}{p₂}{q₁}{q₂}}) 

-}

_,C_ : {Γ : Setoid} → (A : FamSetoid Γ) → (B : FamSetoid (Γ ,c A)) → FamSetoid Γ
_,C_ {Γ} A B = cf (λ γ → cₛ (Σ (∣ F₀ A γ ∣) (λ a → ∣ F₀ B (γ , a) ∣))
                                           (λ {(a₁ , b₁)(a₂ , b₂) → Σ (F₀ A γ ∶ a₁ ∼ a₂)
                                                                                 (λ r →    F₀ B (γ , a₂) ∶ f (F₁ B ( reflₛ Γ , coe (∶∼≡₁ {F₀ A γ} (f≡₁ {δ = idₛ}{σ = F₁ A (reflₛ Γ)}(F-id A ⁻¹))) r )) b₁ ∼ b₂)})
                                           ( λ { {a₁ , b₁}{a₂ , b₂} (p₁ , p₂) (q₁ , q₂) → Σ= ((F₀ A γ ∼prp) p₁ q₁)
                                                                                                                       ((F₀ B (γ , a₂) ∼prp) _ q₂)})
                                           (λ { {a , b} → (reflₛ (F₀ A γ)) , coe (∶∼≡₁ {F₀ B (γ , a)}
                                                                                                           (f≡₁ {δ = idₛ}{σ = (F₁ B (reflₛ Γ , coe (∶∼≡₁ {F₀ A γ} (f≡₁ (F-id A ⁻¹))) (reflₛ (F₀ A γ)))) }
                                                                                                                   (F-id B {γ , a} ⁻¹)))
                                                                                                 (reflₛ (F₀ B (γ , a)) {b}) })
                                           (λ { {a₁ , b₁}{a₂ , b₂} (p₁ , p₂) → (symₛ (F₀ A γ) p₁) , (symₛ (F₀ B (γ , a₁))
                                                                                                                                        {!!} )})
                                           (λ { {a₁ , b₁}{a₂ , b₂}{a₃ , b₃} (p₁ , p₂) (q₁ , q₂) → (transₛ (F₀ A γ) p₁ q₁) , {!!} }))
                             (λ {γ₁}{γ₂} r → {!!})
                             (λ {γ} → {!!})
                             (λ {γ₁}{γ₂}{γ₃} p q → {!!})



_[_]Tlem : {Γ Δ : Setoid} → FamSetoid Δ → Γ ⇒ₛ Δ → FamSetoid Γ
--Ulem : {Γ : Setoid} → FamSetoid Γ
Πlem : {Γ : Setoid} → (A : FamSetoid Γ) → FamSetoid (Γ ,c A) → FamSetoid Γ
_,s_lem : {Γ Δ : Setoid} → (δ : Γ ⇒ₛ Δ) → {A : FamSetoid Δ} → SectSetoid Γ (A [ δ ]Tlem) → Γ ⇒ₛ (Δ ,c A)
π₁lem : {Γ Δ : Setoid}{A : FamSetoid Δ} → Γ ⇒ₛ (Δ ,c A) → Γ ⇒ₛ Δ
_[_]tlem : {Γ Δ : Setoid}{A : FamSetoid Δ} → SectSetoid Δ A → (δ : Γ ⇒ₛ Δ) → SectSetoid Γ ( A [ δ ]Tlem )
π₂lem : {Γ Δ : Setoid}{A : FamSetoid Δ} → (δ : Γ ⇒ₛ (Δ ,c A)) → SectSetoid Γ ( A [ π₁lem {A = A} δ ]Tlem )
app-lem : {Γ : Setoid}{A : FamSetoid Γ}{B : FamSetoid (Γ ,c A)} → SectSetoid Γ (Πlem A B) → SectSetoid (Γ ,c A) B
lam-lem : {Γ : Setoid}{A : FamSetoid Γ}{B : FamSetoid (Γ ,c A)} → SectSetoid (Γ ,c A) B → SectSetoid Γ (Πlem A B)



m : Methods M
m = record
      { •ᴹ = ⊤ₛ
      ; _,Cᴹ_ = λ Γ A → Γ ,c A
      ; _[_]Tᴹ = λ A t → _[_]Tlem A t
      ; Uᴹ = {!!} --Ulem
      ; Πᴹ = λ A B → Πlem A B
      ; Elᴹ = λ t → {!!} --Ulem
      ; εᴹ = cₘ (λ _ → tt) (λ _ → refl)
      ; _,sᴹ_ = λ δ t → _,s_lem δ t
      ; idᴹ = idₛ
      ; _∘ᴹ_ = λ δ σ → δ ∘ₛ σ
      ; π₁ᴹ = λ {Γ}{Δ}{A} δ → π₁lem {A = A} δ
      ; _[_]tᴹ = λ t δ → _[_]tlem t δ
      ; π₂ᴹ = λ δ → π₂lem δ
      ; appᴹ = λ t → app-lem t
      ; lamᴹ = λ t → lam-lem t
      ; [id]Tᴹ = {!!}
      ; [][]Tᴹ = {!!}
      ; U[]ᴹ = {!!} --refl
      ; El[]ᴹ = refl
      ; Π[]ᴹ = {!!}
      ; idlᴹ = refl
      ; idrᴹ = refl
      ; assᴹ = refl
      ; ,∘ᴹ = {!!} --refl
      ; π₁βᴹ = {!!} --refl
      ; πηᴹ = {!!} --refl
      ; εηᴹ = {!!}
      ; [id]tᴹ = refl
      ; [][]tᴹ = refl
      ; π₂βᴹ = refl
      ; lam[]ᴹ = {!!} --DON'T PUT REFL IN - TAKES FOREVER
      ; Πβᴹ = {!!} --refl
      ; Πηᴹ = {!!} --refl
      }




_[_]Tlem {Γ}{Δ} A δ = cf (λ γ → F₀ A (f δ γ))
                                          (λ {γ₁}{γ₂} γ₁∼γ₂ → F₁ A (resp δ γ₁∼γ₂))
                                          (λ {γ} → ap (F₁ A) ( (Δ ∼prp) (resp δ (reflₛ Γ)) (reflₛ Δ)) ◾ F-id A)
                                          (λ {γ₁}{γ₂}{γ₃} p q → F-hom A (resp δ p) (resp δ q) ◾ ap (F₁ A) ( (Δ ∼prp) (transₛ Δ (resp δ p) (resp δ q)) (resp δ (transₛ Γ p q))))

_[_]tlem {Γ}{Δ}{A} t δ = cs (λ γ → s t (f δ γ))
                                               (λ γ₁ γ₂ γ₁∼γ₂ → p t (f δ γ₁) (f δ γ₂) (resp δ γ₁∼γ₂))

{-
Ulem {Γ} = cf (λ γ → Γ)
                        (λ {γ₁}{γ₂} γ₁∼γ₂ → idₛ)
                        refl
                        (λ p q → refl)
-}

-- this is easy but we don't know how to prove equality of record elements.
⇒ₛ-≡ : {Γ : Setoid}{Δ : Setoid} → (σ τ : Γ ⇒ₛ Δ) → (f σ ≡ f τ) → σ ≡ τ
⇒ₛ-≡ {Γ}{Δ} σ τ r = {!Σ= r  !}

-- ⇒ₛ-≡-funext :  {Γ : Setoid}{Δ : Setoid} → (σ τ : Γ ⇒ₛ Δ) → ((γ : ∣ Γ ∣) → f σ γ  ≡ f τ γ) → σ ≡ τ
-- ⇒ₛ-≡-funext = {!!}


Π-∙ : (Γ : Setoid) → FamSetoid Γ → Setoid
Π-∙ Γ A = cₛ (SectSetoid Γ A)                                                                                                                                    --set
                     (λ t₁ t₂ → (γ₁ γ₂ : ∣ Γ ∣) → (r : Γ ∶ γ₁ ∼ γ₂) → F₀ A γ₂ ∶ f (F₁ A r) (s t₁ γ₁) ∼ s t₂ γ₂)                                     --relation
                     (λ {t₁}{t₂} p q → funext (λ γ₁ →                                                                                                            --prp
                                                      funext (λ γ₂ →
                                                          funext (λ r → ((F₀ A γ₂) ∼prp) (p γ₁ γ₂ r) (q γ₁ γ₂ r)))))
                     (λ {t} γ₁ γ₂ r →  ≡∼ {F₀ A γ₂} (p t γ₁ γ₂ r))                                                                                            --refl
                     (λ {t₁}{t₂} rr γ₁ γ₂ r → symₛ (F₀ A γ₂)                                                                                                    --sym
                                                                    (coe (∶∼≡₁ {F₀ A γ₂}(p t₁ γ₁ γ₂ r) ◾ ∶∼≡₂ {F₀ A γ₂}(p t₂ γ₁ γ₂ r) ⁻¹)
                                                                             (rr γ₁ γ₂ r)))
                     (λ {t₁}{t₂}{t₃} rr₁ rr₂ γ₁ γ₂ r → coe (∶∼≡₁ {F₀ A γ₂} (p t₁ γ₁ γ₂ r) ⁻¹)                                                     --trans
                                                                               (transₛ (F₀ A γ₂)
                                                                                           (coe (∶∼≡₁ {F₀ A γ₂}(p t₁ γ₁ γ₂ r)) (rr₁ γ₁ γ₂ r))
                                                                                           (coe (∶∼≡₁ {F₀ A γ₂}(p t₂ γ₁ γ₂ r)) (rr₂ γ₁ γ₂ r))) )


ΠF₁ : {Γ : Setoid}{A : FamSetoid Γ}{B : FamSetoid (Γ ,c A)}{γ : ∣ Γ ∣}{x₁ x₂ : ∣ F₀ A γ ∣} → (r : F₀ A γ ∶ x₁ ∼ x₂) → F₀ B (γ , x₁) ⇒ₛ F₀ B (γ , x₂)
ΠF₁ {Γ}{A}{B}{γ}{x₁}{x₂} r = F₁ B ((reflₛ Γ) , (coe (∶∼≡₁ { F₀ A γ }  (f≡₁ {δ = F₁ A (reflₛ Γ)} {σ = idₛ} (F-id A) ⁻¹)) ) r)

ΠF-id : {Γ : Setoid}{A : FamSetoid Γ}{B : FamSetoid (Γ ,c A)}{γ : ∣ Γ ∣}{x : ∣ F₀ A γ ∣}
          → (ΠF₁ {Γ}{A}{B}{γ}{x}{x} (reflₛ (F₀ A γ))) ≡ (idₛ {F₀ B (γ , x)})
ΠF-id {Γ}{A}{B}{γ}{x} = 
   (ΠF₁ {Γ}{A}{B}{γ}{x}{x} (reflₛ (F₀ A γ)))
        ≡⟨ refl ⟩ 
   (F₁ B (reflₛ Γ , coe (∶∼≡₁ {F₀ A γ} (f≡₁ (F-id A) ⁻¹)) (reflₛ (F₀ A γ))))
        ≡⟨ ap (λ z → (F₁ B z)) (((Γ ,c A) ∼prp) (reflₛ Γ , coe (∶∼≡₁ {F₀ A γ} (f≡₁ (F-id A) ⁻¹)) (reflₛ (F₀ A γ))) (reflₛ (Γ ,c A )))  ⟩
   (F₁ B (reflₛ (Γ ,c A )))
        ≡⟨ F-id B ⟩
   (idₛ {F₀ B (γ , x)})
        ∎


lem : {Γ : Setoid}{A : FamSetoid Γ}{γ : ∣ Γ ∣}{x₁ x₂ x₃ : ∣ F₀ A γ ∣}{p : F₀ A γ ∶ x₁ ∼ x₂}{q : F₀ A γ ∶ x₂ ∼ x₃}
    → transₛ (Γ ,c A)
                     ((reflₛ Γ) , (coe (∶∼≡₁ { F₀ A γ }  (f≡₁ {δ = F₁ A (reflₛ Γ)} {σ = idₛ} (F-id A) ⁻¹)) p))
                     ((reflₛ Γ) , (coe (∶∼≡₁ { F₀ A γ }  (f≡₁ {δ = F₁ A (reflₛ Γ)} {σ = idₛ} (F-id A) ⁻¹)) q))
       ≡
       ((reflₛ Γ) , (coe (∶∼≡₁ { F₀ A γ }  (f≡₁ {δ = F₁ A (reflₛ Γ)} {σ = idₛ} (F-id A) ⁻¹)) ) (transₛ (F₀ A γ) p q ))
lem {Γ}{A}{γ}{x₁}{x₂}{x₃}{p}{q} = ((Γ ,c A) ∼prp)
                                                                            (transₛ (Γ ,c A)
                                                                                        ((reflₛ Γ) , ((coe (∶∼≡₁ { F₀ A γ }  (f≡₁ {δ = F₁ A (reflₛ Γ)} {σ = idₛ} (F-id A) ⁻¹)) p)))
                                                                                        ((reflₛ Γ) , ((coe (∶∼≡₁ { F₀ A γ }  (f≡₁ {δ = F₁ A (reflₛ Γ)} {σ = idₛ} (F-id A) ⁻¹)) q))))
                                                                            ((reflₛ Γ) , ((coe (∶∼≡₁ { F₀ A γ }  (f≡₁ {δ = F₁ A (reflₛ Γ)} {σ = idₛ} (F-id A) ⁻¹)) (transₛ (F₀ A γ) p q))))

ΠF-hom : {Γ : Setoid}{A : FamSetoid Γ}{B : FamSetoid (Γ ,c A)}{γ : ∣ Γ ∣}{x₁ x₂ x₃ : ∣ F₀ A γ ∣}{p : F₀ A γ ∶ x₁ ∼ x₂}{q : F₀ A γ ∶ x₂ ∼ x₃}
            → (ΠF₁ {Γ}{A}{B}{γ}{x₂}{x₃} q) ∘ₛ (ΠF₁ {Γ}{A}{B}{γ}{x₁}{x₂} p) ≡ (ΠF₁ {Γ}{A}{B}{γ}{x₁}{x₃} (transₛ (F₀ A γ) p q))
ΠF-hom {Γ}{A}{B}{γ}{x₁}{x₂}{x₃}{p}{q} =
  (ΠF₁ {Γ}{A}{B}{γ}{x₂}{x₃} q) ∘ₛ (ΠF₁ {Γ}{A}{B}{γ}{x₁}{x₂} p)
       ≡⟨ refl ⟩
  F₁ B ((reflₛ Γ) , (coe (∶∼≡₁ { F₀ A γ }  (f≡₁ {δ = F₁ A (reflₛ Γ)} {σ = idₛ} (F-id A) ⁻¹)) ) q) ∘ₛ F₁ B ((reflₛ Γ) , (coe (∶∼≡₁ { F₀ A γ }  (f≡₁ {δ = F₁ A (reflₛ Γ)} {σ = idₛ} (F-id A) ⁻¹)) ) p)
       ≡⟨ F-hom B ((reflₛ Γ) , (coe (∶∼≡₁ { F₀ A γ }  (f≡₁ {δ = F₁ A (reflₛ Γ)} {σ = idₛ} (F-id A) ⁻¹)) p)) ((reflₛ Γ) , (coe (∶∼≡₁ { F₀ A γ }  (f≡₁ {δ = F₁ A (reflₛ Γ)} {σ = idₛ} (F-id A) ⁻¹)) q))  ⟩
  F₁ B (transₛ (Γ ,c A)
                     ((reflₛ Γ) , (coe (∶∼≡₁ { F₀ A γ }  (f≡₁ {δ = F₁ A (reflₛ Γ)} {σ = idₛ} (F-id A) ⁻¹)) p))
                     ((reflₛ Γ) , (coe (∶∼≡₁ { F₀ A γ }  (f≡₁ {δ = F₁ A (reflₛ Γ)} {σ = idₛ} (F-id A) ⁻¹)) q)) )
       ≡⟨ F₁≡ {Γ ,c A}{B} (lem {Γ}{A}{γ}{x₁}{x₂}{x₃}{p}{q}) ⟩
  F₁ B ((reflₛ Γ) , (coe (∶∼≡₁ { F₀ A γ }  (f≡₁ {δ = F₁ A (reflₛ Γ)} {σ = idₛ} (F-id A) ⁻¹)) ) (transₛ (F₀ A γ) p q ))
       ≡⟨ refl ⟩
  (ΠF₁ {Γ}{A}{B}{γ}{x₁}{x₃} (transₛ (F₀ A γ) p q))
       ∎

-- No longer used
lemma : {Γ : Setoid}{A : FamSetoid Γ}{B : FamSetoid (Γ ,c A)}{γ₁ γ₂ : ∣ Γ ∣}{r : Γ ∶ γ₁ ∼ γ₂}{γ₁' : ∣ F₀ A γ₁ ∣}{γ₂' : ∣ F₀ A γ₂ ∣} → ∣ F₀ B (γ₁ , γ₁') ∣ → ∣ F₀ B (γ₂ , f (F₁ A r) γ₁') ∣
lemma {Γ}{A}{B}{γ₁}{γ₂}{r}{γ₁'}{γ₂'} x = f (F₁ B (r , (reflₛ (F₀ A γ₂)))) x



Πlem {Γ} A B = cf (λ γ → Π-∙ (F₀ A γ)
                                               (cf (λ a → F₀ B (γ , a))
                                                    (λ {x₁}{x₂} r → ΠF₁ {Γ}{A}{B}{γ}{x₁}{x₂} r )
                                                    (λ {x} → ΠF-id {Γ}{A}{B}{γ}{x})
                                                    (λ {x₁}{x₂}{x₃} p q → ΠF-hom {Γ}{A}{B}{γ}{x₁}{x₂}{x₃}{p}{q})))
                              (λ {γ₁}{γ₂} r → cₘ (λ { (cs s p) → cs (λ γ₂' → transport (λ γ → ∣ F₀ B (γ₂ , γ)∣) 
                                                                                                                    (f (F₁ A r) (f (F₁ A (symₛ Γ r)) γ₂') 
                                                                                                                       ≡⟨ refl ⟩
                                                                                                                    f ((F₁ A r) ∘ₛ (F₁ A (symₛ Γ r))) γ₂' 
                                                                                                                       ≡⟨ f≡₁ (F-hom A (symₛ Γ r) r) ⟩
                                                                                                                    f (F₁ A (transₛ Γ (symₛ Γ r) r)) γ₂'  
                                                                                                                       ≡⟨ f≡₁ (F₁≡ {Γ}{A} (tr-sy-re₂ {Γ}{γ₁}{γ₂}{r})) ⟩
                                                                                                                    f (F₁ A (reflₛ Γ)) γ₂'
                                                                                                                       ≡⟨ f≡₁ (F-id A) ⟩
                                                                                                                    f (idₛ {F₀ A γ₂}) γ₂'
                                                                                                                       ≡⟨ refl ⟩
                                                                                                                    γ₂' ∎ ) 
                                                                                                                    (f (F₁ B (r , (reflₛ (F₀ A γ₂)))) (s (f (F₁ A (symₛ Γ r)) γ₂'))))
                                                                                                                    --(lemma {Γ}{A}{B}{γ₁}{γ₂}{r}{(f (F₁ A (symₛ Γ r)) γ₂')}{γ₂'}(s (f (F₁ A (symₛ Γ r)) γ₂')))) --s (f (F₁ A (symₛ Γ r)) γ₂')
                                                                                        (λ a₂ a₂' ra → {!p ? ? (resp (F₁ A (symₛ Γ r)) ra)!})})          
                                                            {!!} )
                              (λ {γ} → {!!} )
                              (λ {γ₁}{γ₂}{γ₃} p q → {!!} )


{-  (λ γ → F₀ B (γ , {!!}))  --∣F₀∣))
                              (λ {γ₁}{γ₂} γ₁∼γ₂ → F₁ B (γ₁∼γ₂ , F₀∼ {a₁ = ∣F₀∣}{a₂ = ∣F₀∣}))
                              (F-id B)
                              (λ {γ₁}{γ₂}{γ₃} p q → F-hom B (p , F₀∼ {a₁ = ∣F₀∣}{a₂ = ∣F₀∣}) (q , F₀∼ {a₁ = ∣F₀∣}{a₂ = ∣F₀∣}))
-}


π₁lem {Γ}{Δ}{A} δ = cₘ (λ γ → proj₁ (f δ γ))
                                         (λ r → proj₁ (resp δ r))

π₂lem {Γ}{Δ}{A} δ = cs (λ γ → proj₂ (f δ γ)) 
                                         (λ γ₁ γ₂ r → {!!} )

{-
app-lem {Γ}{A}{B} t = cs (λ {(γ , a) → s {!t!} {!!}})
                                            (λ {(γ₁ , a₁) (γ₂ , a₂) γa₁∼γa₂ → p {!!} (f (F₁ A (proj₁ γa₁∼γa₂)) a₁) a₂ F₀∼ }) --(f (F₁ A {!proj₁ γa₁∼γa₂!}) a₁) a₂ (proj₂ γa₁∼γa₂)})

lam-lem {Γ}{A}{B} t = cs (λ γ → s t (γ , ∣F₀∣))
                                            (λ γ₁ γ₂ γ₁∼γ₂ → p t (γ₁ , ∣F₀∣) (γ₂ , ∣F₀∣) (γ₁∼γ₂ , F₀∼ {a₁ = ∣F₀∣}{a₂ = ∣F₀∣}))
-}

app-lem {Γ}{A}{B} t = cs (λ {(γ , a) → {!!}  })
                                            (λ {(γ₁ , a₁) (γ₂ , a₂) (r₁ , r₂) → {!!}})

lam-lem {Γ}{A}{B} t = cs (λ γ → {!!})
                                            (λ γ₁ γ₂ r → {!!})

_,s_lem {Γ}{Δ} δ {A} t = cₘ (λ γ → (f δ γ) , s t γ)
                                               (λ {γ₁}{γ₂} r → (resp δ r) , {!!}  ) -- p ? (f δ γ₁) (f δ γ₂) (resp δ r) 


