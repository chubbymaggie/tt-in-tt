{-# OPTIONS --no-eta #-}

open import Cats

module PSh.HTm (C : Cat) where

open import lib
open import JM.JM

open import PSh.Cxt C
open import PSh.Ty C
open import PSh.Tms C
open import PSh.Tm C
open import PSh.HTy C
open import PSh.HTms C
open import PSh.Exp C

open Cat C

----------------------------------------------------------------------
-- Substitution calculus
----------------------------------------------------------------------

abstract
  π₂βᴹ   : ∀{Γ Δ : PSh C}{A : FamPSh Δ}{δ : Γ →n Δ}{a : Γ →S A [ δ ]Tᴹ}
         → π₂ᴹ {A = A} (δ ,sᴹ a) ≡[ Γ→S= (A [ π₁βᴹ {a = a} ]Tᴹ=)  ]≡ a
  π₂βᴹ {A = A}{a = a} = from≃ ( uncoe (Γ→S= (A [ π₁βᴹ {a = a} ]Tᴹ=)) ⁻¹̃
                              ◾̃ ≃→S (ap (_[_]Tᴹ A) (π₁βᴹ {a = a})) r̃)

----------------------------------------------------------------------
-- Pi
----------------------------------------------------------------------

abstract
  Πβᴹ : {Γ : PSh C}{A : FamPSh Γ}{B : FamPSh (Γ ,Cᴹ A)}{t : (Γ ,Cᴹ A) →S B}
      → _≡_ {A = (Γ ,Cᴹ A) →S B} (appᴹ {Γ}{A}{B} (lamᴹ t)) t
  Πβᴹ {Γ}{A}{B}{t} = from≃ (≃→S refl
                                (funexti≃' (λ I →
                                   funext≃' (λ α →
                                       uncoe (ap (_$F_ B) (_,P_idP A)) ⁻¹̃
                                     ◾̃ t$S≃ {t = t} (,= (idP Γ) (idF A))))))

abstract
  Πηᴹ : {Γ : PSh C}{A : FamPSh Γ}{B : FamPSh (Γ ,Cᴹ A)}{t : Γ →S (Πᴹ A B)}
      → lamᴹ (appᴹ t) ≡ t
  Πηᴹ {Γ}{A}{B}{t} = from≃ (≃→S refl
                                (funexti≃' (λ I →
                                   funext≃' (λ α →
                                     ≃ExpPShΓAB {Γ}{A}{B} refl
                                                ( funexti≃' (λ J
                                                → funext≃' (λ f
                                                → funext≃' (λ x
                                                → uncoe ( ap (_$F_ B) (_,P_idP A)) ⁻¹̃
                                                        ◾̃ ( to≃ (ap {A = Πᴹ A B $F (Γ $P f $ α)}
                                                                    {B $F ( Γ $P idc $ (Γ $P f $ α)
                                                                          , A $F idc $ x)}
                                                                    (λ z → proj₁ z idc (A $F idc $ x))
                                                                    (natS t {f = f}{α} ⁻¹))
                                                          ◾̃ (uncoe (ap (_$F_ B)
                                                                       (,= (compP Γ)
                                                                           (coeap2 (compP Γ)))) ⁻¹̃
                                                          ◾̃ mapEABw≃ {Γ}{A}{B}{I}{α}{t $S α} idl
                                                                  ( uncoe (merge A) ⁻¹̃
                                                                  ◾̃ from≡ (ap (_$F_ A) (idP Γ))
                                                                           (idF A))))))))))))

abstract
  lam[]ᴹ : {Γ Δ : PSh C}{δ : Γ →n Δ}{A : FamPSh Δ}{B : FamPSh (Δ ,Cᴹ A)}
           {t : (Δ ,Cᴹ A) →S B}
         → lamᴹ t [ δ ]tᴹ
         ≡[ Γ→S= Π[]ᴹ ]≡
           lamᴹ (t [ δ ^ᴹ A ]tᴹ)
  lam[]ᴹ {Γ}{Δ}{δ}{A}{B}{t}
  
    = from≃ ( uncoe (Γ→S= Π[]ᴹ) ⁻¹̃
            ◾̃ ≃→S Π[]ᴹ
                       ( funexti≃' (λ I
                       → funext≃' (λ α
                       → ,≃ (funext≃ pΠ pΠeq)
                            p
                            ( funexti≃' (λ J
                            → funexti≃' (λ K
                            → funexti≃' (λ f
                            → funexti≃' (λ g
                            → funexti≃ (ap (_$F_ A) (natn δ))
                                       (λ x₂ → from≃ (≡≃ (B[^] {f = f}{g}{δ $n α}{α} refl x₂)
                                                         (B[^]$F {I}{δ $n α}{α} refl p f g x₂)
                                                         (A[] {I}{δ $n α}{α} refl p f g
                                                              ( $Ff$≃ A (natn δ) x₂
                                                              ◾̃ uncoe (ap (_$F_ A) (natn δ)))))
                                             , (UIP _ _)))))))))))
    where
      open Π[]ᴹ-lemmas {Γ}{Δ}{δ}{A}{B}

      abstract
        p : {I : Obj}{α : Γ $P I}
          → (λ {J : Obj}(f : I ⇒ J) x → t $S (Δ $P f $ (δ $n α) , x))
          ≃ (λ {J : Obj}(f : I ⇒ J) x → t $S ( δ $n (Γ $P f $ α)
                                             , coe (Γ→S= [][]Tᴹ)
                                                   (π₂ᴹ {A = A [ δ ]Tᴹ} idᴹ)
                                               $S (Γ $P f $ α , x)))
        p {I}{J}

          = funexti≃' (λ J
          → funext≃' (λ f
          → funext≃ (ap (_$F_ A) (natn δ))
                    (λ x₂ → t$S≃ {t = t}
                                 (,= (natn δ)
                                     (from≃ ( uncoe (ap (_$F_ A) (natn δ)) ⁻¹̃
                                            ◾̃ x₂
                                            ◾̃ coe$S [][]Tᴹ ⁻¹̃))))))

