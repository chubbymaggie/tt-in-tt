{-# OPTIONS --no-eta #-}

module PSh.PSh where

open import lib
open import Cats

-- the interpretation is parameterised by a base type U and a base
-- family El
module PShInt (C : Cat)(⟦U⟧ : PSh C)(⟦El⟧ : FamPSh ⟦U⟧) where

  open Cat C

  open import TT.Syntax
  open import TT.Rec
  open import JM.JM

  private
    M : Motives
    M = record
        { Conᴹ = PSh C
        ; Tyᴹ  = FamPSh
        ; Tmsᴹ = _→n_
        ; Tmᴹ  = _→S_
        }

  open import PSh.Cxt C
  open import PSh.Ty C
  open import PSh.Tms C
  open import PSh.Tm C
  open import PSh.HTy C
  open import PSh.HTms C
  open import PSh.HTm C

  private
    m : Methods M
    m = record
        { •ᴹ     = ∙ᴹ
        ; _,Cᴹ_  = _,Cᴹ_
        
        ; _[_]Tᴹ = _[_]Tᴹ
        ; Uᴹ     = Uᴹ ⟦U⟧
        ; Elᴹ    = Elᴹ ⟦El⟧
        ; Πᴹ     = Πᴹ
        
        ; εᴹ     = εᴹ
        ; _,sᴹ_  = _,sᴹ_
        ; idᴹ    = idᴹ
        ; _∘ᴹ_   = _∘ᴹ_
        ; π₁ᴹ    = λ {_}{_}{A} → π₁ᴹ {A = A}
        
        ; _[_]tᴹ = _[_]tᴹ
        ; π₂ᴹ    = π₂ᴹ
        ; appᴹ   = appᴹ
        ; lamᴹ   = lamᴹ
        
        ; [id]Tᴹ = [id]Tᴹ
        ; [][]Tᴹ = [][]Tᴹ
        ; U[]ᴹ   = U[]ᴹ ⟦U⟧
        ; El[]ᴹ  = El[]ᴹ ⟦El⟧
        ; Π[]ᴹ   = Π[]ᴹ
        
        ; idlᴹ   = idlᴹ
        ; idrᴹ   = idrᴹ
        ; assᴹ   = λ {_}{_}{_}{_}{σ}{δ}{ν} → assᴹ {σ = σ}{δ}{ν}
        ; ,∘ᴹ    = ,∘ᴹ
        ; π₁βᴹ   = λ {_}{_}{_}{_}{a} → π₁βᴹ {a = a}
        ; πηᴹ    = πηᴹ
        ; εηᴹ    = εηᴹ
        
        ; π₂βᴹ   = π₂βᴹ
        ; lam[]ᴹ = lam[]ᴹ
        ; Πβᴹ    = Πβᴹ
        ; Πηᴹ    = Πηᴹ
        }

  open rec M m

  ⟦_⟧C : Con → PSh C
  ⟦_⟧T : ∀{Γ} → Ty Γ → FamPSh ⟦ Γ ⟧C
  ⟦_⟧s : ∀{Γ Δ} → Tms Γ Δ → ⟦ Γ ⟧C →n ⟦ Δ ⟧C
  ⟦_⟧t : ∀{Γ}{A : Ty Γ} → (t : Tm Γ A) → ⟦ Γ ⟧C →S ⟦ A ⟧T

  ⟦_⟧C = RecCon
  ⟦_⟧T = RecTy
  ⟦_⟧s = RecTms
  ⟦_⟧t = RecTm

-- interestingly, all equality rules for the substitution calculus are
-- refl (upto function extensionality and coerces)
