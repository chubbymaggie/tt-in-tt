{-# OPTIONS --no-eta #-}

open import Cats

module PSh.HTms (C : Cat) where

open import lib
open import JM.JM

open import PSh.Cxt C
open import PSh.Ty C
open import PSh.Tms C
open import PSh.Tm C
open import PSh.HTy C

open Cat C

idlᴹ : {Γ Δ : PSh C}{δ : Γ →n Δ} → idᴹ ∘ᴹ δ ≡ δ
idlᴹ = =→n refl

idrᴹ : {Γ Δ : PSh C}{δ : Γ →n Δ} → δ ∘ᴹ idᴹ ≡ δ
idrᴹ = =→n refl

assᴹ : {Δ Γ Σ Ω : PSh C}{σ : Σ →n Ω}{δ : Γ →n Σ}{ν : Δ →n Γ}
     → (σ ∘ᴹ δ) ∘ᴹ ν ≡ σ ∘ᴹ (δ ∘ᴹ ν)
assᴹ = =→n refl

abstract
  ,∘ᴹ : {Γ Δ Σ : PSh C}{δ : Γ →n Δ}{σ : Σ →n Γ}{A : FamPSh Δ}{a : Γ →S (A [ δ ]Tᴹ)}
      → (δ ,sᴹ a) ∘ᴹ σ ≡ δ ∘ᴹ σ ,sᴹ coe (Γ→S= [][]Tᴹ) (a [ σ ]tᴹ)
  ,∘ᴹ {Γ}{Δ}{Σ}{δ}{σ}{A}{a} = =→n ( funexti (λ I
                                  → funext  (λ { α
                                  → ,= refl
                                       (from≃ (ap≃' {A = FamPSh Σ}
                                                    {B = _→S_ Σ}
                                                    (λ z → z $S α)
                                                    [][]Tᴹ 
                                                    (uncoe (Γ→S= [][]Tᴹ)) )) })))

π₁βᴹ : {Γ Δ : PSh C}{A : FamPSh Δ}{δ : Γ →n Δ}{a : Γ →S A [ δ ]Tᴹ}
     → π₁ᴹ {A = A} (δ ,sᴹ a) ≡ δ
π₁βᴹ = =→n refl

πηᴹ : {Γ Δ : PSh C}{A : FamPSh Δ}{δ : Γ →n (Δ ,Cᴹ A)}
    → π₁ᴹ {A = A} δ ,sᴹ π₂ᴹ {A = A} δ ≡ δ
πηᴹ = =→n refl

εηᴹ : {Γ : PSh C} {σ : Γ →n ∙ᴹ} → σ ≡ εᴹ
εηᴹ = =→n (funexti (λ I → refl))
