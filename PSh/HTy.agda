{-# OPTIONS --no-eta #-}

open import Cats

module PSh.HTy (C : Cat) where

open import lib
open import JM.JM

open import PSh.Cxt C
open import PSh.Exp C
open import PSh.Ty C
open import PSh.Tms C
open import PSh.Tm C

open Cat C

----------------------------------------------------------------------
-- Substitution calculus
----------------------------------------------------------------------

abstract
  [id]Tᴹ : {Γ : PSh C}{A : FamPSh Γ} → A [ idᴹ ]Tᴹ ≡ A
  [id]Tᴹ = =FamPSh refl refl

abstract
  [][]Tᴹ : {Γ Δ Σ : PSh C}{A : FamPSh Σ}{σ : Γ →n Δ}{δ : Δ →n Σ}
         → A [ δ ]Tᴹ [ σ ]Tᴹ ≡ A [ δ ∘ᴹ σ ]Tᴹ
  [][]Tᴹ {Γ}{Δ}{Σ}{A}{σ}{δ}

    = =FamPSh refl
              ( funexti (λ I
              → funexti (λ J
              → funext  (λ f
              → funexti (λ α
              → funext  (λ a
              → from≃ ( uncoe (ap (λ z → (_$F_ A) (δ $n z)) (natn σ)) ⁻¹̃
                      ◾̃ uncoe (ap (_$F_ A) (natn δ)) ⁻¹̃
                      ◾̃ uncoe (ap (_$F_ A) (natn δ ◾ ap (_$n_ δ) (natn σ))))))))))

----------------------------------------------------------------------
-- Base type and base family
----------------------------------------------------------------------

abstract
  U[]ᴹ : (U : PSh C){Γ Δ : PSh C}{δ : Γ →n Δ}
       → Uᴹ U [ δ ]Tᴹ ≡ Uᴹ U
  U[]ᴹ U {Γ}{Δ}{δ}

    = =FamPSh refl
              ( funexti (λ I
              → funexti (λ J
              → funext  (λ f
              → funexti (λ α
              → funext  (λ a
              → from≃ (uncoe (ap (λ _ → U $P J) (natn δ)) ⁻¹̃)))))))

abstract
  El[]ᴹ : {U : PSh C}(El : FamPSh U){Γ Δ : PSh C}{δ : Γ →n Δ}{Â : Δ →S Uᴹ U}
        → Elᴹ El Â [ δ ]Tᴹ ≡ Elᴹ El (coe (Γ→S= (U[]ᴹ U)) (Â [ δ ]tᴹ))
  El[]ᴹ {U} El {Γ}{Δ}{δ}{Â}

    = =FamPSh p
              (from≃ ( uncoe (ap (λ z → {I J : Obj}(f : I ⇒ J){α : Γ $P I}
                                      → z α → z (Γ $P f $ α)) p) ⁻¹̃  -- p
                     ◾̃ funexti≃' (λ I
                     → funexti≃' (λ J
                     → funext≃'  (λ f
                     → funexti≃' (λ α
                     → funext≃   (ap (λ z → z α) p)
                                 (λ {t₀}{t₁} t₂ → uncoe (ap (λ α₁ → (_$F_ El) (Â $S α₁)) (natn δ)) ⁻¹̃
                                                ◾̃ uncoe (ap (_$F_ El) (natS Â)) ⁻¹̃
                                                ◾̃ ap≃ {A = U $P I}
                                                      {_$F_ El}
                                                      (_$F_$_ El f)
                                                      q
                                                      t₂
                                                ◾̃ uncoe (ap (_$F_ El)
                                                            (natS (coe (Γ→S= (U[]ᴹ U))
                                                                       (Â [ δ ]tᴹ)))))))))))

    where

      abstract
        q : {I : Obj}{α : Γ $P I}
          → Â $S (δ $n α) ≡ coe (Γ→S= (U[]ᴹ U)) (Â [ δ ]tᴹ) $S α
        q {I}{α} = from≃ (ap≃' {A = FamPSh Γ}
                               {B = _→S_ Γ}
                               (λ z → z $S α)
                               (U[]ᴹ U)
                               (uncoe (Γ→S= (U[]ᴹ U))))
                        
        p : _≡_ {A = ∀{I} → Γ $P I → Set}
                (_$F_ (Elᴹ El Â [ δ ]Tᴹ))
                (_$F_ (Elᴹ El (coe (Γ→S= (U[]ᴹ U)) (Â [ δ ]tᴹ))))
        p = funexti (λ I
          → funext (λ α
          → ap (_$F_ El) q))

----------------------------------------------------------------------
-- Pi
----------------------------------------------------------------------

_^ᴹ_ : {Γ Δ : PSh C}(δ : Γ →n Δ)(A : FamPSh Δ) → (Γ ,Cᴹ A [ δ ]Tᴹ) →n (Δ ,Cᴹ A)
δ ^ᴹ A = (δ ∘ᴹ π₁ᴹ {A = A [ δ ]Tᴹ} idᴹ) ,sᴹ coe (Γ→S= [][]Tᴹ) (π₂ᴹ {A = A [ δ ]Tᴹ} idᴹ)

module Π[]ᴹ-lemmas
  {Γ Δ : PSh C}
  {δ : Γ →n Δ}
  {A : FamPSh Δ}
  {B : FamPSh (Δ ,Cᴹ A)}
  where

    abstract
      α₂nat : {I J : Obj}{f : I ⇒ J}
              {α₀ : Δ $P I}{α₁ : Γ $P I}(α₂ : α₀ ≡ δ $n α₁)
            → Δ $P f $ α₀ ≡ δ $n (Γ $P f $ α₁)
      α₂nat {I}{J}{f} α₂ = ap (_$P_$_ Δ f) α₂ ◾ natn δ

    abstract
      B[^] : {I J K : Obj}{f : I ⇒ J}{g : J ⇒ K}
             {α₀ : Δ $P I}{α₁ : Γ $P I}(α₂ : α₀ ≡ δ $n α₁)
             {x₀ : A $F (Δ $P f $ α₀)}{x₁ : A [ δ ]Tᴹ $F (Γ $P f $ α₁)}(x₂ : x₀ ≃ x₁)
           → B $F ((Δ ,Cᴹ A) $P g $ (Δ $P f $ α₀ , x₀))
           ≡ B [ δ ^ᴹ A ]Tᴹ  $F ((Γ ,Cᴹ A [ δ ]Tᴹ) $P g $ (Γ $P f $ α₁ , x₁))
      B[^] {I}{J}{K}{f}{g}{α₀}{α₁} α₂ x₂
      
        = ap (_$F_ B)
             (,= (ap (_$P_$_ Δ g) (ap (_$P_$_ Δ f) α₂ ◾ natn δ) ◾ natn δ)
                 (from≃ ( uncoe (ap (_$F_ A) (ap (_$P_$_ Δ g) (α₂nat α₂) ◾ natn δ)) ⁻¹̃
                        ◾̃ $Ff$≃ A (α₂nat α₂) x₂
                        ◾̃ uncoe (ap (_$F_ A) (natn δ))
                        ◾̃ coe$S [][]Tᴹ ⁻¹̃)))

    abstract
      B[^]' : {I : Obj}{α₀ : Δ $P I}{α₁ : Γ $P I}(α₂ : α₀ ≡ δ $n α₁)
              {J : Obj}{f : I ⇒ J}
            → (λ x → B              $F (Δ $P f $ α₀ , x))
            ≃ (λ x → B [ δ ^ᴹ A ]Tᴹ $F (Γ $P f $ α₁ , x))
      B[^]' {I}{α₀}{α₁} α₂ {J}{f}

        = funext≃ (ap (_$F_ A) (α₂nat α₂))
                  (λ x₂ → to≃ (ap (_$F_ B)
                                  (,= (ap (_$P_$_ Δ f) α₂ ◾ natn δ)
                                      (from≃ ( uncoe (ap (_$F_ A) (ap (_$P_$_ Δ f) α₂ ◾ natn δ)) ⁻¹̃
                                             ◾̃ x₂
                                             ◾̃ coe$S [][]Tᴹ ⁻¹̃)))))

    abstract
      wapp : {I : Obj}{α₀ : Δ $P I}{α₁ : Γ $P I}(α₂ : α₀ ≡ δ $n α₁)
             {w₀ : {J : Obj}(f : I ⇒ J)(x : A         $F (Δ $P f $ α₀)) → B              $F (Δ $P f $ α₀ , x)}
             {w₁ : {J : Obj}(f : I ⇒ J)(x : A [ δ ]Tᴹ $F (Γ $P f $ α₁)) → B [ δ ^ᴹ A ]Tᴹ $F (Γ $P f $ α₁ , x)}
             (w₂ : (λ {J} → w₀ {J}) ≃ (λ {J} → w₁ {J}))
             {J : Obj}{f : I ⇒ J}
             {x₀ : A         $F (Δ $P f $ α₀)}
             {x₁ : A [ δ ]Tᴹ $F (Γ $P f $ α₁)}(x₂ : x₀ ≃ x₁)
           → w₀ f x₀ ≃ w₁ f x₁
      wapp {I}{α₀}{α₁} α₂ {w₀}{w₁} w₂ {J}{f}{x₀}{x₁} x₂

        = apd≃' {A₀ = A         $F (Δ $P f $ α₀)}
                {A₁ = A [ δ ]Tᴹ $F (Γ $P f $ α₁)}
                (B[^]' α₂)
                (ap≃'' {C₀ = λ J f → (x : A         $F (Δ $P f $ α₀)) → B              $F (Δ $P f $ α₀ , x)}
                       {C₁ = λ J f → (x : A [ δ ]Tᴹ $F (Γ $P f $ α₁)) → B [ δ ^ᴹ A ]Tᴹ $F (Γ $P f $ α₁ , x)}
                       (funext (λ J → funext (λ f → →≃ (ap (_$F_ A) (α₂nat α₂)) (B[^]' α₂))))
                       w₂
                       {J}
                       {f})
                x₂

    abstract
      B[^]$F : {I : Obj}{α₀ : Δ $P I}{α₁ : Γ $P I}(α₂ : α₀ ≡ δ $n α₁)
               {w₀ : {J : Obj}(f : I ⇒ J)(x : A         $F (Δ $P f $ α₀)) → B              $F (Δ $P f $ α₀ , x)}
               {w₁ : {J : Obj}(f : I ⇒ J)(x : A [ δ ]Tᴹ $F (Γ $P f $ α₁)) → B [ δ ^ᴹ A ]Tᴹ $F (Γ $P f $ α₁ , x)}
               (w₂ : (λ {J} → w₀ {J}) ≃ (λ {J} → w₁ {J}))
               {J : Obj}(f : I ⇒ J)
               {K : Obj}(g : J ⇒ K)
               {x₀ : A         $F (Δ $P f $ α₀)}
               {x₁ : A [ δ ]Tᴹ $F (Γ $P f $ α₁)}(x₂ : x₀ ≃ x₁)
             → B $F g $ w₀ f x₀ ≃ B [ δ ^ᴹ A ]Tᴹ $F g $ w₁ f x₁
      B[^]$F {I}{α₀}{α₁} α₂ {w₀}{w₁} w₂ {J} f {K} g x₂

        = $Ff$≃ B
                (,= (ap (_$P_$_ Δ f) α₂ ◾ natn δ)
                    (from≃ ( uncoe (ap (_$F_ A) (ap (_$P_$_ Δ f) α₂ ◾ natn δ)) ⁻¹̃
                           ◾̃ x₂
                           ◾̃ coe$S [][]Tᴹ ⁻¹̃)))
                (wapp α₂ w₂ x₂)
        ◾̃ uncoe (ap (_$F_ B) (natn (δ ^ᴹ A)))

    abstract
      A[] : {I : Obj}{α₀ : Δ $P I}{α₁ : Γ $P I}(α₂ : α₀ ≡ δ $n α₁)
            {w₀ : {J : Obj}(f : I ⇒ J)(x : A         $F (Δ $P f $ α₀)) → B              $F (Δ $P f $ α₀ , x)}
            {w₁ : {J : Obj}(f : I ⇒ J)(x : A [ δ ]Tᴹ $F (Γ $P f $ α₁)) → B [ δ ^ᴹ A ]Tᴹ $F (Γ $P f $ α₁ , x)}
            (w₂ : (λ {J} → w₀ {J}) ≃ (λ {J} → w₁ {J}))
            {J : Obj}(f : I ⇒ J)
            {K : Obj}(g : J ⇒ K)
            {x₀ : A $F (Δ $P g $ (Δ $P f $ α₀))}
            {x₁ : A $F (δ $n (Γ $P g $ (Γ $P f $ α₁)))}
            (x₂ : x₀ ≃ x₁)
          → coe (unmerge A B)
                (w₀ (g ∘c f) (coe (merge A) x₀))
          ≃ coe (unmerge (A [ δ ]Tᴹ) (B [ δ ^ᴹ A ]Tᴹ))
                (w₁ (g ∘c f) (coe (merge (A [ δ ]Tᴹ)) x₁))
      A[] {I}{α₀}{α₁} α₂ {w₀}{w₁} w₂ {J} f {K} g x₂

        = uncoe (unmerge A B) ⁻¹̃
        ◾̃ wapp α₂
               w₂
               ( uncoe (merge A) ⁻¹̃
               ◾̃ x₂
               ◾̃ uncoe (merge (A [ δ ]Tᴹ)))
        ◾̃ uncoe (unmerge (A [ δ ]Tᴹ) (B [ δ ^ᴹ A ]Tᴹ))

    abstract
      wapp' : {I : Obj}{α₀ : Δ $P I}{α₁ : Γ $P I}(α₂ : α₀ ≡ δ $n α₁)
              {w₀ : {J : Obj}(f : I ⇒ J)(x : A         $F (Δ $P f $ α₀)) → B              $F (Δ $P f $ α₀ , x)}
              {w₁ : {J : Obj}(f : I ⇒ J)(x : A [ δ ]Tᴹ $F (Γ $P f $ α₁)) → B [ δ ^ᴹ A ]Tᴹ $F (Γ $P f $ α₁ , x)}
              (w₂ : (λ {J} → w₀ {J}) ≃ (λ {J} → w₁ {J}))
              {J : Obj}(f : I ⇒ J)
              {K : Obj}(g : J ⇒ K)
              {x₀ : A         $F (Δ $P f $ α₀)}
              {x₁ : A [ δ ]Tᴹ $F (Γ $P f $ α₁)}(x₂ : x₀ ≃ x₁)
            → coe (unmerge A B)
                  (w₀ (g ∘c f) (coe (merge A) (A $F g $ x₀)))
            ≃ coe (unmerge (A [ δ ]Tᴹ) (B [ δ ^ᴹ A ]Tᴹ))
                  (w₁ (g ∘c f) (coe (merge (A [ δ ]Tᴹ)) (A [ δ ]Tᴹ $F g $ x₁)))
      wapp' α₂ w₂ f g x₂ = A[] α₂ w₂ f g ( $Ff$≃ A (α₂nat α₂) x₂
                                         ◾̃ uncoe (ap (_$F_ A) (natn δ)))

    abstract
      pΠ : {I : Obj}{α : Γ $P I}
         → ({J : Obj}(f : I ⇒ J)(x : A         $F (Δ $P f $ (δ $n α))) → B              $F (Δ $P f $ (δ $n α) , x))
         ≡ ({J : Obj}(f : I ⇒ J)(x : A [ δ ]Tᴹ $F (Γ $P f $ α))        → B [ δ ^ᴹ A ]Tᴹ $F (Γ $P f $ α , x))
      pΠ {I}{α}

        = →i≃ refl
              (funext≃' (λ J → to≃ (→≃ refl (funext≃' (λ f → to≃
                (→≃ (ap (_$F_ A) (natn δ))
                    (funext≃ (ap (_$F_ A) (natn δ))
                             (λ x₂ → to≃ (ap (_$F_ B)
                                             (,= (natn δ)
                                                 (from≃ ( uncoe (ap (_$F_ A) (natn δ)) ⁻¹̃
                                                        ◾̃ x₂
                                                        ◾̃ coe$S [][]Tᴹ ⁻¹̃))))))))))))

    abstract
      pΠeq : {I : Obj}{α : Γ $P I}
             {w₀ : {J : Obj}(f : I ⇒ J)(x : A         $F (Δ $P f $ (δ $n α))) → B              $F (Δ $P f $ (δ $n α) , x)}
             {w₁ : {J : Obj}(f : I ⇒ J)(x : A [ δ ]Tᴹ $F (Γ $P f $ α))        → B [ δ ^ᴹ A ]Tᴹ $F (Γ $P f $ α , x)}
             (w₂ : (λ {J} → w₀ {J}) ≃ (λ {J} → w₁ {J}))
           → ( {J K : Obj}{f : I ⇒ J}{g : J ⇒ K}{a : A $F (Δ $P f $ (δ $n α))}
             → B $F g $ w₀ f a
             ≡ coe (unmerge A B) (w₀ (g ∘c f) (coe (merge A) (A $F g $ a))))
           ≃ ( {J K : Obj}{f : I ⇒ J}{g : J ⇒ K}{a : A [ δ ]Tᴹ $F (Γ $P f $ α)}
             → B [ δ ^ᴹ A ]Tᴹ $F g $ w₁ f a
             ≡ coe (unmerge (A [ δ ]Tᴹ) (B [ δ ^ᴹ A ]Tᴹ))
                   (w₁ (g ∘c f)
                       (coe (merge (A [ δ ]Tᴹ)) (A [ δ ]Tᴹ $F g $ a))))
      pΠeq {I}{α} w₂

        = to≃ (→i≃ refl
                   (funext≃' (λ J → to≃ (→i≃ refl
                   (funext≃' (λ K → to≃ (→i≃ refl
                   (funext≃' (λ f → to≃ (→i≃ refl
                   (funext≃' (λ g → to≃ (→i≃ (ap (_$F_ A) (natn δ))
                     (funext≃ (ap (_$F_ A) (natn δ))
                       (λ x₂ → ≡≃ (B[^] {f = f}{g}{δ $n α}{α} refl x₂)
                                  (B[^]$F {I}{δ $n α}{α} refl w₂ f g x₂)
                                  (wapp' {I}{δ $n α}{α} refl w₂ f g x₂))))))))))))))))

    abstract                                                          
      pΠᴹ : _≡_ {A = {I : Obj} → Γ $P I → Set}
                (_$F_ (Πᴹ A B [ δ ]Tᴹ))
                (_$F_ (Πᴹ (A [ δ ]Tᴹ) (B [ δ ^ᴹ A ]Tᴹ)))
      pΠᴹ = funexti (λ I → funext (λ α → Σ≃ pΠ (funext≃ pΠ pΠeq)))

    abstract
      map≃ : {I : Obj}{α : Γ $P I}
             {w₀ : ExpPSh A B (δ $n α)}{w₁ : ExpPSh (A [ δ ]Tᴹ) (B [ δ ^ᴹ A ]Tᴹ) α}(w₂ : w₀ ≃ w₁)
           → (λ {J} → mapE A B {I}{δ $n α} w₀ {J})
           ≃ (λ {J} → mapE (A [ δ ]Tᴹ)(B [ δ ^ᴹ A ]Tᴹ){I}{α} w₁ {J})
      map≃ {I}{α} (p₁ , p₂)

        = pΠ , from≃ ( uncoe pΠ ⁻¹̃
                     ◾̃ proj₁coe
                         pΠ
                         (from≃ ( uncoe (ap (λ z → z → Set) pΠ) ⁻¹̃
                                ◾̃ funext≃ pΠ
                                          (λ {w₀}{w₁} w₂
                                          → to≃ (→i≃ refl (funext≃' (λ J
                                          → to≃ (→i≃ refl (funext≃' (λ K
                                          → to≃ (→i≃ refl (funext≃' (λ f
                                          → to≃ (→i≃ refl (funext≃' (λ g
                                          → to≃ (→i≃ (ap (_$F_ A) (natn δ))
                                                     (funext≃ (ap (_$F_ A) (natn δ))
                                                              (λ {x₀}{x₁} x₂
                                                              → ≡≃ (B[^]   {f = f}{g}{δ $n α}{α} refl x₂)
                                                                   (B[^]$F {I}{δ $n α}{α} refl w₂ f g x₂)
                                                                   (wapp'  {I}{δ $n α}{α} refl w₂ f g x₂)))))))))))))))))))
                         p₁
                     ◾̃ uncoe (ap (λ v → {J₁ : Obj} (f : I ⇒ J₁) (x : A [ δ ]Tᴹ $F (Γ $P f $ α)) → B [ δ ^ᴹ A ]Tᴹ $F (Γ $P f $ α , x)) p₂)
                     ◾̃ to≃ (apd proj₁ p₂))

    abstract
      B[^]$F' : {I J K L : Obj}{f : I ⇒ J}{g : J ⇒ K}{h : K ⇒ L}
          {α : Γ $P I}
          {w₀ : Πᴹ A B [ δ ]Tᴹ                  $F α}
          {w₁ : Πᴹ (A [ δ ]Tᴹ) (B [ δ ^ᴹ A ]Tᴹ) $F α}
          (w₂ : w₀ ≃ w₁)
          {x₀ : A $F ((Δ $P g $ (Δ $P f $ (δ $n α))))}
          {x₁ : A $F (δ $n (Γ $P g $ (Γ $P f $ α)))}
          (x₂ : x₀ ≃ x₁)
         →
         (B $F h $
           coe (unmerge A {a = x₀} B) (mapE A B w₀ (g ∘c f) (coe (merge A) x₀))
           ≡
           coe (unmerge A B)
           (coe (unmerge A B)
            (mapE A B w₀ ((h ∘c g) ∘c f)
             (coe (merge A) (coe (merge A) (A $F h $ x₀))))))
          ≃
          (B [ δ ^ᴹ A ]Tᴹ $F h $
           coe (unmerge (A [ δ ]Tᴹ) (B [ δ ^ᴹ A ]Tᴹ))
           (mapE (A [ δ ]Tᴹ)(B [ δ ^ᴹ A ]Tᴹ) w₁ (g ∘c f) (coe (merge (A [ δ ]Tᴹ)) x₁))
           ≡
           coe (unmerge (A [ δ ]Tᴹ) (B [ δ ^ᴹ A ]Tᴹ))
           (coe (unmerge (A [ δ ]Tᴹ) (B [ δ ^ᴹ A ]Tᴹ))
            (mapE (A [ δ ]Tᴹ)(B [ δ ^ᴹ A ]Tᴹ) w₁ ((h ∘c g) ∘c f)
             (coe (merge (A [ δ ]Tᴹ))
              (coe (merge (A [ δ ]Tᴹ)) (A [ δ ]Tᴹ $F h $ x₁))))))
      B[^]$F' {I}{J}{K}{L}{f}{g}{h}{α}{w₀}{w₁} w₂ {x₀}{x₁} x₂

        = ≡≃ (B[^] {f = g}{h}{Δ $P f $ (δ $n α)}{Γ $P f $ α}(natn δ) x₂)
             ( $Ff$≃ B
                     (,= (ap (_$P_$_ Δ g) (natn δ) ◾ natn δ)
                         ( from≃ (uncoe (ap (_$F_ A) (ap (_$P_$_ Δ g) (natn δ) ◾ natn δ)) ⁻¹̃
                         ◾̃ x₂
                         ◾̃ coe$S [][]Tᴹ ⁻¹̃)))
                     ( uncoe (unmerge A B) ⁻¹̃
                     ◾̃ wapp {I}{δ $n α}{α} refl (map≃ w₂) {K}{g ∘c f}
                            ( uncoe (merge A) ⁻¹̃
                            ◾̃ x₂
                            ◾̃ uncoe (merge (A [ δ ]Tᴹ)))
                     ◾̃ uncoe (unmerge (A [ δ ]Tᴹ) (B [ δ ^ᴹ A ]Tᴹ)))
             ◾̃ uncoe (ap (_$F_ B) (natn (δ ^ᴹ A))))
             ( uncoe (unmerge A B) ⁻¹̃
             ◾̃ uncoe (unmerge A B) ⁻¹̃
             ◾̃ wapp {I}{δ $n α}{α} refl (map≃ w₂) {L}{(h ∘c g) ∘c f}
                    ( uncoe (merge A) ⁻¹̃
                    ◾̃ uncoe (merge A) ⁻¹̃
                    ◾̃ $Ff$≃ A (ap (_$P_$_ Δ g) (natn δ) ◾ natn δ) x₂
                    ◾̃ uncoe (ap (_$F_ A) (natn δ))
                    ◾̃ uncoe (merge (A [ δ ]Tᴹ))
                    ◾̃ uncoe (merge (A [ δ ]Tᴹ)))
             ◾̃ uncoe (unmerge (A [ δ ]Tᴹ) (B [ δ ^ᴹ A ]Tᴹ))
             ◾̃ uncoe (unmerge (A [ δ ]Tᴹ) (B [ δ ^ᴹ A ]Tᴹ)))

abstract
  Π[]ᴹ : {Γ Δ : PSh C}{δ : Γ →n Δ}{A : FamPSh Δ}{B : FamPSh (Δ ,Cᴹ A)}
       → Πᴹ A B [ δ ]Tᴹ
       ≡ Πᴹ (A [ δ ]Tᴹ) (B [ δ ^ᴹ A ]Tᴹ)
  Π[]ᴹ {Γ}{Δ}{δ}{A}{B}

    = =FamPSh
        pΠᴹ
        (from≃ ( uncoe (ap (λ z → {I : Obj}
                                  {J : Obj}
                                  (f : I ⇒ J)
                                  {α : Γ $P I}
                                → z α
                                → z (Γ $P f $ α)) pΠᴹ) ⁻¹̃
               ◾̃ funexti≃' (λ I
               → funexti≃' (λ J
               → funext≃' (λ f
               → funexti≃' (λ α
               → funext≃
                   (ap (λ z → z α) pΠᴹ)
                   (λ w₂
                   → uncoe (ap (_$F_ (Πᴹ A B)) (natn δ)) ⁻¹̃
                   ◾̃ ,≃ (funext≃
                           (→i≃ refl (funext≃' (λ K → to≃ (→≃ refl (funext≃' (λ g → to≃ (→≃
                             (ap (_$F_ A) (ap (_$P_$_ Δ g) (natn δ) ◾ natn δ))
                             (funext≃ (ap (_$F_ A) (ap (_$P_$_ Δ g) (natn δ) ◾ natn δ))
                                      (λ y₂ → to≃ (ap (_$F_ B)
                                                      (,= (ap (_$P_$_ Δ g) (natn δ) ◾ natn δ)
                                                          (from≃ ( uncoe (ap (_$F_ A)
                                                                             ( ap (_$P_$_ Δ g)
                                                                                 (natn δ)
                                                                             ◾ natn δ)) ⁻¹̃
                                                                 ◾̃ y₂
                                                                 ◾̃ coe$S [][]Tᴹ ⁻¹̃)))))))))))))
                           (λ w₂ → to≃ (→i≃ refl (funext≃' (λ K
                                 → to≃ (→i≃ refl (funext≃' (λ L
                                 → to≃ (→i≃ refl (funext≃' (λ g
                                 → to≃ (→i≃ refl (funext≃' (λ h
                                 → to≃ (→i≃ (ap (_$F_ A) (ap (_$P_$_ Δ g) (natn δ) ◾ natn δ))
                                            (funext≃
                                               (ap (_$F_ A) (ap (_$P_$_ Δ g) (natn δ) ◾ natn δ))
                                               (λ y₂ → ≡≃ (B[^]   {f = g}{h}{Δ $P f $ (δ $n α)}{Γ $P f $ α}(natn δ) y₂)
                                                          (B[^]$F {J}{Δ $P f $ (δ $n α)}{Γ $P f $ α} (natn δ) w₂ g h y₂)
                                                          (wapp'  {J}{Δ $P f $ (δ $n α)}{Γ $P f $ α} (natn δ) w₂ g h y₂))))))))))))))))))
                         ( funexti≃' (λ K → funext≃' (λ g
                         → funext≃ (ap (_$F_ A)
                                       (ap (_$P_$_ Δ g) (natn δ) ◾ natn δ))
                                   (λ x₂ → A[] {I}{δ $n α}{α} refl (map≃ w₂) f g x₂))))
                         ( funexti≃' (λ K
                         → funexti≃' (λ L
                         → funexti≃' (λ g
                         → funexti≃' (λ h
                         → funexti≃ (ap (_$F_ A) (ap (_$P_$_ Δ g) (natn δ) ◾ natn δ))
                                    (λ x₂ → from≃ (B[^]$F' {I}{J}{K}{L}{f}{g}{h}{α} w₂ x₂)
                                          , (UIP _ _))))))))))))))

    where
      open Π[]ᴹ-lemmas {Γ}{Δ}{δ}{A}{B}
