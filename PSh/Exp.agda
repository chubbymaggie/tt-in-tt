{-# OPTIONS --no-eta #-}

open import Cats

module PSh.Exp (C : Cat) where

open import lib
open import JM.JM

open Cat C

open import PSh.Cxt C

----------------------------------------------------------------------
-- Helper equalities
----------------------------------------------------------------------

merge : {Γ : PSh C}(A : FamPSh Γ){I J K : Obj}{f : I ⇒ J}{g : J ⇒ K}{α : Γ $P I}
      → A $F (Γ $P g $ (Γ $P f $ α))
      ≡ A $F (Γ $P (g ∘c f) $ α)
merge {Γ} A = ap (_$F_ A) (compP Γ ⁻¹)

unmerge : {Γ : PSh C}(A : FamPSh Γ){I J K : Obj}{f : I ⇒ J}{g : J ⇒ K}{α : Γ $P I}
          {a : A $F (Γ $P g $ (Γ $P f $ α))}(B : FamPSh (Γ ,Cᴹ A))
        → B $F (Γ $P (g ∘c f) $ α     , coe (merge A) a)
        ≡ B $F (Γ $P g $ (Γ $P f $ α) , a)
unmerge {Γ} A B = ap (_$F_ B) (,= (compP Γ) (coeap2 (compP Γ)))

----------------------------------------------------------------------
-- Presheaf exponential (E)
----------------------------------------------------------------------

ExpPSh : {Γ : PSh C}(A : FamPSh Γ)(B : FamPSh (Γ ,Cᴹ A)){I : Obj}(α : Γ $P I) → Set
ExpPSh {Γ} A B {I} α

  = Σ ({J : Obj}(f : I ⇒ J)(x : A $F (Γ $P f $ α)) → B $F (Γ $P f $ α , x))
      (λ map → {J K : Obj}{f : I ⇒ J}{g : J ⇒ K}{a : A $F (Γ $P f $ α)}
             → B $F g $ map f a ≡ coe (unmerge A B) (map (g ∘c f) (coe (merge A) (A $F g $ a))))

mapE : {Γ : PSh C}(A : FamPSh Γ)(B : FamPSh (Γ ,Cᴹ A)){I : Obj}{α : Γ $P I}
     → ExpPSh A B α
     → {J : Obj}(f : I ⇒ J)(x : A $F (Γ $P f $ α)) → B $F (Γ $P f $ α , x)
mapE A B = proj₁

natE : {Γ : PSh C}(A : FamPSh Γ)(B : FamPSh (Γ ,Cᴹ A)){I : Obj}{α : Γ $P I}
       (w : ExpPSh A B α)
     → ({J K : Obj}{f : I ⇒ J}{g : J ⇒ K}{a : A $F (Γ $P f $ α)}
       → B $F g $ mapE A B w f a ≡ coe (unmerge A B) (mapE A B w (g ∘c f) (coe (merge A) (A $F g $ a))))
natE A B = proj₂

----------------------------------------------------------------------
-- Equality constructors
----------------------------------------------------------------------

abstract
  =ExpPShΓAB : {Γ : PSh C}{A : FamPSh Γ}{B : FamPSh (Γ ,Cᴹ A)}
               {I : Obj}{α : Γ $P I}{w₀ w₁ : ExpPSh A B α}
               (mapw₂ : _≡_ (λ {J} → mapE A B w₀ {J})
                            (λ {J} → mapE A B w₁ {J}))
             → w₀ ≡ w₁
  =ExpPShΓAB {w₀ = _,_ map₀ natE₀} {_,_ .map₀ natE₁} refl
  
    = ap (_,_ (λ {J} → map₀ {J}))
         ( funexti (λ J
         → funexti (λ K
         → funexti (λ f
         → funexti (λ g
         → funexti (λ a
         → UIP natE₀ natE₁))))))

abstract
  ≃ExpPShΓAB : {Γ : PSh C}{A : FamPSh Γ}{B : FamPSh (Γ ,Cᴹ A)}{I : Obj}
               {α₀ α₁ : Γ $P I}(α₂ : α₀ ≡ α₁)
               {w₀ : ExpPSh A B α₀}{w₁ : ExpPSh A B α₁}
               (mapw₂ : _≃_ (λ {J} → mapE A B w₀ {J}) (λ {J} → mapE A B w₁ {J}))
             → w₀ ≃ w₁
  ≃ExpPShΓAB refl {w₀ = _,_ map₀ Enat₀} {_,_ .map₀ Enat₁} (refl , refl)
  
    = refl , ap (_,_ (λ {J} → map₀ {J}))
                ( funexti (λ J
                → funexti (λ K
                → funexti (λ f
                → funexti (λ g
                → funexti (λ a
                → UIP Enat₀ Enat₁))))))

----------------------------------------------------------------------
-- Congruence
----------------------------------------------------------------------

mapEABw≃ : {Γ : PSh C}{A : FamPSh Γ}{B : FamPSh (Γ ,Cᴹ A)}{I : Obj}{α : Γ $P I}
           {w : ExpPSh A B α}{J : Obj}{f₀ f₁ : I ⇒ J}(f₂ : f₀ ≡ f₁)
           {x₀ : A $F (Γ $P f₀ $ α)}{x₁ : A $F (Γ $P f₁ $ α)}(x₂ : x₀ ≃ x₁)
         → mapE A B w f₀ x₀ ≃ mapE A B w f₁ x₁
mapEABw≃ refl (refl , refl) = refl , refl

----------------------------------------------------------------------
-- Lifting a presheaf exponential by a morphism
----------------------------------------------------------------------

liftExpPSh : {Γ : PSh C}(A : FamPSh Γ)(B : FamPSh (Γ ,Cᴹ A))
             {I J : Obj}(f : I ⇒ J){α : Γ $P I}
           → ExpPSh A B {I} α → ExpPSh A B {J} (Γ $P f $ α)
liftExpPSh {Γ} A B {I}{J} f {α} w

  = _,_ (λ g x → coe (unmerge A B) (mapE A B w (g ∘c f) (coe (merge A) x)))
        (liftnat {w = w})
      
  where
    abstract
      liftnat : {I J : Obj}{f : I ⇒ J}{α : Γ $P I}{w : ExpPSh A B α}
                {K L : Obj}{g : J ⇒ K}{h : K ⇒ L}{a : A $F (Γ $P g $ (Γ $P f $ α))}
              → B $F h $ coe (unmerge A B) (mapE A B w (g ∘c f) (coe (merge A) a))
              ≡ coe (unmerge A B)
                    (coe (unmerge A B)
                         (mapE A B w
                              ((h ∘c g) ∘c f)
                              (coe (merge A) (coe (merge A) (A $F h $ a)))))
      liftnat {I}{J}{f}{α}{w}{K}{L}{g}{h}{a}

        = from≃ ( B $F h $ coe (unmerge A B) (mapE A B w (g ∘c f) (coe (merge A) a))
                ≃⟨ $Ff$≃ B (,= (compP Γ ⁻¹) refl) (uncoe (unmerge A B) ⁻¹̃) ⟩
                  B $F h $ mapE A B w (g ∘c f) (coe (merge A) a)
                ≃⟨ to≃ (natE A B w) ⟩
                  coe (unmerge A B)
                      (mapE A B
                           w
                           (h ∘c (g ∘c f))
                           (coe (merge A) (A $F h $ coe (merge A) a)))
                ≃⟨ uncoe (unmerge A B) ⁻¹̃ ⟩
                  mapE A B
                      w
                      (h ∘c (g ∘c f))
                      (coe (merge A) (A $F h $ coe (merge A) a))
                ≃⟨ mapEABw≃ {Γ}{A}{B}{w = w} (ass ⁻¹) ( uncoe (merge A) ⁻¹̃
                                                   ◾̃ $Ff$≃ A (compP Γ) (uncoe (merge A) ⁻¹̃)
                                                   ◾̃ uncoe (merge A) ◾̃ uncoe (merge A)) ⟩
                  mapE A B
                      w
                      ((h ∘c g) ∘c f)
                      (coe (merge A) (coe (merge A) (A $F h $ a)))
                ≃⟨ uncoe (unmerge A B) ◾̃ uncoe (unmerge A B) ⟩
                  coe (unmerge A B)
                      (coe (unmerge A B)
                           (mapE A B
                                w
                                ((h ∘c g) ∘c f)
                                (coe (merge A)
                                     (coe (merge A) (A $F h $ a)))))
                ∎̃)
