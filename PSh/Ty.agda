{-# OPTIONS --no-eta #-}

open import Cats

module PSh.Ty (C : Cat) where

open import lib
open import JM.JM

open Cat C

open import PSh.Cxt C

----------------------------------------------------------------------
-- Substitution calculus
----------------------------------------------------------------------

_[_]Tᴹ : ∀{Γ Δ : PSh C} → FamPSh Δ → Γ →n Δ → FamPSh Γ
_[_]Tᴹ {Γ}{Δ} A σ = record

  { _$F_   = λ { α → A $F (σ $n α) }
  ; _$F_$_ = λ { f a → coe (ap (_$F_ A) (natn σ)) (A $F f $ a) }
  ; idF    = pid
  ; compF  = pcomp }
  
  where
    abstract
      pid : {I : Obj}{α : Γ $P I}{x : A $F (σ $n α)}
          → coe (ap (λ z → A $F (σ $n z)) (idP Γ))
                (coe (ap (_$F_ A) (natn σ)) (A $F idc $ x))
          ≡ x
      pid = from≃ ( uncoe (ap (λ z → A $F (σ $n z)) (idP Γ)) ⁻¹̃
                  ◾̃ uncoe (ap (_$F_ A) (natn σ)) ⁻¹̃
                  ◾̃ from≡ (ap (_$F_ A) (idP Δ)) (idF A))

    abstract
      pcomp : {I : Obj}{J : Obj}{K : Obj}{f : I ⇒ J}{g : J ⇒ K}
              {α : Γ $P I}{x : A $F (σ $n α)}
            → coe (ap (λ z → A $F (σ $n z)) (compP Γ))
                  (coe (ap (_$F_ A) (natn σ)) (A $F g ∘c f $ x))
            ≡ coe (ap (_$F_ A) (natn σ))
                  (A $F g $ coe (ap (_$F_ A) (natn σ)) (A $F f $ x))
      pcomp {I}{J}{K}{f}{g}{α}{x}

        = from≃ ( uncoe (ap (λ z → A $F (σ $n z)) (compP Γ)) ⁻¹̃
                ◾̃ uncoe (ap (_$F_ A) (natn σ)) ⁻¹̃
                ◾̃ from≡ (ap (_$F_ A) (compP Δ)) (compF A)
                ◾̃ ap≃ {B = _$F_ A}
                      (_$F_$_ A g)
                      {Δ $P f $ (σ $n α)}
                      {σ $n (Γ $P f $ α)}
                      (natn σ)
                      (uncoe (ap (_$F_ A) (natn σ)))
                ◾̃ uncoe (ap (_$F_ A) (natn σ)))    

infixl 7 _[_]Tᴹ

_[_]Tᴹ= : {Γ Δ : PSh C}(A : FamPSh Δ)
          {δ₀ δ₁ : Γ →n Δ}(δ₂ : δ₀ ≡ δ₁)
        → A [ δ₀ ]Tᴹ ≡ A [ δ₁ ]Tᴹ
_[_]Tᴹ= = λ { A → ap (_[_]Tᴹ A) }

----------------------------------------------------------------------
-- Base type and base family
----------------------------------------------------------------------

Uᴹ : (U : PSh C){Γ : PSh C} → FamPSh Γ
Uᴹ U {Γ} = record

  { _$F_   = λ {I} _ → U $P I
  ; _$F_$_ = λ f {_} → _$P_$_ U f
  ; idF    = p
  ; compF  = q }

  where
    abstract
      p : {I : Obj} {α : Γ $P I} {x : U $P I}
        → (_$P_$_ U idc) x ≡[  ap (λ _ → U $P I) (idP Γ {I}{α})  ]≡ x
      p = from≃ (uncoe (ap (λ _ → U $P _) (idP Γ)) ⁻¹̃ ◾̃ to≃ (idP U))
    abstract
      q : {I J K : Obj}{f : I ⇒ J}{g : J ⇒ K}{α : Γ $P I}{x : U $P I}
        → U $P (g ∘c f) $ x ≡[ ap (λ _ → U $P K) (compP Γ {f = f}{g}{α}) ]≡ U $P g $ (U $P f $ x)
      q = from≃ (uncoe (ap (λ _ → U $P _) (compP Γ)) ⁻¹̃ ◾̃ to≃ (compP U))

Elᴹ : {U : PSh C}(El : FamPSh U){Γ : PSh C}
    → Γ →S (Uᴹ U) → FamPSh Γ
Elᴹ {U} El {Γ} Â = record
             
  { _$F_   = λ α → El $F (Â $S α)
  ; _$F_$_ = λ {I}{J} f {α} t → coe (ap (_$F_ El) (natS Â)) (El $F f $ t)
  ; idF    = p
  ; compF  = q }

  where
    abstract
      p : {I : Obj}{α : Γ $P I}{x : El $F (Â $S α)}
        → coe (ap (_$F_ El) (natS Â)) (El $F idc $ x)
        ≡[ ap (λ α₁ → El $F (Â $S α₁)) (idP Γ) ]≡
          x
      p = from≃ ( uncoe (ap (λ z → El $F (Â $S z)) (idP Γ)) ⁻¹̃
                ◾̃ uncoe (ap (_$F_ El) (natS Â)) ⁻¹̃
                ◾̃ from≡ (ap (_$F_ El) (idP U)) (idF El))

    abstract
      q : {I J K : Obj}{f : I ⇒ J}{g : J ⇒ K}
          {α : Γ $P I} {x : El $F (Â $S α)}
        → coe (ap (_$F_ El) (natS Â)) (El $F g ∘c f $ x)
        ≡[ ap (λ α₁ → El $F (Â $S α₁)) (compP Γ) ]≡
          coe (ap (_$F_ El) (natS Â))
              (El $F g $ coe (ap (_$F_ El) (natS Â)) (El $F f $ x))
      q {I}{J}{K}{f}{g}{α}{t}
        = from≃ ( uncoe (ap (λ α₁ → El $F (Â $S α₁)) (compP Γ)) ⁻¹̃
                ◾̃ uncoe (ap (_$F_ El) (natS Â)) ⁻¹̃
                ◾̃ from≡ (ap (_$F_ El) (compP U)) (compF El)
                ◾̃ ap≃ {A = U $P J}
                      {_$F_ El}
                      (_$F_$_ El g)
                      {a₀ = U $P f $ (Â $S α)}
                      {a₁ = Â $S (Γ $P f $ α)}
                      (natS Â)
                      (uncoe (ap (_$F_ El) (natS Â)))
                ◾̃ uncoe (ap (_$F_ El) (natS Â)))

----------------------------------------------------------------------
-- Pi
----------------------------------------------------------------------

open import PSh.Exp C

Πᴹ : {Γ : PSh C}(A : FamPSh Γ)(B : FamPSh (Γ ,Cᴹ A)) → FamPSh Γ

Πᴹ {Γ} A B = record
             
  { _$F_   = ExpPSh A B
  ; _$F_$_ = liftExpPSh A B
  ; idF    = Πᴹid
  ; compF  = Πᴹcomp }

  where

    abstract
      Πᴹid : {I : Obj}{α : Γ $P I}{w : ExpPSh A B α}
           → liftExpPSh A B idc w
           ≡[ ap (ExpPSh A B) (idP Γ) ]≡ w
      Πᴹid {I}{α}{w}
      
        = from≃ ( uncoe (ap (ExpPSh A B) (idP Γ)) ⁻¹̃
                ◾̃ ≃ExpPShΓAB {Γ}{A}{B}
                             (idP Γ)
                             ( funexti≃' (λ J
                             → funext≃' (λ f
                             → funext≃ (ap (λ z → A $F (Γ $P f $ z)) (idP Γ)) (λ x₂
                             → uncoe (unmerge A B) ⁻¹̃
                             ◾̃ mapEABw≃ {Γ}{A}{B}{w = w} idr (uncoe (merge A) ⁻¹̃ ◾̃ x₂))))))

    abstract
      Πᴹcomp : {I J K : Obj}{f : I ⇒ J}{g : J ⇒ K}{α : Γ $P I}{w : ExpPSh A B α}
             → liftExpPSh A B (g ∘c f) w
             ≡[ ap (ExpPSh A B) (compP Γ) ]≡
               liftExpPSh A B g (liftExpPSh A B f w)
      Πᴹcomp {I}{J}{K}{f}{g}{α}{w}

        = from≃ ( uncoe (ap (ExpPSh A B) (compP Γ)) ⁻¹̃
                ◾̃ ≃ExpPShΓAB {Γ}{A}{B}
                             (compP Γ)
                             ( funexti≃' (λ L
                             → funext≃' (λ h
                             → funext≃ (ap (λ z → A $F (Γ $P h $ z)) (compP Γ)) (λ x₂
                             → uncoe (unmerge A B) ⁻¹̃
                             ◾̃ mapEABw≃ {Γ}{A}{B}{w = w}
                                        (ass ⁻¹)
                                        ( uncoe (merge A) ⁻¹̃
                                        ◾̃ x₂
                                        ◾̃ uncoe (merge A)
                                        ◾̃ uncoe (merge A))
                             ◾̃ uncoe (unmerge A B)
                             ◾̃ uncoe (unmerge A B))))))
