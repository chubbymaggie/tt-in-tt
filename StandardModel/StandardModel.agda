{-# OPTIONS --no-eta #-}
-- note : we rely on eta for Σ (in library.agda, eta is not turned on)

module StandardModel.StandardModel (⟦U⟧ : Set)(⟦El⟧ : ⟦U⟧ → Set) where

open import lib
open import TT.Syntax
open import TT.Rec

-- set-theoretic interpretation

M : Motives
M = record
      { Conᴹ =             Set
      ; Tyᴹ  = λ ⟦Γ⟧     → ⟦Γ⟧ → Set
      ; Tmsᴹ = λ ⟦Γ⟧ ⟦Δ⟧ → ⟦Γ⟧ → ⟦Δ⟧
      ; Tmᴹ  = λ ⟦Γ⟧ ⟦A⟧ → (γ : ⟦Γ⟧) → (⟦A⟧ γ)
      }

m : Methods M
m = record
      { •ᴹ     =             ⊤
      ; _,Cᴹ_  = λ ⟦Γ⟧ ⟦A⟧ → Σ ⟦Γ⟧ ⟦A⟧
      
      ; _[_]Tᴹ = λ ⟦A⟧ ⟦δ⟧ γ → ⟦A⟧ (⟦δ⟧ γ)
      ; Uᴹ     = λ         _ → ⟦U⟧
      ; Elᴹ    = λ ⟦Â⟧     γ → ⟦El⟧ (⟦Â⟧ γ)
      ; Πᴹ     = λ ⟦A⟧ ⟦B⟧ γ → (x : ⟦A⟧ γ) → ⟦B⟧ (γ , x)
      
      ; εᴹ     = λ         _ → tt
      ; _,sᴹ_  = λ ⟦δ⟧ ⟦t⟧ γ → ⟦δ⟧ γ , ⟦t⟧ γ
      ; idᴹ    = λ         γ → γ
      ; _∘ᴹ_   = λ ⟦δ⟧ ⟦σ⟧ γ → ⟦δ⟧ (⟦σ⟧ γ)
      ; π₁ᴹ    = λ ⟦δ⟧     γ → (proj₁ (⟦δ⟧ γ))
      
      ; _[_]tᴹ = λ ⟦t⟧ ⟦δ⟧ γ → ⟦t⟧ (⟦δ⟧ γ)
      ; π₂ᴹ    = λ ⟦δ⟧     γ → (proj₂ (⟦δ⟧ γ))
      ; appᴹ   = λ ⟦t⟧     γ → ⟦t⟧ (proj₁ γ) (proj₂ γ)
      ; lamᴹ   = λ ⟦t⟧     γ → λ a → ⟦t⟧ (γ , a)
      
      ; [id]Tᴹ = refl
      ; [][]Tᴹ = refl
      ; U[]ᴹ   = refl
      ; El[]ᴹ  = refl
      ; Π[]ᴹ   = refl
      
      ; idlᴹ   = refl
      ; idrᴹ   = refl
      ; assᴹ   = refl
      ; ,∘ᴹ    = refl
      ; π₁βᴹ   = refl
      ; πηᴹ    = refl
      ; εηᴹ    = refl
      
      ; π₂βᴹ   = refl
      ; lam[]ᴹ = refl
      ; Πβᴹ    = refl
      ; Πηᴹ    = refl
      }

open rec M m

⟦_⟧C : Con → Set
⟦_⟧T : ∀{Γ} → Ty Γ → ⟦ Γ ⟧C → Set
⟦_⟧s : ∀{Γ Δ} → Tms Γ Δ → ⟦ Γ ⟧C → ⟦ Δ ⟧C
⟦_⟧t : ∀{Γ}{A : Ty Γ} → (t : Tm Γ A) → (γ : ⟦ Γ ⟧C) → ⟦ A ⟧T γ

⟦_⟧C = RecCon
⟦_⟧T = RecTy
⟦_⟧s = RecTms
⟦_⟧t = RecTm
