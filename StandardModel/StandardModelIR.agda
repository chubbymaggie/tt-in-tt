{-# OPTIONS --no-eta #-}
-- note : we rely on eta for Σ (in library.agda, eta is not turned on)

module StandardModel.StandardModelIR (⟦U⟧ : Set)(⟦El⟧ : ⟦U⟧ → Set) where

open import lib
open import TT.Syntax
open import TT.Rec

-- an inductive-recursive universe

data UU : Set
EL : UU → Set

data UU where
  ′Π′  : (A : UU) → (EL A → UU) → UU -- \'
  ′Σ′  : (A : UU) → (EL A → UU) → UU
  ′⊤′  : UU
  ′U′  : UU
  ′El′ : EL ′U′ → UU

EL (′Π′ A B) =   (x : EL A) → EL (B x)
EL (′Σ′ A B) = Σ (EL A) λ x → EL (B x)
EL ′⊤′       = ⊤
EL ′U′       = ⟦U⟧
EL (′El′ Â)  = ⟦El⟧ Â

-- set-theoretic interpretation

M : Motives
M = record
      { Conᴹ =             UU
      ; Tyᴹ  = λ ⟦Γ⟧     → EL ⟦Γ⟧ → UU
      ; Tmsᴹ = λ ⟦Γ⟧ ⟦Δ⟧ → EL ⟦Γ⟧ → EL ⟦Δ⟧
      ; Tmᴹ  = λ ⟦Γ⟧ ⟦A⟧ → (γ : EL ⟦Γ⟧) → EL (⟦A⟧ γ)
      }

m : Methods M
m = record
      { •ᴹ     =             ′⊤′
      ; _,Cᴹ_  = λ ⟦Γ⟧ ⟦A⟧ → ′Σ′ ⟦Γ⟧ ⟦A⟧
      
      ; _[_]Tᴹ = λ ⟦A⟧ ⟦δ⟧ γ → ⟦A⟧ (⟦δ⟧ γ)
      ; Uᴹ     = λ         _ → ′U′
      ; Elᴹ    = λ ⟦Â⟧     γ → ′El′ (⟦Â⟧ γ)
      ; Πᴹ     = λ ⟦A⟧ ⟦B⟧ γ → ′Π′ (⟦A⟧ γ) (λ x → ⟦B⟧ (γ , x))
      
      ; εᴹ     = λ         _ → tt
      ; _,sᴹ_  = λ ⟦δ⟧ ⟦t⟧ γ → ⟦δ⟧ γ , ⟦t⟧ γ
      ; idᴹ    = λ         γ → γ
      ; _∘ᴹ_   = λ ⟦δ⟧ ⟦σ⟧ γ → ⟦δ⟧ (⟦σ⟧ γ)
      ; π₁ᴹ    = λ ⟦δ⟧     γ → (proj₁ (⟦δ⟧ γ))
      
      ; _[_]tᴹ = λ ⟦t⟧ ⟦δ⟧ γ → ⟦t⟧ (⟦δ⟧ γ)
      ; π₂ᴹ    = λ ⟦δ⟧     γ → (proj₂ (⟦δ⟧ γ))
      ; appᴹ   = λ ⟦t⟧     γ → ⟦t⟧ (proj₁ γ) (proj₂ γ)
      ; lamᴹ   = λ ⟦t⟧     γ → λ a → ⟦t⟧ (γ , a)
      
      ; [id]Tᴹ = refl
      ; [][]Tᴹ = refl
      ; U[]ᴹ   = refl
      ; El[]ᴹ  = refl
      ; Π[]ᴹ   = refl
      
      ; idlᴹ   = refl
      ; idrᴹ   = refl
      ; assᴹ   = refl
      ; ,∘ᴹ    = refl
      ; π₁βᴹ   = refl
      ; πηᴹ    = refl
      ; εηᴹ    = refl
      
      ; π₂βᴹ   = refl
      ; lam[]ᴹ = refl
      ; Πβᴹ    = refl
      ; Πηᴹ    = refl
      }

open rec M m

⟦_⟧C : Con → UU
⟦_⟧T : ∀{Γ} → Ty Γ → EL (⟦ Γ ⟧C) → UU
⟦_⟧s : ∀{Γ Δ} → Tms Γ Δ → EL (⟦ Γ ⟧C) → EL (⟦ Δ ⟧C)
⟦_⟧t : ∀{Γ}{A : Ty Γ} → (t : Tm Γ A) → (γ : EL (⟦ Γ ⟧C)) → EL (⟦ A ⟧T γ)

⟦_⟧C = RecCon
⟦_⟧T = RecTy
⟦_⟧s = RecTms
⟦_⟧t = RecTm
