{-# OPTIONS --no-eta #-}

open import lib
open import JM.JM
open import TT.Syntax
open import TT.Elim
open import TT.Congr
open import TT.Laws
open import LogPred.Motives
open import LogPred.Cxt
open import LogPred.Ty
open import LogPred.Tms
open import LogPred.Tm
open import LogPred.Tm.Proofs
open import LogPred.HTy.Proofs1

open Motives M
open MethodsCon mCon
open MethodsTy mTy
open MethodsTms mTms
open MethodsTm mTm

module LogPred.HTy.Proofs3
  {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ : Con}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}
  {δᴹ : Tmsᴹ Γᴹ Δᴹ δ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
  where

_^ᴹ_ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}(δᴹ : Tmsᴹ Γᴹ Δᴹ δ)
       {A : Ty Δ}(Aᴹ : Tyᴹ Δᴹ A) → Tmsᴹ (Γᴹ ,Cᴹ Aᴹ [ δᴹ ]Tᴹ) (Δᴹ ,Cᴹ Aᴹ) (δ ^ A)
_^ᴹ_ = λ δᴹ Aᴹ → (δᴹ ∘ᴹ π₁ᴹ idᴹ) ,sᴹ coe (TmΓᴹ= [][]T [][]Tᴹ refl) (π₂ᴹ idᴹ)

abstract
  π₂Pr : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}
       → π₂ id [ Pr (Γᴹ ,Cᴹ Aᴹ) ]t ≃ π₂ {A = A [ Pr Γᴹ ]T} id [ π₁ {A = Aᴹ} id ]t
  π₂Pr {Γ}{Γᴹ}{A}{Aᴹ} = π₂[]'
                      ◾̃ π₂≃ (idl ◾ ,∘)
                      ◾̃ π₂β'
                      ◾̃ coecoe[]t [][]T [][]T

eqA : A [ δ ]T [ Pr Γᴹ ]T ≃ A [ Pr Δᴹ ]T [ =s δᴹ ]T
eqA = to≃ (TyNat δᴹ ⁻¹)
eqΓA : (=C Γᴹ , (A [ δ ]T) [ Pr Γᴹ ]T) ≡ (=C Γᴹ , (A [ Pr Δᴹ ]T) [ =s δᴹ ]T)
eqΓA = ,C≃ refl eqA
eqAᴹ : Aᴹ [ δᴹ ]Tᴹ ≃ Aᴹ [ =s δᴹ ^ A [ Pr Δᴹ ]T ]T
eqAᴹ = []T≃ eqΓA (uncoe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) ⁻¹̃)
eqΓAAᴹ : =C Γᴹ , A [ δ ]T [ Pr Γᴹ ]T     , Aᴹ [ δᴹ ]Tᴹ
       ≡ =C Γᴹ , A [ Pr Δᴹ ]T [ =s δᴹ ]T , Aᴹ [ =s δᴹ ^ A [ Pr Δᴹ ]T ]T
eqΓAAᴹ = ,C≃ eqΓA eqAᴹ

=s^ : =s (δᴹ ^ᴹ Aᴹ) ≃ =s δᴹ ^ A [ Pr Δᴹ ]T ^ Aᴹ
=s^ = ,s≃ eqΓAAᴹ
         refl
         ( ,s≃ eqΓAAᴹ
               refl
               ( ∘≃ eqΓAAᴹ
                    ( π₁π₁id≃ refl eqA eqAᴹ
                    ◾̃ to≃ (ap π₁ idl ⁻¹ ◾ π₁∘ ⁻¹))
               ◾̃ to≃ (ass ⁻¹))
               r̃
               ( coecoe[]t [][]T (TyNat (δᴹ ∘ᴹ π₁ᴹ idᴹ) ⁻¹)
               ◾̃ π₂Pr {Γᴹ = Γᴹ}
               ◾̃ π₂[]'
               ◾̃ π₂≃' eqΓAAᴹ
                      refl
                      eqA
                      ( to≃ idl
                      ◾̃ π₁id≃ eqΓA eqAᴹ
                      ◾̃ to≃ (idl ⁻¹))
               ◾̃ π₂[]' ⁻¹̃
               ◾̃ coecoe[]t [][]T [][]T ⁻¹̃)
         ◾̃ to≃ (,∘ ⁻¹))
         r̃
         (uncoe (TmΓ= p,sᴹ) ⁻¹̃
         ◾̃ uncoe (TmΓᴹ= [][]T [][]Tᴹ refl) ⁻¹̃
         ◾̃ uncoe (TmΓ= pπ₂ᴹ) ⁻¹̃
         ◾̃ π₂id≃ eqΓA eqAᴹ
         ◾̃ uncoe (TmΓ= [][]T))
