{-# OPTIONS --no-eta #-}

open import lib
open import JM.JM
open import TT.Syntax
open import TT.Elim
open import TT.Congr
open import TT.Laws
open import LogPred.Motives
open import LogPred.Cxt
open import LogPred.Ty
open import LogPred.Tms
open import LogPred.Tm
open import LogPred.Tm.Proofs
open import LogPred.HTy.Proofs1

open Motives M
open MethodsCon mCon
open MethodsTy mTy
open MethodsTms mTms
open MethodsTm mTm

module LogPred.HTy.Proofs6
  {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ : Con}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}
  {δᴹ : Tmsᴹ Γᴹ Δᴹ δ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}{B : Ty (Δ , A)}
  where

open import LogPred.HTy.Proofs2 {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}{B}
open import LogPred.HTy.Proofs3 {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}
open import LogPred.HTy.Proofs4 {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}{B}
open import LogPred.HTy.Proofs5 {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}{B}

Goal : (π₁ id ^ A [ Pr Δᴹ ]T ^ Aᴹ ,
       coe (TmΓ= (pΠᴹ Δᴹ)) (app (coe (TmΓ= (pΠᴹ' Δᴹ)) (π₂ id)) [ π₁ id ]t))
      ∘
      (coe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) (=s δᴹ ^ Π A B [ Pr Δᴹ ]T) ^
       (A [ Pr Δᴹ ]T) [ π₁ id ]T
       ^ Aᴹ [ π₁ id ^ A [ Pr Δᴹ ]T ]T)
      ≃
      coe (Tms-Γ= (,C≃ refl (to≃ (TyNat (δᴹ ^ᴹ Aᴹ)))))
      (=s (δᴹ ^ᴹ Aᴹ) ^ B [ Pr (Δᴹ ,Cᴹ Aᴹ) ]T)
      ∘
      (π₁ id ^ (A [ δ ]T) [ Pr Γᴹ ]T ^ Aᴹ [ δᴹ ]Tᴹ ,
       coe (TmΓ= (pΠᴹ Γᴹ)) (app (coe (TmΓ= (pΠᴹ' Γᴹ)) (π₂ id)) [ π₁ id ]t))

Goal = to≃ ,∘
     ◾̃ ,s≃ (,C≃ (,C≃ p r) s)
           refl
           p5
           r̃
           p4
     ◾̃ to≃ (,∘ ⁻¹ ◾ coe∘ (,C≃ refl (to≃ (TyNat (δᴹ ^ᴹ Aᴹ)))) ⁻¹)
