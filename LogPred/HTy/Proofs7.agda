{-# OPTIONS --no-eta #-}

module LogPred.HTy.Proofs7 where

open import lib
open import JM.JM
open import TT.Syntax
open import TT.Elim
open import TT.Congr
open import LogPred.Motives
open import LogPred.Cxt
open import LogPred.Ty
open import LogPred.Tms
open import LogPred.Tm
open import LogPred.Tm.Proofs
open import LogPred.HTy.Proofs1
import LogPred.HTy.Proofs2
import LogPred.HTy.Proofs6

open Motives M
open MethodsCon mCon
open MethodsTy mTy
open MethodsTms mTms
open MethodsTm mTm

abstract
  Π[]ᴹ : {Γ : Con} {Γᴹ : Conᴹ Γ} {Δ : Con} {Δᴹ : Conᴹ Δ} {δ : Tms Γ Δ}
         {δᴹ : Tmsᴹ Γᴹ Δᴹ δ} {A : Ty Δ} {Aᴹ : Tyᴹ Δᴹ A} {B : Ty (Δ , A)}
         {Bᴹ : Tyᴹ (Δᴹ ,Cᴹ Aᴹ) B}
       → Πᴹ {Γᴹ = Δᴹ} Aᴹ Bᴹ [ δᴹ ]Tᴹ
       ≡[ TyΓᴹ= {Γᴹ = Γᴹ} Π[] ]≡
         Πᴹ {Γᴹ = Γᴹ}
            (Aᴹ [ δᴹ ]Tᴹ)
            (Bᴹ [ δᴹ ∘ᴹ π₁ᴹ idᴹ ,sᴹ coe (TmΓᴹ= [][]T [][]Tᴹ refl) (π₂ᴹ idᴹ) ]Tᴹ)
            
  Π[]ᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}{B}{Bᴹ}
  
    = from≃ ( uncoe (TyΓᴹ= Π[]) ⁻¹̃
            ◾̃ to≃ Π[]
            ◾̃ Π≃ p
                 r
                 ( to≃ Π[]
                 ◾̃ Π≃ (,C≃ p r)
                      s
                      ( to≃ [][]T
                      ◾̃ []T≃ (,C≃ (,C≃ p r) s)
                             Goal
                      ◾̃ to≃ ([][]T ⁻¹))))
    where
      open LogPred.HTy.Proofs2 {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}{B}
      open LogPred.HTy.Proofs6 {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}{B}
