{-# OPTIONS --no-eta #-}

open import lib
open import TT.Syntax

module LogPred.HTy.Proofs1
  (U̅  : Ty (• , U))
  (E̅l : Ty (• , U , El (coe (TmΓ= U[]) vz) , U̅ [ wk ]T))
  where
  
open import JM.JM
open import TT.Elim
open import TT.Congr
open import TT.Laws
open import LogPred.Motives
open import LogPred.Cxt
open import LogPred.Ty U̅ E̅l
open import LogPred.Tms U̅ E̅l
open import LogPred.Tm U̅ E̅l
open import LogPred.Tm.Proofs U̅ E̅l

open Motives M
open MethodsCon mCon
open MethodsTy mTy
open MethodsTms mTms
open MethodsTm mTm
open Conᵐ
open Tmsᵐ

abstract
  [id]Tᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}
         → _[_]Tᴹ {Γᴹ = Γᴹ} Aᴹ idᴹ ≡[ TyΓᴹ= {Γᴹ = Γᴹ} [id]T ]≡ Aᴹ
  [id]Tᴹ {Γ}{Γᴹ}{A}{Aᴹ} = {!!}
{-
    = let
        q = (to≃ [][]T ◾̃ []T≃ refl (to≃ idl) )
        p = (,C≃ refl q)
        r = (to≃ [][]T ◾̃ []T≃ refl (to≃ idr))
      in
        from≃ ( uncoe (TyΓᴹ= [id]T) ⁻¹̃
              ◾̃ []T≃ p
                     ( uncoe (Tms-Γ= (,C≃ refl (to≃ (TyNat idᴹ)))) ⁻¹̃
                     ◾̃ ,s≃ (,C≃ refl r)
                           refl
                           (to≃ idl ◾̃ π₁id≃ refl r)
                           r̃
                           ( uncoe (TmΓ= [][]T) ⁻¹̃
                           ◾̃ π₂id≃ refl r)
                     ◾̃ to≃ πη)
              ◾̃ to≃ [id]T)
-}
abstract
  [][]Tᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{Σ}{Σᴹ : Conᴹ Σ}{A : Ty Σ}{Aᴹ : Tyᴹ Σᴹ A}
           {σ : Tms Γ Δ}{σᴹ : Tmsᴹ Γᴹ Δᴹ σ}{δ : Tms Δ Σ}{δᴹ : Tmsᴹ Δᴹ Σᴹ δ}
         → Aᴹ [ δᴹ ]Tᴹ [ σᴹ ]Tᴹ ≡[ TyΓᴹ= {Γᴹ = Γᴹ} [][]T ]≡ Aᴹ [ δᴹ ∘ᴹ σᴹ ]Tᴹ
  [][]Tᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{Σ}{Σᴹ}{A}{Aᴹ}{σ}{σᴹ}{δ}{δᴹ} = {!!}
{-    = let
        q = to≃ ([][]T ◾ [][]T ◾ ap (_[_]T A) ass ⁻¹ ◾ [][]T ⁻¹)
        p = (,C≃ refl q)
        r = to≃ ( [][]T
                ◾ [][]T
                ◾ ap (_[_]T A)
                     ( ap (_∘_ δ) (PrNat σᴹ ⁻¹)
                     ◾ ass ⁻¹
                     ◾ ap (λ z → z ∘ =s σᴹ) (PrNat δᴹ ⁻¹)
                     ◾ ass)
                ◾ [][]T ⁻¹)
        s : (A [ δ ]T [ σ ]T [ Pr Γᴹ ]T) ≃ (A [ δ ]T [ Pr Δᴹ ]T [ =s σᴹ ]T)
        s = to≃ ( [][]T
                ◾ [][]T
                ◾ ap (λ z → A [ δ ∘ z ]T)
                     (PrNat σᴹ ⁻¹)
                ◾ [][]T ⁻¹
                ◾ [][]T ⁻¹)
        w = (to≃ ([][]T ◾ ap (_[_]T A) (PrNat δᴹ) ◾ [][]T ⁻¹))
        v = ( uncoe (TmsΓ-= (,C≃ refl (to≃ (TyNat δᴹ)) ⁻¹)) ⁻¹̃
            ◾̃ uncoe (Tms-Γ= (,C≃ refl (to≃ (TyNat σᴹ)))) ⁻¹̃)
        q = (to≃ ( [][]T
                 ◾ [][]T
                 ◾ ap (_[_]T A)
                      ( ass ⁻¹
                      ◾ ap (λ z → z ∘ =s σᴹ)
                           (PrNat δᴹ ⁻¹)
                      ◾ ass)
                 ◾ [][]T ⁻¹))
      in
        from≃ (uncoe (TyΓᴹ= [][]T) ⁻¹̃
              ◾̃ to≃ [][]T
              ◾̃ []T≃ p
                     ( to≃ (coe∘ (,C≃ refl (to≃ (TyNat δᴹ))) ◾ ,∘)
                     ◾̃ ,s≃ (,C≃ refl r)
                           refl
                           ( to≃ ass
                           ◾̃ ∘≃ (,C≃ refl r)
                                ( to≃ (π₁∘ ◾ ap π₁ idl)
                                ◾̃ π₁≃ (,C≃ refl s) refl w v
                                ◾̃ to≃ π₁β
                                ◾̃ ∘≃ (,C≃ refl q) (π₁id≃ refl q))
                           ◾̃ to≃ (ass ⁻¹))
                           r̃
                           ( coecoe[]t [][]T [][]T
                           ◾̃ π₂[]'
                           ◾̃ π₂≃ idl
                           ◾̃ π₂≃' (,C≃ refl s) refl w v
                           ◾̃ π₂β'
                           ◾̃ uncoe (TmΓ= [][]T) ⁻¹̃
                           ◾̃ π₂id≃ refl q
                           ◾̃ uncoe (TmΓ= [][]T))
                     ◾̃ uncoe (Tms-Γ= (,C≃ refl (to≃ (TyNat (δᴹ ∘ᴹ σᴹ)))))))
-}

abstract
  U[]ᴹ : {Γ : Con} {Γᴹ : Conᴹ Γ} {Δ : Con} {Δᴹ : Conᴹ Δ} {δ : Tms Γ Δ}
         {δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
       → (Uᴹ {Γᴹ = Δᴹ}) [ δᴹ ]Tᴹ ≡[ TyΓᴹ= {Γᴹ = Γᴹ} U[] ]≡ (Uᴹ {Γᴹ = Γᴹ})
  U[]ᴹ {δᴹ = δᴹ} = {!!}
{-    = from≃ ( uncoe (TyΓᴹ= U[]) ⁻¹̃
            ◾̃ to≃ [][]T
            ◾̃ []T≃ (,C≃ refl (to≃ ([][]T ◾ U[] ◾ U[] ⁻¹)))
                   ( to≃ ,∘
                   ◾̃ ,s≃ (,C≃ refl (to≃ ([][]T ◾ U[] ◾ U[] ⁻¹)))
                         refl
                         ( to≃ εη
                         ◾̃ ε≃ (,C≃ refl (to≃ ([][]T ◾ U[] ◾ U[] ⁻¹))))
                         r̃
                         ( uncoe (TmΓ= [][]T) ⁻¹̃
                         ◾̃ coe[]t' (pU _)
                         ◾̃ []t≃ (,C≃ refl (to≃ ([][]T ◾ U[] ◾ U[] ⁻¹ ◾ [][]T ⁻¹)))
                                (uncoe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) ⁻¹̃)
                         ◾̃ π₂idβ
                         ◾̃ uncoe (TmΓ= [][]T) ⁻¹̃
                         ◾̃ π₂id≃ refl
                                 (to≃ ([][]T ◾ U[] ◾ U[] ⁻¹))
                         ◾̃ uncoe (TmΓ= (pU _)))))
-}
abstract
  El[]ᴹ : {Γ : Con} {Γᴹ : Conᴹ Γ} {Δ : Con} {Δᴹ : Conᴹ Δ} {δ : Tms Γ Δ}
          {δᴹ : Tmsᴹ Γᴹ Δᴹ δ} {Â : Tm Δ U} {Âᴹ : Tmᴹ Δᴹ (Uᴹ {Γᴹ = Δᴹ}) Â}
        → Elᴹ Âᴹ [ δᴹ ]Tᴹ
        ≡[ TyΓᴹ= {Γᴹ = Γᴹ} El[] ]≡
          Elᴹ (coe (TmΓᴹ= U[] U[]ᴹ refl) (Âᴹ [ δᴹ ]tᴹ))
  El[]ᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{Â}{Âᴹ}
    = from≃ ( uncoe (TyΓᴹ= El[]) ⁻¹̃
            ◾̃ to≃ [][]T
            ◾̃ []T≃ (,C≃ refl (to≃ p))
                   ( to≃ ,∘
                   ◾̃ ,s≃ (,C≃ refl (to≃ p))
                         refl
                         ( to≃ ,∘
                         ◾̃ ,s≃ (,C≃ refl (to≃ p)) refl
                               ( to≃ ,∘
                               ◾̃ ,s≃ (,C≃ refl (to≃ p)) refl (to≃ εη ◾̃ ε≃ (,C≃ refl (to≃ p))) r̃
                                     ( uncoe (TmΓ= [][]T) ⁻¹̃
                                     ◾̃ coe[]t' (pU Δᴹ)
                                     ◾̃ [][]t' ◾̃ [][]t'
                                     ◾̃ []t≃ (,C≃ refl (to≃ p))
                                            ( ∘≃ (,C≃ refl (to≃ p))
                                                 ( ∘≃ (,C≃ refl (to≃ (TyNat δᴹ ⁻¹))) (uncoe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) ⁻¹̃)
                                                 ◾̃ to≃ π₁idβ
                                                 ◾̃ ∘≃ (,C≃ refl (to≃ (TyNat δᴹ ◾ p)))
                                                      (π₁id≃ refl (to≃ (TyNat δᴹ ◾ p))))
                                            ◾̃ to≃ (ass ⁻¹)
                                            ◾̃ to≃ (ap (λ z → z ∘ wk) (PrNat δᴹ))
                                            ◾̃ to≃ ass)
                                     ◾̃ [][]t' ⁻¹̃
                                     ◾̃ coe[]t' U[] ⁻¹̃
                                     ◾̃ [][]t' ⁻¹̃
                                     ◾̃ uncoe (TmΓ= (pU Γᴹ))))
                               r̃
                               ( uncoe (TmΓ= [][]T) ⁻¹̃
                               ◾̃ coe[]t' (pEl Âᴹ)
                               ◾̃ []t≃ (,C≃ refl (to≃ (TyNat δᴹ ⁻¹))) (uncoe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) ⁻¹̃)
                               ◾̃ π₂idβ
                               ◾̃ uncoe (TmΓ= [][]T) ⁻¹̃
                               ◾̃ π₂id≃ refl (to≃ (TyNat δᴹ) ◾̃ to≃ p)
                               ◾̃ uncoe (TmΓ= (pEl (coe (TmΓᴹ= U[] U[]ᴹ refl) (Âᴹ [ δᴹ ]tᴹ))))))
                         r̃
                         ( uncoe (TmΓ= [][]T) ⁻¹̃
                         ◾̃ coe[]t' (pEl' Âᴹ)
                         ◾̃ [][]t'
                         ◾̃ []t≃ (,C≃ refl (to≃ p))
                                (∘≃ (,C≃ refl (to≃ (TyNat δᴹ ⁻¹)))
                                    (uncoe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) ⁻¹̃)
                                ◾̃ to≃ π₁idβ
                                ◾̃ ∘≃ (,C≃ refl (to≃ (TyNat δᴹ ◾ p))) (π₁id≃ refl (to≃ (TyNat δᴹ ◾ p))))
                         ◾̃ [][]t' ⁻¹̃
                         ◾̃ []t≃' refl refl
                                 ( to≃ [][]T
                                 ◾̃ []T≃ refl
                                        ( to≃ ,∘
                                        ◾̃ ,s≃ refl refl (to≃ idl) r̃
                                              ( uncoe (TmΓ= [][]T) ⁻¹̃
                                              ◾̃ coe[]t' ([id]T ⁻¹)
                                              ◾̃ TmNat δᴹ
                                              ◾̃ uncoe (TmΓ= (TyNat δᴹ ⁻¹)))
                                        ◾̃ to≃ (^∘<> (TyNat δᴹ) ⁻¹))
                                 ◾̃ to≃ ([][]T ⁻¹))
                                 (uncoe (TmΓ= p[]tᴹ)) r̃
                         ◾̃ []t≃' refl
                                 refl
                                 ([]T≃' refl (,C≃ refl (to≃ ([][]T ◾ U[] ◾ U[] ⁻¹)))
                                 (uncoe (TyΓᴹ= U[]) ◾̃ to≃ U[]ᴹ)
                                 (,s≃ refl refl r̃ (to≃ ([][]T ◾ U[] ◾ U[] ⁻¹))
                                      ( uncoe (TmΓ= ([id]T ⁻¹)) ⁻¹̃
                                      ◾̃ coe[]t' U[] ⁻¹̃
                                      ◾̃ uncoe (TmΓ= ([id]T ⁻¹)))))
                                 (uncoe (TmΓᴹ= U[] U[]ᴹ refl)) r̃
                         ◾̃ uncoe (TmΓ= (pEl' (coe (TmΓᴹ= U[] U[]ᴹ refl) (Âᴹ [ δᴹ ]tᴹ)))))))

    where
      p : El Â [ δ ]T [ Pr Γᴹ ]T
        ≡ El (coe (TmΓ= U[]) (Â [ δ ]t)) [ Pr Γᴹ ]T
      p = [][]T
        ◾ El[]
        ◾ ap El (from≃ ( uncoe (TmΓ= U[]) ⁻¹̃
                       ◾̃ [][]t' ⁻¹̃
                       ◾̃ coe[]t' U[] ⁻¹̃
                       ◾̃ uncoe (TmΓ= U[])))
        ◾ El[] ⁻¹

mHTy : MethodsHTy M mCon mTy mTms mTm
mHTy = record
         { [id]Tᴹ = {!!}
         ; [][]Tᴹ = {!!}
         ; U[]ᴹ = U[]ᴹ
         ; El[]ᴹ = El[]ᴹ
         ; Π[]ᴹ = {!!} }

{-
abstract
  U[]ᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
       → Uᴹ {Γᴹ = Δᴹ} [ δᴹ ]Tᴹ ≡[ TyΓᴹ= {Γᴹ = Γᴹ} U[] ]≡ Uᴹ {Γᴹ = Γᴹ}
  U[]ᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}

    = let
        q = to≃ ([][]T ◾ U[] ◾ U[] ⁻¹)
        p = ,C≃ refl q
        r : (El (coe (TmΓ= ([][]T ◾ U[])) (π₂ id))
              [ coe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) (=s δᴹ ^ (U [ Pr Δᴹ ]T)) ]T)
          ≃ El (coe (TmΓ= ([][]T ◾ U[])) (π₂ id))
        r = to≃ El[]
          ◾̃ El≃ p
                ( coecoe[]t ([][]T ◾ U[]) U[]
                ◾̃ π₂[]'
                ◾̃ π₂≃ idl
                ◾̃ π₂≃' (,C≃ refl (to≃ ([][]T ◾ U[] ◾ U[] ⁻¹ ◾ [][]T ⁻¹)))
                       refl
                       r̃
                       (uncoe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) ⁻¹̃)
                ◾̃ π₂β'
                ◾̃ uncoe (TmΓ= [][]T) ⁻¹̃
                ◾̃ π₂id≃ refl (to≃ ([][]T ◾ U[] ◾ U[] ⁻¹))
                ◾̃ uncoe (TmΓ= ([][]T ◾ U[])))
      in
        from≃ ( uncoe (TyΓᴹ= U[]) ⁻¹̃
              ◾̃ to≃ Π[]
              ◾̃ Π≃ p
                   r
                   (to≃ U[] ◾̃ U≃ (,C≃ p r)))

abstract
  El[]ᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
          {Â : Tm Δ U}{Âᴹ : Tmᴹ Δᴹ (Uᴹ {Γᴹ = Δᴹ}) Â}
        → Elᴹ {Γᴹ = Δᴹ} Âᴹ [ δᴹ ]Tᴹ
        ≡[ TyΓᴹ= {Γᴹ = Γᴹ} El[] ]≡
          Elᴹ {Γᴹ = Γᴹ} (coe (TmΓᴹ= U[] U[]ᴹ refl) (Âᴹ [ δᴹ ]tᴹ))
  El[]ᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{Â}{Âᴹ}

    = from≃ ( uncoe (TyΓᴹ= El[]) ⁻¹̃
            ◾̃ to≃ El[]
            ◾̃ El≃ (,C≃ refl q)
                  ( uncoe (TmΓ= U[]) ⁻¹̃
                  ◾̃ []t≃ (,C≃ refl r) (uncoe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) ⁻¹̃)
                  ◾̃ to≃ (app[] ⁻¹)
                  ◾̃ app≃ refl
                         s
                         (to≃ U[] ◾̃ U≃ (,C≃ refl s))
                         ( coecoe[]t (pElᴹ Δᴹ Âᴹ) Π[]
                         ◾̃ uncoe (TmΓ= p[]tᴹ)
                         ◾̃ uncoe (TmΓᴹ= U[] U[]ᴹ refl)
                         ◾̃ uncoe (TmΓ= (pElᴹ Γᴹ (coe (TmΓᴹ= U[] U[]ᴹ refl) (Âᴹ [ δᴹ ]tᴹ)))))))
    where
      abstract
        q : (El Â [ δ ]T [ Pr Γᴹ ]T) ≃ (El (coe (TmΓ= U[]) (Â [ δ ]t)) [ Pr Γᴹ ]T)
        q = to≃ ([][]T ◾ El[])
          ◾̃ El≃ refl
                ( uncoe (TmΓ= U[]) ⁻¹̃
                ◾̃ [][]t' ⁻¹̃
                ◾̃ coecoe[]t U[] U[] ⁻¹̃)
          ◾̃ to≃ (El[] ⁻¹)

      abstract
        r : (El Â [ δ ]T [ Pr Γᴹ ]T) ≃ (El Â [ Pr Δᴹ ]T [ =s δᴹ ]T)
        r = to≃ [][]T
          ◾̃ []T≃ refl (to≃ (PrNat δᴹ ⁻¹))
          ◾̃ to≃ ([][]T ⁻¹)

      abstract
        s : (El Â [ Pr Δᴹ ]T [ =s δᴹ ]T) ≃ (El (coe (TmΓ= U[]) (Â [ δ ]t)) [ Pr Γᴹ ]T)
        s = r ⁻¹̃ ◾̃ []T≃' refl refl (to≃ El[]) r̃

-}
