{-# OPTIONS --no-eta #-}

open import lib
open import TT.Syntax

module LogPred.HTy.Proofs2
  (U̅  : Ty (• , U))
  (E̅l : Ty (• , U , El (coe (TmΓ= U[]) vz) , U̅ [ wk ]T))
  {Γ : Con}{Γᴹ : Conᴹ Γ}
  {Δ : Con}{Δᴹ : Conᴹ Δ}
  {δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
  {A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
  {B : Ty (Δ , A)}
    where

open import JM.JM
open import TT.Elim
open import TT.Congr
open import TT.Laws
open import LogPred.Motives
open import LogPred.Cxt
open import LogPred.Ty U̅ E̅l
open import LogPred.Tms U̅ E̅l
open import LogPred.Tm U̅ E̅l
open import LogPred.Tm.Proofs U̅ E̅l
open import LogPred.HTy.Proofs1 U̅ E̅l

open Motives M
open MethodsCon mCon
open MethodsTy mTy
open MethodsTms mTms
open MethodsTm mTm

abstract
  p : _≡_ {A = Con}
          (=C Γᴹ , (Π A B [ δ ]T) [ Pr Γᴹ ]T)
          (=C Γᴹ , Π (A [ δ ]T) (B [ δ ^ A ]T) [ Pr Γᴹ ]T)
  p = ,C≃ refl (to≃ (ap (λ z → z [ Pr Γᴹ ]T) Π[]))
  
abstract
  q : Π A B [ Pr Δᴹ ]T [ =s δᴹ ]T ≃ Π (A [ δ ]T) (B [ δ ^ A ]T) [ Pr Γᴹ ]T
  q = to≃ ( [][]T
          ◾ ap (_[_]T _) (PrNat δᴹ)
          ◾ [][]T ⁻¹)
    ◾̃ []T≃' refl refl (to≃ Π[]) r̃

abstract
  r : A [ Pr Δᴹ ]T [ π₁ id ]T
        [ coe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) (=s δᴹ ^ Π A B [ Pr Δᴹ ]T) ]T
    ≃ A [ δ ]T [ Pr Γᴹ ]T [ π₁ {A = Π (A [ δ ]T) (B [ δ ^ A ]T) [ Pr Γᴹ ]T} id ]T
  r = to≃ ([][]T ◾ [][]T)
    ◾̃ []T≃ p
           ( ∘≃ p
                ( ∘≃ (p ◾ ,C≃ refl (q ⁻¹̃))
                     (uncoe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) ⁻¹̃)
                ◾̃ to≃ (∘π₁id {ρ = =s δᴹ} ⁻¹)
                ◾̃ ∘≃ (,C≃ refl q) (π₁id≃ refl q))
           ◾̃ to≃ (ass ⁻¹ ◾ ap (λ z → z ∘ π₁ id) (PrNat δᴹ) ◾ ass))
    ◾̃ to≃ ([][]T ⁻¹ ◾ [][]T ⁻¹)
    
abstract
  s : Aᴹ [ π₁ id ^ A [ Pr Δᴹ ]T ]T
         [ coe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) (=s δᴹ ^ Π A B [ Pr Δᴹ ]T) ^ (A [ Pr Δᴹ ]T) [ π₁ id ]T ]T
    ≃ Aᴹ [ δᴹ ]Tᴹ [ π₁ {A = Π (A [ δ ]T) (B [ δ ^ A ]T) [ Pr Γᴹ ]T} id ^ (A [ δ ]T) [ Pr Γᴹ ]T ]T
  s = to≃ [][]T
    ◾̃ []T≃ (,C≃ p r)
           ( ∘^ ⁻¹̃
           ◾̃ ^≃' p
                 ( π₁id∘coe' (,C≃ refl (to≃ (TyNat δᴹ)))
                 ◾̃ to≃ π₁β
                 ◾̃ ∘≃ (,C≃ refl (to≃ (TyNat δᴹ)) ◾ p)
                      (π₁id≃ refl q))
           ◾̃ ∘^
           ◾̃ coe∘'' (TyNat δᴹ) ⁻¹̃)
    ◾̃ to≃ ([][]T ⁻¹)

abstract
  s' : ((B [ Pr Δᴹ ^ A ]T) [ π₁ id ^ A [ Pr Δᴹ ]T ]T) [
      coe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) (=s δᴹ ^ Π A B [ Pr Δᴹ ]T)
      ^ (A [ Pr Δᴹ ]T) [ π₁ id ]T
      ]T
      ≃
      ((B [ δ ^ A ]T) [ Pr Γᴹ ^ A [ δ ]T ]T) [
      π₁ {A = Π (A [ δ ]T) (B [ δ ^ A ]T) [ Pr Γᴹ ]T} id ^ (A [ δ ]T) [ Pr Γᴹ ]T ]T
  s' = to≃ ([][]T ◾ [][]T)
     ◾̃ []T≃ (,C≃ p r)
            ( to≃ (ass ⁻¹)
            ◾̃ (∘≃' (,C≃ refl (to≃ [][]T))
                   refl
                   refl
                   (∘^ ⁻¹̃)
                   (TmsΓ-= (,C≃ refl (to≃ [][]T)) , refl)
            ◾̃ ∘≃' (,C≃ refl (to≃ ([][]T ⁻¹)))
                  refl
                  refl
                  ((Tms-Γ= (,C≃ refl (to≃ ([][]T ⁻¹)))) , refl)
                  (uncoe (TmsΓ-= (,C≃ refl (to≃ [][]T))) ⁻¹̃)
            ◾̃ coe∘'' ([][]T ⁻¹)
            ◾̃ ∘^ ⁻¹̃
            ◾̃ ^≃' p
                  ( to≃ ass
                  ◾̃ ∘≃ p
                       ( to≃ (π₁id∘coe (,C≃ refl (to≃ (TyNat δᴹ))))
                       ◾̃ uncoe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) ⁻¹̃
                       ◾̃ to≃ π₁β
                       ◾̃ ∘≃ (,C≃ refl (to≃ (TyNat δᴹ)) ◾ p)
                            (π₁id≃ refl (to≃ (TyNat δᴹ ◾ ap (λ z → z [ Pr Γᴹ ]T) Π[]))))
                  ◾̃ to≃ ( ass ⁻¹
                        ◾ ap (λ z → z ∘ π₁ id) (PrNat δᴹ))))
            ◾̃ ∘^
            ◾̃ ∘≃' (,C≃ refl (to≃ ([][]T ⁻¹)))
                  refl
                  (,C≃ refl (to≃ (ap (λ z → z [ π₁ id ]T) ([][]T ⁻¹))))
                  ∘^
                  (^≃''' (,C≃ refl r̃)
                         refl
                         (π₁id≃ refl r̃)
                         (to≃ ([][]T ⁻¹)))
            ◾̃ to≃ ass)
     ◾̃ to≃ ([][]T ⁻¹ ◾ [][]T ⁻¹)
