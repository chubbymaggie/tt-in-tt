{-# OPTIONS --no-eta #-}

open import lib
open import JM.JM
open import TT.Syntax
open import TT.Elim
open import TT.Congr
open import TT.Laws
open import LogPred.Motives
open import LogPred.Cxt
open import LogPred.Ty
open import LogPred.Tms
open import LogPred.Tm
open import LogPred.Tm.Proofs
open import LogPred.HTy.Proofs1

open Motives M
open MethodsCon mCon
open MethodsTy mTy
open MethodsTms mTms
open MethodsTm mTm

module LogPred.HTy.Proofs4 {Γ : Con} {Γᴹ : Conᴹ Γ} {Δ : Con} {Δᴹ : Conᴹ Δ} {δ : Tms Γ Δ}
       {δᴹ : Tmsᴹ Γᴹ Δᴹ δ} {A : Ty Δ} {Aᴹ : Tyᴹ Δᴹ A} {B : Ty (Δ , A)}
 where

open import LogPred.HTy.Proofs2 {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}{B}
open import LogPred.HTy.Proofs3 {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}

abstract
  p4 : coe (TmΓ= [][]T)
        (coe (TmΓ= (pΠᴹ Δᴹ)) (app (coe (TmΓ= (pΠᴹ' Δᴹ)) (π₂ id)) [ π₁ id ]t) [
         coe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) (=s δᴹ ^ Π A B [ Pr Δᴹ ]T)
         ^ (A [ Pr Δᴹ ]T) [ π₁ id ]T
         ^ Aᴹ [ π₁ id ^ A [ Pr Δᴹ ]T ]T
         ]t)
        ≃
        coe (TmΓ= [][]T)
        (coe (TmΓ= [][]T) (π₂ id) [
         coe (TmsΓ-= (,C≃ refl (to≃ (TyNat {A = B}(δᴹ ^ᴹ Aᴹ))) ⁻¹))
         (π₁ id ^ (A [ δ ]T) [ Pr Γᴹ ]T ^ Aᴹ [ δᴹ ]Tᴹ ,
          coe (TmΓ= (pΠᴹ Γᴹ)) (app (coe (TmΓ= (pΠᴹ' Γᴹ)) (π₂ id)) [ π₁ id ]t))
         ]t)

  p4 = coecoe[]t (pΠᴹ Δᴹ) [][]T
     ◾̃ [][]t'
     ◾̃ []t≃'' (∘π₁id ⁻¹)
     ◾̃ [][]t' ⁻¹̃
     ◾̃ []t≃' (,C≃ (,C≃ p r) s)
             (,C≃ p r)
             s'
             ( to≃ (app[] ⁻¹)
             ◾̃ app≃ p
                    r
                    s'
                    ( coecoe[]t (pΠᴹ' Δᴹ) Π[]
                    ◾̃ π₂coe (,C≃ refl (to≃ (TyNat δᴹ)))
                    ◾̃ π₂β'
                    ◾̃ uncoe (TmΓ= [][]T) ⁻¹̃
                    ◾̃ π₂id≃ refl (to≃ (TyNat δᴹ ◾ ap (λ z → z [ Pr Γᴹ ]T) Π[]))
                    ◾̃ uncoe (TmΓ= (pΠᴹ' Γᴹ))))
             (π₁id≃ (,C≃ p r) s)
     ◾̃ uncoe (TmΓ= (pΠᴹ Γᴹ))
     ◾̃ π₂β' ⁻¹̃
     ◾̃ π₂coe⁻¹ (TyNat (δᴹ ^ᴹ Aᴹ)) ⁻¹̃
     ◾̃ coecoe[]t [][]T [][]T ⁻¹̃
