{-# OPTIONS --no-eta #-}

open import lib
open import JM.JM
open import TT.Syntax
open import TT.Elim
open import TT.Congr
open import TT.Laws
open import LogPred.Motives
open import LogPred.Cxt
open import LogPred.Ty
open import LogPred.Tms
open import LogPred.Tm
open import LogPred.Tm.Proofs
open import LogPred.HTy.Proofs1

open Motives M
open MethodsCon mCon
open MethodsTy mTy
open MethodsTms mTms
open MethodsTm mTm

module LogPred.HTy.Proofs5
  {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ : Con}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}
  {δᴹ : Tmsᴹ Γᴹ Δᴹ δ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}{B : Ty (Δ , A)}
  where

open import LogPred.HTy.Proofs2 {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}{B}
open import LogPred.HTy.Proofs3 {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}

abstract
  p5 : (π₁ id ^ A [ Pr Δᴹ ]T ^ Aᴹ) ∘
      (coe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ))))
       (=s δᴹ ^ Π A B [ Pr Δᴹ ]T)
       ^ (A [ Pr Δᴹ ]T) [ π₁ id ]T
       ^ Aᴹ [ π₁ id ^ A [ Pr Δᴹ ]T ]T)
      ≃
      (=s (δᴹ ^ᴹ Aᴹ) ∘ π₁ id) ∘
      coe (TmsΓ-= (,C≃ refl (to≃ (TyNat {A = B} (δᴹ ^ᴹ Aᴹ))) ⁻¹))
      (π₁ id ^ (A [ δ ]T) [ Pr Γᴹ ]T ^ Aᴹ [ δᴹ ]Tᴹ ,
       coe (TmΓ= (pΠᴹ Γᴹ)) (app (coe (TmΓ= (pΠᴹ' Γᴹ)) (π₂ id)) [ π₁ id ]t))

  p5 = ∘^ ⁻¹̃
        ◾̃ ^≃' (,C≃ refl (to≃ [][]T)) (∘^ ⁻¹̃)
        ◾̃ ^≃' (,C≃ (,C≃ refl (to≃ (TyNat δᴹ ⁻¹)))
                   ([]T≃ (,C≃ refl (to≃ (TyNat δᴹ ⁻¹)))
                         ( π₁id∘coe''' (,C≃ refl (to≃ (TyNat δᴹ)))
                         ◾̃ to≃ (∘π₁id ⁻¹))))
              (^≃' (,C≃ refl (to≃ (TyNat δᴹ ⁻¹)))
                   (π₁id∘coe''' (,C≃ refl (to≃ (TyNat δᴹ))) ◾̃ to≃ (∘π₁id ⁻¹)))
        ◾̃ ^≃' (,C≃ (,C≃ refl qqq)
                   ([]T≃ (,C≃ refl qqq)
                         (∘≃ (,C≃ refl qqq)
                             (π₁id≃ refl qqq))))
              (^≃' (,C≃ refl qqq)
                   (∘≃ (,C≃ refl qqq)
                       (π₁id≃ refl qqq)))
        ◾̃ ∘^^
        ◾̃ ∘≃' (,C≃ (,C≃ refl (to≃ (TyNat δᴹ))) ppp)
              refl
              (,C≃ (,C≃ refl (to≃ (ap (λ z → z [ π₁ id ]T) (TyNat δᴹ))))
                   (to≃ [][]T
                   ◾̃ []T≃ (,C≃ refl (to≃ (ap (λ z → z [ π₁ id ]T) (TyNat δᴹ))))
                          ( ∘^ ⁻¹̃
                          ◾̃ ∘^
                          ◾̃ coe∘'' (TyNat δᴹ) ⁻¹̃)
                   ◾̃ to≃ ([][]T ⁻¹)))
              (=s^ ⁻¹̃)
              {σ₀ = π₁ id ^ A [ Pr Δᴹ ]T [ =s δᴹ ]T ^ Aᴹ [ =s δᴹ ^ A [ Pr Δᴹ ]T ]T}
              (^≃''' (,C≃ refl (to≃ (ap (λ z → z [ π₁ id ]T) (TyNat δᴹ))))
                     ((,C≃ refl (to≃ (TyNat δᴹ))))
                     (^≃'' (TyNat δᴹ))
                     ppp)
        ◾̃ ∘≃ refl (to≃ (π₁β ⁻¹) ◾̃ π₁id∘coe⁻¹ (TyNat (δᴹ ^ᴹ Aᴹ)) ⁻¹̃)
        ◾̃ to≃ (ass ⁻¹)
    where
      ppp : Aᴹ [ =s δᴹ ^ A [ Pr Δᴹ ]T ]T
          ≃ Aᴹ [ coe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) (=s δᴹ ^ A [ Pr Δᴹ ]T) ]T
      ppp = []T≃ (,C≃ refl (to≃ (TyNat δᴹ))) (uncoe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))))

      qqq : Π A B [ Pr Δᴹ ]T [ =s δᴹ ]T
          ≃ Π (A [ δ ]T) (B [ δ ^ A ]T) [ Pr Γᴹ ]T
      qqq = to≃ (TyNat δᴹ ◾ ap (λ z → z [ Pr Γᴹ ]T) Π[])
