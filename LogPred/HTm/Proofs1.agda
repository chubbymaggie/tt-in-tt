{-# OPTIONS --no-eta #-}

open import lib
open import JM.JM
open import TT.Syntax
open import TT.Elim
open import TT.Congr
open import TT.Laws
open import LogPred.Motives
open import LogPred.Cxt
open import LogPred.Ty
open import LogPred.Tms
open import LogPred.Tm
open import LogPred.Tm.Proofs
open import LogPred.HTy
import LogPred.HTy.Proofs3
open import LogPred.HTms

open Motives M
open MethodsCon mCon
open MethodsTy mTy
open MethodsTm mTm
open MethodsTms mTms
open MethodsHTy mHTy
open MethodsHTms mHTms

module LogPred.HTm.Proofs1
  {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
  {A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}{B : Ty (Δ , A)}{Bᴹ : Tyᴹ (Δᴹ ,Cᴹ Aᴹ) B}
  {t : Tm (Δ , A) B}{tᴹ : Tmᴹ (Δᴹ ,Cᴹ Aᴹ) Bᴹ t}
  where

abstract
  lam[]ᴹ' : lamᴹ {Γᴹ = Δᴹ} tᴹ [ δᴹ ]tᴹ
          ≃ lamᴹ {Γᴹ = Γᴹ} (tᴹ [ δᴹ ^ᴹ Aᴹ ]tᴹ)
  lam[]ᴹ'

    = uncoe (TmΓ= p[]tᴹ) ⁻¹̃
    ◾̃ coe[]t' (plamᴹ {Γᴹ = Δᴹ})
    ◾̃ from≡ (TmΓ= Π[]) lam[]
    ◾̃ lam≃ refl
           r̃
           (to≃ Π[])
           (from≡ (TmΓ= Π[]) lam[])
    ◾̃ lam≃ refl
           (to≃ (TyNat δᴹ))
           (Π≃ pΓA pAᴹ pBᴹ)
           (lam≃ pΓA
                 ([]T≃ pΓA (uncoe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ))))))
                 pBᴹ
                 ([]t≃ (,C≃ pΓA pAᴹ) (=s^ ⁻¹̃) ◾̃ uncoe (TmΓ= p[]tᴹ)))
    ◾̃ uncoe (TmΓ= (plamᴹ {Γᴹ = Γᴹ}))
    
    where

      open LogPred.HTy.Proofs3 {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ} hiding (_^ᴹ_)

      pΓA : _≡_ {A = Con}
                (=C Γᴹ , A [ Pr Δᴹ ]T [ =s δᴹ ]T)
                (=C Γᴹ , A [ δ ]T [ Pr Γᴹ ]T)
      pΓA = ,C≃ refl (to≃ (TyNat δᴹ))

      pAᴹ : Aᴹ [ =s δᴹ ^ A [ Pr Δᴹ ]T ]T ≃ Aᴹ [ δᴹ ]Tᴹ
      pAᴹ = []T≃ pΓA (uncoe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))))

      pBᴹ : Bᴹ [ < t [ Pr (Δᴹ ,Cᴹ Aᴹ) ]t > ]T [ =s δᴹ ^ A [ Pr Δᴹ ]T ^ Aᴹ ]T
          ≃ Bᴹ [ δᴹ ^ᴹ Aᴹ ]Tᴹ [ < t [ δ ^ A ]t [ Pr (Γᴹ ,Cᴹ Aᴹ [ δᴹ ]Tᴹ) ]t > ]T
      pBᴹ = to≃ [][]T
          ◾̃ []T≃ (,C≃ pΓA pAᴹ)
                 ( to≃ ,∘
                 ◾̃ ,s≃ (,C≃ pΓA pAᴹ)
                       refl
                       (to≃ idl ◾̃ =s^ ⁻¹̃)
                       r̃
                       ( coecoe[]t ([id]T ⁻¹) [][]T
                       ◾̃ []t≃ (,C≃ pΓA pAᴹ) (=s^ ⁻¹̃)
                       ◾̃ TmNat (δᴹ ^ᴹ Aᴹ)
                       ◾̃ uncoe (TmΓ= (TyNat (δᴹ ^ᴹ Aᴹ) ⁻¹)))
                 ◾̃ to≃ (^∘<> (TyNat (δᴹ ^ᴹ Aᴹ)) ⁻¹))
          ◾̃ to≃ [][]T ⁻¹̃
