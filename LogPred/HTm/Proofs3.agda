{-# OPTIONS --no-eta #-}

module LogPred.HTm.Proofs3 where

open import lib
open import JM.JM
open import TT.Syntax
open import TT.Elim
open import TT.Congr
open import TT.Laws
open import LogPred.Motives
open import LogPred.Cxt
open import LogPred.Ty
open import LogPred.Tms
open import LogPred.Tm
open import LogPred.Tm.Proofs
open import LogPred.HTy
open import LogPred.HTms

open Motives M
open MethodsCon mCon
open MethodsTy mTy
open MethodsTm mTm
open MethodsTms mTms
open MethodsHTy mHTy
open MethodsHTms mHTms

abstract
  π₂βᴹ : {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ : Con}{Δᴹ : Conᴹ Δ}{A : Ty Δ}
         {Aᴹ : Tyᴹ Δᴹ A}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
         {a : Tm Γ (A [ δ ]T)}{aᴹ : Tmᴹ Γᴹ (Aᴹ [ δᴹ ]Tᴹ) a}
       → π₂ᴹ (δᴹ ,sᴹ aᴹ)
       ≡[ TmΓᴹ= (ap (_[_]T A) π₁β) ([]Tᴹ= Aᴹ π₁β π₁βᴹ) π₂β ]≡
         aᴹ
  π₂βᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{A}{Aᴹ}{δ}{δᴹ}{a}{aᴹ}

    = from≃ ( uncoe (TmΓᴹ= (ap (_[_]T A) π₁β) ([]Tᴹ= Aᴹ π₁β π₁βᴹ) π₂β) ⁻¹̃
            ◾̃ uncoe (TmΓ= pπ₂ᴹ) ⁻¹̃
            ◾̃ π₂β'
            ◾̃ uncoe (TmΓ= p,sᴹ) ⁻¹̃)

applam : ∀{Γ}{A₀ A₁ : Ty Γ}
         {B₀ : Ty (Γ , A₀)}{B₁ : Ty (Γ , A₁)}
         {C₀₀ C₀₁ : Ty (Γ , A₀ , B₀)}{C₁ : Ty (Γ , A₁ , B₁)}
         {t : Tm (Γ , A₀ , B₀) C₀₀}
         {ΠABC₁ : Ty Γ}
         (ΠABC₂ : Π A₀ (Π B₀ C₀₀) ≡ ΠABC₁)
         {ΠBC₁ : Ty (Γ , A₁)}
         (AΠBC₂ : ΠABC₁ ≡ Π A₁ ΠBC₁)
         (BC₂ : ΠBC₁ ≡ Π B₁ C₁)
         (C₀₂ : C₀₀ ≡ C₀₁)
         (p : Tm (Γ , A₁ , B₁) C₁ ≡ Tm (Γ , A₀ , B₀) C₀₁)
       → coe p (app (coe (TmΓ= BC₂) (app (coe (TmΓ= AΠBC₂) ((coe (TmΓ= ΠABC₂) (lam (lam t))))))))
       ≃ t
applam refl refl refl refl refl = to≃ (ap app Πβ ◾ Πβ)

lamapp : ∀{Γ}{A₀ A₁ : Ty Γ}
         {B₀ : Ty (Γ , A₀)}{B₁ : Ty (Γ , A₁)}
         {C₀ : Ty (Γ , A₀ , B₀)}{C₁ : Ty (Γ , A₁ , B₁)}
         {ΠABC₀ ΠABC₁ : Ty Γ}
         {t : Tm Γ ΠABC₀}
         {ΠBC₀ : Ty (Γ , A₀)}
         (AΠBC₀ : ΠABC₀ ≡ Π A₀ ΠBC₀)
         (BΠC₀ : ΠBC₀ ≡ Π B₀ C₀)
         (ΓAB₂ : (Γ , A₀ , B₀) ≡ (Γ , A₁ , B₁))
         (C₂ : C₀ ≃ C₁)
         (p : Π A₁ (Π B₁ C₁) ≡ ΠABC₁)
       → coe (TmΓ= p) (lam (lam (coe (Tm≃ ΓAB₂ C₂) (app (coe (TmΓ= BΠC₀) (app (coe (TmΓ= AΠBC₀) t)))))))
       ≃ t
lamapp refl refl refl (refl , refl) refl = to≃ (ap lam Πη ◾ Πη)
