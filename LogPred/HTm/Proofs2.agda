{-# OPTIONS --no-eta #-}

open import lib
open import JM.JM
open import TT.Syntax
open import TT.Elim
open import TT.Congr
open import TT.Laws
open import LogPred.Motives
open import LogPred.Cxt
open import LogPred.Ty
open import LogPred.Tms
open import LogPred.Tm
open import LogPred.Tm.Proofs
open import LogPred.HTy
open import LogPred.HTms
import LogPred.HTm.Proofs1

open Motives M
open MethodsCon mCon
open MethodsTy mTy
open MethodsTm mTm
open MethodsTms mTms
open MethodsHTy mHTy
open MethodsHTms mHTms

module LogPred.HTm.Proofs2
  {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
  {A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}{B : Ty (Δ , A)}{Bᴹ : Tyᴹ (Δᴹ ,Cᴹ Aᴹ) B}
  {t : Tm (Δ , A) B}{tᴹ : Tmᴹ (Δᴹ ,Cᴹ Aᴹ) Bᴹ t}
  where

eq : Tm (=C Γᴹ) (Πᴹ {Γᴹ = Δᴹ} Aᴹ Bᴹ [ δᴹ ]Tᴹ [ < lam t [ δ ]t [ Pr Γᴹ ]t > ]T)
   ≡ Tm (=C Γᴹ) (Πᴹ {Γᴹ = Γᴹ} (Aᴹ [ δᴹ ]Tᴹ) (Bᴹ [ δᴹ ^ᴹ Aᴹ ]Tᴹ) [ < lam (t [ δ ^ A ]t) [ Pr Γᴹ ]t > ]T)
eq = TmΓᴹ= (Π[] {Γ}{Δ}{δ}{A}{B})
           {Πᴹ {Γᴹ = Δᴹ} Aᴹ Bᴹ [ δᴹ ]Tᴹ}
           {Πᴹ {Γᴹ = Γᴹ} (Aᴹ [ δᴹ ]Tᴹ) (Bᴹ [ δᴹ ^ᴹ Aᴹ ]Tᴹ)}
           (Π[]ᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}{B}{Bᴹ})
           {lam t [ δ ]t}
           {lam (t [ δ ^ A ]t)}
           (lam[] {Γ}{Δ}{δ}{A}{B}{t})

abstract
  lam[]ᴹ : lamᴹ {Γᴹ = Δᴹ} tᴹ [ δᴹ ]tᴹ
         ≡[ eq ]≡
           lamᴹ {Γᴹ = Γᴹ} (tᴹ [ δᴹ ^ᴹ Aᴹ ]tᴹ)
  lam[]ᴹ

    = from≃ (uncoe eq ⁻¹̃ ◾̃ lam[]ᴹ')

    where
      open LogPred.HTm.Proofs1 {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}{B}{Bᴹ}{t}{tᴹ}
