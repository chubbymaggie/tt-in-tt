{-# OPTIONS --no-eta #-}

module LogPred.Motives where

open import lib
open import TT.Syntax
open import TT.Elim

record Conᵐ (Γ : Con) : Set where
  constructor cConᵐ
  field
    =C : Con
    Pr : Tms =C Γ

open Conᵐ public

Tyᵐ : {Γ : Con} → Conᵐ Γ → Ty Γ → Set
Tyᵐ {Γ} Γᴹ A = Ty (=C Γᴹ , A [ Pr Γᴹ ]T)

record Tmsᵐ {Γ Δ : Con}(Γᴹ : Conᵐ Γ)(Δᴹ : Conᵐ Δ)(ρ : Tms Γ Δ) : Set where
  constructor cTmsᵐ
  field
    =s    : Tms (=C Γᴹ) (=C Δᴹ)
    PrNat : (Pr Δᴹ) ∘ =s ≡ ρ ∘ (Pr Γᴹ)

open Tmsᵐ public

Tmᵐ : {Γ : Con} (Γᴹ : Conᵐ Γ) {A : Ty Γ} → Tyᵐ Γᴹ A → Tm Γ A → Set
Tmᵐ {Γ} Γᴹ {A} Aᴹ a = Tm (=C Γᴹ) (Aᴹ [ < a [ Pr Γᴹ ]t > ]T)

M : Motives
M = record { Conᴹ = Conᵐ
           ; Tyᴹ = Tyᵐ
           ; Tmsᴹ = Tmsᵐ
           ; Tmᴹ = Tmᵐ }
