{-# OPTIONS --no-eta #-}

module LogPred.LogPred where

open import TT.Syntax
open import TT.Elim
open import LogPred.Motives
open import LogPred.Cxt
open import LogPred.Ty
open import LogPred.Tms
open import LogPred.Tm
open import LogPred.HTy
open import LogPred.HTms
open import LogPred.HTm

open elim M mCon mTy mTms mTm mHTy mHTms mHTm

-- the logical predicate interpretation

_ᴾᶜ : Con → Con
_ᴾˢ : ∀{Γ Δ} → Tms Γ Δ → Tms (Γ ᴾᶜ) (Δ ᴾᶜ)
pr  : (Γ : Con) → Tms (Γ ᴾᶜ) Γ
_ᴾᵀ : ∀{Γ}(A : Ty Γ) → Ty ((Γ ᴾᶜ) , A [ pr Γ ]T)
_ᴾᵗ : ∀{Γ}{A : Ty Γ}(t : Tm Γ A) → Tm (Γ ᴾᶜ) ((A ᴾᵀ) [ < t [ pr Γ ]t > ]T)

Γ ᴾᶜ = =C (Con-elim Γ)
ρ ᴾˢ = =s (Tms-elim ρ)
pr Γ = Pr (Con-elim Γ)
A ᴾᵀ = Ty-elim A
t ᴾᵗ = Tm-elim t
