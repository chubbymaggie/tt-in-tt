{-# OPTIONS --no-eta #-}

module LogPred.HTm where

open import lib
open import JM.JM
open import TT.Syntax
open import TT.Elim
open import TT.Congr
open import TT.Laws
open import LogPred.Motives
open import LogPred.Cxt
open import LogPred.Ty
open import LogPred.Tms
open import LogPred.Tm
open import LogPred.Tm.Proofs
open import LogPred.HTy
open import LogPred.HTms
import LogPred.HTm.Proofs2
open import LogPred.HTm.Proofs3

open Motives M
open MethodsCon mCon
open MethodsTy mTy
open MethodsTm mTm
open MethodsTms mTms
open MethodsHTy mHTy
open MethodsHTms mHTms

mHTm : MethodsHTm M mCon mTy mTms mTm mHTy mHTms
mHTm = record

  { π₂βᴹ   = π₂βᴹ
  
  ; lam[]ᴹ = λ { {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}{B}{Bᴹ}{t}{tᴹ}
             → let
                 open LogPred.HTm.Proofs2 {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}{B}{Bᴹ}{t}{tᴹ}
               in
                 lam[]ᴹ }
             
  ; Πβᴹ = λ { {Γ}{Γᴹ}{A}{Aᴹ}{B}{Bᴹ}{t}{tᴹ}
          → from≃ ( uncoe (TmΓAᴹ= Πβ) ⁻¹̃
                  ◾̃ applam {t = tᴹ}
                           (plamᴹ {Γᴹ = Γᴹ})
                           Π[]
                           Π[]
                           (ap (λ z → Bᴹ [ < z [ Pr (Γᴹ ,Cᴹ Aᴹ) ]t > ]T) (Πβ ⁻¹))
                           (pappᴹ {Γᴹ = Γᴹ})) }
  
  ; Πηᴹ    = λ { {Γ}{Γᴹ}{A}{Aᴹ}{B}{Bᴹ}{t}{tᴹ}
             → let
                 open eqΓABᴹ {Γ}{Γᴹ}{A}{Aᴹ}{B}{Bᴹ}{t}
               in
                 from≃ ( uncoe (TmΓAᴹ= Πη) ⁻¹̃
                       ◾̃ lamapp {t = tᴹ}
                                Π[]
                                Π[]
                                eqΓAPrAᴹ
                                eqBᴹ
                                (plamᴹ {Γᴹ = Γᴹ})) } }
