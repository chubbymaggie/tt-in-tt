{-# OPTIONS --no-eta #-}

open import lib
open import TT.Syntax

module LogPred.Tm
  (Uᴹ  : Ty (• , U))
  (Elᴹ : Ty (• , U , El (coe (TmΓ= U[]) vz) , Uᴹ [ wk ]T))
  where

open import TT.Elim
open import LogPred.Motives
open import LogPred.Cxt
open import LogPred.Ty Uᴹ Elᴹ
open import LogPred.Tms Uᴹ Elᴹ
open import LogPred.Tm.Proofs Uᴹ Elᴹ

open MethodsCon mCon
open MethodsTy mTy
open MethodsTms mTms
open Conᵐ
open Tmsᵐ

mTm : MethodsTm M mCon mTy mTms
mTm = record

  { _[_]tᴹ = λ { tᴹ δᴹ        → coe (TmΓ= p[]tᴹ) (tᴹ [ =s δᴹ ]t) }

  ; π₂ᴹ    = λ { δᴹ           → coe (TmΓ= pπ₂ᴹ) (π₂ (=s δᴹ)) }

  ; appᴹ   = λ { {Γᴹ = Γᴹ} tᴹ → coe (pappᴹ {Γᴹ = Γᴹ})
                                     (app (coe (TmΓ= Π[]) (app (coe (TmΓ= Π[]) tᴹ)))) }

  ; lamᴹ   = λ { {Γᴹ = Γᴹ} tᴹ → coe (TmΓ= (plamᴹ {Γᴹ = Γᴹ})) (lam (lam tᴹ))} }
