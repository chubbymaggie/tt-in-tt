{-# OPTIONS --no-eta #-}

open import lib
open import JM.JM
open import TT.Syntax

module LogPred.Ty
  (Uᴹ  : Ty (• , U))
  (Elᴹ : Ty (• , U , El (coe (TmΓ= U[]) vz) , Uᴹ [ wk ]T))
  where

open import TT.Elim
open import TT.Congr
open import TT.Laws
open import LogPred.Motives
open import LogPred.Cxt

open Motives M
open MethodsCon mCon

abstract
  TyNat : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}
          {Γᴹ : Conᴹ Γ}{Δᴹ : Conᴹ Δ}(δᴹ : Tmsᴹ Γᴹ Δᴹ δ)
        → A [ Pr Δᴹ ]T [ =s δᴹ ]T ≡ A [ δ ]T [ Pr Γᴹ ]T
  TyNat δᴹ = [][]T ◾ ap (_[_]T _) (PrNat δᴹ) ◾ [][]T ⁻¹

abstract
  pElᴹ : {Γ : Con}{Â : Tm Γ U}
         (Γᴹ : Conᴹ Γ)(Âᴹ : Tmᴹ Γᴹ (Π (El (coe (TmΓ= ([][]T ◾ U[])) (π₂ id))) U) Â)
       → Π (El (coe (TmΓ= ([][]T ◾ U[])) (π₂ id))) U [ < Â [ Pr Γᴹ ]t > ]T
       ≡ Π (El Â [ Pr Γᴹ ]T) U
  pElᴹ {Γ}{Â} Γᴹ Âᴹ

    = Π[]
    ◾ Π≃' (from≃ p) (to≃ U[] ◾̃ U≃ (,C≃ refl p))

    where
      p : El (coe (TmΓ= ([][]T ◾ U[])) (π₂ id)) [ < Â [ Pr Γᴹ ]t > ]T
        ≃ El Â [ Pr Γᴹ ]T
      p = to≃ El[]
        ◾̃ El≃ refl
              ( coecoe[]t ([][]T ◾ U[]) U[]
              ◾̃ π₂[]' ◾̃ π₂≃ idl ◾̃ π₂β'
              ◾̃ uncoe (TmΓ= ([id]T ⁻¹)) ⁻¹̃ ◾̃ uncoe (TmΓ= U[]))
        ◾̃ to≃ (El[] ⁻¹)

abstract
  pΠᴹ : ∀{Γ}(Γᴹ : Conᴹ Γ){A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}{B : Ty (Γ , A)}
      → B [ Pr Γᴹ ^ A ]T [ π₁ {A = Π A B [ Pr Γᴹ ]T} id ^ A [ Pr Γᴹ ]T ]T [ π₁ id ]T
      ≡ B [ Pr (Γᴹ ,Cᴹ Aᴹ) ]T [ π₁ id ^ A [ Pr Γᴹ ]T ^ Aᴹ ]T
  pΠᴹ {Γ} Γᴹ {A}{Aᴹ}{B}

    = [][]T
    ◾ [][]T
    ◾ ap (_[_]T B)
         (ap (_∘_ (Pr Γᴹ ^ A)) ∘π₁id ◾ ass ⁻¹)
    ◾ [][]T ⁻¹

abstract
  pΠᴹ' : ∀{Γ}(Γᴹ : Conᴹ Γ){A : Ty Γ}{B : Ty (Γ , A)}
       → Π A B [ Pr Γᴹ ]T [ π₁ {A = Π A B [ Pr Γᴹ ]T} id ]T
       ≡ Π (A [ Pr Γᴹ ]T [ π₁ id ]T)
           (B [ Pr Γᴹ ^ A ]T [ π₁ id ^ A [ Pr Γᴹ ]T ]T)
  pΠᴹ' Γᴹ = ap (λ z → z [ π₁ id ]T) Π[] ◾ Π[]

abstract
  pU : ∀{Γ}(Γᴹ : Conᴹ Γ){A  : Ty (=C Γᴹ)}
     → U [ Pr Γᴹ ]T [ wk {A = A} ]T ≡ U [ ε ]T
  pU _ = [][]T ◾ U[] ◾ U[] ⁻¹

abstract
  pEl : ∀{Γ}{Γᴹ : Conᴹ Γ}
        {Â  : Tm Γ U}(Âᴹ : Tmᴹ Γᴹ (Uᴹ [ ε , coe (TmΓ= (pU Γᴹ)) vz ]T) Â)
      → El Â [ Pr Γᴹ ]T [ wk {A = El Â [ Pr Γᴹ ]T} ]T
      ≡ El (coe (TmΓ= U[]) vz) [ ε , coe (TmΓ= (pU Γᴹ)) (Â [ Pr Γᴹ ]t [ wk ]t) ]T
  pEl Âᴹ = [][]T
         ◾ El[]
         ◾ ap El (from≃ ( uncoe (TmΓ= U[]) ⁻¹̃
                        ◾̃ [][]t' ⁻¹̃
                        ◾̃ uncoe (TmΓ= ([][]T ◾ U[] ◾ U[] ⁻¹))
                        ◾̃ π₂idβ ⁻¹̃
                        ◾̃ coe[]t' U[] ⁻¹̃
                        ◾̃ uncoe (TmΓ= U[])))
         ◾ El[] ⁻¹

abstract
  pEl' : ∀{Γ}{Γᴹ : Conᴹ Γ}
         {Â  : Tm Γ U}(Âᴹ : Tmᴹ Γᴹ (Uᴹ [ ε , coe (TmΓ= (pU Γᴹ)) vz ]T) Â)
       → Uᴹ [ ε , coe (TmΓ= (pU Γᴹ)) vz ]T [ < Â [ Pr Γᴹ ]t > ]T [ wk ]T
       ≡ Uᴹ [ wk ]T [ ε , coe (TmΓ= (pU Γᴹ)) (Â [ Pr Γᴹ ]t [ wk ]t) , coe (TmΓ= (pEl Âᴹ)) vz ]T
  pEl' {Γᴹ = Γᴹ} Âᴹ
    = [][]T
    ◾ [][]T
    ◾ from≃ ([]T≃ refl
                  (to≃ ( ,∘
                       ◾ ,s≃' εη
                              ( uncoe (TmΓ= [][]T) ⁻¹̃
                              ◾̃ coe[]t' (pU Γᴹ)
                              ◾̃ []t≃ refl (to≃ ,∘)
                              ◾̃ π₂idβ
                              ◾̃ uncoe (TmΓ= [][]T) ⁻¹̃
                              ◾̃ coe[]t' ([id]T ⁻¹)
                              ◾̃ uncoe (TmΓ= (pU Γᴹ)))
                       ◾ π₁idβ ⁻¹)))
    ◾ [][]T ⁻¹

mTy : MethodsTy M mCon
mTy = record

  { _[_]Tᴹ = λ { {Γ} {Γᴹ} {Δ} {Δᴹ} {A} Aᴹ {δ} δᴹ
  
             → Aᴹ [ coe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ))))
                        (=s δᴹ ^ A [ Pr Δᴹ ]T) ]T}
                   
  ; Uᴹ     = λ {Γ}{Γᴹ} → Uᴹ [ ε , coe (TmΓ= (pU Γᴹ)) vz ]T
             -- Π (El (coe (TmΓ= ([][]T ◾ U[])) (π₂ id))) U
                         
  ; Elᴹ    = λ {Γ}{Γᴹ}{Â} Âᴹ → Elᴹ [ ε
                                   , coe (TmΓ= (pU Γᴹ))   (Â [ Pr Γᴹ ]t [ wk ]t)
                                   , coe (TmΓ= (pEl Âᴹ))  vz
                                   , coe (TmΓ= (pEl' Âᴹ)) (Âᴹ [ wk ]t) ]T
             -- λ { {Γ} {Γᴹ} {Â} Âᴹ → El (app (coe (TmΓ= (pElᴹ Γᴹ Âᴹ)) Âᴹ)) }
  
  ; Πᴹ     = λ { {Γ} {Γᴹ} {A} Aᴹ {B} Bᴹ
  
             → Π (A [ Pr Γᴹ ]T [ π₁ id ]T)
                 (Π (Aᴹ [ π₁ id ^ A [ Pr Γᴹ ]T ]T)
                    (Bᴹ [ (π₁ id ^ A [ Pr Γᴹ ]T ^ Aᴹ)
                        , coe (TmΓ= (pΠᴹ Γᴹ))
                              (app (coe (TmΓ= (pΠᴹ' Γᴹ)) (π₂ id)) [ π₁ id ]t) ]T)) } }
