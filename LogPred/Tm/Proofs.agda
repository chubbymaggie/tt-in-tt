{-# OPTIONS --no-eta #-}

open import lib
open import TT.Syntax

module LogPred.Tm.Proofs
  (Uᴹ  : Ty (• , U))
  (Elᴹ : Ty (• , U , El (coe (TmΓ= U[]) vz) , Uᴹ [ wk ]T))
  where

open import JM.JM
open import TT.Elim
open import TT.Congr
open import TT.Laws
open import LogPred.Motives
open import LogPred.Cxt
open import LogPred.Ty Uᴹ Elᴹ
open import LogPred.Tms Uᴹ Elᴹ

open Motives M
open MethodsCon mCon
open MethodsTy mTy
open MethodsTms mTms

abstract
  TmNat : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}
          {Γᴹ : Conᴹ Γ}{Δᴹ : Conᴹ Δ}(δᴹ : Tmsᴹ Γᴹ Δᴹ δ)
          {t : Tm Δ A}
        → t [ Pr Δᴹ ]t [ =s δᴹ ]t ≃ t [ δ ]t [ Pr Γᴹ ]t
  TmNat δᴹ = [][]t' ◾̃ []t≃'' (PrNat δᴹ) ◾̃ [][]t' ⁻¹̃

abstract
  p[]tᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}{t : Tm Δ A}
          {δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
        → Aᴹ [ < t [ Pr Δᴹ ]t > ]T [ =s δᴹ ]T
        ≡ Aᴹ [ δᴹ ]Tᴹ [ < t [ δ ]t [ Pr Γᴹ ]t > ]T
  p[]tᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{A}{Aᴹ}{t}{δ}{δᴹ}

    = [][]T
    ◾ ap (_[_]T Aᴹ)
         ( ,∘
         ◾ ,s≃' idl
                ( coecoe[]t ([id]T ⁻¹) [][]T
                ◾̃ TmNat δᴹ
                ◾̃ uncoe (TmΓ= (TyNat δᴹ ⁻¹)))
         ◾ ^∘<> (TyNat δᴹ) ⁻¹)
    ◾ [][]T ⁻¹
    
abstract
  pπ₂ᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
         {δ : Tms Γ (Δ , A)}{δᴹ : Tmsᴹ Γᴹ (Δᴹ ,Cᴹ Aᴹ) δ}
       → Aᴹ [ π₁ (=s δᴹ) ]T
       ≡ Aᴹ [ π₁ᴹ {Δᴹ = Δᴹ} δᴹ ]Tᴹ [ < π₂ δ [ Pr Γᴹ ]t > ]T
  pπ₂ᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{A}{Aᴹ}{δ}{δᴹ}

    = ap (_[_]T Aᴹ)
         ( πη ⁻¹
         ◾ ,s≃' refl
                ( π₂≃ (ap π₁ idl ⁻¹ ◾ π₁∘ ⁻¹ ◾ idl ⁻¹)
                ◾̃ π₂[]' ⁻¹̃
                ◾̃ coecoe[]t [][]T [][]T ⁻¹̃
                ◾̃ π₂β' ⁻¹̃
                ◾̃ π₂≃ (,∘ ⁻¹ ◾ ass ⁻¹ ◾ PrNat δᴹ)
                ◾̃ π₂[]' ⁻¹̃
                ◾̃ uncoe (TmΓ= (TyNat (π₁ᴹ δᴹ) ⁻¹)))
         ◾ ^∘<> (TyNat (π₁ᴹ δᴹ)) ⁻¹)
    ◾ [][]T ⁻¹

module eqΓABᴹ {Γ : Con}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}{B : Ty (Γ , A)}
              {Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B}
              {t : Tm Γ (Π A B)} where

  abstract
    eqAPr : A [ Pr Γᴹ ]T [ π₁ id ]T [ < t [ Pr Γᴹ ]t > ]T
          ≃ A [ Pr Γᴹ ]T
    eqAPr = to≃ ( [][]T ◾ ap (_[_]T _) π₁idβ ◾ [id]T)

  abstract
    eqΓAPr : _≡_ {A = Con}
                 (=C Γᴹ , A [ Pr Γᴹ ]T [ π₁ id ]T [ < t [ Pr Γᴹ ]t > ]T)
                 (=C Γᴹ , A [ Pr Γᴹ ]T)
    eqΓAPr = ,C≃ refl eqAPr

  abstract
    eqAᴹ : Aᴹ [ π₁ id ^ A [ Pr Γᴹ ]T ]T [ < t [ Pr Γᴹ ]t > ^ (A [ Pr Γᴹ ]T) [ π₁ id ]T ]T
         ≃ Aᴹ
    eqAᴹ = to≃ [][]T
         ◾̃ []T≃ eqΓAPr (∘^ ⁻¹̃ ◾̃ (^≃ π₁idβ ◾̃ id^))
         ◾̃ to≃ [id]T

  abstract
    eqΓAPrAᴹ : =C Γᴹ
             , A [ Pr Γᴹ ]T [ π₁ id ]T [ < t [ Pr Γᴹ ]t > ]T
             , Aᴹ [ π₁ id ^ A [ Pr Γᴹ ]T ]T [ < t [ Pr Γᴹ ]t > ^ (A [ Pr Γᴹ ]T) [ π₁ id ]T ]T
             ≡ =C (Γᴹ ,Cᴹ Aᴹ)
    eqΓAPrAᴹ = ,C≃ eqΓAPr eqAᴹ

  abstract
    eqBPr : B [ Pr Γᴹ ^ A ]T [ π₁ id ^ A [ Pr Γᴹ ]T ]T
              [ < t [ Pr Γᴹ ]t > ^ (A [ Pr Γᴹ ]T) [ π₁ id ]T ]T
          ≃ B [ Pr Γᴹ ^ A ]T
    eqBPr = to≃ [][]T
          ◾̃ []T≃ eqΓAPr (∘^ ⁻¹̃ ◾̃ ^≃ π₁idβ ◾̃ id^)
          ◾̃ to≃ [id]T

  abstract
    eqBᴹ : Bᴹ [  _,_  (π₁ id ^ A [ Pr Γᴹ ]T ^ Aᴹ)
                      {B [ Pr (Γᴹ ,Cᴹ Aᴹ) ]T}
                      (coe (TmΓ= (pΠᴹ Γᴹ)) (app (coe (TmΓ= (pΠᴹ' Γᴹ)) (π₂ id)) [ π₁ id ]t))  ]T
              [ < t [ Pr Γᴹ ]t > ^ (A [ Pr Γᴹ ]T) [ π₁ id ]T ^ Aᴹ [ π₁ id ^ A [ Pr Γᴹ ]T ]T ]T
         ≃ Bᴹ [ < app t [ Pr (Γᴹ ,Cᴹ Aᴹ) ]t > ]T
    eqBᴹ = to≃ [][]T
         ◾̃ []T≃ eqΓAPrAᴹ
                ( to≃ ,∘
                ◾̃ ,s≃ eqΓAPrAᴹ
                      refl
                      ( ∘^^ ⁻¹̃
                      ◾̃ ^≃' (,C≃ refl ([]T≃ refl (to≃ π₁idβ)))
                            (^≃ π₁idβ)
                      ◾̃ (^≃' (,C≃ refl (to≃ [id]T)) id^ ◾̃ id^))
                      r̃
                      ( coecoe[]t (pΠᴹ Γᴹ) [][]T
                      ◾̃ [][]t'
                      ◾̃ []t≃'' (∘π₁id ⁻¹)
                      ◾̃ [][]t' ⁻¹̃
                      ◾̃ []t≃' eqΓAPrAᴹ
                              eqΓAPr
                              eqBPr
                              ( to≃ (app[] ⁻¹)
                              ◾̃ app≃ refl
                                     eqAPr
                                     eqBPr
                                     ( coecoe[]t (pΠᴹ' Γᴹ) Π[]
                                     ◾̃ π₂idβ
                                     ◾̃ uncoe (TmΓ= ([id]T ⁻¹)) ⁻¹̃ ◾̃ uncoe (TmΓ= Π[]))
                              ◾̃ to≃ app[])
                              (π₁id≃ eqΓAPr eqAᴹ)
                      ◾̃ [][]t'
                      ◾̃ uncoe (TmΓ= ([id]T ⁻¹))))

pappᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}{B : Ty (Γ , A)}{Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B}
        {t : Tm Γ (Π A B)}
      → Tm ( =C Γᴹ
           , A [ Pr Γᴹ ]T [ π₁ id ]T [ < t [ Pr Γᴹ ]t > ]T
           , Aᴹ [ π₁ id ^ A [ Pr Γᴹ ]T ]T [ < t [ Pr Γᴹ ]t > ^ (A [ Pr Γᴹ ]T) [ π₁ id ]T ]T)
           (Bᴹ [ π₁ id ^ A [ Pr Γᴹ ]T ^ Aᴹ , coe (TmΓ= (pΠᴹ Γᴹ)) (app (coe (TmΓ= (pΠᴹ' Γᴹ)) (π₂ id)) [ π₁ id ]t) ]T
               [ < t [ Pr Γᴹ ]t > ^ (A [ Pr Γᴹ ]T) [ π₁ id ]T ^ Aᴹ [ π₁ id ^ A [ Pr Γᴹ ]T ]T ]T)
      ≡ Tm (=C (Γᴹ ,Cᴹ Aᴹ)) (Bᴹ [ < app t [ Pr (Γᴹ ,Cᴹ Aᴹ) ]t > ]T)
pappᴹ {Γ}{Γᴹ}{A}{Aᴹ}{B}{Bᴹ}{t}

  = let
      open eqΓABᴹ {Γ}{Γᴹ}{A}{Aᴹ}{B}{Bᴹ}{t}
    in
      Tm≃ eqΓAPrAᴹ eqBᴹ

abstract
  plamᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}{B : Ty (Γ , A)}{Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B}
          {t : Tm (Γ , A) B}
        → Π (A [ Pr Γᴹ ]T) (Π Aᴹ (Bᴹ [ < t [ Pr (Γᴹ ,Cᴹ Aᴹ) ]t > ]T))
        ≡ Πᴹ {Γᴹ = Γᴹ} Aᴹ Bᴹ [ < lam t [ Pr Γᴹ ]t > ]T
  plamᴹ {Γ}{Γᴹ}{A}{Aᴹ}{B}{Bᴹ}{t}

    = let
        open eqΓABᴹ {Γ}{Γᴹ}{A}{Aᴹ}{B}{Bᴹ}{lam t}
      in
          Π≃' (from≃ (eqAPr ⁻¹̃))
              ( Π≃ (eqΓAPr ⁻¹)
                   (eqAᴹ ⁻¹̃)
                   ( to≃ (ap (λ z → Bᴹ [ < z [ Pr (Γᴹ ,Cᴹ Aᴹ) ]t > ]T) (Πβ ⁻¹))
                   ◾̃ eqBᴹ ⁻¹̃)
              ◾̃ to≃ (Π[] ⁻¹))
        ◾ Π[] ⁻¹
