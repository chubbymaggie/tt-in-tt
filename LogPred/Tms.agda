{-# OPTIONS --no-eta #-}

open import lib
open import TT.Syntax

module LogPred.Tms
  (Uᴹ  : Ty (• , U))
  (Elᴹ : Ty (• , U , El (coe (TmΓ= U[]) vz) , Uᴹ [ wk ]T))
  where

open import JM.JM
open import TT.Elim
open import TT.Congr
open import TT.Laws
open import LogPred.Motives
open import LogPred.Cxt
open import LogPred.Ty Uᴹ Elᴹ

open Motives M
open MethodsCon mCon
open MethodsTy mTy

abstract
  p,sᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
       {A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}{t : Tm Γ (A [ δ ]T)}
     → Aᴹ [ δᴹ ]Tᴹ [ id , coe (TmΓ= ([id]T ⁻¹)) (t [ Pr Γᴹ ]t) ]T
     ≡ Aᴹ [ =s δᴹ , coe (TmΓ= (TyNat δᴹ ⁻¹)) (t [ Pr Γᴹ ]t) ]T
  p,sᴹ {δᴹ = δᴹ}  = [][]T ◾ ap (_[_]T _) (^∘<> (TyNat δᴹ))

abstract
  PrNat,sᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
             {A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}{t : Tm Γ (A [ δ ]T)}{tᴹ : Tmᴹ Γᴹ (Aᴹ [ δᴹ ]Tᴹ) t}
           → Pr (Δᴹ ,Cᴹ Aᴹ) ∘ (=s δᴹ
                              , coe (TmΓ= (TyNat δᴹ ⁻¹)) (t [ Pr Γᴹ ]t)
                              , coe (TmΓ= p,sᴹ) tᴹ)
           ≡ (δ , t) ∘ Pr Γᴹ

  PrNat,sᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}{t}{tᴹ}

    = ass
    ◾ ap (_∘_ (Pr Δᴹ ^ A)) π₁idβ
    ◾ ,∘
    ◾ ,s≃' ( ass
           ◾ ap (_∘_ (Pr Δᴹ)) π₁idβ
           ◾ PrNat δᴹ)
           ( coecoe[]t [][]T [][]T
           ◾̃ π₂idβ
           ◾̃ uncoe (TmΓ= (TyNat δᴹ ⁻¹)) ⁻¹̃ ◾̃ uncoe (TmΓ= [][]T))
    ◾ ,∘ ⁻¹

abstract
  PrNat∘ᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{Σ}{Σᴹ : Conᴹ Σ}{σ : Tms Δ Σ}{σᴹ : Tmsᴹ Δᴹ Σᴹ σ}
            {δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
          → Pr Σᴹ ∘ (=s σᴹ ∘ =s δᴹ) ≡ (σ ∘ δ) ∘ Pr Γᴹ
  PrNat∘ᴹ {σ = σ}{σᴹ}{δ}{δᴹ}

    = ass ⁻¹
    ◾ ap (λ z → z ∘ =s δᴹ) (PrNat σᴹ)
    ◾ ass
    ◾ ap (λ z → σ ∘ z) (PrNat δᴹ)
    ◾ ass ⁻¹

abstract
  PrNatπ₁ᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
             {δ : Tms Γ (Δ , A)}{δᴹ : Tmsᴹ Γᴹ (Δᴹ ,Cᴹ Aᴹ) δ}
           → Pr Δᴹ ∘ π₁ (π₁ (=s δᴹ)) ≡ π₁ δ ∘ Pr Γᴹ
  PrNatπ₁ᴹ {Δᴹ = Δᴹ}{δᴹ = δᴹ}

    = ap (_∘_ (Pr Δᴹ))
         ( ap π₁
              ((π₁∘ ◾ ap π₁ idl) ⁻¹)
         ◾ (π₁∘ ◾ ap π₁ idl) ⁻¹)
    ◾ ass ⁻¹
    ◾ ass ⁻¹
    ◾ π₁β ⁻¹
    ◾ ap π₁ (,∘ ⁻¹ ◾ ap (λ z → z ∘ =s δᴹ) (,∘ ⁻¹) ◾ PrNat δᴹ)
    ◾ π₁∘ ⁻¹

mTms : MethodsTms M mCon mTy
mTms = record

  { εᴹ     =                    record { =s = ε ; PrNat = εη ◾ εη ⁻¹ }
  
  ; _,sᴹ_  = λ { {Γᴹ = Γᴹ} δᴹ {t = t} tᴹ →

                                record { =s    = ( =s δᴹ
                                                 , coe (TmΓ= (TyNat δᴹ ⁻¹)) (t [ Pr Γᴹ ]t))
                                                 , coe (TmΓ= p,sᴹ) tᴹ
                                       ; PrNat = PrNat,sᴹ } }
  
  ; idᴹ    =                    record { =s    = id 
                                       ; PrNat = idr ◾ idl ⁻¹ }
  
  ; _∘ᴹ_   = λ { σᴹ δᴹ →        record { =s    = =s σᴹ ∘ =s δᴹ
                                       ; PrNat = PrNat∘ᴹ {σᴹ = σᴹ}{δᴹ = δᴹ} } }
  
  ; π₁ᴹ    = λ { {Δᴹ = Δᴹ} δᴹ → record { =s    = π₁ (π₁ (=s δᴹ))
                                       ; PrNat = PrNatπ₁ᴹ {Δᴹ = Δᴹ}{δᴹ = δᴹ} } } }
