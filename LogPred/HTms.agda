{-# OPTIONS --no-eta #-}

module LogPred.HTms where

open import lib
open import JM.JM
open import TT.Syntax
open import TT.Elim
open import TT.Congr
open import TT.Laws
open import LogPred.Motives
open import LogPred.Cxt
open import LogPred.Ty
open import LogPred.Tms
open import LogPred.Tm
open import LogPred.Tm.Proofs
open import LogPred.HTy

open Motives M
open MethodsCon mCon
open MethodsTy mTy
open MethodsTms mTms
open MethodsTm mTm
open MethodsHTy mHTy

Tmsᵐ= : {Γ Δ : Con}{Γᴹ : Conᵐ Γ}{Δᴹ : Conᵐ Δ}
        {ρ₀ ρ₁ : Tms Γ Δ}(ρ₂ : ρ₀ ≡ ρ₁)
        {ρᴹ₀ : Tmsᴹ Γᴹ Δᴹ ρ₀}{ρᴹ₁ : Tmsᴹ Γᴹ Δᴹ ρ₁}
      → =s ρᴹ₀ ≡ =s ρᴹ₁ → ρᴹ₀ ≡[ TmsΓΔᴹ= ρ₂ ]≡ ρᴹ₁
Tmsᵐ= refl {ρᴹ₀ = cTmsᵐ =s₀ PrNat₀} {cTmsᵐ .=s₀ PrNat₁} refl = ap (cTmsᵐ =s₀) (K PrNat₀ PrNat₁)
  where
    K : {A : Set}{x y : A}(p q : x ≡ y) → p ≡ q
    K refl refl = refl

abstract
  ,∘ᴹ : {Γ : Con} {Γᴹ : Conᴹ Γ} {Δ : Con} {Δᴹ : Conᴹ Δ}
        {Σ : Con} {Σᴹ : Conᴹ Σ} {δ : Tms Γ Δ} {δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
        {σ : Tms Σ Γ} {σᴹ : Tmsᴹ Σᴹ Γᴹ σ} {A : Ty Δ} {Aᴹ : Tyᴹ Δᴹ A}
        {a : Tm Γ (A [ δ ]T)} {aᴹ : Tmᴹ Γᴹ (Aᴹ [ δᴹ ]Tᴹ) a} →
        (δᴹ ,sᴹ aᴹ) ∘ᴹ σᴹ ≡[ TmsΓΔᴹ= ,∘ ]≡
        (δᴹ ∘ᴹ σᴹ ,sᴹ coe (TmΓᴹ= [][]T [][]Tᴹ refl) (aᴹ [ σᴹ ]tᴹ))
  ,∘ᴹ {σᴹ = σᴹ}
    = Tmsᵐ= ,∘
            ( ,∘
            ◾ ,st= ,∘
            ◾ ,s≃' (,s≃' refl
                         ( uncoe (TmΓ= [][]T) ⁻¹̃
                         ◾̃ coe[]t' (TyNat _ ⁻¹)
                         ◾̃ TmNat σᴹ
                         ◾̃ coe[]t' [][]T ⁻¹̃
                         ◾̃ uncoe (TmΓ= (TyNat (_ ∘ᴹ _) ⁻¹))))
                   ( uncoe (TmΓ= (ap (_[_]T _) ,∘)) ⁻¹̃
                   ◾̃ uncoe (TmΓ= [][]T) ⁻¹̃
                   ◾̃ coe[]t' p,sᴹ
                   ◾̃ uncoe (TmΓ= p[]tᴹ)
                   ◾̃ uncoe (TmΓᴹ= [][]T [][]Tᴹ refl)
                   ◾̃ uncoe (TmΓ= p,sᴹ)))

mHTms : MethodsHTms M mCon mTy mTms mTm mHTy
mHTms = record

  { idlᴹ = Tmsᵐ= idl idl
  ; idrᴹ = Tmsᵐ= idr idr
  ; assᴹ = Tmsᵐ= ass ass
  ; π₁βᴹ = Tmsᵐ= π₁β (ap π₁ π₁β ◾ π₁β)
  ; πηᴹ  = λ { {Γ}{Γᴹ}{Δ}{Δᴹ}{A}{Aᴹ}{δ}{δᴹ}
           → Tmsᵐ= πη
                   ( ,s≃' ( ,s≃' refl
                                 ( uncoe (TmΓ= (TyNat (π₁ᴹ δᴹ) ⁻¹)) ⁻¹̃
                                 ◾̃ π₂[]'
                                 ◾̃ π₂≃ (PrNat δᴹ ⁻¹ ◾ ass ◾ ,∘)
                                 ◾̃ π₂β'
                                 ◾̃ coecoe[]t [][]T [][]T
                                 ◾̃ π₂[]'
                                 ◾̃ π₂≃ (idl ◾ π₁∘ ◾ ap π₁ idl))
                          ◾ πη)
                          ( uncoe (TmΓ= p,sᴹ) ⁻¹̃
                          ◾̃ uncoe (TmΓ= pπ₂ᴹ) ⁻¹̃ )
                   ◾ πη) }
  ; εηᴹ  = Tmsᵐ= εη εη
  ; ,∘ᴹ  = ,∘ᴹ
  }
