{-# OPTIONS --no-eta #-}

module LogPred.HTy where

open import TT.Elim
open import LogPred.Motives
open import LogPred.Cxt
open import LogPred.Ty
open import LogPred.Tms
open import LogPred.Tm
open import LogPred.HTy.Proofs1
open import LogPred.HTy.Proofs7

mHTy : MethodsHTy M mCon mTy mTms mTm
mHTy = record

  { [id]Tᴹ = [id]Tᴹ
  ; [][]Tᴹ = [][]Tᴹ
  ; U[]ᴹ   = U[]ᴹ
  ; El[]ᴹ  = El[]ᴹ
  ; Π[]ᴹ   = Π[]ᴹ }
