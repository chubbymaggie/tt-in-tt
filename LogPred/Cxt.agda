{-# OPTIONS --no-eta #-}

module LogPred.Cxt where

open import TT.Syntax
open import TT.Elim
open import LogPred.Motives

mCon : MethodsCon M
mCon = record

  { •ᴹ     = record { =C = • ; Pr = ε }
  
  ; _,Cᴹ_  = λ { {Γ} Γᴹ {A} Aᴹ
  
             → record { =C = (=C Γᴹ ,  A [ Pr Γᴹ ]T) , Aᴹ
                      ; Pr = (Pr Γᴹ ^ A) ∘ π₁ id } } }
