{-# OPTIONS --without-K #-}

module HoTT.Congr where

open import lib
open import HoTT.Syntax
open import HoTT.JM

coe[]t : ∀{Γ Δ}{ρ : Tms Γ Δ}{A₀ A₁ : Ty Δ}(A₂ : A₀ ≡ A₁){u : Tm Δ A₀}
       → coe (TmΓ= A₂) u [ ρ ]t ≡ coe (TmΓ= (ap (λ z → z [ ρ ]T) A₂)) (u [ ρ ]t)
coe[]t refl = refl

Π≃' : ∀{Γ₀}{Γ₁}{A₀ : Ty Γ₀}{A₁ : Ty Γ₁}{B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}
      (B₂ : Ty ∶ B₀ ≃ B₁) → Ty ∶ Π A₀ B₀ ≃ Π A₁ B₁
Π≃' (refl , refl) = refl , refl

Π≃ : ∀{Γ}{A₀ A₁ : Ty Γ}{B₀ : Ty (Γ , A₀)}{B₁ : Ty (Γ , A₁)}
     (B₂ : Ty ∶ B₀ ≃ B₁) → Π A₀ B₀ ≡ Π A₁ B₁
Π≃ B₂ = from≃ setC _ (Π≃' B₂) 

,C≃ : ∀{Γ₀ Γ₁}{A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : Ty ∶ A₀ ≃ A₁)
      → _≡_ {A = Con} (Γ₀ , A₀) (Γ₁ , A₁)
,C≃ (refl , refl) = refl

[]T≃ : ∀{Γ Δ₀ Δ₁}{A : Ty Γ}{ρ₀ : Tms Δ₀ Γ}{ρ₁ : Tms Δ₁ Γ}
       (ρ₂ : (λ z → Tms z Γ) ∶ ρ₀ ≃ ρ₁)
     → Ty ∶ A [ ρ₀ ]T ≃ A [ ρ₁ ]T
[]T≃ (refl , refl) = refl , refl

{-
,s≃ : ∀{Γ Δ}{ρ₀ ρ₁ : Tms Γ Δ}(ρ₂ : ρ₀ ≡ ρ₁)
      {A : Ty Δ}{t₀ : Tm Γ (A [ ρ₀ ]T)}{t₁ : Tm Γ (A [ ρ₁ ]T)}
      (t₂ : Tm Γ ∶ t₀ ≃ t₁)
    → _≡_ {A = Tms Γ (Δ , A)} (ρ₀ , t₀) (ρ₁ , t₁)
,s≃ {ρ₀ = ρ₀} refl {t₀ = t₀}{t₁} (p , q)

  = ap (_,_ ρ₀) (coe (ap (λ z → t₀ ≡[ TmΓ= z ]≡ t₁) (setT {e0 = p}{e1 = refl})) q)

,s= : ∀{Γ Δ}{ρ₀ ρ₁ : Tms Γ Δ}(ρ₂ : ρ₀ ≡ ρ₁)
      {A : Ty Δ}{t₀ : Tm Γ (A [ ρ₀ ]T)}{t₁ : Tm Γ (A [ ρ₁ ]T)}
      (t₂ : t₀ ≡[ TmΓ= (ap (λ z → A [ z ]T) ρ₂) ]≡ t₁)
    → _≡_ {A = Tms Γ (Δ , A)} (ρ₀ , t₀) (ρ₁ , t₁)
,s= refl refl = refl

,s≃' : ∀{Γ₀}{Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ}
       {ρ₀ : Tms Γ₀ Δ}{ρ₁ : Tms Γ₁ Δ}(ρ₂ : (λ z → Tms z Δ) ∶ ρ₀ ≃ ρ₁)
       {A : Ty Δ}{t₀ : Tm Γ₀ (A [ ρ₀ ]T)}{t₁ : Tm Γ₁ (A [ ρ₁ ]T)}
       (t₂ : (λ b → Tm (proj₁ b) (A [ proj₂ b ]T)) ∶ t₀ ≃ t₁)
     → (λ z → Tms z (Δ , A)) ∶ (ρ₀ , t₀) ≃ (ρ₁ , t₁)
,s≃' {Γ₀} refl {Δ}{ρ₀}{ρ₁} (Γ₂' , ρ₂) {A}{t₀}{t₁} (Γρ₂ , t₂)

  = refl , ,s= ρ₂'
              (coe (ap (λ z → t₀ ≡[ z ]≡ t₁) (l Γρ₂ ρ₂')) t₂)
  where
    ρ₂' : ρ₀ ≡ ρ₁
    ρ₂' = coe (ap (λ z → ρ₀ ≡[ ap (λ z → Tms z Δ) z ]≡ ρ₁)
                  (setC {e0 = Γ₂'}{e1 = refl}))
              ρ₂

    Σ=2 : {A : Set}{B : A → Set}{a : A}{b : B a}
          {α : a ≡ a}{β : b ≡[ ap B α ]≡ b}
        → (w : α ≡ refl) → β ≡[ ap (λ γ → b ≡[ ap B γ ]≡ b) w ]≡ refl
        → Σ= α β ≡ refl
    Σ=2 refl refl = refl

    l : ∀{Γ Δ}{A : Ty Δ}{ρ₀ ρ₁ : Tms Γ Δ}(Γρ₂ : (Γ , ρ₀) ≡ (Γ , ρ₁))(ρ₂' : ρ₀ ≡ ρ₁)
      → ap (λ z → Tm (proj₁ z) (A [ proj₂ z ]T)) Γρ₂ ≡ TmΓ= (ap (_[_]T A) ρ₂')
    l {A = A} p refl = ap (ap (λ z → Tm (proj₁ z) (A [ proj₂ z ]T)))
                          (Σ=η p ⁻¹ ◾ Σ=2 (setC {e0 = Σ=0 p}{e1 = refl}) (sets {e1 = refl}) )
-}

coeπ₂ : ∀{Γ}{A B : Ty Γ}{ρ : Tms (Γ , B) Γ}
        (r : _≡_ {A = Σ Con λ z → Tms z Γ} ((Γ , A) , π₁ id) ((Γ , B) , ρ))
        (p : B [ π₁ id ]T ≡ A [ ρ ]T)
        (AB : A ≡ B)
        (ρ' : π₁ id ≡ ρ)
      → coe (ap (λ z → Tm (proj₁ z) (A [ proj₂ z ]T)) r) (π₂ id) ≡ coe (TmΓ= p) (π₂ id)
coeπ₂ {A = A} r p refl refl

  = ap (λ r → coe (ap (λ z → Tm (proj₁ z) (A [ proj₂ z ]T)) r) (π₂ id))
       (setTs {e0 = r}{e1 = refl})
  ◾ ap (λ p → coe (TmΓ= p) (π₂ id)) (setT {e0 = refl}{e1 = p})

  where
    setTs : ∀{Γ}{α β : Σ Con λ z → Tms z Γ}{e0 e1 : α ≡ β} → e0 ≡ e1
    setTs {Γ}{α}{β}{e0}{e1} = ,=η e0 ⁻¹
                            ◾ Σ== (,=0 e0)
                                  (,=0 e1)
                                  (,=1 e0)
                                  (,=1 e1)
                                  (setC {e0 = ,=0 e0}{e1 = ,=0 e1})
                                  (sets {e0 = coe (ap (λ z → proj₂ α ≡[ ap (λ v → Tms v Γ) z ]≡ proj₂ β)
                                                      (setC {e0 = ,=0 e0}{e1 = ,=0 e1}))
                                                  (,=1 e0)}
                                        {e1 = ,=1 e1})
                            ◾ ,=η e1
      where
        Σ== : {A : Set}{B : A → Set}
              {a a' : A}{b : B a}{b' : B a'}
              (p p' : a ≡ a')(q : b ≡[ ap B p ]≡ b')(q' : b ≡[ ap B p' ]≡ b')
              (r : p ≡ p')(s : q ≡[ ap (λ z → b ≡[ ap B z ]≡ b') r ]≡ q')
            → ,= p q ≡ ,= p' q'
        Σ== p .p q .q refl refl = refl

coeπ₁ : ∀{Γ}{A B : Ty Γ}(r : Γ , A ≡ Γ , B)
   → coe (ap (λ z → Tms z Γ) r) (π₁ id) ≡ π₁ id
coeπ₁ {Γ}{A}{B} p

  =  ap (λ z → coe (ap (λ z → Tms z Γ) z) (π₁ id))
        (,C=η p ◾ ,C== (setC {e0 = ,C=0 p}{e1 = refl}) refl)
  ◾ J {Ty Γ} {A}
      (λ p1 → coe (ap (λ z → Tms z Γ) (,C= refl p1)) (π₁ id) ≡ π₁ id)
      refl
      (coe (ap (λ z → coe (ap Ty z) A ≡ B) (setC {e0 = ,C=0 p}{e1 = refl})) (,C=1 p))
  where
    ,C= : {Γ Δ : Con}{A : Ty Γ}{B : Ty Δ}(p : Γ ≡ Δ)(q : A ≡[ ap Ty p ]≡ B)
       → _≡_ {A = Con} (Γ , A) (Δ , B)
    ,C= refl refl = refl

    ,C=0 : {Γ Δ : Con}{A : Ty Γ}{B : Ty Δ} → _≡_ {A = Con} (Γ , A) (Δ , B) → Γ ≡ Δ
    ,C=0 refl = refl

    ,C=1 : {Γ Δ : Con}{A : Ty Γ}{B : Ty Δ} → (p : _≡_ {A = Con} (Γ , A) (Δ , B)) → A ≡[ ap Ty (,C=0 p) ]≡ B
    ,C=1 refl = refl

    ,C=η : {Γ Δ : Con}{A : Ty Γ}{B : Ty Δ} → (p : _≡_ {A = Con} (Γ , A) (Δ , B)) → p ≡ ,C= (,C=0 p) (,C=1 p)
    ,C=η refl = refl

    ,C=β0 : {Γ Δ : Con}{A : Ty Γ}{B : Ty Δ}(p : Γ ≡ Δ)(q : A ≡[ ap Ty p ]≡ B) → p ≡ ,C=0 (,C= p q)
    ,C=β0 refl refl = refl

    ,C=β1 : {Γ Δ : Con}{A : Ty Γ}{B : Ty Δ}(p : Γ ≡ Δ)(q : A ≡[ ap Ty p ]≡ B)
         → q ≡[ ap (λ z → A ≡[ ap Ty z ]≡ B) (,C=β0 p q) ]≡ ,C=1 (,C= p q) 
    ,C=β1 refl refl = refl
    
    ,C== : {Γ Δ : Con}{A : Ty Γ}{B : Ty Δ}{p p' : Γ ≡ Δ}{q : A ≡[ ap Ty p ]≡ B}{q' : A ≡[ ap Ty p' ]≡ B}
        (α : p ≡ p')(β : q ≡[ ap (λ z → A ≡[ ap Ty z ]≡ B) α ]≡ q') → ,C= p q ≡ ,C= p' q'
    ,C== refl refl = refl

-- the new structuring

record Tmsm : Set where
  constructor c
  field
    Γ : Con
    Δ : Con

Tms' : Tmsm → Set
Tms' (c Γ Δ) = Tms Γ Δ

record Tm[]m : Set where
  constructor c
  field
    Γ : Con
    Δ : Con
    A : Ty Δ
    ρ : Tms Γ Δ

Tm[]' : Tm[]m → Set
Tm[]' (c Γ Δ A ρ) = Tm Γ (A [ ρ ]T)

record Tms,m : Set where
  constructor c
  field
    Γ : Con
    Δ : Con
    A : Ty Δ

Tms,' : Tms,m → Set
Tms,' (c Γ Δ A) = Tms Γ (Δ , A)

,s≃ : {Γ₀ Γ₁ : Con}
      {Δ₀ Δ₁ : Con}
      {ρ₀ : Tms Γ₀ Δ₀}{ρ₁ : Tms Γ₁ Δ₁}
      {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}
      {t₀ : Tm Γ₀ (A₀ [ ρ₀ ]T)}{t₁ : Tm Γ₁ (A₁ [ ρ₁ ]T)}
      (t₂ : Tm[]' ∶ t₀ ≃ t₁)
    → Tms,' ∶ (ρ₀ , t₀) ≃ (ρ₁ , t₁)
,s≃ (refl , refl) = refl , refl

record Tmm : Set where
  constructor c
  field
    Γ : Con
    A : Ty Γ

Tm' : Tmm → Set
Tm' (c Γ A) = Tm Γ A

postulate
  obv : ∀{X : Set} → X

,s≃' : {Γ₀ Γ₁ : Con}
       {Δ₀ Δ₁ : Con}
       {ρ₀ : Tms Γ₀ Δ₀}{ρ₁ : Tms Γ₁ Δ₁}(ρ₂ : Tms' ∶ ρ₀ ≃ ρ₁)
       {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : Ty ∶ A₀ ≃ A₁)
       {t₀ : Tm Γ₀ (A₀ [ ρ₀ ]T)}{t₁ : Tm Γ₁ (A₁ [ ρ₁ ]T)}
       (t₂ : Tm' ∶ t₀ ≃ t₁)
     → Tms' ∶ (ρ₀ , t₀) ≃ (ρ₁ , t₁)
,s≃' (refl , refl) {A₀}{A₁}  (p' , q') {t₀}{t₁} (p'' , q'') with coe (ap (λ z → A₀ ≡[ ap Ty z ]≡ A₁) (setC {e0 = p'}{e1 = refl})) q'
,s≃' (refl , refl) {A₀}{.A₀} (p' , q') {t₀}{t₁} (p'' , q'') | refl

  = refl , obv -- TODO


  where
--    q'2 : A₀ ≡ A₁
--    q'2 = {!!}

,s≃′ : {Γ₀ Γ₁ : Con}
       {Δ : Con}
       {ρ₀ : Tms Γ₀ Δ}{ρ₁ : Tms Γ₁ Δ}(ρ₂ : (λ z → Tms z Δ) ∶ ρ₀ ≃ ρ₁)
       {A : Ty Δ}
       {t₀ : Tm Γ₀ (A [ ρ₀ ]T)}{t₁ : Tm Γ₁ (A [ ρ₁ ]T)}
       (t₂ : Tm' ∶ t₀ ≃ t₁)
     → (λ z → Tms z (Δ , A)) ∶ (ρ₀ , t₀) ≃ (ρ₁ , t₁)
,s≃′ (refl , refl) (p , q) = obv



TmΓ⇒Tm' : ∀{Γ}{A₀ A₁ : Ty Γ}{a₀ : Tm Γ A₀}{a₁ : Tm Γ A₁}
       → Tm Γ ∶ a₀ ≃ a₁ → Tm' ∶ a₀ ≃ a₁
TmΓ⇒Tm' (refl , refl) = refl , refl

Tms1⇒Tms' : ∀{Γ Δ₀ Δ₁}{ρ₀ : Tms Δ₀ Γ}{ρ₁ : Tms Δ₁ Γ}(ρ₂ : (λ z → Tms z Γ) ∶ ρ₀ ≃ ρ₁)
          → Tms' ∶ ρ₀ ≃ ρ₁
Tms1⇒Tms' (refl , refl) = refl , refl

{-
Tm'⇒Tm[]' : ∀{Γ₀}{Γ₁}{A₀ : Ty Γ₀}{A₁ : Ty Γ₁}{a₀ : Tm Γ₀ A₀}{a₁ : Tm Γ₁ A₁}
          → Tm' ∶ a₀ ≃ a₁ → Tm[]' ∶ a₀ [ id ]t ≃ a₁ [ id ]t
Tm'⇒Tm[]' (refl , refl) = refl , refl

Tm[]''emb : ∀{Γ₀ Γ₁ Δ₀ Δ₁}{A₀ : Ty Δ₀}{A₁ : Ty Δ₁}{δ₀ : Tms Γ₀ Δ₀}{δ₁ : Tms Γ₁ Δ₁}
            {a₀ : Tm Γ₀ (A₀ [ δ₀ ]T)}{a₁ : Tm Γ₁ (A₁ [ δ₁ ]T)}
          → Tm' ∶ a₀ ≃ a₁ → Tm[]' ∶ a₀ ≃ a₁
Tm[]''emb (p , q) = {!!}

Tm[]'emb : ∀{Γ Δ₀ Δ₁}{A₀ : Ty Δ₀}{A₁ : Ty Δ₁}{δ₀ : Tms Γ Δ₀}{δ₁ : Tms Γ Δ₁}
           {a₀ : Tm Γ (A₀ [ δ₀ ]T)}{a₁ : Tm Γ (A₁ [ δ₁ ]T)}
         → Tm Γ ∶ a₀ ≃ a₁ → Tm[]' ∶ a₀ ≃ a₁
Tm[]'emb (p , q) = {!!} , {!!}

Tm[]'''emb : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ}{u : Tm Γ (A [ δ ∘ σ ]T)}
             {v : Tm Γ (A [ δ ]T [ σ ]T)}
           → Tm Γ ∶ u ≃ v → Tm[]' ∶ u ≃ v
Tm[]'''emb (p , q) = {!!} , {!!}


coeemb : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ}{u : Tm Γ (A [ δ ]T [ σ ]T)}
       → Tm Γ ∶ (coe (TmΓ= [][]T) u) ≃ u → Tm[]' ∶ (coe (TmΓ= [][]T) u) ≃ u
coeemb (p , q) = {!!}

coe[][]Temb : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ}(u : Tm Γ (A [ δ ]T [ σ ]T))
            → coe (TmΓ= [][]T) u ≡ coe (ap Tm[]' {![][]T!}) u
coe[][]Temb = {!!}
-}

π₂≃ : ∀{Γ Δ}{A : Ty Δ}{ρ₀ ρ₁ : Tms Γ (Δ , A)}(ρ₂ : ρ₀ ≡ ρ₁)
    → Tm Γ ∶ π₂ ρ₀ ≃ π₂ ρ₁
π₂≃ refl = refl , refl

[]t≃ : ∀{Γ Δ}{A : Ty Δ}{t : Tm Δ A}{ρ₀ ρ₁ : Tms Γ Δ}(ρ₂ : ρ₀ ≡ ρ₁)
      → Tm Γ ∶ t [ ρ₀ ]t ≃ t [ ρ₁ ]t
[]t≃ refl = refl , refl

π₂[] : ∀{Γ Δ Θ}{A : Ty Δ}{δ : Tms Γ (Δ , A)}{ρ : Tms Θ Γ}
      → Tm Θ ∶ π₂ δ [ ρ ]t ≃ π₂ (δ ∘ ρ)
π₂[] {Γ}{Δ}{Θ}{A}{δ}{ρ}

  = from≡ [][]T refl
  ◾̃ from≡ ( ap (_[_]T A) π₁β ) π₂β ⁻¹̃
  ◾̃ π₂≃ ,∘ ⁻¹̃
  ◾̃ π₂≃ (ap (λ z → z ∘ ρ) πη)
