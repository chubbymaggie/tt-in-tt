{-# OPTIONS --without-K --no-eta #-}

module HoTT.JM where

open import lib

_∶_≃_ : {A : Set}(B : A → Set){a a' : A} → B a → B a' → Set
_∶_≃_ {A} B {a}{a'} b b' = Σ (a ≡ a') λ p → b ≡[ ap B p ]≡ b'

infix 4 _∶_≃_

_◾̃_ : {A : Set}{B : A → Set}{a a' a'' : A}{b : B a}{b' : B a'}{b'' : B a''}
    → B ∶ b ≃ b' → B ∶ b' ≃ b'' → B ∶ b ≃ b''
(refl , refl) ◾̃ (refl , refl) = refl , refl

infixl 4 _◾̃_

_⁻¹̃ : {A : Set}{B : A → Set}{a a' : A}{b : B a}{b' : B a'}
    → B ∶ b ≃ b' → B ∶ b' ≃ b
(refl , refl) ⁻¹̃ = refl , refl

infix 5 _⁻¹̃

r̃ : {A : Set}{B : A → Set}{a : A}{b : B a} → B ∶ b ≃ b
r̃ = refl , refl

to≃ : {A : Set}{B : A → Set}{a : A}{b b' : B a} → b ≡ b' → B ∶ b ≃ b'
to≃ p = refl , p

uncoe : {A : Set}{B : A → Set}{a a' : A}(p : a ≡ a'){t : B a} → B ∶ t ≃ coe (ap B p) t
uncoe p = p , refl

from≃ : {A : Set}(set : {x y : A}{p q : x ≡ y} → p ≡ q)
        (B : A → Set){a : A}{b b' : B a}
      → B ∶ b ≃ b' → b ≡ b'
from≃ {A} set B {a}{b}{b'} (p , q)

  = coe (ap (λ z → coe (ap B z) b ≡ b') (set {p = p}{q = refl})) q

from≡ : {A : Set}{B : A → Set}{a a' : A}(p : a ≡ a'){t : B a}{u : B a'}
      → t ≡[ ap B p ]≡ u → B ∶ t ≃ u
from≡ refl refl = refl , refl

to≡ : {A : Set}(set : {x y : A}{p q : x ≡ y} → p ≡ q)
      (B : A → Set){a a' : A}{b : B a}{b' : B a'}(r : a ≡ a')
    → B ∶ b ≃ b' → b ≡[ ap B r ]≡ b'
to≡ {A} set B {a}{a'}{b}{b'} r (p , q)

  = coe (ap (λ z → b ≡[ ap B z ]≡ b') (set {a}{a'}{p}{r})) q

{-
ap̃ : {A : Set}{B C : A → Set}(f : {x : A} → B x → C x){a a' : A}{b : B a}{b' : B a'}
   → B ∶ b ≃ b' → C ∶ f b ≃ f b'
ap̃ f (refl , refl) = refl , refl
-}
