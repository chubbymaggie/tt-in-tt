\documentclass[a4paper]{easychair}

\usepackage{doc}
\usepackage{makeidx}

\usepackage{cite}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{hyperref}
\usepackage{proof}
\usepackage{stmaryrd}
\usepackage{tikz}

\newcommand{\U}{\mathsf{U}}
\newcommand{\El}{\mathsf{El}}
\newcommand{\REN}{\mathsf{REN}}
\newcommand{\op}{\mathsf{op}}
\newcommand{\ra}{\rightarrow}
\newcommand{\Ra}{\Rightarrow}
\newcommand{\Set}{\mathsf{Set}}
\newcommand{\PSh}{\mathsf{PSh}}
\newcommand{\FamPSh}{\mathsf{FamPSh}}
\renewcommand{\ll}{\llbracket}
\providecommand{\rr}{\rrbracket}
\newcommand{\Con}{\mathsf{Con}}
\newcommand{\Ty}{\mathsf{Ty}}
\newcommand{\Tm}{\mathsf{Tm}}
\newcommand{\Tms}{\mathsf{Tms}}
\newcommand{\R}{\mathsf{R}}
\newcommand{\TM}{\mathsf{TM}}
\newcommand{\NE}{\mathsf{NE}}
\newcommand{\NF}{\mathsf{NF}}
\newcommand{\q}{\mathsf{q}}
\renewcommand{\u}{\mathsf{u}}
\renewcommand{\ne}{\mathsf{ne}}
\newcommand{\nf}{\mathsf{nf}}
\newcommand{\lQ}{\mathsf{lQ}}
\newcommand{\lU}{\mathsf{lU}}
\renewcommand{\lq}{\mathsf{lq}}
\newcommand{\lu}{\mathsf{lu}}
\newcommand{\cul}{\ulcorner}
\newcommand{\cur}{\urcorner}
\newcommand{\norm}{\mathsf{norm}}
\newcommand{\Nf}{\mathsf{Nf}}
\newcommand{\Ne}{\mathsf{Ne}}
\newcommand{\Nfs}{\mathsf{Nfs}}
\newcommand{\Nes}{\mathsf{Nes}}
\newcommand{\ID}{\mathsf{ID}}
\newcommand{\id}{\mathsf{id}}
\newcommand{\nat}{\,\dot{\rightarrow}\,}
%\newcommand{\nat}{\overset{\mathsf{n}}{\ra}} % this is how we denote it in the formalisation
\newcommand{\Nat}{\overset{\mathsf{N}}{\ra}}
\renewcommand{\S}{\overset{\mathsf{s}}{\ra}} % we have it with uppercase S in the formalisation
\newcommand{\ap}{\mathsf{ap}}
\newcommand{\blank}{\mathord{\hspace{1pt}\text{--}\hspace{1pt}}} %from the book
%\newcommand{\blank}{\!{-}\!}
\newcommand{\lam}{\mathsf{lam}}
\newcommand{\app}{\mathsf{app}}
\newcommand{\circid}{\circ\hspace{-0.2em}\id}
\newcommand{\circcirc}{\circ\hspace{-0.2em}\circ}
\newcommand{\tr}[2]{\ensuremath{{}_{#1 *}\mathopen{}{#2}\mathclose{}}} % from the book
\newcommand{\M}{\mathsf{M}}
\newcommand{\C}{\mathcal{C}}
\newcommand{\data}{\mathsf{data}}
\newcommand{\ind}{\hspace{1em}}
\newcommand{\idP}{\mathsf{idP}}
\newcommand{\compP}{\mathsf{compP}}
\newcommand{\idF}{\mathsf{idF}}
\newcommand{\compF}{\mathsf{compF}}
\newcommand{\proj}{\mathsf{proj}}
\newcommand{\ExpPSh}{\mathsf{ExpPSh}}
\newcommand{\map}{\mathsf{map}}
\newcommand{\Var}{\mathsf{Var}}
\newcommand{\Vars}{\mathsf{Vars}}
\newcommand{\vze}{\mathsf{vze}}
\newcommand{\vsu}{\mathsf{vsu}}
\newcommand{\wk}{\mathsf{wk}}
\newcommand{\neuU}{\mathsf{neuU}}
\newcommand{\neuEl}{\mathsf{neuEl}}
\newcommand{\var}{\mathsf{var}}
\newcommand{\natn}{\mathsf{natn}}
\newcommand{\natS}{\mathsf{natS}}
\newcommand{\LET}{\mathsf{let}}
\newcommand{\IN}{\mathsf{in}}
\newcommand{\refl}{\mathsf{refl}}
\newcommand{\trans}{\mathbin{\raisebox{0.5ex}{$\displaystyle\centerdot$}}}

\newcommand\arcfrombottom{
  \begin{tikzpicture}[scale=0.03em]
    \draw (0,0) arc (0:180:0.5);
    \draw (0,0) edge[->] (0,-0.3);
    \draw (-1,0) edge (-1,-0.3);
  \end{tikzpicture}
}
\newcommand\arcfromtop{
  \begin{tikzpicture}[scale=0.03em]
    \draw (0,0) arc (180:360:0.5);
    \draw (0,0) edge[->] (0,0.3);
    \draw (1,0) edge (1,0.3);
  \end{tikzpicture}
}

%% Document
%%
\begin{document}

%% Front Matter
%%
\title{Normalisation by Evaluation for Dependent Types\footnote{Supported by USAF grant FA9550-16-1-0029.}}

\titlerunning{Normalisation by Evaluation for Dependent Types}

\author{
  Thorsten Altenkirch
  \and
  Ambrus Kaposi
}

\institute{
  University of Nottingham, United Kingdom\\
  \email{\{txa,auk\}@cs.nott.ac.uk}
}

\authorrunning{Altenkirch and Kaposi}

\clearpage

\maketitle

%---------------------------------------------------------------------

\begin{abstract}
  We prove normalisation for a basic dependent type theory using the
  technique of normalisation by evaluation (NBE). We evaluate into a
  proof relevant logical predicate over the syntax. We use an internal
  typed representation of type theory using quotient inductive types
  so we don't mention preterms. Parts of the construction are
  formalised in Agda. A full paper has been submitted to FSCD 2016.
\end{abstract}

\pagestyle{empty}

%---------------------------------------------------------------------

\subsection*{Specifying normalisation}

We denote the type of well typed terms of type $A$ in context $\Gamma$
by $\Tm \, \Gamma \, A$. This type is defined as a quotient inductive
inductive type (QIIT, see \cite{ttintt}): in addition to normal
constructors for terms such as $\lam$ and $\app$, it also has equality
constructors e.g. expressing the $\beta$ computation rule for
functions. An equality $t \equiv_{\Tm \, \Gamma \, A} t'$ expresses
that $t$ and $t'$ are convertible. Typed normal forms are denoted $\Nf
\, \Gamma \, A$ and there are defined mutually with neutral terms $\Ne
\, \Gamma \, A$ and the embedding $\cul \blank \cur : \Nf \, \Gamma \,
A \ra \Tm\,\Gamma\,A$.

Normalisation is given by a function $\norm$ which is an isomorphism:
\begin{equation*}
   \text{completeness }\arcfromtop \hspace{2em} \norm\downarrow\begin{array}{l}\infer={\hspace{1em} \Nf \, \Gamma \, A \hspace{1em} }{\Tm \, \Gamma \, A}\end{array}\uparrow\cul\blank\cur \hspace{2em} \arcfrombottom\text{ stability}
\end{equation*}
Completeness says that normalising a term produces a term which is
convertible to it: $t \equiv \cul \norm \, t \cur$. Stability
expresses that there is no redundancy in the type of normal forms: $n
\equiv \norm \, \cul n \cur$. Soundness, that is, if $t \equiv t'$
then $\norm \, t \equiv \norm \, t'$ is given by congruence of
equality. The elimination rule for the QIIT of the syntax ensures that
every function defined from the syntax respects the equality
constructors.

%---------------------------------------------------------------------

\subsection*{NBE for simple types}

NBE for simple types \cite{alti:ctcs95} works by evaluating the syntax
in a presheaf model over the category of renamings $\REN^\op$ and with
normal forms interpreting the base types. We define a presheaf
structure on substitutions and lists of normal forms by the following
actions on objects.
\begin{alignat*}{7}
  & \TM_\Delta : \PSh \, (\REN^\op) && \NE_\Delta : \PSh \, (\REN^\op) && \NF_\Delta : \PSh \, (\REN^\op)\\
  & \TM_\Delta \, \Gamma := \Tms\,\Gamma\,\Delta \hspace{6em} && \NE_\Delta \, \Gamma := \Nes\,\Gamma\,\Delta \hspace{6em} && \NF_\Delta \, \Gamma := \Nfs\,\Gamma\,\Delta
\end{alignat*}

To normalise a substitution with codomain $\Delta$, one defines two
natural transformations unquote $\u_\Delta$ and quote $\q_\Delta$ by
induction on the structure of contexts and types such that the left
diagram below commutes. $\ll\Delta\rr$ denotes the interpretation of
$\Delta$ in the presheaf model and $\R_\Delta$ denotes the logical
relation at context $\Delta$ between $\TM_\Delta$ and
$\ll\Delta\rr$. The logical relation is equality at the base type. \\
\begin{center}
\begin{tikzpicture}
\node (NE) at (0,0) {$\NE_\Delta$};
\node (XX) at (3,0) {$\Sigma\,(\TM_\Delta \times \ll\Delta\rr)\,\mathsf{R}_\Delta$};
\node (NF) at (6,0) {$\NF_\Delta$};
\node (TM) at (3,-1.5) {$\TM_\Delta$};
\draw[->] (NE) edge node[above] {$\u_\Delta$} (XX);
\draw[->] (XX) edge node[above] {$\q_\Delta$} (NF);
\draw[->] (NE) edge node[below] {$\cul\blank\cur\hspace{1em}$} (TM);
\draw[->] (NF) edge node[below] {$\hspace{1em}\cul\blank\cur$} (TM);
\draw[->] (XX) edge node[right] {$\mathsf{proj}$} (TM);
\end{tikzpicture}
\begin{tikzpicture}
\node (NE) at (0,0) {$\NE_\Delta$};
\node (XX) at (3,0) {$\Sigma\,\TM_\Delta\,\mathsf{P}_\Delta$};
\node (NF) at (6,0) {$\NF_\Delta$};
\node (TM) at (3,-1.5) {$\TM_\Delta$};
\draw[->] (NE) edge node[above] {$\u_\Delta$} (XX);
\draw[->] (XX) edge node[above] {$\q_\Delta$} (NF);
\draw[->] (NE) edge node[below] {$\cul\blank\cur\hspace{1em}$} (TM);
\draw[->] (NF) edge node[below] {$\hspace{1em}\cul\blank\cur$} (TM);
\draw[->] (XX) edge node[right] {$\mathsf{proj}$} (TM);
\end{tikzpicture}
\end{center}

Now a substitution $\sigma$ can be normalised using quote. We need
that the substitution and its interpretation are related, this is
given by the fundamental theorem of the logical relation $\R_\sigma$
at the identity neutral substitution.
\begin{equation*}
  \norm_\Delta\,(\sigma:\TM_\Delta\,\Gamma) : \NF_\Delta\,\Gamma := \q_{\Delta\,\Gamma}\,(\sigma, \ll\sigma\rr, \R_\sigma\,(\u_{\Gamma\,\Gamma}\,\id_\Gamma))
\end{equation*}
Completeness is given by commutativity of the right hand
triangle. Stability can be proven by mutual induction on terms and
normal forms.

A nice property of this approach is that the part of unquote and quote
which gives $\ll\Delta\rr$ can be defined separately from the part
which gives relatedness, hence the normalisation function can be
defined independently from the proof that it is complete.

%---------------------------------------------------------------------

\subsection*{NBE for dependent types}

Simple types act like contexts, so quote at a type $A$ is just a
natural transformation.
\begin{equation*}
  \q_A : \Sigma\,(\TM_A \times \ll A\rr)\,\mathsf{R}_A \nat \NF_A
\end{equation*}
When we have dependent types, this is not the case anymore and
$\TM_{\Gamma \vdash A}$ is a family of presheaves over $\TM_\Gamma$,
$\ll \Gamma \vdash A \rr$ is a family over $\ll\Gamma\rr$ and
$\R_{\Gamma \vdash A}$ depends on $\R_\Gamma$ and quote and unquote
have the following types (omitting naturality).
\begin{alignat*}{3}
  & \R_{\Gamma \vdash A} && : \FamPSh\,\Big(\Sigma\,\big(\Sigma\,(\TM_\Gamma \times \ll\Gamma\rr)\,\R_\Gamma\big)\,\big(\TM_{\Gamma \vdash A}\times\ll\Gamma\vdash A\rr\big)\Big) \\
  & \q_{(\Gamma \vdash A)\,\Psi} && : (p : \R_{\Gamma\,\Psi}\,\rho\,\alpha)(t : \TM_A\,\rho)(v : \ll A\rr\,\alpha) \ra \R_A\,p\,t\,v \ra \Sigma(n:\NF_A\,\rho).t\equiv\cul n\cur \\
  & \u_{(\Gamma \vdash A)\,\Psi} && : (p : \R_{\Gamma\,\Psi}\,\rho\,\alpha)(n : \NE_A\,\rho) \ra \Sigma(v : \ll A\rr\,\alpha).\R_A\,p\,\cul n\cur\,v
\end{alignat*}
When trying to define quote and unquote with these types following the
simply typed case, we get stuck at unquote for $\Pi$. Here we need to
define a semantic function which works for arbitrary inputs, not only
those which are related to a term. It seems that we need to restrict
the presheaf model to only contain such functions.

We solve this problem by replacing the presheaf and the logical
relation by a proof relevant logical predicate denoted by
$\mathsf{P}_\Delta$ for a context $\Delta$. We define normalisation
following the right diagram above. In the presheaf model, the
interpretation of the base type were normal forms of the base type,
and the logical relation at the base type was equality of the term and
the normal form. In our case, the logical predicate at the base type
says that there exists a normal form which is equal to the term.

%---------------------------------------------------------------------

\bibliography{local,alti}{}
\bibliographystyle{plain}

\end{document}
