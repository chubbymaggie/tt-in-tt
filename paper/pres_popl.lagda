\documentclass{beamer}
\usetheme{default}
\useoutertheme{infolines}
\usepackage[greek,english]{babel}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{proof}

\usepackage{scrextend}
\changefontsizes{14pt}

%include lhs2TeX.fmt
%include agda.fmt
%include lib.fmt

\usetheme{Boadilla}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[frame number]
\setbeamerfont{itemize/enumerate subbody}{size=\normalsize}
\setbeamerfont{itemize/enumerate subsubbody}{size=\normalsize}

\begin{document}

%----------------------------------------------------------------------------------------

\begin{frame}[plain]
  \vspace{0.5cm}
  \includegraphics[scale=0.08]{aec-badge-popl}    
  \vspace{-1cm}
  \begin{center}
    \textcolor{blue}{
      {\Large Type Theory in Type Theory \\\vspace{0.2em}
      using\\\vspace{0.5em}
      Quotient Inductive Types}
    }
    
    \vspace{0.7cm}

    Thorsten Altenkirch, Ambrus Kaposi \\

    \vspace{0.2cm}

    University of Nottingham \\

    \vspace{0.7cm}

    POPL, St Petersburg, Florida \\
    20 January 2016
    
  \end{center}
\end{frame}

%----------------------------------------------------------------------------------------

\begin{frame}{Goal}
\begin{itemize}
\item To represent the syntax of Type Theory inside Type Theory
\item Why?
  \begin{itemize}
  \item Study the metatheory in a nice language
  \item Template type theory
  \end{itemize}
\end{itemize}
\end{frame}

%----------------------------------------------------------------------------------------

\begin{frame}{Expressing the judgements of Type Theory}
  \begin{center}
    \vspace{2em}
    
    |Γ ⊢ t : A|

    \vspace{1em}
    
    will be formalised as
    
    \vspace{1em}

    |t' : Tm Γ A|

    \vspace{4em}

    (We are not interested in untyped terms)
  \end{center}
\end{frame}
%----------------------------------------------------------------------------------------

\begin{frame}{Simple type theory in Agda (i)}
\begin{spec}
data Ty     : Set where
  ι         : Ty
  _⇒_       : Ty → Ty → Ty
data Con    : Set where
  •         : Con
  _,_       : Con → Ty → Con
data Var    : Con → Ty → Set where
  zero      : Var (Γ , A) A
  suc       : Var Γ A → Var (Γ , B) A
data Tm     : Con → Ty → Set where
  var       : Var Γ A → Tm Γ A
  app       : Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
  lam       : Tm (Γ , A) B → Tm Γ (A ⇒ B)
\end{spec}
\end{frame}

%----------------------------------------------------------------------------------------

\begin{frame}{Simple type theory in Agda (ii)}
  \begin{itemize}
  \item In addition, we need substitutions:
    \begin{spec}
      Tms   : Con → Con → Set
      _[_]  : Tm Γ A → Tms Δ Γ → Tm Δ A
    \end{spec}
  \item Now we can define a conversion relation:
    \begin{spec}
      _~_ : Tm Γ A → Tm Γ A → Set
    \end{spec}
    eg. |app (lam t) u ~ t [ id , u ]|
  \item The intended syntax is a quotient:
    \begin{spec}
      Tm Γ A / ~
    \end{spec}
  \end{itemize}
\end{frame}

%----------------------------------------------------------------------------------------

\begin{frame}{The syntax of Dependent Type Theory (i)}
  \begin{itemize}
  \item Types depend on contexts
  \item Substitutions are mentioned in the application rule: \\\vspace{0.3em}
    |app : Tm Γ (Π A B)(a : Tm Γ A) → Tm Γ (B [ a ])|
  \item We need an inductive-inductive definition:
    \begin{spec}
      data Con  : Set
      data Ty   : Con → Set
      data Tms  : Con → Con → Set
      data Tm   : (Γ : Con) → Ty Γ → Set
    \end{spec}
  \end{itemize}
\end{frame}

%----------------------------------------------------------------------------------------

\begin{frame}{The syntax of Dependent Type Theory (ii)}
  \begin{itemize}
  \item In addition, there is a coercion rule for terms: \\\vspace{0.1em}
    \begin{equation*}
      \infer{\Gamma \vdash t : B}{\Gamma \vdash A \sim B && \Gamma \vdash t : A}
    \end{equation*}
  \item This forces us to define conversion relations mutually:
    \begin{spec}
      data Con     : Set
      data Ty      : Con → Set
      data Tms     : Con → Con → Set
      data Tm      : (Γ : Con) → Ty Γ → Set
      data _~Con_  : Con → Con → Set
      data _~Ty_   : Ty Γ → Ty Γ → Set
      data _~Tms_  : Tms Δ Γ → Tms Δ Γ → Set
      data _~Tm_   : Tm Γ A → Tm Γ A → Set
    \end{spec}
  \end{itemize}
\end{frame}

%----------------------------------------------------------------------------------------

\begin{frame}{Lots of boilerplate}
  \begin{itemize}
  \item The |_~X_| relations are equivalence relations
  \item Coercion rules
  \item Congruence rules
  \item We need to work with setoids
  \end{itemize}
\end{frame}

%----------------------------------------------------------------------------------------

\begin{frame}{The identity type |_≡_|}
  \begin{itemize}
  \item Equality (the identity type) is an equivalence relation
  \item We can coerce between equal types
  \item Equality is a congruence
  \item What about the extra equalities (eg. |β|, |η| for |Π|)?
  \end{itemize}
\end{frame}

%----------------------------------------------------------------------------------------

\begin{frame}{Higher inductive types}
  \begin{itemize}
  \item An idea from homotopy type theory: \\ constructors for
    equalities.\vspace{-0.5em}
  \item Example:
  \begin{spec}
    data I     : Set where
      zero     : I
      one      : I
      segment  : zero ≡ one
  \end{spec}\pause\vspace{-1.5em}
  \begin{spec}
    RecI  :  (Iᴹ : Set)
             (zeroᴹ     : Iᴹ)
             (oneᴹ      : Iᴹ)
             (segmentᴹ  : zeroᴹ ≡ oneᴹ)
          →  I → Iᴹ
  \end{spec}
  \end{itemize}
\end{frame}

%----------------------------------------------------------------------------------------

\begin{frame}{Quotient inductive types (QITs)}
  \begin{itemize}
  \item A higher inductive type which is truncated to an h-set.
  \item They are \emph{not} the same as quotient types: equality
    constructors are defined at the same time
  \item QITs can be simulated in Agda
  \end{itemize}
\end{frame}

%----------------------------------------------------------------------------------------

\begin{frame}{The syntax of Dependent Type Theory (iii)}
  \begin{itemize}
  \item We defined the syntax of a basic Type Theory as a quotient
    inductive inductive type (with |Π| and an uninterpreted family of
    types |U|, |El|)
  \item We don't need to state the equivalence relation, coercion,
    congruence laws anymore
  \item We collect the arguments of the recursor into a record:
    \begin{spec}
      record Model   : Set where
        field  Conᴹ  : Set
               Tyᴹ   : Conᴹ → Set
               ...
    \end{spec}
  \item \vspace{-1em} which is the type of algebras for the QIT \\= the type of models
    of Type Theory, close to CwF
  \end{itemize}
\end{frame}

%----------------------------------------------------------------------------------------

\begin{frame}{Applications (i): standard model}
  \begin{itemize}
  \item A sanity check
  \item Every syntactic construct is interpreted as the corresponding
    metatheoretic construction.
    \begin{spec}
      Conᴹ              = Set
      Tyᴹ   ⟦Γ⟧         = ⟦Γ⟧ → Set
      
      Πᴹ    ⟦A⟧ ⟦B⟧  γ  = (x : ⟦A⟧ γ) → ⟦B⟧ (γ , x)
      lamᴹ  ⟦t⟧      γ  = λ x → ⟦t⟧ (γ , x)
      ...
    \end{spec}
  \end{itemize}
\end{frame}

%----------------------------------------------------------------------------------------

\begin{frame}{Applications (ii): logical predicate interpretation}
  \begin{itemize}
  \item An interpretation from the syntax into the syntax
  \item Bernardy-Jansson-Paterson: Parametricity and Dependent Types,
    2012
  \item A type is interpreted as a logical predicate over that type
  \item A term is interpreted as a proof that it satisfies the
    predicate
  \item Automated derivation of free theorems
  \end{itemize}
\end{frame}

%----------------------------------------------------------------------------------------

\begin{frame}{Applications (iii): presheaf model}
  \begin{itemize}
  \item Given a category $\mathcal{C}$
  \item Contexts are presheaves over $\mathcal{C}$
  \item Types are families of presheaves, terms are sections
  \item Normalisation by evaluation (NBE):
    \begin{itemize}
    \item A presheaf over the category of renamings
    \item We can generalise NBE from Simple Type Theory to Type Theory
      (formalisation in progress)
    \end{itemize}
  \end{itemize}
\end{frame}

%----------------------------------------------------------------------------------------

\begin{frame}{Further work}
  \begin{itemize}
  \item We internalized a very basic type theory, but this can be
    extended easily with universes and inductive types.
  \item We used axioms (quotient inductive types, functional
    extensionality) in our metatheory. This can be solved by using
    cubical type theory.
  \item If we work within HoTT, we can only eliminate into
    h-sets. Hence, the standard model doesn't work as described.
  \end{itemize}
\end{frame}

%----------------------------------------------------------------------------------------

\begin{frame}{Template type theory}
  \begin{itemize}
  \item Given a model of type theory, together with new constants in
    that model
  \item We can interpret code that uses the new constants inside the
    model
  \item The code can use all the conveniences such as implicit
    arguments, pattern matching etc.
  \item This way we can justify extensions of type theory:
    \begin{itemize}
    \item guarded type theory
    \item local state monad
    \item parametricity
    \item homotopy type theory
    \end{itemize}
  \end{itemize}
\end{frame}

%----------------------------------------------------------------------------------------

\end{document}
