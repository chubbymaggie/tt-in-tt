\subsection{A general way of defining the eliminator for QITs}
\label{sec:general}

In this section we explain how to derive the syntax for the motives
and methods of an inductive-inductive QIT using a logical predicate
interpretation (the logical predicate interpretation has been
formalised in Agda, see \ref{sec:extern-param}).

A general QIT has the following description in Agda notation:
\begin{spec}
  data A₁  : T₁      (signatures of types)
  ...
  data Aₙ  : Tₙ

  data A₁ where      (constructors for A₁)
    c₁₁    : A₁₁
    ...
    c₁ₘ₁   : A₁ₘ₁
  ...
  data Aₙ  where     (constructors for Aₙ)
    cₙ₁    : Aₙ₁
    ...
    cₙₘₙ   : Aₙₘₙ

  postulate          (equality constructors for A₁)
    e₁₁    : E₁₁
    ...
    e₁ₖ₁   : E₁ₖ₁
  ...
  postulate          (equality constructors for Aₙ)
    eₙ₁    : Eₙ₁
    ...
    eₙₖₙ   : Eₙₖₙ
\end{spec}
We have |n| types, the type |Aᵢ| has |mᵢ| constructors and |kᵢ|
equality constructors. Agda does not support quotient types, so the
equality constructors have to be postulated. Agda restricts parameters
of the constructors to have strict positive recursive occurrences of
the type, the equality constructors are subject to the same
restriction (however this needs to be checked by hand).

First we note that the above description can be collected into the
context where the variable names are the type names and constructor
names, and they have the corresponding types:
\begin{spec}
  • , A₁ : T₁ , ..., Aₙ : Tₙ , c₁₁ : A₁₁ , ..., c₁ₘ₁ : A₁ₘ₁ ,
  ..., cₙ₁ : Aₙ₁ , ..., cₙₘₙ : Aₙₘₙ , e₁₁ : E₁₁ , ... , e₁ₖ₁ : E₁ₖ₁ ,
  eₙ₁ : Eₙ₁ , ... , eₙₖₙ : Eₙₖₙ
\end{spec}
We note that to define the motives and methods for the eliminator, we
need a family over the types and fibers of that family over the
constructors. We will define the logical predicate operation |_ᴾ|
following \cite{bernardy} which takes this context into a lifted
context which in addition to this specification contains the types of
motives and methods needed.

We will define this operation on a subset of Agda having $\Pi$ types,
universes, an equality type and the |coe|, |ap| functions. We restrict
ourselves to 


The syntax of contexts, terms and types is the following:
\begin{spec}
  Γ        ::=  • | Γ , x : A
  t,u,A,B  ::=  x | Set | (x:A) → B | λ x → t | t u
             |  t [ σ ]
  σ , δ    ::=  ε | (σ , x ↦ t) | σ ∘ δ | id
\end{spec}
|t [ σ ]| is the notation for a substituted term, weakening is
implicit. we omit the typing rules, they are standard and are given in
the formal development.





We define the unary logical predicate operation |_ᴾ| on the syntax
following \cite{bernardy}. This takes types to their corresponding
logical predicates, contexts (lists of types) to lists of related
types and terms to witnesses of relatedness in the predicate
corresponding to their types. We define the |_ᴾ| by first giving its
typing rules for contexts, terms (which include types) and
substitutions:
\begin{equation*}
  \begin{gathered}
    \infer{|Γ ᴾ| \text{ valid}}{|Γ| \text{ valid}}
  \end{gathered}
  \hspace{2em}
  \begin{gathered}
    \infer{|Γ ᴾ ⊢ t ᴾ : (A ᴾ) t|}{|Γ ⊢ t : A|}
  \end{gathered}
  \hspace{2em}
  \begin{gathered}
    \infer{|σ ᴾ : Γ ᴾ ⟶ Δ ᴾ|}{|σ : Γ ⟶ Δ|}
  \end{gathered}
\end{equation*}
The rule for terms, when specialised to elements of the universe
expresses that |A ᴾ| is a predicate over |A|:
\begin{equation*}
  \infer{|Γ ᴾ ⊢ A ᴾ : A → Set|}{|Γ ⊢ A : Set|}
\end{equation*}
The operation |_ᴾ| is defined by induction on the syntax as follows:
\begin{spec}
  • ᴾ               = •
  (Γ , x : A) ᴾ     = Γ ᴾ , x : A , xᴹ : A ᴾ x
  x ᴾ               = xᴹ
  Set ᴾ             = λ A → (A → Set)
  ((x : A) → B) ᴾ   = λ f →  ((x : A) (xᴹ : A ᴾ x)
                             → B ᴾ (f x))
  (λ x → t) ᴾ       = λ x xᴹ → t ᴾ
  (t u) ᴾ           = t ᴾ u (u ᴾ)
  (t [ σ ]) ᴾ       = t ᴾ [ σ ᴾ ]
  ε ᴾ               = ε
  (σ , x ↦ t) ᴾ     = (σ ᴾ , x ↦ x, xᴹ ↦ t ᴾ)
  (σ ∘ δ) ᴾ         = σ ᴾ ∘ δ ᴾ
  id ᴾ              = id
\end{spec}


